from os import getcwd, environ
from os.path import dirname, realpath
from shyft.api import time, deltahours, deltaminutes, TimeSeries, Calendar, TimeAxis, POINT_INSTANT_VALUE, POINT_AVERAGE_VALUE
from shyft.energy_market.core import ConnectionRole, Point, PointList, XyPointCurve, XyPointCurveWithZ, TurbineEfficiency, TurbineDescription
from shyft.energy_market.stm import HydroPowerSystem, StmSystem, MarketArea
from shyft.energy_market.stm import t_xy, t_turbine_description
from shyft.energy_market.stm.utilities import create_t_xy, create_t_double, create_t_turbine_description
# from shyft.energy_market.stm.shop import ShopCommand, ShopCommander, ShopSystem, ShopCommandList
import pytest

shop = pytest.importorskip("shyft.energy_market.stm.shop")
if not shop.shyft_with_shop():
    pytest.skip('Skip shop-releated test for non-shop build', allow_module_level=True)


def test_stm_shop_command():
    """verify shop command"""
    s1 = shop.ShopCommand.penalty_flag_all(True)
    s2 = shop.ShopCommand('penalty flag /all /on')
    assert s1 == s2


def create_test_optimization_commands(run_id: int, write_files: bool) -> shop.ShopCommandList:
    r = shop.ShopCommandList()
    if write_files: r.append(
        shop.ShopCommand.log_file(f"shop_log_{run_id}.txt")
    )
    r.extend([
        shop.ShopCommand.set_method_primal(),
        shop.ShopCommand.set_code_full(),
        shop.ShopCommand.start_sim(3),
        shop.ShopCommand.set_code_incremental(),
        shop.ShopCommand.start_sim(3)
    ])
    if write_files: r.extend([
        shop.ShopCommand.return_simres(f"shop_results_{run_id}.txt"),
        shop.ShopCommand.save_series(f"shop_series_{run_id}.txt"),
        shop.ShopCommand.save_xmlseries(f"shop_series_{run_id}.xml"),
        shop.ShopCommand.return_simres_gen(f"shop_genres_{run_id}.xml")
    ])
    return r


def test_stm_system_optimize(system_to_optimize):
    """verify shop optimization"""
    stm_system = system_to_optimize
    commands = create_test_optimization_commands(1, False)

    agg = stm_system.hydro_power_systems[0].units[0]

    # Optimize with fixed step arguments
    utc = Calendar()
    t_begin = time('2018-10-17T10:00:00Z')
    t_mid = time('2018-10-17T20:00:00Z')
    t_end = time('2018-10-18T10:00:00Z')
    t_step = deltahours(1)
    n_steps = (t_end - t_begin)/t_step
    ta = TimeAxis(t_begin.seconds, t_step.seconds, n_steps.seconds)
    shop.ShopSystem.optimize(stm_system, ta, commands, False, False)
    assert agg.production.exists
    assert 45 < agg.production.value.values[0] < 46

    # Optimize with fixed step time axis
    shop.ShopSystem.optimize(stm_system, ta, commands, False, False)
    assert 45 < agg.production.value.values[0] < 46

    # Optimize with point time axis but still fixed steps
    t_points = [t for t in range(t_begin.seconds, t_end.seconds, deltahours(1).seconds)]
    # [utc.to_string(t) for t in t_points]
    ta = TimeAxis(t_points, t_end)
    shop.ShopSystem.optimize(stm_system, ta, commands, False, False)
    assert 45 < agg.production.value.values[0] < 46

    # Optimize with point time axis with change in steps
    # 15 minutes first period, then change to 1 hour resolution.
    t_points = [t for t in range(t_begin.seconds, t_mid.seconds, deltaminutes(15).seconds)] \
               + [t for t in range(t_mid.seconds, t_end.seconds, deltahours(1).seconds)]
    # [utc.to_string(t) for t in t_points]
    ta = TimeAxis(t_points, t_end)
    shop.ShopSystem.optimize(stm_system, ta, commands, False, False)
    assert 45 < agg.production.value.values[4] < 46
