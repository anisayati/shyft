from shyft.api import time, deltahours, deltaminutes, TimeSeries, Calendar, TimeAxis, POINT_INSTANT_VALUE, POINT_AVERAGE_VALUE
from shyft.energy_market.core import ConnectionRole, Point, PointList, XyPointCurve, XyPointCurveWithZ, TurbineEfficiency, TurbineDescription
from shyft.energy_market.stm import HydroPowerSystem, StmSystem, MarketArea
from shyft.energy_market.stm import t_xy, t_turbine_description
from shyft.energy_market.stm.utilities import create_t_xy, create_t_double, create_t_turbine_description

import pytest

shop = pytest.importorskip("shyft.energy_market.stm.shop")


def create_test_hydro_power_system(*, hps_id: int = 1, name: str) -> HydroPowerSystem:
    """ helper to create a test system """
    hps = HydroPowerSystem(hps_id, name)
    t0 = time('2000-01-01T00:00:00Z')
    hps.create_ids()

    rsv = hps.create_reservoir(1, 'rsv')
    rsv.volume_descr.value = create_t_xy(t0, XyPointCurve(PointList([Point(0.0, 80.0), Point(2.0, 90.0), Point(3.0, 95.0), Point(5.0, 100.0), Point(16.0, 105.0)])))
    rsv.spill_descr.value = create_t_xy(t0, XyPointCurve(PointList([Point(100.0, 0.0), Point(101.5, 25.0), Point(103.0, 80.0), Point(104.0, 150.0)])))
    rsv.hrl.value = create_t_double(t0, 100.0)
    rsv.lrl.value = create_t_double(t0, 80.0)
    rsv.endpoint_desc.value = TimeSeries(TimeAxis(time('2018-10-17T10:00:00Z'), time(3600), 240), fill_value=36.5, point_fx=POINT_AVERAGE_VALUE)
    rsv.level_historic.value = TimeSeries(TimeAxis(time('2018-10-17T09:00:00Z'), time(3600), 241), fill_value=90.0, point_fx=POINT_AVERAGE_VALUE)
    rsv.inflow.value = TimeSeries(TimeAxis(time('2018-10-17T10:00:00Z'), time(3600), 240), fill_value=55.0, point_fx=POINT_AVERAGE_VALUE)

    gen = hps.create_unit(1, 'agg')
    gen.generator_efficiency.value = create_t_xy(t0, XyPointCurve(PointList([Point(20.0, 96.0), Point(40.0, 98.0), Point(60.0, 99.0), Point(80.0, 98.0)])))
    gen.turbine_description.value = create_t_turbine_description(t0,
                                                                 [XyPointCurveWithZ(
                                                                     XyPointCurve(PointList([Point(20.0, 70.0), Point(40.0, 85.0), Point(60.0, 92.0), Point(80.0, 94.0), Point(100.0, 92.0), Point(110.0, 90.0)])),
                                                                     70.0)
                                                                 ])
    plant = hps.create_power_plant(1, 'plant')
    plant.add_unit(gen)
    plant.outlet_level.value = create_t_double(t0, 10.0)

    wtr_flood = hps.create_river(1, 'wtr rsv_flood')
    wtr_flood.discharge_max_static.value = create_t_double(t0, 150.0)

    wtr_main = hps.create_tunnel(2, 'wtr rsv_main')
    wtr_main.head_loss_coeff.value = create_t_double(t0, 0.00030)

    wtr_penstock = hps.create_tunnel(3, 'wtr penstock')
    wtr_penstock.head_loss_coeff.value = create_t_double(t0, 0.00005)

    wtr_outlet = hps.create_tunnel(4, "wtr outlet")

    wtr_tailrace = hps.create_tunnel(5, "wtr tailrace")

    wtr_river = hps.create_river(6, "wtr outlet_river")

    wtr_flood.input_from(rsv, ConnectionRole.flood).output_to(wtr_river)
    wtr_main.input_from(rsv).output_to(wtr_penstock)
    wtr_penstock.output_to(gen)
    gen.output_to(wtr_outlet)
    wtr_outlet.output_to(wtr_tailrace)
    wtr_tailrace.output_to(wtr_river)
    return hps


def create_test_stm_system(id: int, name: str) -> StmSystem:
    """ helper to create a complete system with market and stm hps"""
    a = StmSystem(id, name, 'json{}')
    a.hydro_power_systems.append(create_test_hydro_power_system(hps_id=1, name='test hps'))
    no_1 = MarketArea(1, 'NO1', 'json{}', a)
    no_1.price.value = TimeSeries(TimeAxis(time('2018-10-17T10:00:00Z'), time(3600), 240), fill_value=40.0, point_fx=POINT_AVERAGE_VALUE)
    no_1.load.value = TimeSeries(TimeAxis(time('2018-10-17T10:00:00Z'), time(3600), 240), fill_value=1300.0, point_fx=POINT_AVERAGE_VALUE)
    no_1.max_buy.value = TimeSeries(TimeAxis(time('2018-10-17T10:00:00Z'), time(3600), 240), fill_value=9999.0, point_fx=POINT_AVERAGE_VALUE)
    no_1.max_sale.value = TimeSeries(TimeAxis(time('2018-10-17T10:00:00Z'), time(3600), 240), fill_value=9999.0, point_fx=POINT_AVERAGE_VALUE)
    no_1.load.value = TimeSeries(TimeAxis(time('2018-10-17T10:00:00Z'), time(3600), 240), fill_value=5.0, point_fx=POINT_AVERAGE_VALUE)
    a.market_areas.append(no_1)
    return a


@pytest.fixture
def system_to_optimize() -> StmSystem:
    return create_test_stm_system(1, "test stm_system")