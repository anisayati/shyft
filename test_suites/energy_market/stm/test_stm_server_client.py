from tempfile import TemporaryDirectory
from pathlib import Path
import pytest
from shyft.time_series import Int64Vector, utctime_now, TimeSeries, TimeAxis, POINT_AVERAGE_VALUE, time, UtcPeriod
from shyft.energy_market.core import ModelInfo
from shyft.energy_market.stm import HpsClient, HpsServer, StmClient, StmServer
from shyft.energy_market.stm import StmSystem, MarketArea
from .models import create_test_hydro_power_system, create_test_hydro_power_system_for_regression_old_data_test
from shyft.energy_market import stm
from time import sleep

def test_hps_client_server():
    with TemporaryDirectory() as root_dir:
        s = HpsServer(str(root_dir))
        port = s.start_server()
        c = HpsClient(host_port=f'localhost:{port}', timeout_ms=1000)
        assert s
        assert c
        mids = Int64Vector()
        mis = c.get_model_infos(mids)
        assert len(mis) == 0
        m = create_test_hydro_power_system(hps_id=0, name='hps m1')
        m.id = 0
        mi = ModelInfo(id=0, name='model m1', created=utctime_now(), json='{"key":"value"}')
        mid = c.store_model(m=m, mi=mi)
        m.id = mid
        mr = c.read_model(mid=mid)
        mi.name = 'Hello world'
        mi.id = mid
        c.update_model_info(mid=mid, mi=mi)
        assert mr
        # equality not yet impl: assert mr == m
        c.close()  # just to illustrate we can disconnect, and reconnect automagigally
        mis = c.get_model_infos(mids)
        assert len(mis) == 1
        assert mis[0].name == mi.name
        c.remove_model(mid)
        mis = c.get_model_infos(mids)
        assert len(mis) == 0
        c.close()
        del s


def create_stm_sys(stm_id: int, name: str, json: str) -> StmSystem:
    a = StmSystem(uid=stm_id, name=name, json=json)
    a.hydro_power_systems.append(create_test_hydro_power_system(hps_id=1, name='ulla-førre'))
    no_1 = MarketArea(1, 'NO1', 'json{}', a)
    no_1.price.value = TimeSeries(TimeAxis(time('2018-10-17T10:00:00Z'), time(3600), 240), fill_value=3.0, point_fx=POINT_AVERAGE_VALUE)
    no_1.load.value = TimeSeries(TimeAxis(time('2018-10-17T10:00:00Z'), time(3600), 240), fill_value=1300.0, point_fx=POINT_AVERAGE_VALUE)
    no_1.max_buy.value = TimeSeries('shyft://stm/no_1/max_buy_mw')
    no_1.max_sale.value = TimeSeries('shyft://stm/no_1/max_sale_mw')
    a.market_areas.append(no_1)
    return a

def create_stm_sys_for_regression_test(stm_id: int, name: str, json: str) -> StmSystem:
    a = StmSystem(uid=stm_id, name=name, json=json)
    a.hydro_power_systems.append(create_test_hydro_power_system_for_regression_old_data_test(hps_id=1, name='ulla-førre'))
    no_1 = MarketArea(1, 'NO1', 'json{}', a)
    no_1.price.value = TimeSeries(TimeAxis(time('2018-10-17T10:00:00Z'), time(3600), 240), fill_value=3.0, point_fx=POINT_AVERAGE_VALUE)
    no_1.load.value = TimeSeries(TimeAxis(time('2018-10-17T10:00:00Z'), time(3600), 240), fill_value=1300.0, point_fx=POINT_AVERAGE_VALUE)
    no_1.max_buy.value = TimeSeries('shyft://stm/no_1/max_buy_mw')
    no_1.max_sale.value = TimeSeries('shyft://stm/no_1/max_sale_mw')
    a.market_areas.append(no_1)
    return a

def test_stm_client_server():
    with TemporaryDirectory() as root_dir:
        s = StmServer(str(root_dir))
        port = s.start_server()
        c = StmClient(host_port=f'localhost:{port}', timeout_ms=1000)
        assert s
        assert c
        mids = Int64Vector()
        mis = c.get_model_infos(mids)
        assert len(mis) == 0
        m = create_stm_sys(stm_id=0, name='stm m1', json="{}")
        m.id = 0
        mi = ModelInfo(id=0, name='model m1', created=utctime_now(), json='{"key":"value"}')
        mid = c.store_model(m=m, mi=mi)
        m.id = mid
        mr = c.read_model(mid=mid)
        mi.name = 'Hello world'
        mi.id = mid
        c.update_model_info(mid=mid, mi=mi)
        assert mr
        # equality not yet impl: assert mr == m
        c.close()  # just to illustrate we can disconnect, and reconnect automagigally
        mis = c.get_model_infos(mids)
        assert len(mis) == 1
        assert mis[0].name == mi.name
        c.remove_model(mid)
        mis = c.get_model_infos(mids)
        assert len(mis) == 0
        c.close()
        del s  # ensure to close down precise


def test_stm_client_server_read_old_data():
    root_dir = Path(__file__).parent / "model_data"
    s = StmServer(str(root_dir))
    port = s.start_server()
    c = StmClient(host_port=f'localhost:{port}', timeout_ms=1000)
    mid = 1
    mr = c.read_model(mid)
    m = create_stm_sys_for_regression_test(stm_id=1, name='stm m1', json="{}")
    assert m.id == mr.id
    hps = m.hydro_power_systems[0]
    hpsr = mr.hydro_power_systems[0]
    assert hps.equal_structure(hpsr), 'ensure hps are equal in structure'
    bs = [res for res in hps.reservoirs if res.id == 16606][0]
    bsr = [res for res in hpsr.reservoirs if res.id == 16606][0]
    assert bsr.spill_descr.exists
    sdr = bsr.spill_descr.value[time("2000-01-01T00:00:00Z")]
    assert sdr.points[0].y == pytest.approx(0.0)
    assert sdr.points[1].y == pytest.approx(20.0)
    assert len(sdr.points) == 3
    assert isinstance(bsr.hrl.value, TimeSeries)
    assert bsr.hrl.value.values[0] == pytest.approx(1349.)
    c.close()
    del s


def test_dstm_server(simple_stm_system):
    """ just test the server side object, no client/io interaction """
    config = stm.LogConfig()
    stm.configure_logger(config, stm.LALL)
    srv = stm.DStmServer()
    with TemporaryDirectory() as doc_root:
        srv.do_add_model("simple", simple_stm_system)
        assert len(srv.do_get_model_ids()) == 1
        # Add model:
        srv.do_add_model(simple_stm_system.name, simple_stm_system)
        assert len(srv.do_get_model_ids()) == 2
        # Get model infos:
        mifs = srv.do_get_model_infos()
        assert len(mifs) == 2
        mif = mifs["simple"]
        assert mif.id == 1
        assert mif.json == ""
        assert mif.name == "Test STM system"
        # Get out model and verify they're equal:
        sys2 = srv.do_get_model(simple_stm_system.name)
        assert sys2.name == simple_stm_system.name and sys2.id == simple_stm_system.id

        # Create model server side:
        assert srv.do_create_model("new model")
        assert len(srv.do_get_model_ids()) == 3
        # Rename
        srv.do_rename_model("new model", "new renamed model")
        # Evaluate model
        period = UtcPeriod(time('2018-01-01T10:00:00Z'), time('2018-02-01T10:00:00Z'))
        assert not srv.do_evaluate_model("new renamed model", period)
        # And remove
        srv.do_remove_model("new renamed model")
        assert len(srv.do_get_model_ids()) == 2
    del srv


def test_dstm_client(port_no, web_api_port, simple_stm_system):
    assert port_no != web_api_port

    # server side fx_callback feature goes here:
    fx_events = []

    def my_server_side_fx(mid: str, fx_arg: str) -> bool:
        if "raise" in mid and "exception" in fx_arg:
            raise RuntimeError("Here it is")
        fx_events.append([mid, fx_arg])  # just to ensure we got the fx call here
        return True

    log_config = stm.LogConfig()
    stm.configure_logger(log_config, stm.LALL)
    srv = stm.DStmServer()
    srv.fx = my_server_side_fx  # hook up server side callback here.
    srv.set_listening_port(port_no)
    with TemporaryDirectory() as doc_root:
        srv.start_server()
        srv.do_add_model("simple", simple_stm_system)
        host = "127.0.0.1"
        srv.start_web_api(f"{host}", web_api_port, doc_root, 1, 1)
        sleep(0.2)  # todo:.. start_web api is not immediately ready ,need a sleep.
        client = stm.DStmClient(f"{host}:{port_no}", 1000)
        try:
            assert len(client.get_model_ids()) == 1
            # Add model:
            assert client.create_model("new model")
            assert len(client.get_model_ids()) == 2
            assert len(client.get_model_infos()) == 2
            assert client.get_model_infos()["new model"].id == 0
            assert len(client.get_model_ids()) == len(srv.do_get_model_ids())
            # Read model:
            sys2 = client.get_model("new model")
            assert isinstance(sys2, StmSystem)
            # Rename model:
            client.rename_model("new model", "new renamed model")
            assert client.get_model("new renamed model")
            # Invoke server-side fx
            assert client.fx("new renamed model", "optimize_this")
            assert len(fx_events)
            assert fx_events[0][0] == "new renamed model" and fx_events[0][1] == "optimize_this"
            # invoke server-side fx, that raise exception
            with pytest.raises(RuntimeError) as r:  # verify that we do get an exception (minimum)
                client.fx("raise","exception")

            # Evaluate model:
            period = UtcPeriod(time('2018-01-01T10:00:00Z'), time('2018-02-01T10:00:00Z'))
            assert not client.evaluate_model("new renamed model", period), "this model should only contain bound time series."
            # Remove model:
            client.remove_model("new renamed model")
            assert len(client.get_model_ids()) == 1
            assert len(client.get_model_ids()) == len(srv.do_get_model_ids())

            # Version info:
            assert client.version_info() == "1.0"
            assert client.version_info() == srv.do_get_version_info()
        finally:
            client.close()
            srv.stop_web_api()
            srv.close()
