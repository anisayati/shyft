import pytest
from tempfile import TemporaryDirectory

websockets = pytest.importorskip("websockets")  # Required for tests in this module
import asyncio
import socket
from shyft.energy_market import stm


def uri(port_num):
    return f"ws://127.0.0.1:{port_num}"


def get_response(req: str, port_no: int):
    async def wrap(wreq):
        async with websockets.connect(uri(port_no)) as websocket:
            await websocket.send(wreq)
            response = await websocket.recv()
            return response

    return asyncio.get_event_loop().run_until_complete(wrap(req))


def test_web_api(simple_stm_system, port_no, web_api_port):
    assert port_no != web_api_port
    import time
    #  ensure there is event loop for this thread
    loop=asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    srv = stm.DStmServer()
    srv.set_listening_port(port_no)
    with TemporaryDirectory() as doc_root:
        srv.start_server()
        srv.do_add_model("simple", simple_stm_system)
        host = "127.0.0.1"
        srv.start_web_api(f"{host}", web_api_port, doc_root, 1, 1)
        time.sleep(0.5)  # todo: the start web api call is not exactly ready on return, so we need a sleep here.
        qa_tuples = [("""get_model_infos {"request_id": "1"}""", """{"request_id":"1","result":[{"model_key":"simple","id":1,"name":"Test STM system"}]}"""),
                     ("""get_hydro_components {
                                    "request_id": "2",
                                    "model_id": "simple",
                                    "hps_id": 1
                                  }""",
                      """{"request_id":"2","result":{"model_id":1,"hps_id":1,"reservoirs":[{"id":1,"name":"simple_res"}],"units":[{"id":1,"name":"simple_unit"}],"power_plants":[{"id":2,"name":"simple_pp","units":[1]}],"waterways":[{"id":1,"name":"r->u","upstreams":[{"role":"input","target":"R1"}],"downstreams":[{"role":"main","target":"A1"}]}]}}"""),
                     ("""read_model {"request_id": "1", "model_id": "simple"}""",
                      """{"request_id":"1","result":{"id":1,"name":"Test STM system","json":"","hps":[{"id":1,"name":"simple hps"}]}}""")]
        try:
            for qa in qa_tuples:
                response = get_response(qa[0], web_api_port)
                assert response == qa[1], 'expected result'
        finally:
            srv.stop_web_api()
            srv.close()
