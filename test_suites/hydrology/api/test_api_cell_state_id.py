from shyft.api import CellStateId


def test_cell_state_id():
    a = CellStateId()
    assert a.cid == 0
    assert a.x == 0
    assert a.y == 0
    assert a.area == 0
