import pytest

from shyft.api import DrmClient, DrmServer, RegionModelType, GeoPointVector, GeoCellData, GeoCellDataVector, LandTypeFractions, \
    TemperatureSourceVector, TemperatureSource, PrecipitationSourceVector, PrecipitationSource, RadiationSourceVector, \
    RadiationSource, WindSpeedSourceVector, WindSpeedSource, RelHumSourceVector, RelHumSource, GeoPoint, ARegionEnvironment
from shyft.api.pt_gs_k import PTGSKStateWithId, PTGSKModel
from shyft.api.pt_ss_k import PTSSKStateWithId, PTSSKModel
from shyft.api.pt_st_k import PTSTKStateWithId, PTSTKModel
from shyft.api.pt_hs_k import PTHSKModel
from shyft.api.pt_hps_k import PTHPSKModel
from shyft.api.r_pm_gs_k import RPMGSKModel
from shyft.api.r_pt_gs_k import RPTGSKModel

from shyft.time_series import TimeAxis, TimeSeries, Calendar, deltahours, POINT_AVERAGE_VALUE
from test_suites.hydrology.api.test_region_model_stacks import build_model


def test_server_client():
    s = DrmServer()
    port = s.start_server()
    assert s.is_running()

    c = DrmClient(f"localhost:{port}", 1000)
    v = c.server_version
    assert v
    assert c.is_open
    c.close()
    assert not c.is_open
    stack_models = [(PTGSKModel, RegionModelType.PT_GS_K),
                    (PTSSKModel, RegionModelType.PT_SS_K),
                    (PTHSKModel, RegionModelType.PT_HS_K),
                    (PTHPSKModel, RegionModelType.PT_HPS_K),
                    (RPMGSKModel, RegionModelType.R_PM_GS_K),
                    (PTSTKModel, RegionModelType.PT_ST_K),
                    (RPTGSKModel, RegionModelType.R_PT_GS_K),
                    ]
    for stk, st_id in stack_models:
        model = build_model(stk, stk.parameter_t, 1)
        gcdv = model.extract_geo_cell_data()
        mid = "m1" + str(st_id)
        success = c.create_model(mid, st_id, gcdv)
        assert success

        assert c.set_state_collection(mid, -1, False)
        assert c.set_snow_sca_swe_collection(mid, -1, True)

        s0 = c.get_state(mid)
        assert stk.__name__.split('Model')[0] in s0.__class__.__name__
        assert stk.__name__.split('Model')[0] in c.get_state(mid, []).__class__.__name__
        with pytest.raises(RuntimeError):
            c.get_state()

        assert c.set_state(mid, s0)
        assert c.set_current_state_as_initial_state(mid)  # notice we need this before revert to initial state.
        assert c.revert_to_initial_state(mid)

        ts_size = 5
        ta = TimeAxis(Calendar().time(2019, 6, 1), deltahours(1), ts_size)
        ts = TimeSeries(ta, 0.5, POINT_AVERAGE_VALUE)
        pos = GeoPoint(500, 500, 0)

        r_env = ARegionEnvironment()
        r_env.temperature = TemperatureSourceVector([TemperatureSource(pos, ts)])
        r_env.precipitation = PrecipitationSourceVector([PrecipitationSource(pos, ts)])
        r_env.radiation = RadiationSourceVector([RadiationSource(pos, ts)])
        r_env.wind_speed = WindSpeedSourceVector([WindSpeedSource(pos, ts)])
        r_env.rel_hum = RelHumSourceVector([RelHumSource(pos, ts)])

        assert c.run_interpolation(mid, model.interpolation_parameter, ta, r_env, True)

        assert c.get_discharge(mid, []).time_axis == TimeAxis()
        assert c.get_charge(mid, []).time_axis == TimeAxis()
        assert c.get_snow_swe(mid, []).time_axis == TimeAxis()
        assert c.get_snow_sca(mid, []).time_axis == TimeAxis()
        assert c.get_temperature(mid, []).time_axis == ta
        assert c.get_precipitation(mid, []).time_axis == ta
        assert c.get_radiation(mid, []).time_axis == ta
        assert c.get_wind_speed(mid, []).time_axis == ta
        assert c.get_rel_hum(mid, []).time_axis == ta

        p0 = stk.parameter_t()
        c.set_region_parameter(mid, p0)  # how to set the remote region parameter
        c.set_catchment_parameter(mid, p0, 1)  # how to set a specific catchment parameter
        assert c.run_cells(mid)
        assert c.is_calculated(mid, model.catchment_ids[0])

        ts_discharge1 = c.get_discharge(mid, [])
        assert ts_discharge1.time_axis == ta and ts_discharge1.size() == ts_size
        ts_snow = c.get_snow_swe(mid, [])
        assert ts_snow.time_axis == ta and ts_snow.size() == ts_size

        wanted_state = ts_discharge1.values[0]*0.8
        result = c.adjust_q(mid=mid, indexes=[], wanted_q=wanted_state, start_step=0, scale_range=3.0, scale_eps=0.001, max_iter=300, n_steps=1)
        obtained_state = result.q_r
        assert abs(obtained_state - wanted_state) < 0.01*wanted_state

        ts_discharge2 = c.get_discharge(mid, [])
        assert ts_discharge2.time_axis == ta
        assert ts_discharge2.size() == ts_size
        assert not ts_discharge2.v[0] == ts_discharge1.v[0]

        assert c.set_catchment_calculation_filter(mid, [])

        mlist = c.get_model_ids()  # this is how to get the list of 'live' models !
        assert len(mlist) == 1 and mlist[0] == mid
        new_mid = f'{mid}.old'
        c.rename_model(mid, new_mid)  # how to rename a model to a new name
        mlist = c.get_model_ids()
        assert len(mlist) == 1 and mlist[0] == new_mid

        clone_mid = f'{new_mid}.clone'
        assert c.clone_model(new_mid, clone_mid)
        assert clone_mid in c.get_model_ids()

        copy_mid = f'{new_mid}.copy'
        assert c.copy_model(new_mid, copy_mid)
        assert copy_mid in c.get_model_ids()
        assert c.get_state(copy_mid)[0].state.kirchner.q == c.get_state(new_mid)[0].state.kirchner.q
        assert c.get_discharge(copy_mid, []) == c.get_discharge(new_mid, [])

        assert c.remove_model(clone_mid)
        assert c.remove_model(copy_mid)
        assert c.remove_model(new_mid), 'remove the model so next round is ready'
        assert len(c.get_model_ids()) == 0

    c.close()
    s.stop_server(1000)
