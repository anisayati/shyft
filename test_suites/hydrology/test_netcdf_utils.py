from shyft.repository.netcdf.utils import fixup_init, calc_P, calc_RH
import numpy as np


def test_fixup_init():
    """ verify pyproj backward fwd compat function to handle just our cases """
    assert fixup_init('EPSG:1234', '1.9.4') == '+init=EPSG:1234', 'add +init= when less than 2.0 and start with EPSG'
    assert fixup_init('+proj=EPSG:1234', '1.9.4') == '+proj=EPSG:1234', 'do not tukle +init= when less than 2.0 and start not with EPSG'
    assert fixup_init('EPSG:1234', '2.0.0') == 'EPSG:1234', 'do not add +init after '
    assert fixup_init('+init=EPSG:1234', '2.0.0') == 'EPSG:1234', 'strip off +init= to avoid warning if version >=2.0'


def test_calc_pressure():
    p = calc_P(elev=100)
    assert abs(p - 100129.45855) < 0.001


def test_calc_rel_hum():
    rh = calc_RH(T=np.array([273.16 + 15.0]), Td=279.0, p=100129)
    assert abs(rh[0] - 0.54) < 0.1
