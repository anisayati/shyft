import pytest
# from shyft.dashboard.test.time_series.test_time_series_fixtures import (TsAdapterTest, TsViewerSourceMock)

from concurrent.futures import ThreadPoolExecutor

from shyft.dashboard.time_series.view_time_axes import ViewTimeAxisProperties
from shyft.time_series import TimeAxis, TsVector, point_interpretation_policy, TimeSeries, DoubleVector, Calendar,time
import numpy as np
# from shyft.dashboard.test.time_series.test_time_series_fixtures import mock_bokeh_document
from shyft.dashboard.time_series.sources.source import Source, DataSource, TsAdapterRequestParameter
from shyft.dashboard.time_series.view import FigureView
from shyft.dashboard.time_series.renderer import BaseFigureRenderer
from shyft.dashboard.base.ports import States
from shyft.dashboard.time_series.axes_handler import DsViewTimeAxisType
from shyft.dashboard.time_series.state import State
from shyft.dashboard.time_series.sources.ts_adapter import TsAdapter

from shyft.dashboard.time_series.state import State, Unit, Quantity
from shyft.dashboard.base.hashable import Hashable


def test_data_source():
    with pytest.raises(ValueError):
        DataSource(unit='MW', ts_adapter='LALELILOLU')


class TsAdapterTest(TsAdapter):
    def __init__(self, unit_to_decorate: Unit, point_interpretation: point_interpretation_policy = None) -> None:
        self.point_interpretation = point_interpretation or point_interpretation_policy.POINT_INSTANT_VALUE
        self.unit_to_decorate = unit_to_decorate
        self.raise_error = None

    def set_error(self, error):
        self.raise_error = error

    def __call__(self, *, time_axis, unit) -> Quantity[TsVector]:
        if self.raise_error:
            raise self.raise_error
        vals = np.random.randn(len(time_axis.time_points[:-1]))
        ts = TimeSeries(time_axis, DoubleVector.from_numpy(vals),
                        self.point_interpretation)
        tsv = TsVector()
        tsv.extend([ts])
        return State.unit_registry.Quantity(tsv, self.unit_to_decorate)


class TsViewerSourceMock:

    def __init__(self):
        self.view_data = []
        self.view_time_axis = TimeAxis(60, 60, 11)
        self.padded_view_time_axis = TimeAxis(0, 60, 12)
        self.raise_error = None
        self.cal=Calendar()


    @property
    def view_axis_properties(self):
        return ViewTimeAxisProperties(dt=time(60),cal=self.cal,view_period=self.view_time_axis.total_period(),padded_view_period=self.padded_view_time_axis.total_period(),extend_mode=False)

    def trigger_view_update(self, view_data):
        self.view_data.append(view_data)


def test_source_sync(mock_bokeh_document):
    unit = "MW"
    # view time axis to for data request
    ts_viewer = TsViewerSourceMock()
    data_source_view_ts = DataSource(ts_adapter=TsAdapterTest(unit_to_decorate=unit), unit='MW',
                                     request_time_axis_type=DsViewTimeAxisType.view_time_axis)
    view = FigureView(view_container=1, color='blue', visible=True, label='Katze',
                      renderer_class=BaseFigureRenderer, unit=unit,
                      )
    source = Source(bokeh_document=mock_bokeh_document, data_source=data_source_view_ts, views=[view],
                    unit_registry=State.unit_registry)
    source.bind(parent=ts_viewer)
    assert source._state == States.ACTIVE

    # Test first data update ! we should receive data with view range time axis

    source.update_data(view_axis=ts_viewer.view_axis_properties)

    assert len(ts_viewer.view_data) == 1
    view_data = ts_viewer.view_data[0]
    assert view in view_data
    assert isinstance(view_data[view], State.unit_registry.Quantity)
    assert isinstance(view_data[view].magnitude, TsVector)
    assert view_data[view].units == State.unit_registry.Unit(unit)
    assert view_data[view][0].time_axis == ts_viewer.view_time_axis

    # Test second update with same time axis, no update should take place
    source.update_data(view_axis=ts_viewer.view_axis_properties)
    assert len(ts_viewer.view_data) == 1

    # test with raising error in ts adapter
    source.current_request_parameter = TsAdapterRequestParameter.create_empty()
    data_source_view_ts.ts_adapter.set_error(RuntimeError())
    source.update_data(view_axis=ts_viewer.view_axis_properties)
    assert source.current_request_parameter.is_empty
    assert len(ts_viewer.view_data) == 2
    view_data = ts_viewer.view_data[1]
    assert view in view_data
    assert len(view_data[view]) == 0
    assert not view_data[view]

    # Test update with empty time series
    ts_viewer.view_time_axis = TimeAxis()

    source.update_data(view_axis=ts_viewer.view_axis_properties)

    assert source.current_request_parameter.is_empty
    assert len(ts_viewer.view_data) == 3
    view_data = ts_viewer.view_data[2]
    assert view in view_data
    assert len(view_data[view]) == 0
    assert not view_data[view]

    # Test update_view_data
    # no Unit
    data = [1, 2, 3]
    source.update_view_data(data)
    assert len(ts_viewer.view_data) == 4
    view_data = ts_viewer.view_data[3]
    assert view in view_data
    assert len(view_data[view]) == 0
    assert isinstance(view_data[view], State.unit_registry.Quantity)
    assert isinstance(view_data[view].magnitude, TsVector)

    # Wrong type
    data = State.unit_registry.Quantity([1, 2, 3], unit)
    source.update_view_data(data)
    assert len(ts_viewer.view_data) == 5
    view_data = ts_viewer.view_data[4]
    assert view in view_data
    assert len(view_data[view]) == 0
    assert isinstance(view_data[view], State.unit_registry.Quantity)
    assert isinstance(view_data[view].magnitude, TsVector)


def test_source_async(mock_bokeh_document):
    thread_pool_executor = ThreadPoolExecutor(1)
    unit = "MW"
    # view time axis to for data request
    ts_adapter = TsAdapterTest(unit_to_decorate=unit)
    ts_viewer = TsViewerSourceMock()
    data_source_view_ts = DataSource(ts_adapter=ts_adapter, unit='MW',
                                     request_time_axis_type=DsViewTimeAxisType.view_time_axis)
    view = FigureView(view_container=1, color='blue', visible=True, label='Katze',
                      renderer_class=BaseFigureRenderer, unit=unit,
                      )
    source = Source(bokeh_document=mock_bokeh_document, data_source=data_source_view_ts, views=[view],
                    unit_registry=State.unit_registry, thread_pool_executor=thread_pool_executor)
    source.bind(parent=ts_viewer)
    assert source._state == States.ACTIVE

    # --------------------------
    assert not source.loading_data_async
    # Test first data update ! we should receive data with view range time axis
    source.update_data(view_axis=ts_viewer.view_axis_properties)
    # check if next tick callback is there
    assert len(mock_bokeh_document.next_tick_callbacks) == 1
    assert mock_bokeh_document.next_tick_callbacks[0] == source._request_data_from_ts_adapter_async
    # call next tick
    assert mock_bokeh_document.next_tick()
    # this should add the next callback for updating the data to the loop
    assert len(mock_bokeh_document.next_tick_callbacks) == 1
    # assert mock_bokeh_document.next_tick_callbacks[0].func == source.update_data
    # call next tick data updater in the document
    assert mock_bokeh_document.next_tick()

    assert len(ts_viewer.view_data) == 1
    view_data = ts_viewer.view_data[0]
    assert view in view_data
    assert isinstance(view_data[view], State.unit_registry.Quantity)
    assert isinstance(view_data[view].magnitude, TsVector)
    assert view_data[view].units == State.unit_registry.Unit(unit)
    assert view_data[view][0].time_axis == ts_viewer.view_time_axis

    source.current_request_parameter = TsAdapterRequestParameter.create_empty()
    # --------------------------
    assert not source.loading_data_async
    # Test no queue but function is called
    source.update_data(view_axis=ts_viewer.view_axis_properties)  # update data
    # check if next tick callback is there
    assert len(mock_bokeh_document.next_tick_callbacks) == 1
    assert mock_bokeh_document.next_tick_callbacks[0] == source._request_data_from_ts_adapter_async
    source.queue = []
    # call next tick
    assert mock_bokeh_document.next_tick()
    # check if data was not updated and update was stopped
    assert len(ts_viewer.view_data) == 1

    source.current_request_parameter = TsAdapterRequestParameter.create_empty()
    # --------------------------
    assert not source.loading_data_async
    # Test new request comes in while working on the first one
    source.update_data(view_axis=ts_viewer.view_axis_properties)  # update data
    # check if queque is filled
    assert len(source.queue) == 1
    # but no next tick callback should be added
    assert len(mock_bokeh_document.next_tick_callbacks) == 1
    assert mock_bokeh_document.next_tick_callbacks[0] == source._request_data_from_ts_adapter_async
    # add something into the queue
    rq2 = TsAdapterRequestParameter(unit=data_source_view_ts.unit, view_time_axis=ts_viewer.view_time_axis,
                                    padded_view_time_axis=ts_viewer.padded_view_time_axis,
                                    request_time_axis_type=data_source_view_ts.request_time_axis_type)
    source.queue.append(rq2)
    # call next tick for the first update data
    assert mock_bokeh_document.next_tick()
    # call next tick for second update added by the first update method
    # this adds 2 new callbacks one for the next_request and one to update_data
    assert len(mock_bokeh_document.next_tick_callbacks) == 2
    assert mock_bokeh_document.next_tick_callbacks[0] == source._request_data_from_ts_adapter_async
    assert mock_bokeh_document.next_tick_callbacks[1].func == source.update_view_data_async

    # trigger next tick
    assert mock_bokeh_document.next_tick()
    # calling source.request_data_from_ts_adapter_async results in 1 more callback
    assert len(mock_bokeh_document.next_tick_callbacks) == 2
    assert mock_bokeh_document.next_tick_callbacks[0].func == source.update_view_data_async
    assert mock_bokeh_document.next_tick_callbacks[1].func == source.update_view_data_async

    # trigger data updates
    assert mock_bokeh_document.next_tick()
    assert len(ts_viewer.view_data) == 2

    assert mock_bokeh_document.next_tick()
    assert len(ts_viewer.view_data) == 3

    # --------------------------
    # Test Future yielding RuntimeError
    source.current_request_parameter = TsAdapterRequestParameter.create_empty()
    assert not source.loading_data_async
    data_source_view_ts.ts_adapter.set_error(RuntimeError())
    source.update_data(view_axis=ts_viewer.view_axis_properties)  # update data
    # check if next tick callback is there
    assert len(mock_bokeh_document.next_tick_callbacks) == 1
    assert mock_bokeh_document.next_tick_callbacks[0] == source._request_data_from_ts_adapter_async
    # call next tick
    assert mock_bokeh_document.next_tick()
    # check if data was not updated and update was stopped
    assert len(ts_viewer.view_data) == 3

    # ---------------------------
    # Test that nothing happens if same TsAdapterRequestParameter is provided
    assert not source.loading_data_async
    assert len(source.queue) == 0
    source.update_data(view_axis=ts_viewer.view_axis_properties)  # update data
    assert len(source.queue) == 0
    assert len(mock_bokeh_document.next_tick_callbacks) == 0
    assert not source.loading_data_async


