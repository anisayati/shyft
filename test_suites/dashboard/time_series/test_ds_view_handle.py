import pytest
from pint import UnitRegistry
#from shyft.dashboard.test.time_series.test_time_series_fixtures import test_ts_line

from shyft.dashboard.time_series.sources.source import DataSource
from shyft.dashboard.time_series.view import Line, LegendItem, TableView
from shyft.dashboard.time_series.ds_view_handle import DsViewHandle, DsViewHandleError


def test_ds_view_handle(test_ts_line):
    ds = DataSource(ts_adapter=test_ts_line, unit='MW')
    # different unit in view but same dimensionality
    fig_view = Line(unit='W', color='blue', label='test', view_container=1, index=0)
    # unit same dimensionality with different unit registry
    temp_ureg = UnitRegistry()
    fig_view2 = Line(unit=temp_ureg.Unit('GW'), color='blue', label='test', view_container=1, index=1)
    table_view = TableView(view_container=1, label='test', unit='MW')
    legend_view = LegendItem(views=[fig_view, table_view], view_container=1, label='test')

    dsvh = DsViewHandle(data_source=ds, views=[fig_view, fig_view2, legend_view, table_view])

    assert dsvh.data_source == ds
    assert dsvh.views == [fig_view, fig_view2, legend_view, table_view]

    # new ds_view_handle with bound source
    fig_view3 = Line(unit='MW', color='blue', label='test', view_container=1, index=0)
    with pytest.raises(DsViewHandleError):
        DsViewHandle(data_source=ds, views=[fig_view3])

    # new ds_view_handle with bound view
    ds2 = DataSource(ts_adapter=test_ts_line, unit='MW')
    with pytest.raises(DsViewHandleError):
        DsViewHandle(data_source=ds2, views=[fig_view])

    # new ds_view_handle with wrong unit definition
    ds = DataSource(ts_adapter=test_ts_line, unit='MW')
    # incompatible units
    fig_view = Line(unit='s', color='blue', label='test', view_container=1, index=0)
    with pytest.raises(DsViewHandleError):
        DsViewHandle(data_source=ds, views=[fig_view])

    # new ds_view_handle with wrong unit definition with different unit registry
    ds = DataSource(ts_adapter=test_ts_line, unit='MW')
    # incompatible units
    fig_view = Line(unit=temp_ureg.Unit('s'), color='blue', label='test', view_container=1, index=0)
    with pytest.raises(DsViewHandleError):
        DsViewHandle(data_source=ds, views=[fig_view])

    # incompatible unit registry
    ds = DataSource(ts_adapter=test_ts_line, unit='euro')
    # incompatible units
    fig_view = Line(unit='euro', color='blue', label='test', view_container=1, index=0)
    with pytest.raises(DsViewHandleError):
        DsViewHandle(data_source=ds, views=[fig_view])
