import pytest


from shyft.dashboard.time_series.ts_viewer import TsViewer, TsViewerError
from shyft.dashboard.time_series.sources.source import DataSource
from shyft.dashboard.time_series.ds_view_handle import DsViewHandle
from shyft.dashboard.time_series.view import Line
from shyft.dashboard.time_series.tools.ts_viewer_tools import ResetTool
from shyft.dashboard.base.ports import States
from shyft.dashboard.time_series.view_container.figure import Figure
from shyft.dashboard.time_series.view_container.table import Table
from shyft.dashboard.time_series.bindable import BindableError
from shyft.dashboard.time_series.axes_handler import BokehViewTimeAxis


def test_ts_viewer_init(mock_bokeh_document):

    viewer = TsViewer(bokeh_document=mock_bokeh_document, title="Test viewer", tools=[ResetTool()])

    # assert viewer._state == States.DEACTIVE
    # assert viewer.time_axis_handler._state == States.DEACTIVE
    # assert viewer.view_time_axis._state == States.DEACTIVE


def test_ds_view_handles(mock_bokeh_document, test_ts_line):
    viewer = TsViewer(bokeh_document=mock_bokeh_document, title="Test viewer")
    ds_view_handles = []
    figure = Figure(viewer=viewer)
    for _ in range(2):
        data_source = DataSource(ts_adapter=test_ts_line, unit='MW')
        fig_view = Line(unit='W', color='blue', label='test', view_container=figure, index=0)
        ds_view_handles.append(DsViewHandle(data_source=data_source, views=[fig_view]))
    viewer.add_ds_view_handles(ds_view_handles=ds_view_handles)
    assert len(viewer.ds_view_handles) == 2
    viewer.remove_ds_view_handles(ds_view_handles=[ds_view_handles[1]])
    assert len(viewer.ds_view_handles) == 1
    with pytest.raises(TsViewerError):
        viewer.add_ds_view_handles(ds_view_handles=[ds_view_handles[0]])


def test_tools(mock_bokeh_document):
    viewer = TsViewer(bokeh_document=mock_bokeh_document, title="Test viewer")
    assert not viewer.tools
    viewer.add_tool(tool=ResetTool())
    assert viewer.tools


def test_figure_state(mock_bokeh_document):
    viewer = TsViewer(bokeh_document=mock_bokeh_document, title="Test viewer")
    assert viewer._state == States.ACTIVE
    assert viewer._receive_state(States.DEACTIVE) is None
    assert viewer._state == States.DEACTIVE
    assert viewer._receive_state(States.ACTIVE) is None
    assert viewer._state == States.ACTIVE
    assert viewer._receive_state(States.LOADING) is None
    assert viewer._receive_state(States.READY) is None
    assert viewer._state == States.ACTIVE
    viewer._receive_state(States.ACTIVE)
    viewer.clear()
    assert viewer._state == States.DEACTIVE


def test_add_and_remove_view_container(mock_bokeh_document):

    viewer = TsViewer(bokeh_document=mock_bokeh_document, title="Test viewer")
    figure = Figure(viewer=viewer)
    table = Table(viewer=viewer)

    assert figure.parent == viewer
    assert table.parent == viewer

    with pytest.raises(BindableError):
        viewer.add_view_container(figure)

    viewer.remove_view_container(table)
    assert table not in viewer.view_container

    with pytest.raises(BindableError):
        viewer.remove_view_container(table)


def test_auto_dt_figure_width(mock_bokeh_document):
    viewer = TsViewer(bokeh_document=mock_bokeh_document, title="Test viewer")
    assert viewer.time_axis_handler.auto_dt_figure_width == viewer.time_axis_handler.default_auto_dt_width
    Figure(viewer=viewer, width=1500)
    assert viewer.time_axis_handler.auto_dt_figure_width == 1500
    Figure(viewer=viewer, width=500)
    assert viewer.time_axis_handler.auto_dt_figure_width == 500

