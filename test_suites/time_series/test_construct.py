from shyft.time_series import TimeAxis, TimeSeries, POINT_AVERAGE_VALUE, DoubleVector
import numpy as np


def test_time_series_constructor() -> None:
    """
    How to create time-series
    """
    ta = TimeAxis(0, 60, 60)
    # time-series can be constructed with a fill value
    assert TimeSeries(ta, 15., POINT_AVERAGE_VALUE)

    # time-series can be constructed with a double vector
    double_vec = DoubleVector([0.]*len(ta))
    assert TimeSeries(ta, double_vec, POINT_AVERAGE_VALUE)

    # time-series can be constructed with a python list with numbers
    number_list = [0.]*len(ta)
    assert TimeSeries(ta, number_list, POINT_AVERAGE_VALUE)

    npa = np.linspace(0, 1, len(ta))
    assert TimeSeries(ta, npa, POINT_AVERAGE_VALUE)
