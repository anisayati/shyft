from shyft.time_series import TimeSeries, TimeAxis, point_interpretation_policy as ts_fx, time, TsVector
import numpy as np


def test_ts_statistics():
    ta = TimeAxis(time(0), time(10), 10)
    tx = TimeAxis(time(0), time(20), 5)

    ts = TimeSeries(ta, [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], ts_fx.POINT_AVERAGE_VALUE)
    r = ts.statistics(tx, 50)
    rv = r.values.to_numpy()
    ev = np.array([1.5, 3.5, 5.5, 7.5, 9.5])
    assert np.allclose(rv, ev, equal_nan=True)
    assert r.time_axis == tx
    assert np.allclose([r.value(0)], [ev[0]])
    assert not r.needs_bind()
    rc = r.evaluate()
    assert rc == r

    # works with ts-vectors as well.
    tsv = TsVector([ts])
    tsvr = tsv.statistics(tx, 50)
    assert np.allclose(tsvr[0].values.to_numpy(), ev, equal_nan=True)
