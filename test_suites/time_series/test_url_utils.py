from shyft.time_series import (utctime_now, urlencode, urldecode, extract_shyft_url_query_parameters, extract_shyft_url_path, extract_shyft_url_container, shyft_url, ext_path_url, ext_query_url)

""" Verify the python exposure for url-utils """


def test_url_encode_decode():
    for s in ['', 'a', ' ', 'This is æøåÆØÅ', 'Something Special %12!']:
        assert urldecode(urlencode(s)) == s
        assert urldecode(urlencode(s, False), False) == s


def test_shyft_url():
    assert shyft_url('test', 'a/ts/path.db') == 'shyft://test/a/ts/path.db'
    assert shyft_url('test', 'a/ts/path.db', {'a': '123'}) == 'shyft://test/a/ts/path.db?a=123'


def test_extract_shyft_url_container():
    assert 'test' == extract_shyft_url_container('shyft://test/something')


def test_extract_shyft_url_path():
    assert 'something/strange/here.db' == extract_shyft_url_path('shyft://test/something/strange/here.db')


def test_extract_shyft_query_parameters():
    assert {'a': '123'} == extract_shyft_url_query_parameters('shyft://test/something/strange/here.db?a=123')

def test_shyft_url_performance():
    t0=utctime_now()
    n=10000
    for i in range(n):
        shyft_url('test',urlencode('some/path/of/length_x'))
    t_used=utctime_now() - t0
    perf = n/t_used
    print(f'shyft_url perf:{perf} pr sec')
    assert perf >10000


def test_ext_path_url():
    assert ext_path_url('smg://','a','b') == 'smg://a/b'


def test_ext_query_url():
    assert ext_query_url('fame://','a','b') == 'fame://a?b'

def test_ext_path_url_performance():
    t0=utctime_now()
    n=10000
    for i in range(n):
        ext_path_url('smg://','test','some/path/of/length_x')
    t_used=utctime_now() - t0
    perf = n/t_used
    print(f'ext_path_url perf:{perf} pr sec')
    assert perf >10000


def test_ext_query_url_performance():
    t0=utctime_now()
    n=10000
    for i in range(n):
        ext_query_url('fame://','01_12_2019.db','R1#em1123#R#em@0')
    t_used=utctime_now() - t0
    perf = n/t_used
    print(f'ext_query_url perf:{perf} pr sec')
    assert perf >10000
