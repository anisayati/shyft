#include <boost/format.hpp>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/stm/water_route.h>
#include <shyft/energy_market/stm/aggregate.h>
#include <shyft/energy_market/stm/power_station.h>
#include <shyft/energy_market/stm/catchment.h>
#include <shyft/energy_market/stm/hps_ds.h>
#include <shyft/energy_market/stm/market_ds.h>
//#include <shyft/energy_market/stm/shop/shop.h>

namespace shyft::energy_market::stm {
    using std::make_shared;
    using std::any_of;
    using std::begin;
    using std::end;
    using boost::format;

    stm_system::stm_system(){
        // as empty as possible.. serialization etc.
    }

    stm_system::stm_system(int id,string name,string json):id_base{id,name,json} {
        ids=make_shared<market_ds>();// from python, - reasonable, - to provide the storage for the attributes (they are still not created)
        rds=make_shared<market_ds>();
        run_params = make_shared<run_parameters>(this);
    }
    
    shared_ptr<stm_system> stm_system::clone_stm_system(const shared_ptr<stm_system>& s) {
        auto blob = to_blob(s);
        return from_blob(blob);
    }

    stm_hps::stm_hps() {
        // as empty as possible.. serialization etc.
    }

    stm_hps::stm_hps(int id, const string&name) :super(id, name) {
        ids=make_shared<hps_ds>();// from python, - reasonable, - to provide the storage for the attributes (they are still not created)
        rds=make_shared<hps_ds>();
    }



    template <class T,class CT>
    static shared_ptr<T> add_ensure_unique_id_and_name(stm_hps_&sys,const string &tp_name,CT& c,int id, const string& name,const string& json) {
        if (any_of(begin(c), end(c), [&name](const auto&w)->bool {return w->name == name;}))
            throw stm_rule_exception((format("%2% name must be unique within a HydroPowerSystem,  name' %1%' already exists")% name%tp_name).str());
        if (any_of(begin(c), end(c), [&id](const auto&w)->bool {return w->id == id;}))
            throw stm_rule_exception((format("%2% id must be unique within a HydroPowerSystem, id %1% already exists")% id%tp_name).str());
        auto o=make_shared<T>(id,name,json,sys);
        c.push_back(o);
        return o;
    }

    reservoir_ stm_hps_builder::create_reservoir(int id,const string&name,const string &json) {
        return add_ensure_unique_id_and_name<reservoir>(s,"Reservoir",s->reservoirs,id,name,json);
    }
    unit_ stm_hps_builder::create_unit(int id,const string&name,const string &json) {
        return add_ensure_unique_id_and_name<unit>(s,"Aggregate",s->units,id,name,json);
    }
    power_plant_ stm_hps_builder::create_power_plant(int id,const string&name,const string &json) {
        return add_ensure_unique_id_and_name<power_plant>(s,"PowerPlant",s->power_plants,id,name,json);
    }
    waterway_ stm_hps_builder::create_waterway(int id,const string&name,const string &json) {
        return add_ensure_unique_id_and_name<waterway>(s,"Waterroute",s->waterways,id,name,json);
    }
    catchment_ stm_hps_builder::create_catchment(int id,const string&name,const string &json) {
        return add_ensure_unique_id_and_name<catchment>(s,"Catchment",s->catchments,id,name,json);
    }
}

