/*
 * Integration of stm with shop api via proxy.
 */
#pragma once
#include <memory>
#include <vector>
#include <iosfwd>
#include <shyft/energy_market/stm/shop/shop_logger.h>
#include <shyft/energy_market/stm/shop/shop_adapter.h>
#include <shyft/energy_market/stm/shop/shop_emitter.h>
#include <shyft/energy_market/stm/shop/shop_command.h>
#include <shyft/energy_market/stm/shop/shop_commander.h>
#include <shyft/energy_market/stm/shop/export/shop_export.h>
#include <shyft/energy_market/stm/shop/shop_visitor.h>

namespace shyft::energy_market::stm::shop {

using namespace shyft::energy_market;
using namespace shyft::energy_market::stm;
using shyft::core::to_seconds64;

struct shop_system {

	shyft::time_axis::generic_dt time_axis;
	shop_api api; // shop api proxy object
	shop_adapter adapter; // basic stm to shop object adapter
	shop_emitter emitter;
	shop_commander commander;
	std::shared_ptr<shop_logger> logger;
	shop_visitor_ vis;


	// Constructors
	shop_system(utcperiod period, utctimespan t_step, stm_system* stm=nullptr, const string& prefix="");
	shop_system(const shyft::time_axis::generic_dt& time_axis, stm_system* stm=nullptr, const string& prefix="");

    shop_system(shop_system && o): api(std::move(o.api)),
                                   time_axis(std::move(o.time_axis)),
                                   adapter{api, time_axis.total_period()}, 
                                   emitter{api, adapter, time_axis.total_period()}, 
                                   commander{api} {
    	vis = o.vis;
    	o.vis = nullptr;
        logger = o.logger;
        o.logger = nullptr;
    }
	~shop_system();

	void set_logging_to_stdstreams(bool on = true) {
		api.set_logging_to_stdstreams(on);
	}
	void set_logging_to_files(bool on = true) {
		api.set_logging_to_files(on);
	}
	std::vector<::shop::data::shop_log_entry> get_log_buffer(int limit = 1024) {
		return api.get_log_buffer(limit);
	}
	static void optimize(stm_system& stm, utctime t_begin, utctime t_end, utctimespan t_step, const std::vector<shop_command>& commands, bool logging_to_stdstreams, bool logging_to_files, const string& prefix="") {
		shop_system shop{ utcperiod{t_begin, t_end}, t_step, &stm, prefix };
		shop.vis->update_run_time_axis(shop.time_axis);
		shop.set_logging_to_stdstreams(logging_to_stdstreams);
		shop.set_logging_to_files(logging_to_files);
		shop.emit(stm);
		shop.command(commands);
		shop.collect(stm);
	}
	static void optimize(stm_system& stm, const generic_dt& time_axis, const std::vector<shop_command>& commands, bool logging_to_stdstreams, bool logging_to_files, const string& prefix="") {
		shop_system shop{ time_axis, &stm, prefix };
        shop.vis->update_run_time_axis(shop.time_axis);
		shop.set_logging_to_stdstreams(logging_to_stdstreams);
		shop.set_logging_to_files(logging_to_files);
		shop.emit(stm);
		shop.command(commands);
		shop.collect(stm);
	}
	void emit(const stm_system& stm) {
		emitter.to_shop(stm);
	}
	void collect(stm_system& stm) {
		emitter.from_shop(stm);
	}
	void command(const std::vector<shop_command> commands) {
		for (const auto& cmd : commands) {
			commander.execute(cmd);
			if (vis) vis->update_by_command(cmd);
		}
	}

	void export_topology(bool all = false, bool raw = false, std::ostream& destination = std::cout) const {
		shop_export::export_topology(api.c, all, raw, destination);
	}
	void export_data(bool all = false, std::ostream& destination = std::cout) const {
		shop_export::export_data(api.c, all, destination);
	}

	//shop_emitter& get_emitter() { return emitter; }
	//shop_commander& get_commander() { return commander; }

	static void set_time_axis(shop_api& api, const utcperiod& period, const utctimespan& t_step);
	static void set_time_axis(shop_api& api, const shyft::time_axis::generic_dt& time_axis);

	static void environment(std::ostream& destination = std::cout);

	void install_logger(std::shared_ptr<shop_logger> logger);
	void uninstall_logger();

};

}
