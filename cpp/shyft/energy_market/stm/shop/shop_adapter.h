/*
 * Adapting stm types to shop api.
 */
#pragma once
#include <vector>
#include <memory>
#include <string>
#include <utility>
#include <algorithm>
#include <stdexcept>

#include <shyft/energy_market/stm/shop/shop_api.h>
#include <shyft/core/utctime_utilities.h>
#include <shyft/core/time_series_dd.h>
#include <shyft/energy_market/hydro_power/xy_point_curve.h>
#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/stm/aggregate.h>
#include <shyft/energy_market/stm/power_station.h>
#include <shyft/energy_market/stm/water_route.h>

namespace shyft::energy_market::stm::shop {

using std::string;
using std::to_string;
using namespace std::string_literals;
using shyft::core::utctime;
using shyft::core::utcperiod;
using shyft::time_series::dd::apoint_ts;
using namespace shyft::energy_market;
using namespace shyft::energy_market::stm;
using hydro_power::xy_point_curve;
using hydro_power::xy_point_curve_with_z;

using shop_api = ::shop::api<xy_point_curve_with_z, apoint_ts>;
template<int T> using shop_object = ::shop::proxy::obj<shop_api, T>;
using shop_reservoir = ::shop::reservoir<shop_api>;
using shop_power_station = ::shop::power_station<shop_api>;
using shop_aggregate = ::shop::aggregate<shop_api>;
using shop_needle_combination = ::shop::needle_combination<shop_api>;
using shop_pump = ::shop::pump<shop_api>;
using shop_gate = ::shop::gate<shop_api>;
using shop_junction = ::shop::junction<shop_api>;
using shop_junction_gate = ::shop::junction_gate<shop_api>;
using shop_creek_intake = ::shop::creek_intake<shop_api>;
using shop_contract = ::shop::contract<shop_api>;
using shop_network = ::shop::network<shop_api>;
using shop_market = ::shop::market<shop_api>;
using shop_optimization = ::shop::optimization<shop_api>;
using shop_reserve_group = ::shop::reserve_group<shop_api>;
using shop_cut = ::shop::cut<shop_api>;
using shop_commit_group = ::shop::commit_group<shop_api>;
using shop_discharge_group = ::shop::discharge_group<shop_api>;
using shop_scenario = ::shop::scenario<shop_api>;
using shop_objective = ::shop::objective<shop_api>;
using shop_bid_group = ::shop::bid_group<shop_api>;
using shop_cut_group = ::shop::cut_group<shop_api>;

struct shop_adapter {
    shop_api& api;
    utcperiod period;

    shop_adapter(shop_api& api, const utcperiod& period) : api{api}, period{ period } {
        if (!period.valid())
            throw std::runtime_error("Creating shop adapter needs a valid period");
    }

    shop_adapter(shop_adapter&& o) = default;

    shop_adapter& operator=(shop_adapter&& o) {
        api = std::move(o.api);
        period = std::move(o.period);
        return *this;    
    }

    #pragma region Attribute/value adapters

    #pragma region Type aliases

    using ts_t = apoint_ts; // Time series, which may be "series-like" time-varying values, or it may be temporal scalar value.
    using ts_value_t = double;
    template<typename V> using tv_t = t_T<V>; // Temporal stm attributes represented by map:utctime->v (t_double_ (legacy), t_xy_, t_xyz_, t_xyz_list_ and t_turbine_description_)
    template<typename T, typename V, typename A, A a, typename C> using stm_attr_t = proxy_attr<T, V, A, a, C>;
    template<typename O, int a, typename V> using shop_attr_t = ::shop::rw<O, a, V>;

    #pragma endregion

    #pragma region Checkers

    // Checking exists, only relevant for stm attributes (but anything can be checked)
    template<typename V> static bool exists(const V& v) { return true; }
    template<typename T, typename V, typename A, A a, typename C> static bool exists(const stm_attr_t<T, V, A, a, C>& v) { return v.exists(); }

    // Checking valid specifically for non-temporal values (just checking exists so not realy a validity check)
    template<typename V> static bool valid(const V& v) { return exists(v); }
    // Checking valid specifically for temporal values, with explicit valid time
    static bool valid_temporal(const ts_t& ts, utctime t) { return ts.total_period().contains(t); } // Should only be called for time series representing temporal attribute value
    template<typename V> static bool valid_temporal(const tv_t<V>& tv, utctime t) { return !tv->empty() && tv->begin()->first <= t; }
    template<typename T, typename V, typename A, A a, typename C> static bool valid_temporal(const stm_attr_t<T, V, A, a, C>& v, utctime t) { return exists(v) && valid_temporal((V)v, t); }
    // Checking valid specifically for temporal values, with implicit valid time according to current period
    template<typename V> bool valid_temporal(const V& v) const { return valid_temporal(v, period.start); }
    // Checking valid for implicitely deduced temporal values, with implicit valid time according to current period
    // Note that this only works for temporal types t_T (map:utctime->v), time series must be explicitely checked
    // as temporal since it could be a non-temporal regular time series value
    template<typename V> bool valid(const tv_t<V>& tv) const { return valid_temporal(tv); }
    template<typename T, typename V, typename A, A a, typename C> bool valid(const stm_attr_t<T, tv_t<V>, A, a, C>& v) const { return valid_temporal(v); }

    #pragma endregion

    #pragma region Getters

    template<typename V> static const V& get(const V& v) { return v; } // Basic value
    template<typename V, typename... Args> static auto get(const V& v,  Args... args) { return get_temporal(v, args...); } // Temporal value
    template<typename T, typename V, typename A, A a, typename C, typename... Args> static auto get(const stm_attr_t<T, V, A, a, C>& v, Args... args) { return get((V)v, args...); } // Stm atribute value (possibly temporal)
    template<typename O, int a, typename V> V get(shop_attr_t<O, a, V>& v) { return (V)v; } // Shop attribute value

    // Get temporal types t_T (map:utctime->v) using start of current period as implicite valid time
    // To get temporal types time series explicit time must be specified, since it cannot be implicitely known if it is a regular time series attribute or a temporal attribute
    template<typename V, typename... Args> V get(const tv_t<V>& v, Args... args) const { return get_temporal(v, period.start, args...); }
    template<typename V, typename... Args> static V get(const tv_t<V>& v, utctime t, Args... args) { return get_temporal(v, t, args...); } // Specific overload to avoid utctime argument being handled as part of args... after period.start in member version!
    template<typename T, typename V, typename A, A a, typename C, typename... Args> V get(const stm_attr_t<T, tv_t<V>, A, a, C>& v, Args... args) const { return get_temporal((tv_t<V>)v, period.start, args...); }
    template<typename T, typename V, typename A, A a, typename C, typename... Args> static V get(const stm_attr_t<T, tv_t<V>, A, a, C>& v, utctime t, Args... args) { return get_temporal((tv_t<V>)v, t, args...); } // Specific overload to avoid utctime argument being handled as part of args... after period.start in member version!
    // Get temporal types time series, assuming the time series represents a temporal attribute value and not "time series data", using start of current period as implicite valid time
    template<typename... Args> auto get_temporal(const ts_t& ts, Args... args) { return get_temporal(ts, period.start, args...); } // Special for time series, explicit temporal - should only be called for time series representing temporal attribute value
    template<typename T, typename V, typename A, A a, typename C, typename... Args> auto get_temporal(const stm_attr_t<T, V, A, a, C>& v, Args... args) { return get_temporal((V)v, period.start, args...); } // Special for time series attributes, explicit temporal

    // Helpers for looking up value of temporal attributes valid at specified time
    static ts_value_t get_temporal(const ts_t& ts, utctime t) { // Get value valid at t from time series (throws if not valid)
        if (!ts)
            throw std::runtime_error("Temporal attribute is empty, hence not valid at time "s + to_string(t.count()));
        if (!ts.total_period().contains(t))
            throw std::runtime_error("Temporal attribute is not valid at time "s + to_string(t.count()));
        return ts(t);
    }
    static ts_value_t get_temporal(const ts_t& ts, utctime t, const ts_value_t& invalid_v) { // Get value valid at t, or specified value if none
        if (!ts.total_period().contains(t)) // Implicitely handles the case of !ts (total_period then returns an empty period)
            return invalid_v;
        return ts(t);
    }
    static ts_value_t get_temporal(const ts_t& ts, utctime t, const ts_value_t& invalid_v, const ts_value_t& nan_v) { // Get value valid at t, or specified value if none, or specifed value if nan
        if (!ts.total_period().contains(t))
            return invalid_v;
        ts_value_t v = ts(t);
        return isfinite(v) ? v : nan_v;
    }
    template<typename V> static V get_temporal(const tv_t<V>& tv, utctime t) { // Get value valid at t (throws if not valid)
        auto it = tv->lower_bound(t);
        if (it == tv->cend()) {
            if (tv->empty())
                throw std::runtime_error("Temporal attribute is empty, hence not valid at time "s + to_string(t.count())); // Never valid
            --it;
        } else if (it->first > t) {
            if (it == tv->cbegin())
                throw std::runtime_error("Temporal attribute is not valid at time "s + to_string(t.count())); // Only valid after t
            --it;
        }
        return it->second;
    }
    template<typename V> static V get_temporal(const tv_t<V>& tv, utctime t, const V& invalid_v) { // Get value valid at t, or specified value if none
        auto it = tv->lower_bound(t);
        if (it == tv->cend()) {
            if (tv->empty())
                return invalid_v; // Return default value since attribute is empty (no validity segments)
            --it;
        } else if (it->first > t) {
            if (it == tv->cbegin())
                return invalid_v; // Return default value since attribute only has values valid after t
            --it;
        }
        return it->second;
    }

    #pragma endregion

    #pragma region Setters

    // Check if specified source is valid considering the destination, which will implicit deduce temporal values and consider according to current period
    // Note that it does not tell if the source can actually be assigned to the destination, it just checks the validity of the source!
    template<typename D, typename S> bool valid_to_set(const D& d, const S& s) const { return valid(s); }
    // Temporal types time series, assuming the time series represents a temporal attribute value and not "time series data" since destination is scalar double, using start of current period as implicite valid time
    bool valid_to_set(const ts_value_t& d, const ts_t& s) const { return valid_temporal(s, period.start); }
    template<typename T, typename A, A a, typename C> bool valid_to_set(const ts_value_t& d, const stm_attr_t<T, ts_t, A, a, C>& s) const { return valid_temporal(s, period.start); }
    template<typename DO, int da> bool valid_to_set(const shop_attr_t<DO, da, ts_value_t>& d, const ts_t& s) const { return valid_temporal(s, period.start); }
    template<typename DO, int da, typename T, typename A, A a, typename C> bool valid_to_set(const shop_attr_t<DO, da, ts_value_t>& d, const stm_attr_t<T, ts_t, A, a, C>& s) const { return valid_temporal(s, period.start); }

    // Set basic types
    template<typename D, typename S> D& set(D& d, const S& s) const { d = s; return d; }
    template<typename DO, int da, typename V> shop_attr_t<DO, da, V>& set(shop_attr_t<DO, da, V>& d, const std::shared_ptr<V>& s) const { d = *s; return d; } // Dereference shared pointer before assigning to Shop attribute
    template<typename DO, int da> shop_attr_t<DO, da, xy_point_curve_with_z>& set(shop_attr_t<DO, da, xy_point_curve_with_z>& d, const xy_point_curve_& s) const { d = xy_point_curve_with_z{ *s, 0.0 }; return d; } // Convert plain xy into xyz (with z=0) before assigning to Shop attribute
    // Set temporal types t_T (map:utctime->v) using start of current period as implicite valid time
    template<typename D, typename V, typename... Args> D& set(D& d, const tv_t<V>& s, Args... args) const { return set(d, get(s, period.start, args...)); }
    template<typename D, typename T, typename V, typename A, A a, typename C, typename... Args> D& set(D& d, const stm_attr_t<T, tv_t<V>, A, a, C>& s, Args... args) const { return set(d, get(s, period.start, args...)); }
    // Set temporal types time series, assuming the time series represents a temporal attribute value and not "time series data" since destination is scalar double, using start of current period as implicite valid time
    template<typename... Args> ts_value_t& set(ts_value_t& d, const ts_t& s, Args... args) const { return set(d, get(s, period.start, args...)); }
    template<typename T, typename A, A a, typename C, typename... Args> ts_value_t& set(ts_value_t& d, const stm_attr_t<T, ts_t, A, a, C>& s, Args... args) const { return set(d, get(s, period.start, args...)); }
    template<typename DO, int da, typename... Args> shop_attr_t<DO, da, ts_value_t>& set(shop_attr_t<DO, da, ts_value_t>& d, const ts_t& s, Args... args) const { return set(d, get(s, period.start, args...)); }
    template<typename DO, int da, typename T, typename A, A a, typename C, typename... Args> shop_attr_t<DO, da, ts_value_t>& set(shop_attr_t<DO, da, ts_value_t>& d, const stm_attr_t<T, ts_t, A, a, C>& s, Args... args) const { return set(d, get(s, period.start, args...)); }
    // Conditional setters, checking if source exists
    // Note if source is temporal it will still throw if source is not valid at specified time, unless an additional default value is also specified (which will be passed into parameter invalid_v of at())
    template<typename D, typename S, typename... Args> D& set_if(D& d, const S& s, Args... args) const // Set value from attribute if the attribute exists, else do nothing.
        { return exists(s) ? set(d, s, args...) : d; }
    template<typename D, typename S, typename V, typename... Args> D& set_or(D& d, const S& s, const V& v_not_exists, Args... args) const // Set value from attribute if the attribute exists, or set specified default value.
        { if (exists(s)) { return set(d, s, args...); } else { return set(d, v_not_exists); } }
    // Higher level setters for pure optional values
    // May or may not set a value. Similar to conditional setters (set_if/set_or) but does not throw (does nothing) if source does not exist or if temporal value not valid at specified time.
    template<typename D, typename S, typename... Args> D& set_optional(D& d, const S& s, Args... args) const // Set pure optional: May or may not set a value. Like set_if does nothing (skip) if source not exists, but in addition also does nothing (does not throw) if source exists but is not valid at specified time and no default value is specified (can still specify additional default value which will be passed into parameter invalid_v of at())
        { try { return set_if(d, s, args...); } catch (...) { return d; } }
    template<typename D, typename S, typename V, typename... Args> D& set_optional(D& d, const S& s, const V& v_not_exists, Args... args) const // Set pure optional: May or may not set a value. Like set_or sets default value if source not exists, but does nothing (does not throw) if source exists but is not valid at specified time and no default value is specified (can still specify additional default value which will be passed into parameter invalid_v of at())
        { try { return set_or(d, s, v_not_exists, args...); } catch (...) { return d; } }
    // Higher level setters for pure required values
    // Will never silently skip setting of results, will either ensure a value is set or throw exception.
    template<typename D, typename S, typename... Args> D& set_required(D& d, const S& s, Args... args) const // Set pure required: Will set value or throw. Is just an alias for set.
        { return set(d, s, args...); }
    template<typename D, typename S, typename V, typename... Args> D& set_required(D& d, const S& s, const V& v_default, Args... args) const // Set pure required: Will always set value and never throw. Like set_optional sets default value if source not exists, but also sets the same value if source exists but is not valid at specified time and no additional default value is specified (can still specify additional default value which will be passed into parameter invalid_v of at())
        { try { return set_or(d, s, v_default, args...); } catch (...) { return set(d, v_default); } }

    #pragma endregion

    #pragma endregion

    #pragma region Send stm objects to shop objects

    shop_market to_shop(const energy_market_area& a) const {
        auto b = api.create<shop_market>(a.name);
        if (valid(a.price)) {
            set(b.sale_price, a.price);
            set(b.buy_price, ((apoint_ts)a.price) + 0.1); // TODO: Currently same as sale-price with fixed small addition, should add as dedicated stm attribute?
        }
        set_optional(b.max_sale, a.max_sale);
        set_optional(b.max_buy, a.max_buy);
        set_optional(b.load, a.load);
        return b;
    }
    shop_reservoir to_shop(const reservoir& a) const {
        auto b = api.create<shop_reservoir>(a.name);
        double default_lrl = 0., default_hrl = 0., default_vol = 0.;
        if (valid(a.volume_descr)) { // Exists and valid at period.start
            auto v = get(a.volume_descr);
            default_lrl = v->points.front().y;
            default_hrl = v->points.back().y; // if spill description this default value will be replaced
            default_vol = v->points.back().x;
            set(b.vol_head, v);
        }
        if (valid(a.spill_descr)) { // Exists and valid at period.start
            auto v = get(a.spill_descr);
            default_hrl = v->points.front().x;
            set(b.flow_descr, v);
        }
        set_required(b.lrl, a.lrl, default_lrl);
        set_required(b.hrl, a.hrl, default_hrl);
        set_required(b.max_vol, a.volume_max_static, default_vol);
        //set_optional(a->endpoint_desc_currency_mwh, b.endpoint_desc_nok_mwh); // NO: endpoint_desc_currency_mwh is time series in STM, but in Shop it is an XY where x value is not used.
        //set_optional(a->level_min, b.min_constr); // TODO: Not exposed in API (yet)!
        //set_optional(a->level_max, b.max_constr); // TODO: Not exposed in API (yet)!
        //set_optional(a->ramping_level_down_d, b.??); // TODO: Not exposed in API (yet)!
        //set_optional(a->ramping_level_down_h, b.??); // TODO: Not exposed in API (yet)!
        //set_optional(a->ramping_level_up_d, b.??); // TODO: Not exposed in API (yet)!
        //set_optional(a->ramping_level_up_h, b.??); // TODO: Not exposed in API (yet)!
        set_optional(b.inflow, a.inflow);
        set_optional(b.schedule, a.level_schedule);
        return b;
    }
    //template<> 
    shop_creek_intake to_shop_creek(const reservoir& a) const {
        auto b = api.create<shop_creek_intake>(a.name);
        set_optional(b.net_head, a.lrl);
        set_optional(b.inflow, a.inflow);
        return b;
    }
    shop_aggregate to_shop(const unit& a) const {
        auto b = api.create<shop_aggregate>(a.name);
        double default_pmin = 0., default_pmax = 0.;
        // Generator efficiency
        if (valid(a.generator_efficiency)) { // Exists and valid at period.start
            auto v = get(a.generator_efficiency);
            default_pmin = v->points.front().x;
            default_pmax = v->points.back().x;
            set(b.gen_eff_curve, v);
        }
        // Min/max/nom prod
        // TODO: Not used on aggregate level for pelton turbines when specified with needle combinations,
        // then it is part of the needle combination instead, but perhaps it does not hurt to always set them?
        set_required(b.p_min, a.production_min_static, default_pmin);
        set_required(b.p_max, a.production_max_static, default_pmax);
        set_required(b.p_nom, a.production_max_static, default_pmax); // Nominal production is just same as maximum, we don't need it to be anything - but Shop requires it to be set!
        // Turbine efficiency
        if (valid(a.turbine_description)) { // Exists and valid at period.start
            auto v = get(a.turbine_description);
            if (v->efficiencies.size() > 0) {
                if (v->efficiencies.size() < 2) {
                    // Only one efficiency: Set it as total turbine efficiency
                    b.turb_eff_curves = v->efficiencies.front().efficiency_curves;
                } else {
                    // More than one efficiency: Assume Pelton turbine, emit as needle combinations.
                    size_t i = 0;
                    for (auto& nc : v->efficiencies) {
                        auto b2 = api.create<shop_needle_combination>(a.name + std::to_string(++i));
                        b2.p_min = nc.production_min;
                        b2.p_max = nc.production_max;
                        b2.p_nom = nc.production_max; // Nominal production is just same as maximum, we don't need it to be anything - but Shop probably requires it to be set!
                        b2.turb_eff_curves = nc.efficiency_curves;
                        api.connect_generator_needle_combination(b.id, b2.id);
                    }
                }
            }
        }
        // Other attributes
        set_optional(b.min_p_constr, a.production_min);
        set_optional(b.max_p_constr, a.production_max);
        set_optional(b.min_q_constr, a.discharge_min);
        set_optional(b.max_q_constr, a.discharge_max);
        set_optional(b.production_schedule, a.production_schedule);
        set_optional(b.discharge_schedule, a.discharge_schedule);
        set_optional(b.startcost, a.cost_start);
        set_optional(b.stopcost, a.cost_stop);
        set_optional(b.maintenance_flag, a.unavailability);
        return b;
    }
    shop_power_station to_shop(const power_plant& a) const {
        auto b = api.create<shop_power_station>(a.name);
        set_optional(b.outlet_line, a.outlet_level);
        set_optional(b.mip_flag, a.mip);
        set_optional(b.min_p_constr, a.production_min);
        set_optional(b.max_p_constr, a.production_max);
        set_optional(b.min_q_constr, a.discharge_min);
        set_optional(b.max_q_constr, a.discharge_max);
        set_optional(b.production_schedule, a.production_schedule);
        set_optional(b.discharge_schedule, a.discharge_schedule);
        //set_if(b.maintenance_flag, a->availability); // TODO: Not exposed in API (yet)!
        return b;
    }
    shop_gate to_shop_gate(const waterway& wtr, gate const* gt) const {
        auto b = api.create<shop_gate>(gt?gt->name:wtr.name);
        set_optional(b.max_discharge, wtr.discharge_max_static); // constraint on the flow in the river bed and so comply to all the parallel gates as a whole
        if (gt) {
            assert(wtr == std::dynamic_pointer_cast<waterway>(gt->wtr_())); // Gate is assumed to be in given water route!
            set_optional(b.schedule_m3s, gt->discharge_schedule);
            set_optional(b.schedule_percent, gt->opening_schedule);
        }
        return b;
    }
    template<class T> T to_shop(const waterway& wtr_junction, vector<waterway_> junction_inputs) const {
        // Send junction represented by the junction output water route and set of junction input water routes.
        // Note that there are two different junction types in shop, shop_junction and shop_junction_gate,
        // here we handle the common parts.
        static_assert(std::is_same<T, shop_junction>::value || std::is_same<T, shop_junction_gate>::value, "Not a shop junction type");
        auto b = api.create<T>(wtr_junction.name);
        //int branch_number = 1;
        for (size_t i = 0; i < junction_inputs.size(); ++i) {
            const auto& l = junction_inputs[i]->head_loss_coeff;
            if (valid_temporal(l)) { // Exists and valid at period.start
                switch (i) {
                    case 0: set_optional(b.loss_factor_1, l); break;
                    case 1: set_optional(b.loss_factor_2, l); break;
                }
            }
        }
        return b;
    }

    #pragma endregion

    #pragma region Read results from shop objects back into stm objects

    void from_shop(energy_market_area & mkt, const shop_market& shop_mkt) {
        mkt.buy = shop_mkt.buy;
        mkt.sale = shop_mkt.sale;
    }
    void from_shop(reservoir & rsv, const shop_reservoir& shop_rsv) {
        rsv.volume = shop_rsv.storage;
        std::cout << "Setting " << rsv.name << " volume.\n";
        rsv.level = shop_rsv.head;
    }
    void from_shop(unit & agg, const shop_aggregate& shop_agg) {
        agg.production = shop_agg.production;
        agg.discharge = shop_agg.discharge;
    }
    void from_shop(waterway & wtr, const shop_gate& shop_gt) {
        wtr.discharge = shop_gt.discharge;
    }

    #pragma endregion
};

}
