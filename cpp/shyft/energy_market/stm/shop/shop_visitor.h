#pragma once
#include <shyft/core/subscription.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/run_parameters.h>
#include <shyft/energy_market/stm/shop/shop_enums.h>
#include <shyft/energy_market/stm/shop/shop_command.h>
#include <vector>
#include <memory>

namespace shyft::energy_market::stm::shop {
    using namespace shyft::energy_market::stm;
    using shyft::core::subscription::manager;
    using std::string;
    using std::vector;
    using std::shared_ptr;
    using shyft::time_axis::generic_dt;
    using ::shop::enums::run_type;
    /** @brief Visitor class to update stm_system or subscriptions
     * at certain points of execution of shop.
     */
    class shop_visitor {
        stm_system& mdl; /// <- the model that will be run SHOP on.
        string prefix; /// <- Prefix used to get attribute <--> url mapping correct.
        vector<string> subs; /// <- A container with what observers should be updated. This must be done by a call to notify_changes.
        run_type rt = run_type::full; // Defaults to full

        // +------------------------------------------+
        // | Handling of different types of commands  |
        // +------------------------------------------+
        void set_code(const string& option) {
            if (option == "full")
                rt = run_type::full;
            else if (option == "incremental")
                rt = run_type::incremental;
            else if (option == "head") {
                mdl.run_params->head_opt = true;
                subs.push_back(mdl.run_params->head_opt.url(prefix));
            }
        }

        /** @brief increase the n_<run_type>_runs attribute of references stm_system
         * based on what run type is currently active.
         * Observable to notify change of is also updated.
         *
         * @param n: Number to increase by.
         */
        void increase_runs(int n) {
            switch (rt) {
                case run_type::full :
                    mdl.run_params->n_full_runs = mdl.run_params->n_full_runs + n;
                    subs.push_back(mdl.run_params->n_full_runs.url(prefix));
                    return;
                case run_type::incremental :
                    mdl.run_params->n_inc_runs = mdl.run_params->n_inc_runs + n;
                    subs.push_back(mdl.run_params->n_inc_runs.url(prefix));
                    return;
            }
        }
    public:
        shop_visitor(stm_system& mdl, const string& prefix): mdl(mdl), prefix{prefix} {
            // We assume the visitor is created before running SHOP optimization.
            // We therefore initialize all attributes:
            mdl.run_params->n_inc_runs = 0;
            mdl.run_params->n_full_runs = 0;
            mdl.run_params->head_opt = false;
        }

        /** @brief Function to call when updating the run_time_axis of the model.
         * Adds url of attribute to vector of observables that need to be updated.
         *
         * @param ta: mdl.run_time_axis will be set to this value.
         */
        void update_run_time_axis(generic_dt const& ta) {
            mdl.run_params->run_time_axis = ta;
            subs.push_back(mdl.run_params->run_time_axis.url(prefix));
        }

        /** @brief Update referenced stm_system based on a command (that has been, or will be executed.
         */
        void update_by_command(shop_command const& cmd) {
            if (cmd.keyword == "set" && cmd.specifier == "code") {
                if (cmd.options.size() > 0) set_code(cmd.options[0]);
            } else if (cmd.keyword == "start" && cmd.specifier == "sim") {
                if (cmd.objects.size() > 0) increase_runs(std::stoi(cmd.objects[0]));
            }
        }

        void notify_changes(manager& sm) const {
            sm.notify_change(subs);
        }
    };
    using shop_visitor_ = std::shared_ptr<shop_visitor>;
}