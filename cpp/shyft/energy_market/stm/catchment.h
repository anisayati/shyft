#pragma once
#include <map>
#include <string>
#include <memory>
#include <vector>
#include <shyft/core/utctime_utilities.h>
#include <shyft/core/core_serialization.h>
#include <shyft/energy_market/dataset.h>
#include <shyft/energy_market/proxy_attr.h>
#include <shyft/energy_market/hydro_power/catchment.h>
#include <shyft/energy_market/hydro_power/hydro_power_system.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/attribute_types.h>

namespace shyft::energy_market::stm {
	using std::string;
	using std::shared_ptr;
    using std::dynamic_pointer_cast;
	using std::map;
	using std::true_type;
	using std::false_type;
	using shyft::core::utctime;
	using core::proxy_attr;

	struct catchment:hydro_power::catchment {
		using super=hydro_power::catchment;
		using ds_collection_t=catchment_ds;
		using ids = hps_ids<catchment>;
        using e_attr=catchment_attr; // enum attr type
        using e_attr_seq_t=catchment_attr_seq_t;// the full sequence of attr type

        string url(const string& prefix="") const {
            std::stringstream ss;
            auto tmp = dynamic_pointer_cast<stm_hps>(hps_());
            if (tmp) ss << tmp->url(prefix);
            else ss << prefix;
            ss << "/C" << id;
            return ss.str();
        }
		//using rds = hps_rds<catchment>; // sih: when needed

		catchment(int id, const string& name,const string&json, stm_hps_ &hps):super(id,name,json,hps) { }
		catchment()=default;

		proxy_attr<catchment, apoint_ts, catchment_attr, catchment_attr::inflow_m3s, ids> inflow_m3s{*this};
        
        // meta-programming support, list all mappings of the above proxy attributes here
        // note: later using more clever enums, we can use boost::hana adapt instead
        struct a_map {
            using proxy_container=catchment;
            //static constexpr auto ref(rsv_attr_c<rsv_attr::lrl>) noexcept { return &proxy_container::lrl;}
            def_proxy_map(catchment_attr,inflow_m3s)
        };

		x_serialize_decl();
	};
	using catchment_=shared_ptr<catchment>;
}

x_serialize_export_key(shyft::energy_market::stm::catchment);

