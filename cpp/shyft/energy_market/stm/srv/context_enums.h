#pragma once
#include <cstdint>

namespace shyft::energy_market::stm::srv {
    
    enum class model_state:int8_t {
        idle,           // Model is available in full to the user
        optimizing     // Model is currently running an optimizing procedure, limited availability
        //, done // as in there is an optimization result ?
    };
    
    enum class shop_flag:int8_t {
        success,    // The SHOP optimization returned without error
        segfault,   // At some stage, SHOP raised a segmentation error signal
        other       // Some other error
        //,undefined, as in not yet started ?
    };
    
}
