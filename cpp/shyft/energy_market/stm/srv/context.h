# pragma once
#include <memory>
#include <atomic>
#include <boost/thread/locks.hpp>
#include <boost/thread/shared_mutex.hpp>
#include <boost/thread/condition_variable.hpp>
#include <memory>

#include <shyft/energy_market/stm/srv/context_enums.h>

namespace shyft::energy_market::stm {
    //fwd goes here: avoid includes for references/pointers and friends
    struct stm_system;
    using stm_system_= std::shared_ptr<stm_system>;

    namespace srv {
        // we use boost sync primitives  since they support upgrade locks
        using srv_shared_mutex=boost::shared_mutex;
        using srv_condition_variable=boost::condition_variable;
        using srv_shared_lock=boost::shared_lock<srv_shared_mutex>;
        using srv_upgradable_lock=boost::upgrade_lock<srv_shared_mutex>;
        using srv_unique_lock=boost::unique_lock<srv_shared_mutex>;
        using srv_upgrade_lock=boost::upgrade_to_unique_lock<srv_shared_mutex>;
        
        /** @brief Handle a model stored in a hot STM service
        *
        * This includes needed shared_mutex to ensure proper
        * threaded access to the model.
        * The computational engine, algorithms, to run/optimize
        * needs a reader-lock while running to ensure
        * writers do not modify the model.
        * Then when computation is done, this is 
        * upgraded to unique lock while updating the model.
        * 
        * We use boost library locks and shared_mutex to do this part of it.
        * 
        * Another important pattern here is that readers needs to have a 
        * shared-lock in the (hopefully) short time-frames they are using the model.
        * 
        * The 'users' of  the hot-service context, should
        *  
        *  [X] get the context from the model, using model srv lock
        *  [X] obtain shared, upgradeable or unique lock, using scoped var around mtx
        *  [X] keep that lock for as long it needs to access the structures in mdl
        *  [X] robust pattern using condition_variable to obtain/wait for data-ready
        *
        */ 
        struct stm_system_context {
            srv_shared_mutex mtx;                      ///< lock for  the stm_system_ mdl
            srv_condition_variable run_ready;          ///< cv waiting for algo-run, using above mtx, state
            std::atomic<model_state> state;            ///< the model(context)-state, as in idle, optimizing ..
            std::atomic<shop_flag> shop_result;        ///< shop_result as in success,segfault,others
            stm_system_ mdl;                           ///< the stm_system for this hot context
            std::streampos logpos;                     ///< Current position in 'shop' logfile. Subsequent calls to get log, will yield log from logpos until current end of file.                        
            std::future<shop_flag> current_run;           ///< future of current run(just to keep it here for a while)        
            stm_system_context(model_state astate, stm_system_& amdl): state{astate},shop_result{shop_flag::success}, mdl{amdl},logpos{0} {}
            // kill stuff that will fail utterly:
            stm_system_context()=delete;
            stm_system_context(stm_system_context const&)=delete;
            stm_system_context& operator=(stm_system_context const&)=delete;
            // move, should/could work.
        };
        
        using stm_system_context_ = std::shared_ptr<stm_system_context>;
        inline stm_system_context_ make_context(model_state const& state, stm_system_ mdl){
            return std::make_shared<stm_system_context>(state, mdl);
        }
    }
}
