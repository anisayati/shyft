#pragma once
#include <memory>
#include <map>
#include <shyft/core/utctime_utilities.h>
#include <shyft/time_series/time_axis.h>
#include <shyft/energy_market/hydro_power/xy_point_curve.h>
#include <shyft/core/time_series_dd.h>
#include <shyft/energy_market/dataset.h>
#include <shyft/energy_market/proxy_attr.h>
#include <boost/fusion/adapted/std_tuple.hpp>
#include <boost/fusion/algorithm.hpp>

namespace shyft::energy_market::stm {
	using std::shared_ptr;
	using std::vector;
	using std::map;
	using shyft::core::utctime;
	using shyft::core::utctimespan;
    using shyft::core::calendar;
	using shyft::time_series::dd::apoint_ts;
    using shyft::time_series::POINT_AVERAGE_VALUE;
    using shyft::time_axis::generic_dt;

	using hydro_power::xy_point_curve_;
	using hydro_power::xyz_point_curve_;
    using xyz_point_curve_list=vector<hydro_power::xy_point_curve_with_z>;
    using xyz_point_curve_list_ = shared_ptr<xyz_point_curve_list>;
	using hydro_power::turbine_description_;

	template<typename T> using t_T = shared_ptr<map<utctime,T>>;
	using t_double_ = t_T<double>;
	using t_xy_ = t_T<xy_point_curve_>;
	using t_xyz_ = t_T<xyz_point_curve_>;
	using t_xyz_list_ = t_T<xyz_point_curve_list_>;
	using t_turbine_description_ = t_T<turbine_description_>;
    using shyft::energy_market::core::proxy_attr;

    /**  @brief  convinience macro to declare the mp types integral constant and corresponding sequence for a named enum. 
     * 
     * @detail
     * using it with mp_c(rsv_attr) results in the 
     * types
    *    template< rsv_attr e> using rsv_attr_c= std::integral_constant<E e>;
     * so that you can use
     * rsv_attr_c<rsv_attr::hrl> instead of std::integral_constant<rsv_attr,rsv_attr::hrl>
     * and
     * rsv_attr_seq_t for the integer_sequence 0..rsv_attr::size, that we use to  span out the lookup-table for member access.
     * 
     */
    
    #define mp_c(E) template<E e> using E##_c  = std::integral_constant<E,e>;using E##_seq_t=std::make_integer_sequence<int,int(E::size)>
    
    /** @brief utility macro to declear member mapping functions 
     *
     * @detail
     * inside the class, e.g. reservoir
     * struct reservoir {
    *    :
    *   struct a_map {
    *      using proxy_container=reservoir;
    *       mp_def_proxy_map(rsv_attr, lrl)
    *       mp_def_proxy_map(rsv_attr,hrl)
    *  }
     *    
     */
    #define def_proxy_map(E, a) static constexpr auto ref (E##_c<E::a>) noexcept { return &proxy_container::a;}
    
	//---- aggregate 
	enum class unit_attr : int64_t {
		// input:
		production_min_static,
		production_max_static,
		unavailability,
		production_min,
		production_max,
		production_schedule,
		discharge_min,
		discharge_max,
		discharge_schedule,
		generator_efficiency,
		turbine_description,
		cost_start,
		cost_stop,
		// result:
		production,
		discharge,
        size // last value, do not add after this, add before!
	};
    mp_c(unit_attr);

	using unit_ds = core::ds_collection<
		core::ds_t<t_xy_, unit_attr>,
		core::ds_t<t_turbine_description_, unit_attr>,
		core::ds_t<apoint_ts, unit_attr>
	>;

	//---- catchment
	enum class catchment_attr : int64_t {
		inflow_m3s,
        size
	};
    mp_c(catchment_attr);
    
	using catchment_ds = core::ds_collection<
	    core::ds_t<apoint_ts, catchment_attr>
	>;

	//---- energy_market_area
	enum class energy_market_area_attr : int64_t {
		// Input:
		price,
		load,
		max_buy,
		max_sale,
		// Result:
		buy,
		sale,
        size // last value, do not add after this, add before!
	};
    mp_c(energy_market_area_attr);
    
	using energy_market_area_ds = core::ds_collection<
		core::ds_t<apoint_ts, energy_market_area_attr>
	>;

	//---- power_station
	enum class pp_attr : int64_t {
		mip,
		outlet_level,
		unavailability,
		production_min,
		production_max,
		production_schedule,
		discharge_min,
		discharge_max,
		discharge_schedule,
        size // last value, do not add after this, add before!
	};
    mp_c(pp_attr);
    
	using power_plant_ds = core::ds_collection<
		core::ds_t<t_xy_, pp_attr>,
		core::ds_t<apoint_ts, pp_attr>
	>;

	//---- reservoir
	enum class rsv_attr : int64_t {
		// Input:
		lrl,
		hrl,
		volume_max_static,
		volume_descr,
		spill_descr,  // deprecated: use gate description on flood waterway instead
		endpoint_desc,
		level_historic,
		level_schedule,
		level_min,
		level_max,
		ramping_level_down_d,
		ramping_level_down_h,
		ramping_level_up_d,
		ramping_level_up_h,
		inflow,
		// Result:
		volume,
		level,
        size // last value, do not add after this, add before!
	};
    mp_c(rsv_attr);
    
   
	using reservoir_ds = core::ds_collection<
		core::ds_t<t_xy_, rsv_attr>,
		core::ds_t<apoint_ts, rsv_attr>
	>;

	//---- water_route
	enum class wtr_attr : int64_t {
		// Input:
		discharge_max_static,
		head_loss_coeff,
		head_loss_func,
		// Result:
		discharge,
        size // last value, do not add after this, add before!
	};
   mp_c(wtr_attr);
   
	using waterway_ds = core::ds_collection<
		core::ds_t<t_xyz_list_, wtr_attr>,
		core::ds_t<apoint_ts, wtr_attr>
	>;

	//---- gate
	enum class gate_attr : int64_t {
        // Input:
        opening_schedule, ///< as in 0= closed, 1= entirely open (relative max opening).
        discharge_schedule, ///< m3/s, wanted flow
        // Result:
        discharge,
        // Physics:
        gate_description,  // gate flow tables.
        size // last value, do not add after this, add before!
	};
    mp_c(gate_attr);
    
	using gate_ids = core::ds_collection<
        core::ds_t<apoint_ts, gate_attr>,
        core::ds_t<t_xyz_list_, gate_attr>
	>;

	//---- stm_system
	enum class run_params_attr: int64_t {
		n_inc_runs, // Number of incremental runs
		n_full_runs, // Number of full runs
		head_opt, // Whether head optimization is turned on.
		run_time_axis, // The time axis SHOP was run on.
		size // last value, do not add after this, add before!
	};
	mp_c(run_params_attr);

	using run_params_ds = core::ds_collection<
	    core::ds_t<int, run_params_attr>,
	    core::ds_t<bool, run_params_attr>,
	    core::ds_t<generic_dt, run_params_attr>
    >;
    

#undef mp_c
}
