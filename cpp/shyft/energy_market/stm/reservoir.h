#pragma once
#include <map>
#include <string>
#include <memory>
#include <vector>
#include <shyft/core/utctime_utilities.h>
#include <shyft/core/core_serialization.h>
#include <shyft/core/time_series_dd.h>
#include <shyft/energy_market/dataset.h>
#include <shyft/energy_market/proxy_attr.h>
#include <shyft/energy_market/hydro_power/reservoir.h>
#include <shyft/energy_market/hydro_power/hydro_power_system.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/attribute_types.h>

namespace shyft::energy_market::stm {
	using std::string;
	using std::shared_ptr;
    using std::dynamic_pointer_cast;
	using std::map;
	using shyft::core::utctime;
	using core::proxy_attr;
	using shyft::time_series::dd::apoint_ts;

	struct reservoir:hydro_power::reservoir {
		using super=hydro_power::reservoir;
		using ds_collection_t=reservoir_ds;
		using ids = hps_ids<reservoir>;
		using rds = hps_rds<reservoir>;
        using e_attr=rsv_attr; // enum attr type
        using e_attr_seq_t=rsv_attr_seq_t;// the full sequence of attr type
        
        string url(const string& prefix="") const {
            std::stringstream ss;
            auto tmp = dynamic_pointer_cast<stm_hps>(hps_());
            if (tmp) ss << tmp->url(prefix);
            else ss << prefix;
            ss << "/R" << id;
            return ss.str();
        }
        
		reservoir(int id, const string& name, const string&json, stm_hps_ &hps);
		reservoir()=default;

		proxy_attr<reservoir, apoint_ts, rsv_attr, rsv_attr::lrl, ids> lrl{*this};
		proxy_attr<reservoir, apoint_ts, rsv_attr, rsv_attr::hrl, ids> hrl{*this};
		proxy_attr<reservoir, apoint_ts, rsv_attr, rsv_attr::volume_max_static, ids> volume_max_static{*this};
		proxy_attr<reservoir, t_xy_, rsv_attr, rsv_attr::volume_descr, ids> volume_descr{*this};
		proxy_attr<reservoir, t_xy_, rsv_attr, rsv_attr::spill_descr, ids> spill_descr{*this};
		proxy_attr<reservoir, apoint_ts, rsv_attr, rsv_attr::endpoint_desc, ids> endpoint_desc{*this};
		proxy_attr<reservoir, apoint_ts, rsv_attr, rsv_attr::inflow, ids> inflow{ *this };
		proxy_attr<reservoir, apoint_ts, rsv_attr, rsv_attr::level_historic, ids> level_historic{ *this };
		proxy_attr<reservoir, apoint_ts, rsv_attr, rsv_attr::level_schedule, ids> level_schedule{*this};
		proxy_attr<reservoir, apoint_ts, rsv_attr, rsv_attr::level_min, ids> level_min{*this};
		proxy_attr<reservoir, apoint_ts, rsv_attr, rsv_attr::level_max, ids> level_max{*this};
		proxy_attr<reservoir, apoint_ts, rsv_attr, rsv_attr::ramping_level_down_d, ids> ramping_level_down_d{ *this };
		proxy_attr<reservoir, apoint_ts, rsv_attr, rsv_attr::ramping_level_down_h, ids> ramping_level_down_h{ *this };
		proxy_attr<reservoir, apoint_ts, rsv_attr, rsv_attr::ramping_level_up_d, ids> ramping_level_up_d{ *this };
		proxy_attr<reservoir, apoint_ts, rsv_attr, rsv_attr::ramping_level_up_h, ids> ramping_level_up_h{ *this };

		proxy_attr<reservoir, apoint_ts, rsv_attr, rsv_attr::volume, rds> volume{*this};
		proxy_attr<reservoir, apoint_ts, rsv_attr, rsv_attr::level, rds> level{*this};
        // meta-programming support, list all mappings of the above proxy attributes here
        // note: later using more clever enums, we can use boost::hana adapt instead
        struct a_map {
            using proxy_container=reservoir;
            //static constexpr auto ref(rsv_attr_c<rsv_attr::lrl>) noexcept { return &proxy_container::lrl;}
            def_proxy_map(rsv_attr,lrl)
            def_proxy_map(rsv_attr,hrl)
            def_proxy_map(rsv_attr, volume_max_static)
            def_proxy_map(rsv_attr, volume_descr)
            def_proxy_map(rsv_attr, spill_descr)
            def_proxy_map(rsv_attr, endpoint_desc)
            def_proxy_map(rsv_attr, inflow)
            def_proxy_map(rsv_attr, level_historic)
            def_proxy_map(rsv_attr, level_schedule)
            def_proxy_map(rsv_attr, level_min)
            def_proxy_map(rsv_attr, level_max)
            def_proxy_map(rsv_attr,ramping_level_down_d)
            def_proxy_map(rsv_attr,ramping_level_down_h)
            def_proxy_map(rsv_attr,ramping_level_up_d)
            def_proxy_map(rsv_attr,ramping_level_up_h)
            def_proxy_map(rsv_attr,volume)
            def_proxy_map(rsv_attr,level)
        };
                
		x_serialize_decl();
	};
	using reservoir_=shared_ptr<reservoir>;

}

x_serialize_export_key(shyft::energy_market::stm::reservoir);

