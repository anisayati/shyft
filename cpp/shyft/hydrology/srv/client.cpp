#include <shyft/hydrology/srv/client.h>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/variant.hpp>
#include <shyft/core/core_archive.h>

namespace shyft::hydrology::srv {

using shyft::core::core_iarchive;
using shyft::core::core_oarchive;
using std::chrono::seconds;
using std::chrono::milliseconds;
using shyft::core::scoped_connect;
using shyft::core::do_io_with_repair_and_retry;
using std::make_unique;
using std::max;
using std::this_thread::sleep_for;

/** @brief a client
*
* 
* This class take care of message exchange to the remote server,
* using the supplied connection parameters.
* 
* It implements the message protocol of the server, sending
* message-prefix, arguments, waiting for response
* deserialize the response and handle it back to the user.
* 
* @see server
* 
* 
*/
client::client(string host_port,int timeout_ms):c{host_port,timeout_ms}{}

//TODO: implement server_version_info()

/** @brief typical client pattern
*
* @param mid the model-id to which we update the model-info on
* @return true if succeeded (model exists, and was removed)
* 
*/
string client::version_info() {
    scoped_connect sc(c);
    string r{};
    do_io_with_repair_and_retry(c,[&r](srv_connection&c) {
        dlib::iosockstream& io = *c.io;
        msg::write_type(message_type::VERSION_INFO,io);
        core_oarchive oa(io, core_arch_flags);
        //oa << mid<<mi;
        auto response_type = msg::read_type(io);
        if (response_type == message_type::SERVER_EXCEPTION) {
            auto re = msg::read_exception(io);
            throw re;
        } else if (response_type == message_type::VERSION_INFO) {
            core_iarchive ia(io, core_arch_flags);
            ia >> r;
        } else {
            throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
        }
    });
    return r;
}

bool client::create_model(string const& mid, 
                    rmodel_type mtype, 
                    vector <core::geo_cell_data> const& gcd){
    scoped_connect sc(c);
    bool r{false};
    do_io_with_repair_and_retry(c,[&r, &mid, mtype, &gcd](srv_connection&c) {
        dlib::iosockstream& io = *c.io;
        msg::write_type(message_type::CREATE_MODEL,io);
        core_oarchive oa(io, core_arch_flags);
        oa << mid<<mtype<<gcd;
        auto response_type = msg::read_type(io);
        if (response_type == message_type::SERVER_EXCEPTION) {
            auto re = msg::read_exception(io);
            throw re;
        } else if (response_type == message_type::CREATE_MODEL) {
            core_iarchive ia(io, core_arch_flags);
            ia >> r;
        } else {
            throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
        }
    });
    return r;
}

bool client::set_state(string const& mid, state_variant_t const& csv){
    scoped_connect sc(c);
    bool r{false};
    do_io_with_repair_and_retry(c,[&r, &mid, &csv](srv_connection&c) {
        dlib::iosockstream& io = *c.io;
        msg::write_type(message_type::SET_STATE,io);
        core_oarchive oa(io, core_arch_flags);
        oa << mid<<csv;
        auto response_type = msg::read_type(io);
        if (response_type == message_type::SERVER_EXCEPTION) {
            auto re = msg::read_exception(io);
            throw re;
        } else if (response_type == message_type::SET_STATE) {
            core_iarchive ia(io, core_arch_flags);
            ia >> r;
        } else {
            throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
        }
    });
    return r;
}

state_variant_t client::get_state(string const& mid, const shyft::api::cids_t& cids){
    scoped_connect sc(c);
    state_variant_t r;
    do_io_with_repair_and_retry(c,[&r, &mid, &cids](srv_connection&c) {
        dlib::iosockstream& io = *c.io;
        msg::write_type(message_type::GET_STATE,io);
        core_oarchive oa(io, core_arch_flags);
        oa << mid<<cids;
        auto response_type = msg::read_type(io);
        if (response_type == message_type::SERVER_EXCEPTION) {
            auto re = msg::read_exception(io);
            throw re;
        } else if (response_type == message_type::GET_STATE) {
            core_iarchive ia(io, core_arch_flags);
            ia >> r;
        } else {
            throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
        }
    });
    return r;
}

bool client::set_region_parameter(string const&mid,parameter_variant_t const& pv) {
    scoped_connect sc(c);
    bool r{false};
    do_io_with_repair_and_retry(c,[&r, &mid, &pv](srv_connection&c) {
        dlib::iosockstream& io = *c.io;
        msg::write_type(message_type::SET_REGION_PARAMETER,io);
        core_oarchive oa(io, core_arch_flags);
        oa << mid<<pv;
        auto response_type = msg::read_type(io);
        if (response_type == message_type::SERVER_EXCEPTION) {
            auto re = msg::read_exception(io);
            throw re;
        } else if (response_type == message_type::SET_REGION_PARAMETER) {
            core_iarchive ia(io, core_arch_flags);
            ia >> r;
        } else {
            throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
        }
    });
    return r;
}

bool client::set_catchment_parameter(string const&mid,parameter_variant_t const& pv,size_t cid) {
    scoped_connect sc(c);
    bool r{false};
    do_io_with_repair_and_retry(c,[&r, &mid, &pv,cid](srv_connection&c) {
        dlib::iosockstream& io = *c.io;
        msg::write_type(message_type::SET_CATCHMENT_PARAMETER,io);
        core_oarchive oa(io, core_arch_flags);
        oa << mid<<pv<<int64_t(cid);
        auto response_type = msg::read_type(io);
        if (response_type == message_type::SERVER_EXCEPTION) {
            auto re = msg::read_exception(io);
            throw re;
        } else if (response_type == message_type::SET_CATCHMENT_PARAMETER) {
            core_iarchive ia(io, core_arch_flags);
            ia >> r;
        } else {
            throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
        }
    });
    return r;
}

bool client::remove_model(string const&mid) {
    scoped_connect sc(c);
    bool r{false};
    do_io_with_repair_and_retry(c,[&r, &mid](srv_connection&c) {
        dlib::iosockstream& io = *c.io;
        msg::write_type(message_type::REMOVE_MODEL,io);
        core_oarchive oa(io, core_arch_flags);
        oa << mid;
        auto response_type = msg::read_type(io);
        if (response_type == message_type::SERVER_EXCEPTION) {
            auto re = msg::read_exception(io);
            throw re;
        } else if (response_type == message_type::REMOVE_MODEL) {
            core_iarchive ia(io, core_arch_flags);
            ia >> r;
        } else {
            throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
        }
    });
    return r;
}

bool client::rename_model(string const& old_mid, string const& new_mid) {
    scoped_connect sc(c);
    bool r{false};
    do_io_with_repair_and_retry(c,[&r, &old_mid, &new_mid](srv_connection&c) {
        dlib::iosockstream& io = *c.io;
        msg::write_type(message_type::RENAME_MODEL,io);
        core_oarchive oa(io, core_arch_flags);
        oa << old_mid << new_mid;
        auto response_type = msg::read_type(io);
        if (response_type == message_type::SERVER_EXCEPTION) {
            auto re = msg::read_exception(io);
            throw re;
        } else if (response_type == message_type::RENAME_MODEL) {
            core_iarchive ia(io, core_arch_flags);
            ia >> r;
        } else {
            throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
        }
    });
    return r;
}

bool client::clone_model(string const& old_mid, string const& new_mid) {
    scoped_connect sc(c);
    bool r{false};
    do_io_with_repair_and_retry(c,[&r, &old_mid, &new_mid](srv_connection&c) {
        dlib::iosockstream& io = *c.io;
        msg::write_type(message_type::CLONE_MODEL,io);
        core_oarchive oa(io, core_arch_flags);
        oa << old_mid << new_mid;
        auto response_type = msg::read_type(io);
        if (response_type == message_type::SERVER_EXCEPTION) {
            auto re = msg::read_exception(io);
            throw re;
        } else if (response_type == message_type::CLONE_MODEL) {
            core_iarchive ia(io, core_arch_flags);
            ia >> r;
        } else {
            throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
        }
    });
    return r;
}

bool client::copy_model(string const& old_mid, string const& new_mid) {
    scoped_connect sc(c);
    bool r{false};
    do_io_with_repair_and_retry(c,[&r, &old_mid, &new_mid](srv_connection&c) {
        dlib::iosockstream& io = *c.io;
        msg::write_type(message_type::COPY_MODEL,io);
        core_oarchive oa(io, core_arch_flags);
        oa << old_mid << new_mid;
        auto response_type = msg::read_type(io);
        if (response_type == message_type::SERVER_EXCEPTION) {
            auto re = msg::read_exception(io);
            throw re;
        } else if (response_type == message_type::COPY_MODEL) {
            core_iarchive ia(io, core_arch_flags);
            ia >> r;
        } else {
            throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
        }
    });
    return r;
}

vector<string> client::get_model_ids() {
    scoped_connect sc(c);
    vector<string> r;
    do_io_with_repair_and_retry(c,[&r](srv_connection&c) {
        dlib::iosockstream& io = *c.io;
        msg::write_type(message_type::GET_MODEL_IDS,io);
        auto response_type = msg::read_type(io);
        if (response_type == message_type::SERVER_EXCEPTION) {
            auto re = msg::read_exception(io);
            throw re;
        } else if (response_type == message_type::GET_MODEL_IDS) {
            core_iarchive ia(io, core_arch_flags);
            ia >> r;
        } else {
            throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
        }
    });
    return r;
    
}

bool client::run_interpolation(string const& mid, const shyft::core::interpolation_parameter& ip_parameter, const shyft::time_axis::generic_dt& ta, const shyft::api::a_region_environment& r_env, bool best_effort) {
    scoped_connect sc(c);
    bool r{false};
    do_io_with_repair_and_retry(c,[&r, &mid, &ip_parameter, &r_env, &ta, &best_effort](srv_connection&c) {
        dlib::iosockstream& io = *c.io;
        msg::write_type(message_type::RUN_INTERPOLATION,io);
        core_oarchive oa(io, core_arch_flags);
        oa << mid<<ip_parameter<<ta<<r_env<<best_effort;
        auto response_type = msg::read_type(io);
        if (response_type == message_type::SERVER_EXCEPTION) {
            auto re = msg::read_exception(io);
            throw re;
        } else if (response_type == message_type::RUN_INTERPOLATION) {
            core_iarchive ia(io, core_arch_flags);
            ia >> r;
        } else {
            throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
        }
    });
    return r;
}

bool client::run_cells(string const& mid,size_t use_ncore,int start_step,int n_steps) {
    scoped_connect sc(c);
    bool r{false};
    do_io_with_repair_and_retry(c,[&r, &mid,use_ncore,start_step,n_steps](srv_connection&c) {
        dlib::iosockstream& io = *c.io;
        msg::write_type(message_type::RUN_CELLS,io);
        core_oarchive oa(io, core_arch_flags);
        oa << mid<<int64_t(use_ncore)<<int64_t(start_step)<<int64_t(n_steps);
        auto response_type = msg::read_type(io);
        if (response_type == message_type::SERVER_EXCEPTION) {
            auto re = msg::read_exception(io);
            throw re;
        } else if (response_type == message_type::RUN_CELLS) {
            core_iarchive ia(io, core_arch_flags);
            ia >> r;
        } else {
            throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
        }
    });
    return r;
}

q_adjust_result client::adjust_q(string const& mid, const shyft::api::cids_t& indexes, double wanted_q,size_t start_step,double scale_range,double scale_eps,size_t max_iter,size_t n_steps) {
    scoped_connect sc(c);
    q_adjust_result q_result;
    do_io_with_repair_and_retry(c,[&q_result, &mid, &wanted_q, &indexes,start_step,scale_range,scale_eps,max_iter,n_steps](srv_connection&c) {
        dlib::iosockstream& io = *c.io;
        msg::write_type(message_type::ADJUST_Q,io);
        core_oarchive oa(io, core_arch_flags);
        oa << mid<<indexes<<wanted_q<<int64_t(start_step)<<scale_range<<scale_eps<<int64_t(max_iter)<<int64_t(n_steps);
        auto response_type = msg::read_type(io);
        if (response_type == message_type::SERVER_EXCEPTION) {
            auto re = msg::read_exception(io);
            throw re;
        } else if (response_type == message_type::ADJUST_Q) {
            core_iarchive ia(io, core_arch_flags);
            ia >> q_result;
        } else {
            throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
        }
    });
    return q_result;
}

apoint_ts client::get_discharge(string const& mid, const shyft::api::cids_t& indexes, shyft::core::stat_scope ix_type) {
    scoped_connect sc(c);
    apoint_ts ts;
    do_io_with_repair_and_retry(c,[&ts, &mid, &indexes,ix_type](srv_connection&c) {
        dlib::iosockstream& io = *c.io;
        msg::write_type(message_type::GET_DISCHARGE,io);
        core_oarchive oa(io, core_arch_flags);
        oa << mid << indexes<<ix_type;
        auto response_type = msg::read_type(io);
        if (response_type == message_type::SERVER_EXCEPTION) {
            auto re = msg::read_exception(io);
            throw re;
        } else if (response_type == message_type::GET_DISCHARGE) {
            core_iarchive ia(io, core_arch_flags);
            ia >> ts;
        } else {
            throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
        }
    });
    return ts;
}

apoint_ts client::get_temperature(string const& mid, const shyft::api::cids_t& indexes, shyft::core::stat_scope ix_type) {
    scoped_connect sc(c);
    apoint_ts ts;
    do_io_with_repair_and_retry(c,[&ts, &mid, &indexes,ix_type](srv_connection&c) {
        dlib::iosockstream& io = *c.io;
        msg::write_type(message_type::GET_TEMPERATURE,io);
        core_oarchive oa(io, core_arch_flags);
        oa << mid << indexes<<ix_type;
        auto response_type = msg::read_type(io);
        if (response_type == message_type::SERVER_EXCEPTION) {
            auto re = msg::read_exception(io);
            throw re;
        } else if (response_type == message_type::GET_TEMPERATURE) {
            core_iarchive ia(io, core_arch_flags);
            ia >> ts;
        } else {
            throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
        }
    });
    return ts;
}

apoint_ts client::get_precipitation(string const& mid, const shyft::api::cids_t& indexes, shyft::core::stat_scope ix_type) {
    scoped_connect sc(c);
    apoint_ts ts;
    do_io_with_repair_and_retry(c,[&ts, &mid, &indexes,ix_type](srv_connection&c) {
        dlib::iosockstream& io = *c.io;
        msg::write_type(message_type::GET_PRECIPITATION,io);
        core_oarchive oa(io, core_arch_flags);
        oa << mid << indexes<<ix_type;
        auto response_type = msg::read_type(io);
        if (response_type == message_type::SERVER_EXCEPTION) {
            auto re = msg::read_exception(io);
            throw re;
        } else if (response_type == message_type::GET_PRECIPITATION) {
            core_iarchive ia(io, core_arch_flags);
            ia >> ts;
        } else {
            throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
        }
    });
    return ts;
}

apoint_ts client::get_snow_swe(string const& mid, const shyft::api::cids_t& indexes, shyft::core::stat_scope ix_type) {
    scoped_connect sc(c);
    apoint_ts ts;
    do_io_with_repair_and_retry(c,[&ts, &mid, &indexes,ix_type](srv_connection&c) {
        dlib::iosockstream& io = *c.io;
        msg::write_type(message_type::GET_SNOW_SWE,io);
        core_oarchive oa(io, core_arch_flags);
        oa << mid << indexes<<ix_type;
        auto response_type = msg::read_type(io);
        if (response_type == message_type::SERVER_EXCEPTION) {
            auto re = msg::read_exception(io);
            throw re;
        } else if (response_type == message_type::GET_SNOW_SWE) {
            core_iarchive ia(io, core_arch_flags);
            ia >> ts;
        } else {
            throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
        }
    });
    return ts;
}

apoint_ts client::get_charge(string const& mid, const shyft::api::cids_t& indexes, shyft::core::stat_scope ix_type) {
    scoped_connect sc(c);
    apoint_ts ts;
    do_io_with_repair_and_retry(c,[&ts, &mid, &indexes,ix_type](srv_connection&c) {
        dlib::iosockstream& io = *c.io;
        msg::write_type(message_type::GET_CHARGE,io);
        core_oarchive oa(io, core_arch_flags);
        oa << mid << indexes<<ix_type;
        auto response_type = msg::read_type(io);
        if (response_type == message_type::SERVER_EXCEPTION) {
            auto re = msg::read_exception(io);
            throw re;
        } else if (response_type == message_type::GET_CHARGE) {
            core_iarchive ia(io, core_arch_flags);
            ia >> ts;
        } else {
            throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
        }
    });
    return ts;
}

apoint_ts client::get_snow_sca(string const& mid, const shyft::api::cids_t& indexes, shyft::core::stat_scope ix_type) {
    scoped_connect sc(c);
    apoint_ts ts;
    do_io_with_repair_and_retry(c,[&ts, &mid, &indexes,ix_type](srv_connection&c) {
        dlib::iosockstream& io = *c.io;
        msg::write_type(message_type::GET_SNOW_SCA,io);
        core_oarchive oa(io, core_arch_flags);
        oa << mid << indexes<<ix_type;
        auto response_type = msg::read_type(io);
        if (response_type == message_type::SERVER_EXCEPTION) {
            auto re = msg::read_exception(io);
            throw re;
        } else if (response_type == message_type::GET_SNOW_SCA) {
            core_iarchive ia(io, core_arch_flags);
            ia >> ts;
        } else {
            throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
        }
    });
    return ts;
}

apoint_ts client::get_radiation(string const& mid, const shyft::api::cids_t& indexes, shyft::core::stat_scope ix_type) {
    scoped_connect sc(c);
    apoint_ts ts;
    do_io_with_repair_and_retry(c,[&ts, &mid, &indexes,ix_type](srv_connection&c) {
        dlib::iosockstream& io = *c.io;
        msg::write_type(message_type::GET_RADIATION,io);
        core_oarchive oa(io, core_arch_flags);
        oa << mid << indexes<<ix_type;
        auto response_type = msg::read_type(io);
        if (response_type == message_type::SERVER_EXCEPTION) {
            auto re = msg::read_exception(io);
            throw re;
        } else if (response_type == message_type::GET_RADIATION) {
            core_iarchive ia(io, core_arch_flags);
            ia >> ts;
        } else {
            throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
        }
    });
    return ts;
}

apoint_ts client::get_wind_speed(string const& mid, const shyft::api::cids_t& indexes, shyft::core::stat_scope ix_type) {
    scoped_connect sc(c);
    apoint_ts ts;
    do_io_with_repair_and_retry(c,[&ts, &mid, &indexes,ix_type](srv_connection&c) {
        dlib::iosockstream& io = *c.io;
        msg::write_type(message_type::GET_WIND_SPEED,io);
        core_oarchive oa(io, core_arch_flags);
        oa << mid << indexes<<ix_type;
        auto response_type = msg::read_type(io);
        if (response_type == message_type::SERVER_EXCEPTION) {
            auto re = msg::read_exception(io);
            throw re;
        } else if (response_type == message_type::GET_WIND_SPEED) {
            core_iarchive ia(io, core_arch_flags);
            ia >> ts;
        } else {
            throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
        }
    });
    return ts;
}

apoint_ts client::get_rel_hum(string const& mid, const shyft::api::cids_t& indexes, shyft::core::stat_scope ix_type) {
    scoped_connect sc(c);
    apoint_ts ts;
    do_io_with_repair_and_retry(c,[&ts, &mid, &indexes,ix_type](srv_connection&c) {
        dlib::iosockstream& io = *c.io;
        msg::write_type(message_type::GET_REL_HUM,io);
        core_oarchive oa(io, core_arch_flags);
        oa << mid << indexes<<ix_type;
        auto response_type = msg::read_type(io);
        if (response_type == message_type::SERVER_EXCEPTION) {
            auto re = msg::read_exception(io);
            throw re;
        } else if (response_type == message_type::GET_REL_HUM) {
            core_iarchive ia(io, core_arch_flags);
            ia >> ts;
        } else {
            throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
        }
    });
    return ts;
}
bool client::set_catchment_calculation_filter(string const& mid, const std::vector<int64_t>& catchment_id_list) {
    scoped_connect sc(c);
    bool r{false};
    do_io_with_repair_and_retry(c,[&r, &mid, &catchment_id_list](srv_connection&c) {
        dlib::iosockstream& io = *c.io;
        msg::write_type(message_type::SET_CATCHMENT_CALCULATION_FILTER,io);
        core_oarchive oa(io, core_arch_flags);
        oa << mid<<catchment_id_list;
        auto response_type = msg::read_type(io);
        if (response_type == message_type::SERVER_EXCEPTION) {
            auto re = msg::read_exception(io);
            throw re;
        } else if (response_type == message_type::SET_CATCHMENT_CALCULATION_FILTER) {
            core_iarchive ia(io, core_arch_flags);
            ia >> r;
        } else {
            throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
        }
    });
    return r;
}

bool client::revert_to_initial_state(string const& mid) {
    scoped_connect sc(c);
    bool r{false};
    do_io_with_repair_and_retry(c,[&r, &mid](srv_connection&c) {
        dlib::iosockstream& io = *c.io;
        msg::write_type(message_type::REVERT_TO_INITIAL_STATE,io);
        core_oarchive oa(io, core_arch_flags);
        oa << mid;
        auto response_type = msg::read_type(io);
        if (response_type == message_type::SERVER_EXCEPTION) {
            auto re = msg::read_exception(io);
            throw re;
        } else if (response_type == message_type::REVERT_TO_INITIAL_STATE) {
            core_iarchive ia(io, core_arch_flags);
            ia >> r;
        } else {
            throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
        }
    });
    return r;
}

bool client::set_state_collection(string const& mid, int64_t catchment_id,bool on_or_off) {
    scoped_connect sc(c);
    bool r{false};
    do_io_with_repair_and_retry(c,[&r, &mid, &catchment_id, &on_or_off](srv_connection&c) {
        dlib::iosockstream& io = *c.io;
        msg::write_type(message_type::SET_STATE_COLLECTION,io);
        core_oarchive oa(io, core_arch_flags);
        oa << mid << catchment_id << on_or_off;
        auto response_type = msg::read_type(io);
        if (response_type == message_type::SERVER_EXCEPTION) {
            auto re = msg::read_exception(io);
            throw re;
        } else if (response_type == message_type::SET_STATE_COLLECTION) {
            core_iarchive ia(io, core_arch_flags);
            ia >> r;
        } else {
            throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
        }
    });
    return r;
}

bool client::set_snow_sca_swe_collection(string const& mid, int64_t catchment_id,bool on_or_off) {
    scoped_connect sc(c);
    bool r{false};
    do_io_with_repair_and_retry(c,[&r, &mid, &catchment_id, &on_or_off](srv_connection&c) {
        dlib::iosockstream& io = *c.io;
        msg::write_type(message_type::SET_SNOW_SCA_SWE_COLLECTION,io);
        core_oarchive oa(io, core_arch_flags);
        oa << mid << catchment_id << on_or_off;
        auto response_type = msg::read_type(io);
        if (response_type == message_type::SERVER_EXCEPTION) {
            auto re = msg::read_exception(io);
            throw re;
        } else if (response_type == message_type::SET_SNOW_SCA_SWE_COLLECTION) {
            core_iarchive ia(io, core_arch_flags);
            ia >> r;
        } else {
            throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
        }
    });
    return r;
}

bool client::is_cell_env_ts_ok(string const& mid) {
    scoped_connect sc(c);
    bool r{false};
    do_io_with_repair_and_retry(c,[&r, &mid](srv_connection&c) {
        dlib::iosockstream& io = *c.io;
        msg::write_type(message_type::IS_CELL_ENV_TS_OK,io);
        core_oarchive oa(io, core_arch_flags);
        oa << mid;
        auto response_type = msg::read_type(io);
        if (response_type == message_type::SERVER_EXCEPTION) {
            auto re = msg::read_exception(io);
            throw re;
        } else if (response_type == message_type::IS_CELL_ENV_TS_OK) {
            core_iarchive ia(io, core_arch_flags);
            ia >> r;
        } else {
            throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
        }
    });
    return r;
}

bool client::is_calculated(string const& mid, size_t cid) {
    scoped_connect sc(c);
    bool r{false};
    do_io_with_repair_and_retry(c,[&r, &mid, &cid](srv_connection&c) {
        dlib::iosockstream& io = *c.io;
        msg::write_type(message_type::IS_CALCULATED,io);
        core_oarchive oa(io, core_arch_flags);
        oa << mid << cid;
        auto response_type = msg::read_type(io);
        if (response_type == message_type::SERVER_EXCEPTION) {
            auto re = msg::read_exception(io);
            throw re;
        } else if (response_type == message_type::IS_CALCULATED) {
            core_iarchive ia(io, core_arch_flags);
            ia >> r;
        } else {
            throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
        }
    });
    return r;
}

bool client::set_initial_state(string const& mid) {
    scoped_connect sc(c);
    bool r{false};
    do_io_with_repair_and_retry(c,[&r, &mid](srv_connection&c) {
        dlib::iosockstream& io = *c.io;
        msg::write_type(message_type::SET_INITIAL_STATE,io);
        core_oarchive oa(io, core_arch_flags);
        oa << mid;
        auto response_type = msg::read_type(io);
        if (response_type == message_type::SERVER_EXCEPTION) {
            auto re = msg::read_exception(io);
            throw re;
        } else if (response_type == message_type::SET_INITIAL_STATE) {
            core_iarchive ia(io, core_arch_flags);
            ia >> r;
        } else {
            throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
        }
    });
    return r;
}

/** @brief close, until needed again, the server connection
*
*/
void client::close() {
    c.close();//just close-down connection, it will auto-open if needed
}
}
