#pragma once


/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <string>
#include <cstdint>
#include <exception>
#include <memory>
#include <mutex>
#include <map>

#include <dlib/server.h>
#include <dlib/iosockstream.h>

#include <shyft/hydrology/srv/msg_types.h>


namespace shyft::hydrology::srv {

    using std::vector;
    using std::shared_ptr;
    using std::make_shared;
    using std::string;
    using std::to_string;
    using std::runtime_error;
    using std::mutex;
    using std::unique_lock;
    using shyft::core::utctime;
    using shyft::time_series::dd::apoint_ts;
    using shyft::core::q_adjust_result;
    using shyft::core::region_model;
    using namespace shyft::core;
    using shyft::api::a_region_environment;


    
    
    /** @brief a server for serializable (parts of) hydrology forecasting models,
    *
    * Currently using dlib server_iostream, 
    */
    
    struct server : dlib::server_iostream {
        
        server() =default;    
        server(server&&)=delete;
        server(const server&) =delete;
        server& operator=(const server&)=delete;
        server& operator=(server&&)=delete;
        ~server() =default;
        
        std::mutex srv_mx;///< protect server context
        std::map<string,model_variant_t> model_map;///< key=mid, model.. shared_ptr
        std::map<string,shared_ptr<mutex>> mx_map;///< key=mid, value- shared mutext for model
        
        /** start the server in background, return the listening port used in case it was set unspecified */
        int start_server();
        
        string do_get_version_info();
        
        /**  create a model based on geo_cell_data,  default parameter of specified type
         * TODO: consider using table-driven meta-programming approach to minimize maintenance
         */
        
        model_variant_t make_shared_model_of_type(rmodel_type &mtype,vector <core::geo_cell_data> const& gcd);
        
        /** @brief creates a new model, with model-id and specified type */
        bool do_create_model(string const& mid, 
                             rmodel_type mtype, 
                             vector <core::geo_cell_data> const& gcd);

        /** @brief remove (free up mem etc) of region-model  model-id */
        bool do_remove_model(string const& mid);
        
        /** @brief get models, returns a string list with model identifiers */
        vector<string> do_get_model_ids() ;

        
        /** @brief given model id, safely get a shared_ptr to that */
        model_variant_t get_model(string mid) ;
        
        /** @brief returns a shared_ptr to the mutex for model id */
        shared_ptr<mutex> get_model_mx(string mid);
        
        /** @brief rename a model */
        bool do_rename_model(string old_mid, string new_mid);
        
        bool do_set_state(string const& mid, state_variant_t const& csv);
        
        state_variant_t do_get_state(string const& mid, shyft::api::cids_t& cids);
        
        bool do_set_region_parameter(string const&mid, parameter_variant_t const&p);

        bool do_set_catchment_parameter(string const&mid, parameter_variant_t const&p,size_t cid);

        bool do_run_interpolation(string const& mid, 
                                       const shyft::core::interpolation_parameter& ip_parameter,
                                       const shyft::time_axis::generic_dt& ta,
                                       const shyft::api::a_region_environment& r_env,
                                       bool best_effort);
        
        bool do_run_cells(string const& mid,size_t use_ncore,int start_step,int n_steps);
        
        q_adjust_result do_adjust_q(string const& mid, const shyft::api::cids_t& indexes, double wanted_q,size_t start_step=0,double scale_range=3.0,double scale_eps=1e-3,size_t max_iter=300,size_t n_steps=1);


        apoint_ts do_get_discharge(string const& mid, const shyft::api::cids_t& cids, shyft::core::stat_scope ix_type=shyft::core::stat_scope::cell_ix);
        
        apoint_ts do_get_temperature(string const& mid, const shyft::api::cids_t& cids, shyft::core::stat_scope ix_type=shyft::core::stat_scope::cell_ix);
        
        apoint_ts do_get_precipitation(string const& mid, const shyft::api::cids_t& cids, shyft::core::stat_scope ix_type=shyft::core::stat_scope::cell_ix);
        
        apoint_ts do_get_snow_swe(string const& mid, const shyft::api::cids_t& cids, shyft::core::stat_scope ix_type=shyft::core::stat_scope::cell_ix);
        
        apoint_ts do_get_charge(string const& mid, const shyft::api::cids_t& cids, shyft::core::stat_scope ix_type=shyft::core::stat_scope::cell_ix);
        
        apoint_ts do_get_snow_sca(string const& mid, const shyft::api::cids_t& cids, shyft::core::stat_scope ix_type=shyft::core::stat_scope::cell_ix);
        
        apoint_ts do_get_radiation(string const& mid, const shyft::api::cids_t& cids, shyft::core::stat_scope ix_type=shyft::core::stat_scope::cell_ix);
        
        apoint_ts do_get_wind_speed(string const& mid, const shyft::api::cids_t& cids, shyft::core::stat_scope ix_type=shyft::core::stat_scope::cell_ix);
        
        apoint_ts do_get_rel_hum(string const& mid, const shyft::api::cids_t& cids, shyft::core::stat_scope ix_type=shyft::core::stat_scope::cell_ix);
        
        bool do_set_catchment_calculation_filter(string const& mid, const std::vector<int64_t>& catchment_id_list);
        
        bool do_revert_state(string const& mid);
        
        bool do_clone_model(string const& old_mid, string const& new_mid) ;
        
        bool do_copy_model(string const& old_mid, string const& new_mid);
        
        bool do_set_state_collection(string const& mid, int64_t catchment_id,bool on_or_off);
        
        bool do_set_snow_sca_swe_collection(string const& mid, int64_t catchment_id,bool on_or_off);
        
        bool do_is_cell_env_ts_ok(string const& mid);
        
        bool do_set_initial_state(string const& mid) ;
        
        bool do_is_calculated(string const& mid, size_t cid);

            
        /**@brief handle one client connection 
        *
        * Reads messages/requests from the clients,
        * - act and perform request,
        * - return response
        * for as long as the client keep the connection 
        * open.
        * 
        */
        void on_connect(
            std::istream & in,
            std::ostream & out,
            const std::string & foreign_ip,
            const std::string & local_ip,
            unsigned short foreign_port,
            unsigned short local_port,
            dlib::uint64 /*connection_id*/
        );    
        
    };
    
}
