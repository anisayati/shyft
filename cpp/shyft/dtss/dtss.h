/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once

#include <cstdint>
#include <cstdio>
#include <string>
#include <string_view>
#include <vector>
#include <map>
#include <unordered_map>
#include <algorithm>
#include <memory>
#include <utility>
#include <functional>
#include <cstring>
#include <regex>
#include <variant>

#include <shyft/core/core_serialization.h>
#include <shyft/time_series/expression_serialization.h>
#include <shyft/core/core_archive.h>

#include <boost/functional/hash.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/shared_ptr.hpp>

#include <dlib/server.h>
#include <dlib/iosockstream.h>
#include <dlib/logger.h>
#include <dlib/misc_api.h>

#include <shyft/time_series/dd/apoint_ts.h> //time_series_dd.h>
#include <shyft/time_series/dd/ats_vector.h>
#include <shyft/dtss/time_series_info.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/dtss/dtss_cache.h>
#include <shyft/dtss/dtss_url.h>
#include <shyft/dtss/dtss_msg.h>
#include <shyft/dtss/dtss_db.h>
#include <shyft/dtss/dtss_krls.h>
#include <shyft/dtss/dtss_subscription.h>


namespace shyft::dtss {

    using shyft::core::utctime;
    using shyft::core::utcperiod;
    using shyft::core::utctimespan;
    using shyft::core::no_utctime;
    using shyft::core::calendar;
    using shyft::core::deltahours;

    using gta_t = shyft::time_axis::generic_dt;
    using gts_t = shyft::time_series::point_ts<gta_t>;

    using shyft::time_series::dd::apoint_ts;
    using shyft::time_series::dd::gpoint_ts;
    using shyft::time_series::dd::gts_t;
    using shyft::time_series::dd::aref_ts;
    using shyft::time_series::dd::ts_as;


    // ========================================


    using ts_vector_t = shyft::time_series::dd::ats_vector;
    using ts_info_vector_t = std::vector<ts_info>;
    using id_vector_t = std::vector<std::string>;
    using read_call_back_t = std::function<ts_vector_t(const id_vector_t& ts_ids, utcperiod p)>;
    using store_call_back_t = std::function<void(const ts_vector_t&)>;
    using find_call_back_t = std::function<ts_info_vector_t(std::string search_expression)>;

    using std::unique_ptr;
    using std::make_unique;
    using std::unique_lock;

    // ========================================


    /** @brief A dtss server with time-series server-side functions
     *
     * The dtss server listens on a port, receives messages, interpret them
     * and ship the response back to the client.
     *
     * Callbacks are provided for extending/delegating find/read_ts/store_ts,
     * as well as internal implementation of storing time-series
     * using plain binary files stored in containers(directory).
     *
     * Time-series are named with url's, and all request involving 'shyft://'
     * like
     *   shyft://<container>/<local_ts_name>
     * resolves to the internal implementation.
     *
     *
     */
    
    struct server : dlib::server_iostream {


        using ts_cache_t = cache<apoint_ts_frag, apoint_ts>;
        using cwrp_t = unique_ptr<its_db>;

        // callbacks for extensions
        read_call_back_t bind_ts_cb;    ///< called to read non shyft:// unbound ts
        find_call_back_t find_ts_cb;    ///< called for all non shyft:// find operations
        store_call_back_t store_ts_cb;  ///< called for all non shyft:// store operations

        // shyft-internal ts container implementation
        /** Query key used to specify container types. */
        inline static const std::string container_query{ "container" };
        /** Sequence of query keys to remove before passing the query map to the containers. */
        inline static const std::array<std::string, 1> remove_queries{{ container_query }};
        mutex c_mx;///< container mutex
        std::unordered_map<std::string, cwrp_t> container;  ///< mapping of internal shyft <container>
        ts_cache_t ts_cache{ 1000000 };  ///< default 1 mill ts in cache
        bool cache_all_reads{ false };
        bool can_remove{ false };
        shyft::core::subscription::manager_ sm=make_shared<shyft::core::subscription::manager>();///< the subscription_manager so that web-api can support observable expression-vectors

        // constructors
        server()=default;
        server(server&&)=delete;
        server(const server&) =delete;
        server& operator=(const server&)=delete;
        server& operator=(server&&)=delete;

        template < class CB >
        explicit server(CB && cb)
            : bind_ts_cb{ std::forward<CB>(cb) } {
        }

        template < class RCB, class FCB>
        server(RCB && rcb, FCB && fcb)
            : bind_ts_cb{ std::forward<RCB>(rcb) }, find_ts_cb{ std::forward<FCB>(fcb) } {
        }

        template < class RCB, class FCB, class SCB >
        server(RCB && rcb, FCB && fcb, SCB && scb)
            : bind_ts_cb{ std::forward<RCB>(rcb) }, find_ts_cb{ std::forward<FCB>(fcb) },
              store_ts_cb{ std::forward<SCB>(scb) } {
        }

        ~server() =default;

        //-- container management, that you can optionally override, 
        virtual void add_container(
            const std::string & container_name, const std::string & root_dir,
            std::string container_type = std::string{}
        );

        virtual its_db & internal(const std::string & container_name, const std::string & container_query = std::string{});

        /** start the server in background, return the listening port used in case it was set unspecified */
        int start_server() {
            if(get_listening_port()==0) {
                start_async();
                while(is_running()&& get_listening_port()==0) //because dlib do not guarantee that listening port is set
                    std::this_thread::sleep_for(std::chrono::milliseconds(10)); // upon return, so we have to wait until it's done
            } else {
                start_async();
            }
            return get_listening_port();
        }
        //-- expose cache functions

        void add_to_cache(id_vector_t&ids, ts_vector_t& tss) { ts_cache.add(ids,tss);}
        void remove_from_cache(id_vector_t &ids) { ts_cache.remove(ids);}
        cache_stats get_cache_stats() { return ts_cache.get_cache_stats();}
        void clear_cache_stats() { ts_cache.clear_cache_stats();}
        void flush_cache() { return ts_cache.flush();}
        void set_cache_size(std::size_t max_size) { ts_cache.set_capacity(max_size);}
        void set_auto_cache(bool active) { cache_all_reads=active;}
        std::size_t get_cache_size() const {return ts_cache.get_capacity();}

        void set_can_remove(bool can_remove) { this->can_remove = can_remove; }

        ts_info_vector_t do_find_ts(const std::string& search_expression);
        ts_info do_get_ts_info(const std::string & ts_url);

        std::string extract_url(const apoint_ts&ats) const {
            auto rts = ts_as<aref_ts>(ats.ts);
            if(rts)
                return rts->id;
            throw runtime_error("dtss store.extract_url:supplied type must be of type ref_ts");
        }

        void do_cache_update_on_write(const ts_vector_t&tsv,bool overwrite_on_write);

        void do_store_ts(const ts_vector_t & tsv, bool overwrite_on_write, bool cache_on_write);

        void do_merge_store_ts(const ts_vector_t & tsv, bool cache_on_write);
        /** @brief Read the time-series from providers for specified period
        *
        * @param ts_ids identifiers, url form, where shyft://.. is specially filtered
        * @param p the period to read
        * @param use_ts_cached_read allow reading results from already existing cached results
        * @param update_ts_cache when reading, also update the ts-cache with the results
        * @return read ts-vector in the order of the ts_ids
        */
        ts_vector_t do_read(const id_vector_t& ts_ids,utcperiod p,bool use_ts_cached_read,bool update_ts_cache);
        void do_remove_ts(const std::string & ts_url);
        void do_bind_ts(utcperiod bind_period, ts_vector_t& atsv,bool use_ts_cached_read,bool update_ts_cache);
        ts_vector_t do_evaluate_ts_vector(utcperiod bind_period, ts_vector_t& atsv,bool use_ts_cached_read,bool update_ts_cache,utcperiod clip_period);
        ts_vector_t do_evaluate_percentiles(utcperiod bind_period, ts_vector_t& atsv, gta_t const&ta,std::vector<int64_t> const& percentile_spec,bool use_ts_cached_read,bool update_ts_cache);

        // ref. dlib, all connection calls are directed here
        void on_connect(
            std::istream & in,
            std::ostream & out,
            const std::string & foreign_ip,
            const std::string & local_ip,
            unsigned short foreign_port,
            unsigned short local_port,
            dlib::uint64 connection_id
        );
    };
    namespace detail {  // namespace for implementation details not considered public api
    // formerly this was only implemented and used in the dtss.cpp translation unit
    struct utcperiod_hasher {
        std::size_t operator()(const utcperiod&k) const {
            using boost::hash_value;
            using boost::hash_combine;
            std::size_t seed=0;
            hash_combine(seed,hash_value(k.start.count()));
            hash_combine(seed,hash_value(k.end.count()));
            return seed;
        }
    };

    }



} // shyft::dtss

