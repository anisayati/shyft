#include <shyft/dtss/dtss.h>
#include <shyft/time_series/dd/compute_ts_vector.h>

namespace shyft::dtss {
        // NOTE: The following methods may be implemented in-class, remove inline in that case.
    using shyft::time_series::dd::ts_as;
    
    void server::add_container(
            const std::string & container_name, const std::string & root_path,
            std::string container_type 
        ) {
        unique_lock<mutex> sl(c_mx);
        // factory.. CD::create_container(container_name, container_type, root_dir, *this);
        std::string server_container_name;
        if ( container_type.empty() || container_type == "ts_db" ) {
            server_container_name = container_name;
            container[server_container_name] = std::make_unique<ts_db>( root_path );
        } else if ( container_type == "krls" ) {
            server_container_name = std::string{"KRLS_"} + container_name;
            container[server_container_name] = std::make_unique<krls_pred_db>(
                    root_path,
                    [this](const std::string & tsid, utcperiod period, bool use_ts_cached_read, bool update_ts_cache) -> ts_vector_t {
                        id_vector_t id_vec{ tsid };
                        return do_read(id_vec, period, use_ts_cached_read, update_ts_cache);
                    }
                );
        } else {
            throw std::runtime_error{ std::string{"Cannot construct unknown container type: "} + container_type };
        }
    }
    
    
    its_db & server::internal(const std::string & container_name, const std::string & container_query) {
        //return CD::get_container(container_name, container_query, *this);
        decltype(container)::iterator f;
        if ( container_query.empty() || container_query == "ts_db" ) {
            f = container.find(container_name);
            if(f == std::end(container)) {
                f = container.find("");// try to find default container
            }
        } else if ( container_query == "krls" ) {
            f = container.find(std::string{"KRLS_"} + container_name);
        }

        if( f == std::end(container) )
            throw std::runtime_error(std::string("Failed to find shyft container: ")+container_name);
        return *f->second;
    }


    ts_info_vector_t server::do_find_ts(const std::string& search_expression) {
        // 1. filter shyft://<container>/
        auto pattern = extract_shyft_url_container(search_expression);
        if ( pattern.size() > 0 ) {
            // assume it is a shyft url -> look for query flags
            auto queries = extract_shyft_url_query_parameters(search_expression);
            auto container_query_it = queries.find(container_query);
            if ( ! queries.empty() && container_query_it != queries.end() ) {
                auto container_query = container_query_it->second;
                filter_shyft_url_parsed_queries(queries, remove_queries);
                return internal(pattern, container_query).find(extract_shyft_url_path(search_expression,pattern), queries);
            } else {
                filter_shyft_url_parsed_queries(queries, remove_queries);
                return internal(pattern).find(extract_shyft_url_path(search_expression,pattern), queries);
            }
        } else if (find_ts_cb) {
            return find_ts_cb(search_expression);
        } else {
            return ts_info_vector_t();
        }
    }


    ts_info server::do_get_ts_info(const std::string & ts_name) {
        // 1. filter shyft://<container>/
        auto pattern = extract_shyft_url_container(ts_name);
        if ( pattern.size() > 0 ) {
            // assume it is a shyft url -> look for query flags
            auto queries = extract_shyft_url_query_parameters(ts_name);
            auto container_query_it = queries.find(container_query);
            if ( ! queries.empty() && container_query_it != queries.end() ) {
                auto container_query = container_query_it->second;
                 filter_shyft_url_parsed_queries(queries, remove_queries);
                return internal(pattern, container_query).get_ts_info(extract_shyft_url_path(ts_name,pattern), queries);
            } else {
                filter_shyft_url_parsed_queries(queries, remove_queries);
                return internal(pattern).get_ts_info(extract_shyft_url_path(ts_name,pattern), queries);
            }
        } else {
            return ts_info{};
        }
    }


    /** if overwrite on write, then flush the cache prior to writing */
    void server::do_cache_update_on_write(const ts_vector_t&tsv,bool overwrite_on_write) {
        vector<string> ts_ids;ts_ids.reserve(tsv.size());
        ts_vector_t tss;
        for (std::size_t i = 0; i < tsv.size(); ++i) {
            auto rts = ts_as<aref_ts>(tsv[i].ts);
            ts_ids.emplace_back(rts->id);
            tss.emplace_back(apoint_ts(rts->rep));
        }
        ts_cache.add(ts_ids,tss,overwrite_on_write);
    }


    void server::do_store_ts(const ts_vector_t & tsv, bool overwrite_on_write, bool cache_on_write) {
        if(tsv.size()==0) return;
        // 1. filter out all shyft://<container>/<ts-path> elements
        //    and route these to the internal storage controller (threaded)
        //    std::map<std::string, ts_db> shyft_internal;
        //
        std::vector<std::size_t> other;
        other.reserve(tsv.size());
        std::vector<string> subs;
        bool sub_active= sm && sm->is_active();
        if(sub_active)
            subs.reserve(tsv.size());
            
        for( std::size_t i = 0; i < tsv.size(); ++i ) {
            auto rts = ts_as<aref_ts>(tsv[i].ts);
            if ( ! rts )
                throw std::runtime_error("dtss store: require ts with url-references");
            if(sub_active) subs.push_back(rts->id);
            auto c = extract_shyft_url_container(rts->id);
            if( c.size() > 0 ) {
                auto queries = extract_shyft_url_query_parameters(rts->id);
                auto container_query_it = queries.find(container_query);
                if ( ! queries.empty() && container_query_it != queries.end() ) {
                    auto container_query = container_query_it->second;
                    filter_shyft_url_parsed_queries(queries, remove_queries);
                    internal(c, container_query).save(
                        extract_shyft_url_path(rts->id,c),  // path
                        rts->core_ts(),      // ts to save
                        overwrite_on_write,  // should do overwrite instead of merge
                        queries              // query key/values from url
                    );
                } else {
                    filter_shyft_url_parsed_queries(queries, remove_queries);
                    internal(c).save(
                        extract_shyft_url_path(rts->id,c),  // path
                        rts->core_ts(),      // ts to save
                        overwrite_on_write,  // should do overwrite instead of merge
                        queries              // query key/values from url
                    );
                }
                if ( cache_on_write ) { // ok, this ends up in a copy, and lock for each item(can be optimized if many)
                    if(overwrite_on_write)
                        ts_cache.remove(rts->id);//invalidate previous defs. if any
                    ts_cache.add(rts->id, apoint_ts(rts->rep));
                }
            } else {
                other.push_back(i); // keep idx of those we have not saved
            }
        }

        // 2. for all non shyft:// forward those to the
        //    store_ts_cb
        if(store_ts_cb && other.size()) {
            if(other.size()==tsv.size()) { //avoid copy/move if possible
                store_ts_cb(tsv);
                if (cache_on_write) do_cache_update_on_write(tsv,overwrite_on_write);
            } else { // have to do a copy to new vector
                ts_vector_t r;
                for(auto i:other) r.push_back(tsv[i]);
                store_ts_cb(r);
                if (cache_on_write) do_cache_update_on_write(r,overwrite_on_write);
            }
        }
        if(sub_active)
            sm->notify_change(subs);
    }


    void server::do_merge_store_ts(const ts_vector_t& tsv, bool cache_on_write) {
        if ( tsv.size() == 0 )
            return;

        //
        // 0. check & prepare the read time-series in tsv for the specified period of each ts
        //    (we optimize a little bit grouping on common period, and reading in batches with equal periods)
        //
        id_vector_t ts_ids; ts_ids.reserve(tsv.size());
        std::unordered_map<utcperiod, id_vector_t, detail::utcperiod_hasher> read_map;
        std::unordered_map<std::string, apoint_ts> id_map;

        for ( std::size_t i=0; i < tsv.size(); ++i ) {
            auto rts = ts_as<aref_ts>(tsv[i].ts);
            if ( ! rts )
                throw std::runtime_error("dtss store merge: require ts with url-references");
            // sanity check
            if ( id_map.find(rts->id) != end(id_map) )
                throw std::runtime_error("dtss store merge requires distinct set of ids, first duplicate found:" + rts->id);
            id_map[rts->id] = apoint_ts(rts->rep);
            // then just build up map[period] = list of time-series to read
            auto rp = rts->rep->total_period();
            if ( read_map.find(rp) != end(read_map) ) {
                read_map[rp].push_back(rts->id);
            } else {
                read_map[rp] = id_vector_t{ rts->id };
            }
        }

        //
        // 1. do the read-merge for each common period, append to final minimal write list
        //
        ts_vector_t tsv_store; tsv_store.reserve(tsv.size());
        for ( auto rr = read_map.begin(); rr != read_map.end(); ++rr ) {
            auto read_ts = do_read(rr->second, rr->first, false, cache_on_write);
            // read_ts is in the order of the ts-id-list rr->second
            for ( std::size_t i = 0; i < read_ts.size(); ++i ) {
                auto ts_id = rr->second[i];
                read_ts[i].merge_points(id_map[ts_id]);
                tsv_store.push_back(apoint_ts(ts_id, read_ts[i]));
            }
        }

        //
        // 2. finally write the merged result back to whatever store is there
        //
        do_store_ts(tsv_store, false, cache_on_write);

    }

    ts_vector_t server::do_read(const id_vector_t & ts_ids, utcperiod p, bool use_ts_cached_read, bool update_ts_cache) {
        if( ts_ids.size() == 0 )
            return ts_vector_t{};

        // should cache?
        bool cache_read_results = update_ts_cache || cache_all_reads;

        // 0. filter out ts's we can get from cache, given we are allowed to use cache
        std::unordered_map<std::string, apoint_ts> cc;  // cached series
        if( use_ts_cached_read )
            cc = ts_cache.get(ts_ids, p);

        ts_vector_t results(ts_ids.size());
        std::vector<std::size_t> external_idxs;
        if ( cc.size() == ts_ids.size() ) {
            // if we got all ts's from cache -> map in the results
            for( std::size_t i = 0; i < ts_ids.size(); ++i )
                results[i] = cc[ts_ids[i]];
        } else {
            // 1. filter out shyft://
            //    if all shyft: return internal read
            external_idxs.reserve(ts_ids.size()); // only reserve space when needed
            for ( std::size_t i = 0; i < ts_ids.size(); ++i ) {
                if ( cc.find(ts_ids[i]) == cc.end() ) {
                    auto c = extract_shyft_url_container(ts_ids[i]);
                    if ( c.size() > 0 ) {
                        // check for queries in shyft:// url's
                        auto queries = extract_shyft_url_query_parameters(ts_ids[i]);
                        auto container_query_it = queries.find(container_query);
                        if ( ! queries.empty() && container_query_it != queries.end() ) {
                            auto container_query = container_query_it->second;
                            filter_shyft_url_parsed_queries(queries, remove_queries);
                            results[i] = apoint_ts(make_shared<const gpoint_ts>(internal(c, container_query).read(
                                extract_shyft_url_path(ts_ids[i],c), p, queries)));
                        } else {
                            filter_shyft_url_parsed_queries(queries, remove_queries);
                            results[i] = apoint_ts(make_shared<const gpoint_ts>(internal(c).read(
                                extract_shyft_url_path(ts_ids[i],c), p, queries)));
                        }
                        // caching?
                        if ( cache_read_results )
                            ts_cache.add(ts_ids[i], results[i]);
                    } else
                        external_idxs.push_back(i);
                } else {
                    results[i] = cc[ts_ids[i]];
                }
            }
        }

        // 2. if other/more than shyft get all those
        if( external_idxs.size() > 0 ) {
            if( ! bind_ts_cb )
                throw std::runtime_error("dtss: read-request to external ts, without external handler");

            // only externaly handled series => return only external result
            if( external_idxs.size() == ts_ids.size() ) {
                auto rts = bind_ts_cb(ts_ids,p);
                if( cache_read_results )
                    ts_cache.add(ts_ids,rts);

                return rts;
            }

            // collect & handle external references
            vector<string> external_ts_ids; external_ts_ids.reserve(external_idxs.size());
            for( auto i : external_idxs )
                external_ts_ids.push_back(ts_ids[i]);
            auto ext_resolved = bind_ts_cb(external_ts_ids, p);

            // caching?
            if( cache_read_results )
                ts_cache.add(external_ts_ids, ext_resolved);

            // merge external results into output results
            for(std::size_t i=0;i<ext_resolved.size();++i)
                results[external_idxs[i]] = ext_resolved[i];
        }

        return results;
    }

    void server::do_bind_ts(utcperiod bind_period, ts_vector_t& atsv, bool use_ts_cached_read, bool update_ts_cache) {

        using shyft::time_series::dd::ts_bind_info;

        std::unordered_map<std::string, std::vector<ts_bind_info>> ts_bind_map;
        std::vector<std::string> ts_id_list;

        // step 1: bind not yet bound time-series (ts with only symbol, needs to be resolved using bind_cb)
        for ( auto & ats : atsv ) {
            auto ts_refs = ats.find_ts_bind_info();
            for ( const auto & bi : ts_refs ) {
                if ( ts_bind_map.find(bi.reference) == ts_bind_map.end() ) { // maintain unique set
                    ts_id_list.push_back(bi.reference);
                    ts_bind_map[bi.reference] = std::vector<ts_bind_info>();
                }
                ts_bind_map[bi.reference].push_back(bi);
            }
        }

        // step 2: (optional) bind_ts callback should resolve symbol time-series with content
        if ( ts_bind_map.size() > 0 ) {
            auto bts = do_read(ts_id_list, bind_period, use_ts_cached_read, update_ts_cache);
            if ( bts.size() != ts_id_list.size() )
                throw std::runtime_error(std::string{"failed to bind all of "} + std::to_string(bts.size()) + std::string{" ts"});

            for ( std::size_t i = 0; i < ts_id_list.size(); ++i ) {
                for ( auto & bi : ts_bind_map[ts_id_list[i]] )
                    bi.ts.bind(bts[i]);
            }
        }

        // step 3: after the symbolic ts are read and bound, we iterate over the
        //         expression tree and calls .do_bind() so that
        //         the new information is taken into account and the expression tree are
        //         ready for evaluate with everything const so threading is safe.
        for ( auto & ats : atsv )
            ats.do_bind();
    }


    ts_vector_t server::do_evaluate_ts_vector(utcperiod bind_period, ts_vector_t& atsv,bool use_ts_cached_read,bool update_ts_cache,utcperiod clip_period) {
        do_bind_ts(bind_period, atsv, use_ts_cached_read, update_ts_cache);
        if(clip_period.valid())
            return clip_to_period(ts_vector_t{ shyft::time_series::dd::deflate_ts_vector<apoint_ts>(atsv) },clip_period);
        else
            return ts_vector_t{ shyft::time_series::dd::deflate_ts_vector<apoint_ts>(atsv) };
    }


    ts_vector_t server::do_evaluate_percentiles(utcperiod bind_period, ts_vector_t& atsv, gta_t const&ta, std::vector<int64_t> const& percentile_spec,bool use_ts_cached_read,bool update_ts_cache) {
        do_bind_ts(bind_period, atsv,use_ts_cached_read,update_ts_cache);
        std::vector<int64_t> p_spec;for(const auto p:percentile_spec) p_spec.push_back(int(p));// convert
        return percentiles(atsv, ta, p_spec);// we can assume the result is trivial to serialize
    }


    void server::do_remove_ts(const std::string & ts_url) {
        if ( ! can_remove ) {
            throw std::runtime_error("dtss::server: server does not support removing");
        }

        // 1. filter shyft://<container>/
        auto pattern = extract_shyft_url_container(ts_url);
        if ( pattern.size() > 0 ) {
            // assume it is a shyft url -> look for query flags
            auto queries = extract_shyft_url_query_parameters(ts_url);
            auto container_query_it = queries.find(container_query);
            auto shyft_ts_url = extract_shyft_url_path(ts_url, pattern);
            if ( ! queries.empty() && container_query_it != queries.end() ) {
                auto container_query = container_query_it->second;
                filter_shyft_url_parsed_queries(queries, remove_queries);
                internal(pattern, container_query).remove(shyft_ts_url, queries);
            } else {
                filter_shyft_url_parsed_queries(queries, remove_queries);
                internal(pattern).remove(shyft_ts_url, queries);
            }
            ts_cache.remove(shyft_ts_url);// remove it from cache as well!
        } else {
            throw std::runtime_error("dtss::server: server does not allow removing for non shyft-url type data");
        }
    }


    void server::on_connect(
        std::istream & in,
        std::ostream & out,
        const std::string & foreign_ip,
        const std::string & local_ip,
        unsigned short foreign_port,
        unsigned short local_port,
        dlib::uint64 connection_id
    ) {

        using shyft::core::core_iarchive;
        using shyft::core::core_oarchive;
        using shyft::time_series::dd::expression_decompressor;
        using shyft::time_series::dd::compressed_ts_expression;
        try {
            while (in.peek() != EOF) {
                auto msg_type= msg::read_type(in);
                try { // scoping the binary-archive could be ok, since it forces destruction time (considerable) to taken immediately, reduce memory foot-print early
                    //  at the cost of early& fast response. I leave the commented scopes in there for now, and aim for fastest response-time
                    switch (msg_type) { // currently switch, later maybe table[msg_type]=msg_handler
                    case message_type::EVALUATE_TS_VECTOR:
                    case message_type::EVALUATE_TS_VECTOR_CLIP:
                    case message_type::EVALUATE_EXPRESSION:
                    case message_type::EVALUATE_EXPRESSION_CLIP:{
                        utcperiod bind_period;bool use_ts_cached_read,update_ts_cache;utcperiod clip_period;
                        ts_vector_t rtsv;
                        core_iarchive ia(in,core_arch_flags);
                        ia>>bind_period;
                        if(msg_type==message_type::EVALUATE_EXPRESSION || msg_type==message_type::EVALUATE_EXPRESSION_CLIP) {
                            compressed_ts_expression c_expr;
                            ia>>c_expr;
                            rtsv=expression_decompressor::decompress(c_expr);
                        } else {
                            ia>>rtsv;
                        }
                        ia>>use_ts_cached_read>>update_ts_cache;
                        if(msg_type==message_type::EVALUATE_TS_VECTOR_CLIP || msg_type==message_type::EVALUATE_EXPRESSION_CLIP) {
                            ia>>clip_period;
                        }
                        auto result=do_evaluate_ts_vector(bind_period, rtsv,use_ts_cached_read,update_ts_cache,clip_period);//first get result
                        msg::write_type(message_type::EVALUATE_TS_VECTOR,out);// then send
                        core_oarchive oa(out,core_arch_flags);
                        oa<<result;

                    } break;
                    case message_type::EVALUATE_EXPRESSION_PERCENTILES:
                    case message_type::EVALUATE_TS_VECTOR_PERCENTILES: {
                        utcperiod bind_period;bool use_ts_cached_read,update_ts_cache;
                        ts_vector_t rtsv;
                        std::vector<int64_t> percentile_spec;
                        gta_t ta;
                        core_iarchive ia(in,core_arch_flags);
                        ia >> bind_period;
                        if(msg_type==message_type::EVALUATE_EXPRESSION_PERCENTILES) {
                            compressed_ts_expression c_expr;
                            ia>>c_expr;
                            rtsv=expression_decompressor::decompress(c_expr);
                        } else {
                            ia>>rtsv;
                        }

                        ia>>ta>>percentile_spec>>use_ts_cached_read>>update_ts_cache;

                        auto result = do_evaluate_percentiles(bind_period, rtsv,ta,percentile_spec,use_ts_cached_read,update_ts_cache);//{
                        msg::write_type(message_type::EVALUATE_TS_VECTOR_PERCENTILES, out);
                        core_oarchive oa(out,core_arch_flags);
                        oa << result;
                    } break;
                    case message_type::FIND_TS: {
                        std::string search_expression; //{
                        search_expression = msg::read_string(in);// >> search_expression;
                        auto find_result = do_find_ts(search_expression);
                        msg::write_type(message_type::FIND_TS, out);
                        core_oarchive oa(out,core_arch_flags);
                        oa << find_result;
                    } break;
                    case message_type::GET_TS_INFO: {
                        std::string ts_url;
                        ts_url = msg::read_string(in);
                        auto result = do_get_ts_info(ts_url);
                        msg::write_type(message_type::GET_TS_INFO, out);
                        core_oarchive oa(out, core_arch_flags);
                        oa << result;
                    } break;
                    case message_type::STORE_TS: {
                        ts_vector_t rtsv;
                        bool overwrite_on_write{ true };
                        bool cache_on_write{ false };
                        core_iarchive ia(in,core_arch_flags);
                        ia >> rtsv >> overwrite_on_write >> cache_on_write;
                        do_store_ts(rtsv, overwrite_on_write, cache_on_write);
                        msg::write_type(message_type::STORE_TS, out);
                    } break;
                    case message_type::MERGE_STORE_TS: {
                        ts_vector_t rtsv;
                        bool cache_on_write{ false };
                        core_iarchive ia(in,core_arch_flags);
                        ia >> rtsv >> cache_on_write;
                        do_merge_store_ts(rtsv, cache_on_write);
                        msg::write_type(message_type::MERGE_STORE_TS, out);
                    } break;
                    case message_type::REMOVE_TS: {
                        std::string ts_url = msg::read_string(in);
                        do_remove_ts(ts_url);
                        msg::write_type(message_type::REMOVE_TS, out);
                    } break;
                    case message_type::CACHE_FLUSH: {
                        flush_cache();
                        clear_cache_stats();
                        msg::write_type(message_type::CACHE_FLUSH,out);
                    } break;
                    case message_type::CACHE_STATS: {
                        auto cs = get_cache_stats();
                        msg::write_type(message_type::CACHE_STATS,out);
                        core_oarchive oa(out,core_arch_flags);
                        oa<<cs;
                    } break;
                    default:
                        throw std::runtime_error(std::string("Server got unknown message type:") + std::to_string(static_cast<int>(msg_type)));
                    }
                } catch(dlib::socket_error const &) {
                    // silent ignore, later log. It means we got trouble read/write socket, we do not crash on that, just 
                    // continue, as its most likely the connection to remote that went away
                    // either because connection was broken, or client was killed
                }  catch (std::exception const& e) {
                    msg::send_exception(e,out);
                }
            }
        } catch(...) { // when we reach here.. we are going to close down the socket
            // so we just log to std err, then return.
            // the dlib thread, will forcibly close the socket
            // but survive the server it self.
            std::cerr<< "dtss: failed and cleanup connection from '"<<foreign_ip<<"'@"<<foreign_port<<", served at local '"<< local_ip<<"'@"<<local_port<<"\n";
        }
    }

}
