# ensure to have python last, since it could contain ref to another boost version
include_directories(
  ${CMAKE_SOURCE_DIR}/cpp
  ${SHYFT_DEPENDENCIES}/include
  ${python_include} 
  ${python_numpy_include}
)

set(py_stm "stm")
set(cpps 
    stm_api.cpp
    stm_server.cpp
)

add_library(${py_stm} SHARED ${cpps})
    set_target_properties(${py_stm} PROPERTIES
        OUTPUT_NAME ${py_stm} VISIBILITY_INLINES_HIDDEN TRUE
        PREFIX "_" # Python extensions do not use the 'lib' prefix
        INSTALL_RPATH "$ORIGIN/../../lib")
    if(MSVC)
        set_target_properties(${py_stm} PROPERTIES SUFFIX ".pyd") # Python extension use .pyd instead of .dll on Windows
    elseif(APPLE)
        set_target_properties(${py_stm} PROPERTIES SUFFIX ".so")
    endif()
    target_link_libraries(${py_stm} stm_core em_model_core shyft_core ${boost_py_link_libraries})
    if(SHYFT_WITH_SHOP)
        target_link_libraries(${py_stm} stm_shop)
    endif()
install(TARGETS ${py_stm} DESTINATION ${CMAKE_SOURCE_DIR}/shyft/energy_market/stm)

