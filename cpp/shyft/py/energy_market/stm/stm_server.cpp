#include <shyft/py/energy_market/api_utils.h>
#include <shyft/py/scoped_gil.h>
#include <mutex>
#include <csignal>

#include <shyft/energy_market/stm/srv/server.h>
#include <shyft/energy_market/stm/srv/client.h>
#include <shyft/energy_market/stm/srv/server_logger.h>
#include <shyft/web_api/energy_market/request_handler.h>


namespace shyft::energy_market::stm::srv {
    using shyft::py::scoped_gil_release;
    using shyft::py::scoped_gil_aquire;
    using shyft::web_api::energy_market::request_handler;
    namespace py=boost::python;
    
    struct py_client {
        mutex mx; ///< to enforce just one thread active on this client object at a time
        client impl;
        py_client(const std::string& host_port,int timeout_ms):impl{host_port,timeout_ms} {}
        ~py_client() { }

        py_client()=delete;
        py_client(py_client const&) = delete;
        py_client(py_client &&) = delete;
        py_client& operator=(py_client const&o) = delete;

        void close() {
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            impl.close();
        }

        bool create_model(string const& mid) {
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.create_model(mid);
        }

        bool add_model(string const& mid, stm_system_ mdl){
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.add_model(mid, mdl);
        }

        bool remove_model(string const& mid) {
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.remove_model(mid);
        }

        bool clone_model(string const& old_mid, string const& new_mid) {
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.clone_model(old_mid, new_mid);
        }

        string version_info() {
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.version_info();
        }

        bool rename_model(string const& old_mid, string const& new_mid) {
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.rename_model(old_mid, new_mid);
        }

        vector<string> get_model_ids() {
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.get_model_ids();
        }

        map<string, model_info> get_model_infos() {
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.get_model_infos();
        }

        stm_system_ get_model(string const& mid) {
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.get_model(mid);
        }

        #ifdef SHYFT_WITH_SHOP
        bool optimize(const string& mid, const generic_dt& ta, const vector<shop_command>& cmd){
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.optimize(mid, ta, cmd);
        }
        #endif
        
        model_state get_state(const string& mid){
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.get_state(mid);
        }
        
        
        bool evaluate_model(const string& mid, utcperiod bind_period, bool use_ts_cached_read, bool update_ts_cache, utcperiod clip_period) {
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.evaluate_model(mid, bind_period, use_ts_cached_read, update_ts_cache, clip_period);
        }
        
        string get_log(const string& mid){
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.get_log(mid);
        }
        bool fx(const string& mid, const string &fx_arg){
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.fx(mid,fx_arg);
        }

    };

    /** @brief  Server implementation
     *
     *  TODO: consider use server as impl instead of inheritance, and use scoped_gil for all python exposed calls
     */
    struct py_server: server {
        request_handler bg_server;///< this object handle the requests from the web-api
        std::future<int> web_srv; ///< mutex
        py::object py_fx_cb;///< python callback that can be set by the py user
        
        /** we need to handle errors when executing the user python code, 
         */
        void handle_pyerror() {
            // from SO: https://stackoverflow.com/questions/1418015/how-to-get-python-exception-text
            using namespace boost::python;
            using namespace boost;
            std::string msg{"unspecified error"};
            if(PyErr_Occurred()) {
                PyObject *exc,*val,*tb;
                object formatted_list, formatted;
                PyErr_Fetch(&exc,&val,&tb);
                handle<> hexc(exc),hval(allow_null(val)),htb(allow_null(tb));
                object traceback(import("traceback"));
                if (!tb) {
                    object format_exception_only{ traceback.attr("format_exception_only") };
                    formatted_list = format_exception_only(hexc,hval);
                } else {
                    object format_exception{traceback.attr("format_exception")};
                    if (format_exception) {
                        try {
                            formatted_list = format_exception(hexc, hval, htb);
                        } catch (...) { // any error here, and we bail out, no crash please
                            msg = "not able to extract exception info";
                        }
                    } else
                        msg="not able to extract exception info";
                }
                if (formatted_list) {
                    formatted = str("\n").join(formatted_list);
                    msg = extract<std::string>(formatted);
                }
            }
            handle_exception();
            PyErr_Clear();
            throw std::runtime_error(msg);
        }
        
        /** this is where we attempt to fire user specified callback, */
        bool py_do_fx(string const & mid, string const& fx_arg) {
            bool r{false};
            if (py_fx_cb.ptr() != Py_None) {
                scoped_gil_aquire gil;// we need to use the GIL here before trying to call python.
                try {
                    r = boost::python::call<bool>(py_fx_cb.ptr(), mid,fx_arg);
                } catch  (const boost::python::error_already_set&) {
                    handle_pyerror();
                }
            }
            return r;
        }
        
        py_server() {
            if (!PyEval_ThreadsInitialized()) {
                PyEval_InitThreads(); // ensure threads is enabled
            }
            bg_server.srv = this;
            // rig the c++ fx_cb to forward calls to this python-layer
            fx_cb = [=](string mid,string fx_args)->bool { return this->py_do_fx(mid,fx_args); };
        }
        
        py_server(string log_dir): server(log_dir) {
            if (!PyEval_ThreadsInitialized()) {
                PyEval_InitThreads(); // ensure threads is enabled
            }
            bg_server.srv = this;
        }

        void start_web_api(string host_ip, int port, string doc_root, int fg_threads, int bg_threads){
            scoped_gil_release gil;
            if (!web_srv.valid()) {
                web_srv = std::async(std::launch::async,
                    [this, host_ip, port, doc_root, fg_threads, bg_threads]()->int {
                        return shyft::web_api::run_web_server(
                            bg_server,
                            host_ip,
                            static_cast<unsigned short>(port),
                            make_shared<string>(doc_root),
                            fg_threads,
                            bg_threads
                            );
                    }
                );
            }
        }

        void stop_web_api() {
            scoped_gil_release gil;
            if(web_srv.valid()) {
                std::raise(SIGTERM);
                (void) web_srv.get();
            }
        }
    };
}


namespace expose {
    using namespace boost::python;
    namespace py = boost::python;
    using std::string;

    using server = shyft::energy_market::stm::srv::py_server;
    using client = shyft::energy_market::stm::srv::py_client;
    using shyft::energy_market::srv::model_info;
    using shyft::energy_market::stm::srv::model_state;
    using namespace shyft::energy_market::stm::srv;

    void dstm_client_server() {
        py::enum_<model_state>("ModelState",
                doc_intro("Describes the possible state of the STM model")
                )
                .value("IDLE",model_state::idle)
                .value("OPTIMIZING", model_state::optimizing)
                .export_values()
            ;
        
            
        class_<map<string, model_info>>("ModelInfoDict",
            doc_intro("A map from model keys to ModelInfo, containing skeleton information about a model."), 
            init<>(args("self"))
            )
            .def(py::map_indexing_suite<map<string, model_info>>());
            
        auto py_dstm = class_<server, boost::noncopyable>("DStmServer",
            doc_intro("A server object serving distributed, 'live' STM systems.\n"
                "The server contains an DTSS that handles time series for the models stored in the server."
            ),
            init<>(args("self"))
            )
            .def(py::init<string>( args("self","log_dir"), "Constructor that sets user specified directory for log files.")
            )
            .def("start_server", &server::start_server)
            .def("set_listening_port", &server::set_listening_port, args("self","port_no"),
                doc_intro("set the listening port for the service")
                doc_parameters()
                doc_parameter("port_no","int","a valid and available tcp-ip port number to listen on.")
                doc_paramcont("typically it could be 20000 (avoid using official reserved numbers)")
                doc_returns("nothing","None","")
            )
            .def("add_container", &server::add_container, (py::arg("self"), py::arg("container_name"), py::arg("root_dir")),
                 doc_intro("Add a container to the server's DTSS.")
                 doc_parameters()
                 doc_parameter("container_name", "str", "name of container to create.")
                 doc_parameter("root_dir", "str", "Directory where container's time series are stored.")
                 doc_returns("nothing", "None", "")
            )
            .def("get_listening_port",&server::get_listening_port, (py::arg("self")),
                "returns the port number it's listening at for serving incoming request"
            )
            .def("do_get_version_info", &server::do_get_version_info, (py::arg("self")),
                "returns the version number of the StsServer"
            )
            .def("do_create_model", &server::do_create_model, (py::arg("self"), py::arg("mid")),
                doc_intro("Create a new model")
                doc_parameters()
                doc_parameter("mid", "str", "ID of new model")
                doc_returns("mdl", "StmSystem", "Empty model with ID set to 'mid'")
                doc_raises()
                doc_raise("RuntimeError", "If 'mid' already is ID of a model")
            )
            .def("do_add_model", &server::do_add_model, (py::arg("self"), py::arg("mid"), py::arg("mdl")),
                doc_intro("Add model to server")
                doc_parameters()
                doc_parameter("mid", "str", "ID/key to store model as.")
                doc_parameter("mdl", "StmSystem", "STM System to store in key 'mid'")
                doc_returns("success", "bool", "Returns True on success")
                doc_raises()
                doc_raise("RuntimeError", "If 'mid' is already an ID of a model.")
            )
            .def("do_remove_model", &server::do_remove_model, (py::arg("self"), py::arg("mid")),
                doc_intro("Remove model by ID.")
                doc_parameters()
                doc_parameter("mid", "str", "ID of model to remove.")
                doc_returns("success", "bool", "Returns True on success.")
                doc_raises()
                doc_raise("RuntimeError", "If model with ID 'mid' does not exist.")
            )
            .def("do_rename_model", &server::do_rename_model, (py::arg("self"), py::arg("old_mid"), py::arg("new_mid")),
                doc_intro("Rename a model")
                doc_parameters()
                doc_parameter("old_mid", "str", "ID of model to rename")
                doc_parameter("new_mid", "str", "New ID of model")
                doc_returns("success", "bool", "Returns True on success.")
                doc_raises()
                doc_raise("RuntimeError", "'new_mid' already stores a model")
                doc_raise("RuntimeError", "'old_mid' doesn't store a model to rename")
            )
            .def("do_clone_model", &server::do_clone_model, (py::arg("self"), py::arg("old_mid"), py::arg("new_mid")),
                doc_intro("Clone existing model with ID.")
                doc_parameters()
                doc_parameter("old_mid", "str", "ID of model to clone")
                doc_parameter("new_mid", "str", "ID to store cloned model against")
                doc_returns("success", "bool", "Returns True on success")
                doc_raises()
                doc_raise("RuntimeError", "'new_mid' already stores a model")
                doc_raise("RuntimeError", "'old_mid' doesn't store a model to clone")
            )
            .def("do_get_model_ids",&server::do_get_model_ids, (py::arg("self")),
                doc_intro("Get IDs of all models stored")
                doc_returns("id_list", "List[str]", "List of model IDs")
            )
            .def("do_get_model_infos", &server::do_get_model_infos, (py::arg("self")),
                doc_intro("Get model infos of all models stored")
                doc_returns("mi_list", "ModelInfoList", "Dict of (ModelKey, ModelInfo) for each model stored.")
                doc_see_also("shyft.energy_market.core.ModelInfo")
            )
            .def("do_get_model",&server::do_get_model, (py::arg("self"), py::arg("mid")),
                doc_intro("Get a stored model by ID")
                doc_parameters()
                doc_parameter("mid", "str", "ID of model to get")
                doc_returns("mdl", "StmSystem", "Requested model")
                doc_raises()
                doc_raise("RuntimeError", "Uanble to find model with ID 'mid'")
            )
            .def("do_get_state", &server::do_get_state, (py::arg("self"), py::arg("mid")),
                 doc_intro("Get state of stored model by ID")
                 doc_parameters()
                 doc_parameter("mid", "str", "ID of model to get state of")
                 doc_returns("state", "ModelState", "State of requested model")
                 doc_raises()
                 doc_raise("RuntimeError", "Unable to find model with ID 'mid'")
             )
            .def_readwrite("fx",&server::py_fx_cb,
                doc_intro("server-side callable function(lambda) that takes two parameters:")
                doc_intro("mid :  the model id")
                doc_intro("fx_arg: arbitrary string to pass to the server-side function")
                doc_intro("The server-side fx is called when the client (or web-api) invoke the c.fx(mid,fx_arg).")
                doc_intro("The signature of the callback function should be fx_cb(mid:str, fx_arg:str)->bool")
                doc_intro("This feature is simply enabling the users to tailor server-side functionality in python!")
                doc_intro("\nExample\n--------\n")
                doc_intro(
                    "from shyft.energy_market.stm import Server\n\n"
                    "s=Server()\n"
                    "def my_fx(mid:str, fx_arg:str)->bool:\n"
                    "    print(f'invoked with mid={mid} fx_arg={fx_arg}')\n"
                    "  # note we can use captured Server s here!"
                    "    return True\n"
                    "# and then bind the function to the callback\n"
                    "s.fx=my_fxresolve_and_read_ts\n"
                    "s.start_server()\n"
                    ": # later using client from anywhere to invoke the call\n"
                    "fx_result=c.fx('my_model_id', 'my_args')\n"
                )
            )
#ifdef SHYFT_WITH_SHOP
            .def("do_optimize", &server::do_optimize, (py::arg("self"), py::arg("mid"), py::arg("ta"), py::arg("cmd")),
                doc_intro("Run SHOP optimization on a model")
                doc_parameters()
                doc_parameter("mid", "str", "ID of model to run optimization on")
                doc_parameter("ta", "TimeAxis", "Time span to run optimization over")
                doc_parameter("cmd", "List[ShopCommand]", "List of SHOP commands")
                doc_see_also("ShopCommand")
                doc_returns("success", "bool", "Stating whether optimization was started successfully or not.")
            )
#endif
            .def("do_evaluate_model", &server::do_evaluate_model, (py::arg("self"), py::arg("mid"), py::arg("bind_period"), py::arg("use_ts_cached_read")=true, py::arg("update_ts_cache")=false, py::arg("clip_period")=utcperiod{}),
                 doc_intro("Evaluate any unbound time series attributes of a model")
                 doc_parameters()
                 doc_parameter("mid", "str", "ID of model to evaluate")
                 doc_parameter("bind_period", "UtcPeriod", "Period for bind in evaluate.")
                 doc_parameter("use_ts_cached_read", "bool", "allow use of cached results. Use it for immutable data reads!")
                 doc_parameter("update_ts_cache", "bool", "when reading time-series, also update the cache with the data. Use it for immutable data reads!")
                 doc_parameter("clip_period", "UtcPeriod", "Period for clip in evaluate.")
                 doc_see_also("UtcPeriod")
                 doc_returns("bound", "bool", "Returns whether any of the model's attributes had to be bound.")
             )
            .def("start_web_api", &server::start_web_api, (py::arg("self"), py::arg("host_ip"), py::arg("port"),
                py::arg("doc_root"), py::arg("fg_threads")=2, py::arg("bg_threads")=4),
                doc_intro("Start a web API for communicating with server")
                doc_parameters()
                doc_parameter("host_ip", "str", "0.0.0.0 for any interface, 127.0.0.1 for local only, etc.")
                doc_parameter("port", "int", "port number to serve the web API on. Ensure it's available!")
                doc_parameter("doc_root", "str", "directory from which we will serve http/https documents.")
                doc_parameter("fg_threads", "int", "number of web API foreground threads, typical 1-4 depending on load.")
                doc_parameter("bg_threads", "int", "number of long running background thread workers to serve requests etc.")
            )
            .def("stop_web_api", &server::stop_web_api, (py::arg("self")),
                doc_intro("Stops any ongoing web API service")
            )
            .def("close",&server::clear,(py::arg("self")),doc_intro("close and stop serving requests on the hpc binary socket interface"))
        ;
        
        class_<client, boost::noncopyable>("DStmClient",
            doc_intro("Client side for the DStmServer")
            doc_intro("")
            doc_intro("Takes care of message exchange to the remote server, using the supplied parameters.")
            doc_intro("It implements the message protocol of the server, sending message-prefix, ")
            doc_intro("arguments, waiting for the response, deserialize the response ")
            doc_intro("and handle it back to the user.")
            doc_see_also("DStmServer"),
            init<string, int>(args("self", "host_port", "timeout_ms"))
            )
            .def("version_info",&client::version_info, (py::arg("self")),
                doc_intro("Get version number of remote server.")
                doc_returns("version", "str", "Version number")
            )
            .def("create_model",&client::create_model, (py::arg("self"), py::arg("mid")),
                doc_intro("Create an empty model and store it server side.")
                doc_parameters()
                doc_parameter("mid", "str", "ID of new model")
                doc_returns("success", "bool", "Returns true on success")
            )
            .def("add_model", &client::add_model, (py::arg("self"), py::arg("mid"), py::arg("mdl")),
                doc_intro("Add model to server")
                doc_parameters()
                doc_parameter("mid", "str", "ID/key to store model as.")
                doc_parameter("mdl", "StmSystem", "STM System to store in key 'mid'")
                doc_returns("success", "bool", "Returns True on success")
            )
            .def("remove_model", &client::remove_model, (py::arg("self"), py::arg("mid")),
                doc_intro("Remove model by ID.")
                doc_parameters()
                doc_parameter("mid", "str", "ID of model to remove.")
                doc_returns("success", "bool", "Returns True on success.")
            )
            .def("rename_model",&client::rename_model, (py::arg("self"), py::arg("old_mid"), py::arg("new_mid")),
                doc_intro("Rename a model")
                doc_parameters()
                doc_parameter("old_mid", "str", "ID of model to rename")
                doc_parameter("new_mid", "str", "New ID of model")
                doc_returns("success", "bool", "Returns True on success.")
            )
            .def("clone_model", &client::clone_model, (py::arg("self"), py::arg("old_mid"), py::arg("new_mid")),
                doc_intro("Clone existing model with ID.")
                doc_parameters()
                doc_parameter("old_mid", "str", "ID of model to clone")
                doc_parameter("new_mid", "str", "ID to store cloned model against")
                doc_returns("success", "bool", "Returns True on success")
            )
            .def("get_model_ids", &client::get_model_ids, (py::arg("self")),
                doc_intro("Get IDs of all models stored")
                doc_returns("id_list", "List[str]", "List of model IDs")
            )
            .def("get_model_infos", &client::get_model_infos, (py::arg("self")),
                doc_intro("Get model infos of all models stored")
                doc_returns("mi_list", "ModelInfoList", "Dict of (ModelKey, ModelInfo) for each model stored.")
                doc_see_also("shyft.energy_market.core.ModelInfo")
            )
            .def("get_model", &client::get_model, (py::arg("self"), py::arg("mid")),
                doc_intro("Get a stored model by ID")
                doc_parameters()
                doc_parameter("mid", "str", "ID of model to get")
                doc_returns("mdl", "StmSystem", "Requested model")
            )
            .def("get_state", &client::get_state, (py::arg("self"), py::arg("mid")),
                 doc_intro("Get the state of a model by ID")
                 doc_parameters()
                 doc_parameter("mid", "str", "ID of model to get state of")
                 doc_returns("state", "ModelState", "State of requested model")
            )
            .def("close", &client::close, (py::arg("self")),
                doc_intro("Close down connection. It will automatically reopen if needed.")
            )
            #ifdef SHYFT_WITH_SHOP
            .def("optimize", &client::optimize, (py::arg("self"), py::arg("mid"), py::arg("ta"), py::arg("cmd")),
                doc_intro("Run SHOP optimization on a model")
                doc_parameters()
                doc_parameter("mid", "str", "ID of model to run optimization on")
                doc_parameter("ta", "TimeAxis", "Time span to run optimization over")
                doc_parameter("cmd", "List[ShopCommand]", "List of SHOP commands")
                doc_see_also("ShopCommand")
                doc_returns("success", "bool", "Stating whether optimization was started successfully or not.")
            )
            #endif
            .def("evaluate_model", &client::evaluate_model, (py::arg("self"), py::arg("mid"), py::arg("bind_period"), py::arg("use_ts_cached_read")=true, py::arg("update_ts_cache")=false, py::arg("clip_period")=utcperiod{}),
                 doc_intro("Evaluate any unbound time series attributes of a model")
                 doc_parameters()
                 doc_parameter("mid", "str", "ID of model to evaluate")
                 doc_parameter("bind_period", "UtcPeriod", "Period for bind in evaluate.")
                 doc_parameter("use_ts_cached_read", "bool", "allow use of cached results. Use it for immutable data reads!")
                 doc_parameter("update_ts_cache", "bool", "when reading time-series, also update the cache with the data. Use it for immutable data reads!")
                 doc_parameter("clip_period", "UtcPeriod", "Period for clip in evaluate.")
                 doc_see_also("UtcPeriod")
                 doc_returns("bound", "bool", "Returns whether any of the model's attributes had to be bound.")
             )
            .def("get_log", &client::get_log, (py::arg("self"), py::arg("mid")),
                 doc_intro("Get SHOP log for a model")
                 doc_parameters()
                 doc_parameter("mid", "str", "ID of model to get log for")
                 doc_returns("log", "str", "Log as a string")
            )
            .def("fx", &client::fx, (py::arg("self"), py::arg("mid"),py::arg("fx_arg")),
                 doc_intro("Execute the serverside fx, passing supplied arguments")
                 doc_parameters()
                 doc_parameter("mid", "str", "ID of model for the server-side fx")
                 doc_parameter("fx_arg","str","any argument passed to the server-side fx")
                 doc_returns("success", "bool", "true if call successfully done")
            )
            ;

    }
    
    void dstm_server_logging() {
        auto stm_log_levels = class_<dlib::log_level, boost::noncopyable>("LogLevel",
                                                    doc_intro("Logging level for STM logging utility."),
                                                    no_init
                                                   )
            .def(py::init<int, const char*>(args("priority", "name")))
            .def_readonly("priority", &dlib::log_level::priority)
            .add_property("name", +[](dlib::log_level& ll) -> string { return string(ll.name); });
        // DLIB LOGGING LEVELS:
        // The naive approach of setting py::scope().attr("LALL") = dlib::LALL;
        // yields a SystemError with unexpected Exception on the python side. (Possibly due to my misunderstanding of the lifetime of globals)
        // Work around for now is to hard code log levels from http://dlib.net/dlib/logger/logger_kernel_abstract.h.html
        py::scope().attr("LALL") = stm_log_levels(std::numeric_limits<int>::min(), "ALL");
        py::scope().attr("LNONE") = stm_log_levels(std::numeric_limits<int>::max(), "NONE");
        py::scope().attr("LTRACE") = stm_log_levels(-100, "TRACE");
        py::scope().attr("LDEBUG") = stm_log_levels(0, "DEBUG");
        py::scope().attr("LINFO") = stm_log_levels(100, "INFO");
        py::scope().attr("LWARN") = stm_log_levels(200, "WARN");
        py::scope().attr("LERROR") = stm_log_levels(300, "ERROR");
        py::scope().attr("LFATAL") = stm_log_levels(400, "FATAL");
        
        class_<server_log_hook, boost::noncopyable>("LogConfig",
                                                    doc_intro("Class for storing logging configuration"));
        
        
        def("configure_logger", &configure_logger, args("config", "log_level"),
            doc_intro("Configure logger with log level and what file to write to")
            doc_parameters()
            doc_parameter("config", "LogConfig", "Configuration for logger")
            doc_parameter("log_level", "LogLevel", "What log level is the lowest to report. ALL < TRACE < DEBUG < INFO < WARN < ERROR < FATAL")
            doc_returns("nothing", "None", "")
        );
    }
}
