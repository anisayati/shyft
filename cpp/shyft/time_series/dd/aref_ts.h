/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS See file COPYING for more details **/
#pragma once
#include <shyft/time_series/dd/gpoint_ts.h>

namespace shyft::time_series::dd {// dd= dynamic_dispatch version of the time_series library, aiming at python api

struct aref_ts:ipoint_ts {
    using ref_ts_t=shared_ptr<const gpoint_ts>;// shyft::time_series::ref_ts<gts_t> ref_ts_t;
    ref_ts_t rep;
    string id;
    explicit aref_ts(const string& sym_ref):id(sym_ref) {}
    aref_ts() = default; // default for serialization conv
    // implement ipoint_ts contract:
    ts_point_fx point_interpretation() const override {return rep->point_interpretation();}
    void set_point_interpretation(ts_point_fx point_interpretation) override {if(rep) dref(rep).set_point_interpretation(point_interpretation);}
    const gta_t& time_axis() const override {return rep->time_axis();}
    utcperiod total_period() const override {return rep->total_period();}
    size_t index_of(utctime t) const override {return rep->index_of(t);}
    size_t size() const override {return rep->size();}
    utctime time(size_t i) const override {return rep->time(i);};
    double value(size_t i) const override {return rep->value(i);}
    double value_at(utctime t) const override {return rep->value_at(t);}
    vector<double> values() const override {return rep->values();}

    // implement some extra functions to manipulate the points
    void set(size_t i, double x) {dref(rep).set(i,x);}
    void fill(double x) {dref(rep).fill(x);}
    void scale_by(double x) {dref(rep).scale_by(x);}
    bool needs_bind() const override { return rep==nullptr;}
    void do_bind()  override {}
    gts_t& core_ts() {
        if(rep)
            return dref(rep).core_ts();
        throw runtime_error("Attempt to use unbound ref_ts");
    }
    const gts_t& core_ts() const {
        if(rep)
            return rep->core_ts();
        throw runtime_error("Attempt to use unbound ref_ts");
    }
    ipoint_ts_ref evaluate(eval_ctx& ctx, ipoint_ts_ref const& shared_this) const override;
    shared_ptr<ipoint_ts> clone_expr() const override;
    void prepare(eval_ctx&ctx) const override;
    string stringify() const override;
    x_serialize_decl();
};
  
}
x_serialize_export_key(shyft::time_series::dd::aref_ts);
