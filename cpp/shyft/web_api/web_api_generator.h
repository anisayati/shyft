/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/phoenix.hpp>
#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/fusion/include/adapt_adt.hpp>
#include <boost/spirit/include/support_adapt_adt_attributes.hpp>

#include <boost/spirit/include/karma.hpp>
#include <boost/spirit/include/karma_string.hpp>
#include <boost/iterator/iterator_facade.hpp>

#include <string_view>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/ats_vector.h>

#include <shyft/dtss/time_series_info.h>

// -- we need some more stuff into the dd namespace to ease working with spirit::karma:
//
namespace shyft::time_series::dd {

    /** @brief point proxy
     *
     *  To help creating apoint_ts iterator that could allow modifying the
     * underlying time-series.
     *
     */
    struct point_proxy {
        ipoint_ts *ts; ///< not owning,
        size_t i;
        operator point() const {
            return point(ts->time(i),ts->value(i));
        }
        point_proxy& operator=(point const& p) {
            if(ts->time(i)!=p.t)
                throw runtime_error("not supported, change time in an interator of points");
            if(dynamic_cast<gpoint_ts*>(ts))
                dynamic_cast<gpoint_ts*>(ts)->set(i,p.v);
            return *this;
        }
        utctime time() const {return ts->time(i);}
        double value() const {return ts->value(i);}
    };

    struct apoint_ts_c {
        using value_type=point_proxy;
        apoint_ts ats;
        apoint_ts_c(apoint_ts const &ats):ats(ats) {}

        struct iterator
        : public boost::iterator_facade<
                iterator
            , point
            , boost::random_access_traversal_tag
            , point_proxy
            >
        {
            iterator(){}
            iterator(const apoint_ts&a,size_t i=0):ats{a},i{i} {}
            void increment() {++i;}
            void decrement() {--i;}
            void advance(difference_type n) {i=static_cast<size_t>(static_cast<difference_type>(i)+n);}

            bool equal(iterator const& other) const { return ats.ts == other.ats.ts && i==other.i;}
            difference_type distance_to(iterator const& other) const {return static_cast<difference_type>(other.i) - static_cast<difference_type>(i);}

            point_proxy dereference() const { return point_proxy{ const_cast<ipoint_ts*>(ats.ts.get()),i}; } //TODO: consider difference between const and non-const dereference

        private:
            apoint_ts ats;
            size_t i{0};
        };
        using const_iterator= iterator;
        iterator begin() const {return iterator(ats,0u);}
        iterator end() const {return iterator(ats,ats.size());}
    };

//    apoint_ts_c::iterator begin(apoint_ts const&ts) {return apoint_ts_c::iterator{ts};};
//    apoint_ts_c::iterator end(apoint_ts const&ts) {return apoint_ts_c::iterator{ts,ts.size()};};
}

//-- then fusion adapt that needs to go into global namespace
/** @brief point adapter for boost spirit  context
 *
 * members of the tuple are:
 * bool: true if value is finite
 * double: time in si-units, ref. utctime.
 * double: value
 */
BOOST_FUSION_ADAPT_ADT(
    shyft::time_series::point,
    (bool,bool, std::isfinite(obj.v),/**/)
    (double,double,shyft::core::to_seconds(obj.t),/**/)
    (double,double,obj.v,/**/)
)

BOOST_FUSION_ADAPT_ADT(
   shyft::dtss::ts_info,
   (std::string,std::string,obj.name,val)
   (bool,bool,obj.point_fx==shyft::time_series::ts_point_fx::POINT_AVERAGE_VALUE,val)
   (shyft::core::utctime,shyft::core::utctime,obj.created,val)
   (std::string,std::string,obj.olson_tz_id,val)
   (shyft::core::utcperiod,shyft::core::utcperiod,obj.data_period,val)
   (shyft::core::utctime,shyft::core::utctime,obj.created,val)
   (shyft::core::utctime,shyft::core::utctime,obj.modified,val)
)

BOOST_FUSION_ADAPT_ADT(shyft::core::utctime,
    (bool,bool,obj!=shyft::core::no_utctime,obj=shyft::core::no_utctime)
    (double,double,shyft::core::to_seconds(obj),obj=shyft::core::from_seconds(val))
)

BOOST_FUSION_ADAPT_ADT(shyft::core::utcperiod,
    (bool,bool,obj.valid(),obj=shyft::core::utcperiod{})
    (shyft::core::utctime,shyft::core::utctime,obj.start,val)
    (shyft::core::utctime,shyft::core::utctime,obj.end,val)
)

/** @brief adapter for apoint_ts, suited for boost.spirit.karma
 *
 * members of the tuple are:
 *  bool: true if the ts is non-empty
 *  bool: true if stair-case start of step (otherwise linear between points)
 *  apoint_ts_container: a boost.spirit.traits enabled container that provide the points
 */
BOOST_FUSION_ADAPT_ADT(
    shyft::time_series::dd::apoint_ts,
    (bool,bool, obj.size()>0,/**/)
    (bool,bool, obj.size() && obj.point_interpretation()==shyft::time_series::ts_point_fx::POINT_AVERAGE_VALUE,/**/)
    (shyft::time_series::dd::apoint_ts_c,shyft::time_series::dd::apoint_ts_c, shyft::time_series::dd::apoint_ts_c(obj),/**/)
)

namespace shyft::web_api::generator {
    namespace qi = boost::spirit::qi;
    namespace ka=boost::spirit::karma;
    namespace phx=boost::phoenix;
    using std::string;
    using qi::double_;
    using shyft::time_series::dd::apoint_ts;
    using shyft::time_series::dd::gta_t;
    using shyft::time_series::ts_point_fx;
    using shyft::time_series::point;
    using shyft::core::to_seconds;
    using shyft::dtss::ts_info;
    using shyft::core::utctime;
    using shyft::core::utcperiod;
    using shyft::time_axis::fixed_dt;
    using shyft::time_axis::calendar_dt;
    using shyft::time_axis::point_dt;
    using shyft::time_axis::generic_dt;


    // ref to: https://www.boost.org/doc/libs/1_69_0/libs/spirit/doc/html/spirit/karma/reference/numeric/real_number.html
    template <typename T>
    struct time_policy : ka::real_policies<T> {
        using base_policy_type=ka::real_policies<T>;
        static unsigned int precision(T) { return 6; }
        static int floatfield(T) {return base_policy_type::fmtflags::fixed;}
    };

    /** @brief grammer for emitting a time,value point
    *
    */
    template<class OutputIterator>
    struct point_generator:ka::grammar<OutputIterator,point()> {

        point_generator():point_generator::base_type(pg) {
            using ka::true_;
            using ka::bool_;
            using ka::omit;
            using ka::real_generator;

            pg =
                 &true_     << ( '[' <<  time_<< ',' << double_ << ']')
                    |          // notice that we emit null here instead of NaN, we can adjust to fit!
                 omit[bool_]<< ( '[' <<  time_<< ',' << "null" << ']')<<omit[double_]

            ;
            pg.name("point");
        }
        ka::rule<OutputIterator,point()> pg;
        ka::real_generator<double, time_policy<double> > time_;
    };

    /** @brief a time-series generator
    *
    * outputs:
    *   {pfx:(true|false),data:[[t,v],..]}
    * or if empty
    *   {pfx:null,data:null}
    */
    template<class OutputIterator>
    struct apoint_ts_generator:ka::grammar<OutputIterator,apoint_ts()> {

        apoint_ts_generator():apoint_ts_generator::base_type(tsg) {
            using ka::true_;
            using ka::false_;
            using ka::bool_;
            using ka::omit;
            using ka::eps;
            using ka::lit;
            pts_ =  '['<< -( pt_ % ',') <<']';
            tsg =
                (&true_ << "{\"pfx\":" << bool_ << ",\"data\":" << pts_ <<'}')
                    |
                ( omit[bool_]<< "{\"pfx\":"<< bool_<< lit(",\"data\":")<<pts_<<'}')//<<omit[pts_])

            ;
            tsg.name("apoint_ts");
            //debug(tsg);
        }
        ka::rule<OutputIterator,apoint_ts()> tsg;
        ka::rule<OutputIterator,shyft::time_series::dd::apoint_ts_c()> pts_;
        point_generator<OutputIterator> pt_;
    };

    /** @brief a time-series vector generator
    *
    * outputs:
    *   {pfx:(true|false),data:[[t,v],..]}
    * or if empty
    *   {pfx:true,data:[]}
    */
    template<class OutputIterator>
    struct atsv_generator:ka::grammar<OutputIterator,std::vector<apoint_ts>()> {

        atsv_generator():atsv_generator::base_type(tsg) {
            using ka::true_;
            using ka::bool_;
            using ka::omit;
            tsg = '[' << -( ats_ % ',') << ']'
            ;
            tsg.name("atsv");
        }
        ka::rule<OutputIterator,std::vector<apoint_ts>()> tsg;
        apoint_ts_generator<OutputIterator> ats_;
    };

    /** @brief grammar for emitting utctime
    *
    * generates double_|null
    */
    template<class OutputIterator>
    struct utctime_generator:ka::grammar<OutputIterator,utctime()> {

        utctime_generator():utctime_generator::base_type(pg) {
            using ka::true_;
            using ka::bool_;
            using ka::omit;
            using ka::real_generator;

            pg =
                  &true_ << time_
                           |
                  omit[bool_] <<"null"<<omit[time_]

            ;
            pg.name("utctime");
        }
        ka::rule<OutputIterator,utctime()> pg;
        ka::real_generator<double, time_policy<double> > time_;
    };

    /** @brief grammar for emitting utcperiod
    *
    * generates [ double_,double_ ] | null
    */
    template<class OutputIterator>
    struct utcperiod_generator:ka::grammar<OutputIterator,utcperiod()> {

        utcperiod_generator():utcperiod_generator::base_type(pg) {
            using ka::true_;
            using ka::bool_;
            using ka::omit;
            using ka::real_generator;

            pg =
                  &true_ << '['<< time_<<','<<time_<<']'
                           |
                  omit[bool_] <<"null"<<omit[time_]<<omit[time_]

            ;
            pg.name("utcperiod");
        }
        ka::rule<OutputIterator,utcperiod()> pg;
        utctime_generator<OutputIterator> time_;
    };

    /** @brief grammar for emitting different types of time axes
     *
     */
    template<class OutputIterator>
    struct fixed_dt_generator: ka::grammar<OutputIterator, fixed_dt()> {

        fixed_dt_generator(): fixed_dt_generator::base_type(pg) {
            using ka::int_;
            pg = ka::lit("{\"t\":") << time_[ka::_1=phx::bind(&fixed_dt::t, ka::_val)]
                << ka::lit(",\"dt\":") << time_[ka::_1=phx::bind(&fixed_dt::dt, ka::_val)]
                << ka::lit(",\"n\":") << int_[ka::_1=phx::bind(&fixed_dt::n, ka::_val)]
                << ka::lit("}");
            pg.name("fixed_dt");
        }
        ka::rule<OutputIterator, fixed_dt()> pg;
        utctime_generator<OutputIterator> time_;
    };

    template<class OutputIterator>
    struct calendar_dt_generator: ka::grammar<OutputIterator, calendar_dt()> {

        calendar_dt_generator(): calendar_dt_generator::base_type(pg) {
            using ka::int_;
            pg = ka::lit("{\"tz\":") << "\"" << ka::string[ka::_1=phx::bind(&calendar_dt::get_tz_name,ka::_val)] << "\""
                << ka::lit(",\"t\":") << time_[ka::_1=phx::bind(&calendar_dt::t, ka::_val)]
                << ka::lit(",\"dt\":") << time_[ka::_1=phx::bind(&calendar_dt::dt, ka::_val)]
                << ka::lit(",\"n\":") << int_[ka::_1=phx::bind(&calendar_dt::n, ka::_val)]
                << ka::lit("}");
            pg.name("calendar_dt");
        }
        ka::rule<OutputIterator, calendar_dt()> pg;
        utctime_generator<OutputIterator> time_;
    };

    template<class OutputIterator>
    struct point_dt_generator: ka::grammar<OutputIterator, point_dt()> {

        point_dt_generator(): point_dt_generator::base_type(pg) {
            pg = ka::lit("{\"t\":[") << (t_ % ",")[ka::_1 = phx::bind(&point_dt::t, ka::_val)] << "]"
                << ka::lit(",\"t_end\":") << t_[ka::_1 = phx::bind(&point_dt::t_end, ka::_val)]
                << ka::lit("}");
            pg.name("point_dt");
        }
        ka::rule<OutputIterator, point_dt()> pg;
        utctime_generator<OutputIterator> t_;

    };

    template<class OutputIterator>
    struct generic_dt_generator: ka::grammar<OutputIterator, generic_dt()> {

        generic_dt_generator(): generic_dt_generator::base_type(pg) {
            using ka::int_;
            pg = ka::lit("{\"gt\":")
                << ( (&int_(0)[ka::_1 = phx::bind(&generic_dt::gt, ka::_val)] << "FIXED,\"f\":" << f_[ka::_1 = phx::bind(&generic_dt::f, ka::_val)])
                | (&int_(1)[ka::_1 = phx::bind(&generic_dt::gt, ka::_val)] << "CALENDAR,\"c\":" << c_[ka::_1 = phx::bind(&generic_dt::c, ka::_val)])
                | (&int_(2)[ka::_1 = phx::bind(&generic_dt::gt, ka::_val)] <<    "POINT,\"p\":" << p_[ka::_1 = phx::bind(&generic_dt::p, ka::_val)]))
                << ka::lit("}");
        }

        ka::rule<OutputIterator, generic_dt()> pg;
        fixed_dt_generator<OutputIterator> f_;
        calendar_dt_generator<OutputIterator> c_;
        point_dt_generator<OutputIterator> p_;
    };

    /** @brief generator for ts_info
    *
    * ts_info provides a minimal set of dtss time-series
    * and is the result-item of the find_ts_request.
    * @see find_ts_request
    */
    template<class OutputIterator>
    struct ts_info_generator:ka::grammar<OutputIterator,ts_info()> {

        ts_info_generator():ts_info_generator::base_type(pg) {
            using ka::true_;
            using ka::bool_;
            using boost::spirit::ascii::string;
            using ka::omit;
            using ka::real_generator;

            pg =
                "{\"name\":\""<<string<<
                "\",\"pfx\":"<<bool_<<
                ",\"delta_t\":"<<time_<<
                ",\"olson_tz_id\":\""<<string<<"\""
                ",\"data_period\":"<<period_<<
                ",\"created\":"<<time_<<
                ",\"modified\":"<<time_<<"}"
            ;
            pg.name("ts_info");
        }
        ka::rule<OutputIterator,ts_info()> pg;
        utctime_generator<OutputIterator> time_;
        utcperiod_generator<OutputIterator> period_;
    };

    /** @brief a ts_info vector generator
    *
    * outputs:[ ts_infos ]
    * @see ts_info_generator
    */
    template<class OutputIterator>
    struct ts_info_vector_generator:ka::grammar<OutputIterator,std::vector<ts_info>()> {

        ts_info_vector_generator():ts_info_vector_generator::base_type(tsig) {
            tsig = '[' << -( tsi_ % ',') << ']'
            ;
            tsig.name("ts_info_vector");
        }
        ka::rule<OutputIterator,std::vector<ts_info>()> tsig;
        ts_info_generator<OutputIterator> tsi_;
    };

}

