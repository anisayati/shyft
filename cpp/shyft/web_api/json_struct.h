/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <map>
#include <iosfwd>
#include <type_traits>

#include <boost/variant/recursive_wrapper.hpp>
#include <boost/variant/recursive_variant.hpp>
#include <boost/variant.hpp>
#include <boost/fusion/include/std_pair.hpp>
#include <boost/optional.hpp>

#include <shyft/time_series/time_axis.h>
#include <shyft/energy_market/srv/model_info.h>
#include <shyft/energy_market/stm/srv/server.h>
#include <shyft/energy_market/stm/attribute_types.h>

namespace shyft::web_api::energy_market {
    using std::vector;
    using std::string;
    using std::map;
    using std::shared_ptr;
    
    using shyft::energy_market::stm::t_turbine_description_;
    using shyft::energy_market::stm::t_xy_;
    using shyft::energy_market::stm::t_xyz_list_;
    using shyft::energy_market::stm::apoint_ts;
    using shyft::core::utctime;
    using shyft::core::utcperiod;
    using shyft::time_axis::generic_dt;
    
    /** @brief The value types of proxy attributes
     * We include string so we also can pass along e.g. exception messages
     */
    typedef boost::variant<
        apoint_ts,
        t_turbine_description_,
        t_xy_,
        t_xyz_list_,
        string,
        int,
        bool,
        generic_dt
        > proxy_attr_range;

    /** @brief visitor for comparing two proxy attributes.
     */
    class proxy_attr_compare: public boost::static_visitor<bool> {
    public:
                
        /** @brief The case of type mismatch */
        template<class LV, class RV>
        bool operator()(LV const&, RV const&) const { return false; }
        
        /** @brief Comparison in the case of apoint_ts */
        bool operator()(apoint_ts const& lhs, apoint_ts const& rhs) const {
            if (lhs.needs_bind() || rhs.needs_bind()) return false;
            return lhs == rhs;
        }

        /** @brief Comparison in the other cases.
         *  That is, both lhs and rhs are shared_ptr's to t-maps, with the same range.
         * 
         *  A somewhat complicated implementation as we want to compare by value.
         */
        template<class V>
        bool operator()(shared_ptr<map<utctime, shared_ptr<V>>> const& lhs, shared_ptr<map<utctime, shared_ptr<V>>> const& rhs) const {
            // Check if both are null:
            if (!(lhs || rhs)) return true;
            // If at least one is null
            if (!(lhs && rhs)) return false;
            // There size should be equal:
            if ( lhs->size() != rhs->size() ) return false;
            // Iterate over each item in the map
            for (auto const& it : *lhs){
                auto key = it.first;
                auto val = it.second;
                auto rit = rhs->find(key);
                if (rit == rhs->end()) return false;
                else if (*(rit->second) != *val) return false;
            }
            return true;
        }

        /** @brief Default comparison. Assumes equal-operator is overloaded
         */
        template<class T>
        bool operator()(T const& lhs, T const & rhs) const {
            return lhs == rhs;
        }
    };
    /** The json struct is a recursive map with keys that are strings and
     * values in a set here given by the boost variant value_type.
     **/
    /** The range of the json maps, aside from being recursive should be finite. **/
    struct json;
    typedef boost::variant<
        int,
        vector<int>,
        string,
        utcperiod,
        generic_dt,
        bool,
        boost::recursive_wrapper<json>,
        vector< json >,
        proxy_attr_range,
        vector<proxy_attr_range>
        > value_type;
    /** the json struct is then simply a map from strings with value_type as range. **/
    struct json {
        map<string, value_type> m;
        
        /** @brief access item in json map. Returns lvalue */
        value_type& operator[](const string& key){
            return m[key];
        }
        
                
        /** @brief Returns item in map. Throws runtime error if provided key is invalid */
        value_type required(const string& key) const{
            auto it = m.find(key);
            if (it != m.end() ){
                return it->second;
            }
            else {
                throw std::runtime_error("Unable to find required key '" + key + "'");
            }
        }
        
        /** @brief access value in map, and throw error if unable to retrieve it.
         *  Also throws error if the value of the required key is not of specified type.
         * 
         * @tparam ExpType: What type you expect to get back. Will throw a runtime error if unable to convert to this type.
         */
        template<class ExpType>
        ExpType required(const string& key) const {
            auto it = m.find(key);
            if (it != m.end()) {
                try {
                    return boost::get<ExpType>(it->second);
                } catch(boost::bad_get e) {
                    throw std::runtime_error("Failed attempt at boost::get with key '" + key + "'");
                }
            }
            else {
                throw std::runtime_error("Unable to find required key '" + key + "'");
            }
        }

        /** @brief access optional value in map.
         * If provided key is not in map, the optional will be empty.
         */
        boost::optional<value_type> optional(const string& key) const noexcept {
            auto it = m.find(key);
			return (it != m.end()) ? boost::optional<value_type>(it->second) : boost::none;
        }
        
        /** @brief access optional value in map
         * and return a boost::optional containing the expected type
         *
         * Returns boost::none if either the key is invalid or value is not of expected type.
         * @tparam ExpType: What type to expect to get back.
         */
        template<class ExpType>
        boost::optional<ExpType> optional(const string& key) const noexcept {
            auto it = m.find(key);
            if (it == m.end()) { // Key is not in map
                return boost::none;
            } else {
                try {
                    return boost::get<ExpType>(it->second);
                } catch (boost::bad_get e) {
                    // Possibly: Instead throw a warning or error?
                    return boost::none;
                }
                
            }
        }

        json() = default;
    };
}
