
#include <shyft/web_api/energy_market/grammar.h>
#include <shyft/web_api/energy_market/generators.h>
#include <shyft/web_api/energy_market/generators/hydro_power.h>
#include <shyft/web_api/generators/proxy_attr.h>
#include <shyft/web_api/generators/json_struct.h>

#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/run_parameters.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/stm/power_station.h>
#include <shyft/energy_market/stm/aggregate.h>
#include <shyft/energy_market/stm/water_route.h>
#include <shyft/energy_market/stm/catchment.h>
#include <shyft/energy_market/stm/attribute_types.h>

#include <shyft/energy_market/stm/srv/dstm_subscription.h>
#include <shyft/energy_market/stm/srv/context.h>

namespace shyft::web_api::energy_market {
    using namespace shyft::web_api::generator;
    using shyft::energy_market::stm::srv::srv_shared_lock;// for read access
    using shyft::energy_market::stm::srv::srv_unique_lock;// for exclusive write access to models
    using shyft::energy_market::srv::model_info;
    using shyft::energy_market::stm::stm_hps;
    using shyft::energy_market::stm::reservoir;
    using shyft::energy_market::stm::power_plant;
    using shyft::energy_market::stm::waterway;
    using shyft::energy_market::stm::unit;
    using shyft::energy_market::stm::catchment;
    using shyft::energy_market::stm::stm_system;
    using shyft::energy_market::stm::run_parameters;
    
    using shyft::energy_market::core::proxy_attr;
    using shyft::energy_market::stm::subscription::proxy_attr_observer;
    using shyft::energy_market::stm::subscription::proxy_attr_observer_;

    using shyft::web_api::grammar::phrase_parser;
    
    using shyft::time_series::dd::aref_ts;
    using shyft::time_series::dd::gpoint_ts;
    using shyft::time_series::dd::abin_op_ts;
    using shyft::time_series::dd::ts_as;
    
    map<string, model_info> request_handler::get_model_infos() { return srv->do_get_model_infos(); }
    
    bg_work_result request_handler::handle_request(const request &req) {
        if (req.keyword == "read_model") {
            return handle_read_model_request(req.request_data);
        } else if (req.keyword == "read_attributes") {
            return handle_read_attribute_request(req.request_data);
        } else if (req.keyword == "get_model_infos") {
            return handle_get_model_infos_request(req.request_data);
        } else if (req.keyword == "get_hydro_components") {
            return handle_get_hydro_components_request(req.request_data);
        } else if (req.keyword == "set_attributes") {
            return handle_set_attribute_request(req.request_data);
        } else if (req.keyword == "unsubscribe") {
            return handle_unsubscribe_request(req.request_data);
        } else if (req.keyword == "fx") {
            return handle_fx_request(req.request_data);
        } else if (req.keyword == "run_params") {
            return handle_run_params_request(req.request_data);
        }
        return bg_work_result("Unknown keyword: " + req.keyword);
    }

    bg_work_result request_handler::do_subscription_work(observer_base_ const&o) {
        // we want subscription on proxy-type expressions
        // so ts_expression_observer_ , url, might look different for the in-memory em server
        // TODO: 
        // we need a map o->request_id -> request_json, (from the original request)
        // and then we can simply do
        //  handle_read_attribute_request(request_json)
        //
        if(o->recalculate()) {
            auto proxy_observer = std::dynamic_pointer_cast<proxy_attr_observer>(o);
            return proxy_observer->re_emit_response();
            // TODO: Basically, we want the correct handler-function to be called
            // based on what type of request asked for the subscription
            //return handle_run_params_request(proxy_observer->request_data);
        } else {
            return bg_work_result{};
        }
    }

    bg_work_result request_handler::do_the_work(const string &input) {
        // 1: Parse into a request
        // 2: Error handling: On success, do a switch on keyword
        // 3: Handle every case, and emit a response string

        bg_work_result b;
        // 1) Parse the request:
        request arequest;
        shyft::web_api::grammar::request_grammar<const char *> request_;
        bool ok_parse = false;
        bg_work_result response;
        try {
            ok_parse = phrase_parser(input.c_str(), request_, arequest);
            if (ok_parse) {
                response = handle_request(arequest);
            } else {
                response = bg_work_result{string("not understood: ") + input};
            }
        } catch (std::runtime_error const &re) {
            response = bg_work_result{string("request_parse:") + re.what()};
        }
        return response;
    }

    shared_ptr<stm_system_context> request_handler::get_system(const string &mid) {
        return srv->do_get_context(mid);
    }

    
    /** @brief visitor class for dispatching how to read attributes based on type
     * 
     */
    struct read_proxy_handler : public boost::static_visitor<proxy_attr_range> {
        read_proxy_handler(const json& data, server *const srv, std::function<bg_work_result(json const&)>&& cb): srv{srv} {
            // Set up for subscription:
            auto osub = boost::get_optional_value_or(data.optional<bool>("subscribe"), false);
            if (osub) {
                json sub_data = data;
                sub_data.m.erase("subscribe");
                subscription = std::make_shared<proxy_attr_observer>(srv, data.required<string>("request_id"), sub_data,
                    std::move(cb));
                subscription->recalculate();//important: since we emit version 0, set initial value of terminals to 0
            }
            
            // Setting up read_type, -period and time_axis;
            read_type = boost::get_optional_value_or(data.optional<string>("read_type"), "read");
            if (read_type == "percentiles")
                percentiles = data.required<vector<int>>("percentiles");
            ta = data.required<generic_dt>("time_axis");
            read_period = boost::get_optional_value_or(data.optional<utcperiod>("read_period"), ta.total_period());
        }
        
        proxy_attr_range operator()(apoint_ts const& ts) {
            apoint_ts nts;
            if (ts.needs_bind()) { // Case 1: The series is unbound:
                if ( !srv || !(srv->dtss) )
                    throw std::runtime_error("Dtss has to be set to read unbound time series.");
                dtss::ts_vector_t tsv;
                tsv.emplace_back(ts.clone_expr());
                tsv = srv->dtss->do_evaluate_ts_vector(read_period, tsv, false, false, read_period);
                nts = tsv[0];
            } else {
                nts = ts;
            }
            return nts.average(ta);
        }
        
        
        template<typename V>
        proxy_attr_range operator()(shared_ptr<map<utctime,V>> const& tv) {
            // We only read the values that are in the read_period:
            auto res = make_shared<map<utctime, V>>();
            for (auto const& kv : *tv) {
                if (read_period.contains(kv.first))
                    res->insert(res->end(),kv);
            }
            return res;
        }
        
        template<typename V>
        proxy_attr_range operator()(V const& v) {
            return v;
            
        }
        
        proxy_attr_observer_ subscription = nullptr;
        string read_type;
        vector<int> percentiles;
        generic_dt ta;
        utcperiod read_period;
        server *const srv;
    };
    
    template<class T, class E, E e>
    void add_proxy_attribute_value(vector<json>& attr_vals, T const &t, vector<int> const& attr_ids, read_proxy_handler& rph) {
        typename T::a_map amap;
        // Check if e is in attr_ids:
        if (find(attr_ids.begin(), attr_ids.end(), (int) e) != attr_ids.end()) {
            auto attr_ptr = amap.ref(std::integral_constant<E, e>{}); // Reference to proxy attribute
            auto attr = t.*attr_ptr;
            json attr_struct;
            attr_struct["attribute_id"] = (int) e;
            if (attr.exists()) {
                proxy_attr_range val = attr.get();
                attr_struct["data"] = boost::apply_visitor(rph, val);
            } else {
                attr_struct["data"] = string("not found");
            }
            if (rph.subscription) {
                rph.subscription->add_subscription(attr);
                // TODO: Have a vector of ts-urls to subscribe to for the given proxy attribute.
                // Update the mapping from dtss-subscriptions -> dstm subscriptions.
            }
            attr_vals.push_back(attr_struct);
        }
    }

    template<class T, int ...es>
    void get_proxy_attributes(vector<json>& attr_vals,
            T const & t, vector<int> const& attr_ids,
            std::integer_sequence<int, es...>,
            read_proxy_handler& rph) {
        using attr_map = typename T::e_attr;
        json comp_struct ;
        comp_struct["component_id"] = (int) t.id;
        comp_struct["component_data"] = vector<json>{};
        auto temporary = {
            (add_proxy_attribute_value<T, attr_map, (attr_map) es>(boost::get<std::vector<json>>(comp_struct["component_data"]), t, attr_ids, rph), 0)...
        };
        attr_vals.push_back(comp_struct);
    }


    /** @brief Reads a list of proxy attributes from a list of T specified by data.
     *
     * @tparam T container type for proxy attributes (reservoir, unit, waterway &c.)
     * @tparam Tc container type of base type (So we can do dynamic casting)
     *      This template parameters is required in this solution due to invariation,
     *      i.e. even if T extends Tc, vector<T> DOES NOT extend vector<Tc>
     * @param vecvals the vector with a series of references to instances of T.
     * @param data: JSON struct specifying which components, and which attributes to retrieve
     *      Requires keys:
     *          @key component_ids: vector<int> of IDs of which containers to retrieve from.
     *          @key attribute_ids: vector<int> of IDs of which attributes to retrieve for each container.
     * @param rph: instance of handler, used in innermost loop to handle the different value types.
     * @return
     */
    template<class T, class Tc>
    vector<json> get_attribute_value_table(vector<shared_ptr<Tc>> const& vecvals, json const& data, read_proxy_handler& rph){
        vector<json> result;
        // Get out component_ids and attribute_ids:
        auto comp_ids = data.required<vector<int>>("component_ids");
        auto attr_ids = data.required<vector<int>>("attribute_ids");

        // Attribute mapping:
        using attr_map = typename T::e_attr;
        using attr_seq = typename T::e_attr_seq_t;

        shared_ptr<T> comp;
        // For each attribute container:
        for (auto cid : comp_ids) {
            // Find container in vector:
            auto it = find_if(vecvals.begin(), vecvals.end(), [&cid](auto el) {
                return el->id == cid;
            });
            if (it == vecvals.end()) {
                json error_struct;
                error_struct["component_id"] = cid;
                error_struct["component_data"] = string("Unable to find component");
                result.push_back(error_struct);
            } else {
                comp = dynamic_pointer_cast<T>(*it);
                get_proxy_attributes(result, *comp,attr_ids, attr_seq{}, rph);
            }
        }
        return result;
    }

    /** @brief generate a response for a read_attributes request */
    bg_work_result request_handler::handle_read_attribute_request(const json &data) {
        // Get model and HPS:
        //TODO: we need to have a shared-lock on the model in order to read the model.
        auto mid = data.required<string>("model_id");
        auto hps_id = data.required<int>("hps_id");
        auto ctx = get_system(mid);
        srv_shared_lock sl(ctx->mtx);// grab shared lock here
        auto mdl=std::const_pointer_cast<const stm_system>(ctx->mdl);// get a const stm_system to ensure we are only reading


        auto it = std::find_if(mdl->hps.begin(), mdl->hps.end(),
                           [&hps_id](auto ihps) { return ihps->id == hps_id; });
        if (it == mdl->hps.end()) {
            throw runtime_error( string("Unable to find HPS ") + std::to_string(hps_id) + string(" in model '") + mid + "'");
        }
        auto hps = *it;
        
        // Prepare response:
        auto req_id = data.required<string>("request_id");
        
        // We use a read_proxy_handler (visitor) to hold subscription and read data,
        // and pass that along to the innermost loop over components and attributes
        read_proxy_handler vis(data, srv,
            [this](json const& data) {
                return handle_read_attribute_request(data);
            }
        );
        
        std::string response = string(R"_({"request_id":")_") + req_id + string(R"_(","result":)_");
        auto sink = std::back_inserter(response);
        //---- EMIT DATA: ----//
        {
            emit_object<decltype(sink)> oo(sink);
            oo.def("model_id", mdl->id)
                .def("hps_id", hps->id);
            // Get reservoir attributes:
            auto comp_attrs = data.optional<json>("reservoirs");
            if (comp_attrs) oo.def("reservoirs",
                get_attribute_value_table<reservoir>(hps->reservoirs, *comp_attrs, vis));
            // Aggregates:
            comp_attrs = data.optional<json>("units");
            if (comp_attrs) oo.def("units", 
                get_attribute_value_table<unit>(hps->units, *comp_attrs, vis));
            // Power plants:
            comp_attrs = data.optional<json>("power_plants");
            if (comp_attrs) oo.def("power_plants",
                get_attribute_value_table<power_plant>(hps->power_plants, *comp_attrs, vis));
            // Waterways:
            comp_attrs = data.optional<json>("waterways");
            if (comp_attrs) oo.def("waterways",
                get_attribute_value_table<waterway>(hps->waterways, *comp_attrs, vis));
            // Catchments:
            comp_attrs = data.optional<json>("catchments");
            if (comp_attrs) oo.def("catchments",
                get_attribute_value_table<catchment>(hps->catchments, *comp_attrs, vis));
        }
        //---- RETURN RESPONSE: ------//
        response += "}";
        if (vis.subscription)
            return bg_work_result{response, std::move(vis.subscription)};
        else
            return bg_work_result{response};
    }
    
    
    /** @brief Visitor class for updating values of the proxy_attr
     * 
     * @tparam ProxyAttr: Proxy attribute type.
     *      requires:
     *          - ::value_type signifying stored value type
     *          - .set(const value_type& v) setter method for stored value.
     */
    template<typename ProxyAttr>
    struct set_proxy_value_visitor : public boost::static_visitor<string> {
        using V = typename ProxyAttr::value_type;
        set_proxy_value_visitor(ProxyAttr* const pa, server* const srv, map<string, vector<string>>*const subs, bool merge=false, const string& mid=""): pa{pa}, merge{merge}, srv{srv}, subs{subs}, mid{mid} {}
        
        /** @brief in the case of different types for lhs and rhs */
        template<typename OV>
        string operator()(const OV& new_val){
            return "type mismatch";
        }
        
        /** @brief in the case of same type and apoint_ts*/
        template<class T = V,
            typename std::enable_if_t<std::is_same<T,apoint_ts>::value, int> = 0 >
        string operator()(const V& new_val) {
            // TODO: How should we resolve if they have different urls?
            if (!pa->exists()) {
                pa->set(new_val);
            } else {
                auto old_val = pa->get();
                if (ts_as<gpoint_ts>(old_val.ts)) { // Case 1: Treated as a local variable
                    if (merge) {
                        old_val.merge_points(new_val);
                        pa->set(old_val);
                    } else
                        pa->set(new_val);
                } else if (ts_as<aref_ts>(old_val.ts)) { // Case 2: Reference to a time series
                    // Case 2.1: The ID starts with shyft:// and so should be stored in the dtss (if, present)
                    if (old_val.id().rfind("shyft://",0) == 0) {
                        // TODO: Should also do a check on the ts-id of new_val:
                        //  What if it's different than the id of pa?
                        //  What if pa doesn't have a shyft url, but new_val does?
                        if (srv && srv->dtss) {
                            dtss::ts_vector_t tsv;
                            tsv.push_back(apoint_ts(old_val.id(), new_val));
                            if (merge) srv->dtss->do_merge_store_ts(tsv, true);
                            else srv->dtss->do_store_ts(tsv, true, true);
                            // NO need to update subs here. They are incremented in the do_store_ts function
                            return "stored to dtss";
                        } else {
                            return "Cannot set dtss time series without dtss.";
                        }
                    } else {
                        if (merge && pa->exists()) {
                            old_val.merge_points(new_val);
                            pa->set(old_val);
                        } else
                            pa->set(new_val);
                    }
                } else { // Case 3: The time series is an expression and should not be set
                    return "Time series is an expression. Cannot be set.";
                }
            }
            (*subs)["time_series"].push_back(pa->url("dstm://M" + mid + "/"));
            return "OK";
        }
        
        /** @brief in the case of same type and not apoint_ts */
        template<class T = V,
                typename std::enable_if_t<!std::is_same<T,apoint_ts>::value, int> = 0 >
        string operator()(const V& new_val) {
            if (merge && pa->exists()) {
                auto old_val = pa->get();
                // We want to overwrite, so insert doesn't do the trick:
                for (auto const& kv : *new_val) {
                    (*old_val)[kv.first] = kv.second;
                }
                pa->set(old_val);
            } else {
                pa->set(new_val);
            }
            (*subs)["other"].push_back(pa->url("dstm://M" + mid + "/"));
            return "OK";
        }
                
        ProxyAttr* const pa;
        server* const srv;
        map<string, vector<string>>* const subs;
        const bool merge;
        string mid;
    };
    
    template<class T, class E, E e>
    void set_proxy_attribute_value(server* const srv,
                                   vector<json>& output,
                                   const vector<proxy_attr_range>& attr_vals,
                                   T &t, 
                                   vector<int> const& attr_ids,
                                   map<string, vector<string>>& subs,
                                   string const& mid="",
                                   bool merge = false
                                  ) {
        typename T::a_map amap;
        // Check if e is in attr_ids:
        auto it = find(attr_ids.begin(), attr_ids.end(), (int) e);
        if (it != attr_ids.end()) {
            // Initialize status struct
            json attr_struct;
            attr_struct["attribute_id"] = (int) e;
            // Get index in attr_ids:
            int ind = std::distance(attr_ids.begin(), it);
            auto attr_ptr = amap.ref(std::integral_constant<E, e>{}); // Reference to proxy attribute
            auto &attr  = t.*attr_ptr; // _ref_ to the proxy attribute we want to change the value of.
            auto pa_exists = attr.exists(); // if not existing, then for sure, we are going to set it
            bool a_equal = false; // a bit verbose here due to search for ms c++ compile problem
            if (pa_exists) {// ok,it's there, but then we check if it's equal (a noop) to avoid propagating a lot of changes for nothing
                proxy_attr_compare pa_equal{};
                proxy_attr_range pa_variant{ attr.get() }; // got it: ms needs this to resolve the next line
                a_equal = boost::apply_visitor(pa_equal, attr_vals[ind],pa_variant);
            }
            
            if (!pa_exists || !a_equal) {
                set_proxy_value_visitor vis(&attr, srv, &subs, merge, mid);
                attr_struct["status"] = boost::apply_visitor(vis, attr_vals[ind]);
            } else
                attr_struct["status"] = std::string("No need to update");
            output.push_back(attr_struct);
        } 
    }
    
    template<class T, int ...es>
    void set_proxy_attributes(
            server* const srv,
            vector<json>& output,
            const vector<proxy_attr_range>& attr_vals, 
            T & t,
            vector<int> const& attr_ids,
            std::integer_sequence<int, es...>,
            map<string, vector<string>>& subs,
            string const& mid="",
            bool merge=false) {
        using attr_map = typename T::e_attr;
        auto temporary = {
            (set_proxy_attribute_value<T, attr_map, (attr_map) es>(srv, output, attr_vals, t, attr_ids, subs, mid, merge), 0)...
        };
    }
    
    
    struct check_empty_visitor : public boost::static_visitor<bool> {
        template<typename T>
        bool operator()(const vector<T>& t) const {
            return t.size() == 0;
        }
        
        template<typename T>
        bool operator()(const T& t) const {
            return true;
        }
    };
    /** @brief Sets a list of proxy attributes from a list of T specified by data.
     *
     * @tparam T container type for proxy attributes (reservoir, unit, waterway &c.)
     * @tparam Tc container type of base type (So we can do dynamic casting)
     *      This template parameters is required in this solution due to invariation,
     *      i.e. even if T extends Tc, vector<T> DOES NOT extend vector<Tc>
     * @param srv ref to server so that we can notify on subscription (server.sm subscription manager)
     * @param vecvals the vector with a series of references to instances of T.
     * @param data: JSON struct specifying which components, and which attributes to retrieve
     *      Requires keys:
     *          @key component_ids: vector<int> of IDs of which containers to retrieve from.
     *          @key attribute_ids: vector<int> of IDs of which attributes to retrieve for each container.
     *          @key values: vector<proxy_attr_range> of values to set for each component and attribute
     *              Must have length equal to |component_ids| * |attribute_ids|.
     * @return
     */
    template<class T, class Tc>
    vector<json> set_attribute_value_table(server* const srv,vector<shared_ptr<Tc>> const& vecvals, json const& data, string const& mid="", bool merge=false){
        vector<json> result;
        // Get out component_ids and attribute_ids:
        auto comp_ids = data.required<vector<int>>("component_ids");
        auto attr_ids = data.required<vector<int>>("attribute_ids");
        auto temp = data.required("values");
        vector<proxy_attr_range> attr_vals;
        if(!boost::apply_visitor(check_empty_visitor(), temp))
            attr_vals = data.required<vector<proxy_attr_range>>("values");
        
        if (attr_vals.size() != comp_ids.size() * attr_ids.size()){
            json error_struct;
            std::stringstream ss;
            ss << "Number of values did not match number of components and attributes. ";
            ss << attr_vals.size() << " =/= " << attr_ids.size() << "*" << comp_ids.size();
            error_struct["message"] = ss.str();
            result.push_back(error_struct);
            return result;
        }
        // Attribute mapping:
        using attr_map = typename T::e_attr;
        using attr_seq = typename T::e_attr_seq_t;
        
        shared_ptr<T> comp;
        // prep subscription identifiers to run notify on:
        map<string, vector<string>> subs;
        subs["time_series"] = {};
        subs["other"] = {};
        bool sub_active= srv->sm && srv->sm->is_active();
        if(sub_active) {
            subs["time_series"].reserve(attr_vals.size());
            subs["other"].reserve(attr_vals.size());
        }

        // For each attribute container:
        int offset = 0;
        vector<proxy_attr_range> compattrs; // Vector to hold one component's attribute values to be set
        for (auto cid : comp_ids) {
            // Find container in vector:
            auto it = find_if(vecvals.begin(), vecvals.end(), [&cid](auto el) {
                return el->id == cid;
            });
            if (it == vecvals.end()) {
                json error_struct;
                error_struct["component_id"] = cid;
                error_struct["status"] = "Unable to find component";
                result.push_back(error_struct);
            } else {
                // Downcast to proper component type:
                comp = dynamic_pointer_cast<T>(*it);
                if (!comp) {
                    json error_struct;
                    error_struct["component_id"] = cid;
                    error_struct["status"] = "Unable to cast hydro component";
                    result.push_back(error_struct);
                }
                else {
                    compattrs = vector<proxy_attr_range>(attr_vals.begin()+offset, attr_vals.begin()+offset+attr_ids.size());
                    json comp_struct;
                    comp_struct["component_id"] = cid;
                    comp_struct["status"] = vector<json>{};
                    set_proxy_attributes(srv, boost::get<vector<json>>(comp_struct["status"]), compattrs, *comp,attr_ids, attr_seq{}, subs, mid, merge);
                    result.push_back(comp_struct);
                    offset += attr_ids.size();
                }
            }
        }
        if(sub_active) { //propagate/notify changes due to write of attributes(e.g. time-series)
            srv->sm->notify_change(subs["other"]);
            srv->dtss->sm->notify_change(subs["time_series"]);// notify the dtss sm about time-series changes
        }

        return result;
    }
        
    bg_work_result request_handler::handle_set_attribute_request(const json &data) {
        // Get model and HPS:
        auto mid = data.required<string>("model_id");
        auto ctx = get_system(mid);
        srv_unique_lock ul(ctx->mtx);// grab unique lock for modification here
        auto mdl=ctx->mdl;//for bw compat code below, and we have a mutable stm_system
        auto hps_id = data.required<int>("hps_id");
        
        auto it = std::find_if(mdl->hps.begin(), mdl->hps.end(),
                           [&hps_id](auto ihps) { return ihps->id == hps_id; });
        if (it == mdl->hps.end()) {
            throw runtime_error( string("Uanble to find HPS ") + std::to_string(hps_id) + string(" in model '") + mid + "'");
        }
        auto hps = *it;

        // Prepare response:
        auto req_id = data.required<string>("request_id");
        std::string response = string(R"_({"request_id":")_") + req_id + string(R"_(","result":)_");
        
        auto merge = boost::get_optional_value_or(data.optional<bool>("merge"), false);
        auto sink = std::back_inserter(response);
        //---- EMIT DATA: ----//
        {
            emit_object<decltype(sink)> oo(sink);
            oo.def("model_id", mdl->id)
                .def("hps_id", hps->id);
            // Get reservoir attributes:
            auto comp_attrs = data.optional<json>("reservoirs");
            if (comp_attrs) oo.def("reservoirs",
                set_attribute_value_table<reservoir>(srv,hps->reservoirs, *comp_attrs, mid, merge));
            // Aggregates:
            comp_attrs = data.optional<json>("units");
            if (comp_attrs) oo.def("units",
                set_attribute_value_table<unit>(srv,hps->units, *comp_attrs, mid, merge));
            // Power plants:
            comp_attrs = data.optional<json>("power_plants");
            if (comp_attrs) oo.def("power_plants",
                set_attribute_value_table<power_plant>(srv,hps->power_plants, *comp_attrs, mid, merge));
            // Waterways:
            comp_attrs = data.optional<json>("waterways");
            if (comp_attrs) oo.def("waterways",
                set_attribute_value_table<waterway>(srv,hps->waterways, *comp_attrs, mid, merge));
            // Catchments:
            comp_attrs = data.optional<json>("catchments");
            if (comp_attrs) oo.def("catchments",
                set_attribute_value_table<catchment>(srv,hps->catchments, *comp_attrs, mid, merge));
        }
        //---- RETURN RESPONSE: ------//
        response += "}";
        return bg_work_result{response};
    }

    template<class OutputIterator>
    void emit_system_hps(OutputIterator &oi, const stm_system &mdl) {
        emit_vector_fx(oi, mdl.hps, [](OutputIterator &oi, auto el) {
            emit_object <OutputIterator> oo(oi);
            oo.def("id", el->id)
                .def("name", el->name);
        });
    }

    bg_work_result request_handler::handle_read_model_request(const json &data) {
        // Get model:
        auto mid = data.required<string>("model_id");
        auto ctx = get_system(mid);
        srv_shared_lock sl(ctx->mtx);// grab a shared lock while fiddling model with output
        auto mdl=std::const_pointer_cast<const stm_system>(ctx->mdl);// get a const stm_system to ensure we are only reading
        // Get request id:
        auto req_id = data.required<string>("request_id");
        std::string response = string("{\"request_id\":\"") + req_id + string("\",\"result\":");
        auto sink = std::back_inserter(response);
        // Emit bare-bones model structure:
		{
			emit_object<decltype(sink)> oo(sink);
			oo.def("id", mdl->id)
				.def("name", mdl->name)
				.def("json", mdl->json)
				.def_fx("hps", [&mdl](decltype(sink) oi) {
				emit_system_hps(oi, *mdl);
					});
		}
        response += "}";
        return bg_work_result{response};
    }

    bg_work_result request_handler::handle_get_model_infos_request(const json &data) {
        // Communicate with server
        auto model_infos = get_model_infos();

        // Prepare response:
        auto req_id = data.required<string>("request_id");
        std::string response = string(R"_({"request_id":")_") + req_id + string(R"_(","result":)_");
        auto sink = std::back_inserter(response);
        emit_vector_fx(sink, model_infos, [](auto oi, auto mi) {
            emit_object<decltype(oi)> oo(oi);
            oo.def("model_key", mi.first)
                .def("id", mi.second.id)
                .def("name", mi.second.name);
        });
        response += "}";
        return bg_work_result{response};
    }


    template<class T, class E, E e>
    void add_if_available_proxy_id(vector<int64_t> &res, T const &t) {
        typename T::a_map amap;
        auto attr_ptr = amap.ref(std::integral_constant<E, e>{}); // Reference to proxy attribute
        auto temp = t.*attr_ptr;
        if (temp.exists()) {
            res.push_back(int64_t(e));
        }
    }

    template<class T, class E, int ... es>
    vector<int64_t> available_proxy_attributes(T const &t, E, std::integer_sequence<int, es...>) {
        vector<int64_t> res{};
        auto temporary = {
            (add_if_available_proxy_id<T, E, (E) es>(res, t), 0)...
        };
        return res;
    }

    template<class T>
    vector<int64_t> get_available_time_series(T const &t) {
        using attr_seq = typename T::e_attr_seq_t;
        return available_proxy_attributes(t, typename T::e_attr(), attr_seq{});
    }

    template<class OutputIterator>
    void emit_power_plant_skeleton(OutputIterator &oi, const power_plant &pp, bool get_data = false) {
        emit_object <OutputIterator> oo(oi);
        oo.def("id", pp.id)
            .def("name", pp.name)
            .def_fx("units", [&pp](auto oi) {
                *oi++ = arr_begin;
                for (auto it = pp.units.begin(); it != pp.units.end(); ++it) {
                    if (it != pp.units.begin()) *oi++ = comma;
                    emit(oi, (*it)->id);
                }
                *oi++ = arr_end;
            });
        if (get_data) oo.def("set_attrs", get_available_time_series(pp));
    }

    template<class OutputIterator>
    void emit_waterway_skeleton(OutputIterator &oi, const waterway &wr, bool get_data = false) {
        emit_object <OutputIterator> oo(oi);
        oo.def("id", wr.id)
            .def("name", wr.name)
            .def("upstreams", wr.upstreams)
            .def("downstreams", wr.downstreams);
        if (get_data) oo.def("set_attrs", get_available_time_series(wr));
    }


    template<class OutputIterator>
    void emit_hps_reservoirs(OutputIterator &oi, const stm_hps &hps, bool get_data = false) {
        emit_vector_fx(oi, hps.reservoirs, [&get_data](auto oi, auto res_) {
            emit_object <OutputIterator> oo(oi);
            oo.def("id", res_->id)
                .def("name", res_->name);
            if (get_data) {
                oo.def("set_attrs", get_available_time_series(*dynamic_pointer_cast<reservoir>(res_)));
            }
        });
    }

    template<class OutputIterator>
    void emit_hps_units(OutputIterator &oi, const stm_hps &hps, bool get_data = false) {
        emit_vector_fx(oi, hps.units, [&get_data](auto oi, auto unit_) {
            emit_object <OutputIterator> oo(oi);
            oo.def("id", unit_->id)
                .def("name", unit_->name);
            if (get_data) {
                oo.def("set_attrs", get_available_time_series(*dynamic_pointer_cast<unit>(unit_)));
            }
        });
    }

    template<class OutputIterator>
    void emit_hps_power_plants(OutputIterator &oi, const stm_hps &hps, bool get_data = false) {
        emit_vector_fx(oi, hps.power_plants, [&get_data](auto oi, auto pp_) {
            emit_power_plant_skeleton(oi, *dynamic_pointer_cast<power_plant>(pp_), get_data);
        });
    }

    template<class OutputIterator>
    void emit_hps_waterways(OutputIterator &oi, const stm_hps &hps, bool get_data = false) {
        emit_vector_fx(oi, hps.waterways, [&get_data](auto oi, auto wr_) {
            emit_waterway_skeleton(oi, *dynamic_pointer_cast<waterway>(wr_), get_data);
        });
    }

    bg_work_result request_handler::handle_get_hydro_components_request(const json &data) {
        // Get model ID and HPS ID:
        auto mid = data.required<string>("model_id");
        auto hps_id = data.required<int>("hps_id");

        // Find if whether to find available data as well:
        auto avail_data_kwarg = data.optional("available_data");
        bool avail_data = false;
        if (avail_data_kwarg) {
            avail_data = boost::get<bool>(*avail_data_kwarg);
        }
        // Get model and search for requested HPS in model:
        auto ctx = get_system(mid);
        srv_shared_lock sl(ctx->mtx);// shared-lock because we are reading.
        auto mdl=std::const_pointer_cast<const stm_system>(ctx->mdl);// get a const stm_system to ensure we are only reading
        auto it = std::find_if(mdl->hps.begin(), mdl->hps.end(),
                               [&hps_id](auto ihps) { return ihps->id == hps_id; });
        if (it == mdl->hps.end()) {
            throw runtime_error( string("Uanble to find HPS ") + std::to_string(hps_id) + string(" in model '") + mid + "'");
        }
        auto hps = *it;

        // Prepare response:
        auto req_id = data.required<string>("request_id");
        std::string response = string(R"_({"request_id":")_") + req_id + string(R"_(","result":)_");
        auto sink = std::back_inserter(response);
        //---- EMIT DATA: ----//
        { // Has to be in scope so destructor is called appropriately.
            emit_object<decltype(sink)> oo(sink);
            oo.def("model_id", mdl->id)
                .def("hps_id", hps->id)
                .def_fx("reservoirs", [&hps, &avail_data](auto oi) {
                    emit_hps_reservoirs(oi, *hps, avail_data);
                })
                .def_fx("units", [&hps, &avail_data](auto oi) {
                    emit_hps_units(oi, *hps, avail_data);
                })
                .def_fx("power_plants", [&hps, &avail_data](auto oi) {
                    emit_hps_power_plants(oi, *hps, avail_data);
                })
                .def_fx("waterways", [&hps, &avail_data](auto oi) {
                    emit_hps_waterways(oi, *hps, avail_data);
                });
        }
        //---- RETURN RESPONSE: ------//
        response += "}";
        return bg_work_result{response};
    }
    
    bg_work_result request_handler::handle_unsubscribe_request(const json& data) {
        auto req_id = data.required<string>("request_id");
        auto unsub_id = data.required<string>("subscription_id");
        std::string response = "";
        auto sink = std::back_inserter(response);
        {
            emit_object<decltype(sink)> oo(sink);
            oo.def("request_id", req_id)
                .def("subscription_id", unsub_id)
                .def("diagnostics", string{});
        }
        
        return bg_work_result{response, unsub_id};
    }
    /** @brief handle fx(mid,fx_arg) request
     *
     * In the first approach, just call the server callback, 
     * doing no claim to the model/nor server.
     * Assume the callback will do proper claim on shared resources
     * when needed.
     */
    bg_work_result request_handler::handle_fx_request(const json& data) {
        auto req_id = data.required<string>("request_id");
        auto model_id = data.required<string>("model_id");
        auto fx_arg = data.required<string>("fx_arg");
        auto success= srv->fx_cb ? srv->fx_cb(model_id,fx_arg):false;
        
        std::string response = "";
        auto sink = std::back_inserter(response);
        {
            emit_object<decltype(sink)> oo(sink);
            oo.def("request_id", req_id)
                .def("diagnostics", string{(success? "":"Failed")});
        }        
        return bg_work_result{response};
    }

    template<int...es>
    vector<json> get_system_run_parameters(run_parameters const& rp,
            vector<int> const& attr_ids,
            std::integer_sequence<int, es...>,
            read_proxy_handler& rph) {
        using attr_map = run_parameters::e_attr;
        vector<json> result;
        auto temporary = {
            (add_proxy_attribute_value<run_parameters, attr_map, (attr_map) es>(result, rp, attr_ids, rph), 0)...
        };
        return result;
    }
    /** @brief handle run_params request
     *
     */
    bg_work_result request_handler::handle_run_params_request(const json& data) {
        auto req_id = data.required<string>("request_id");
        auto model_id = data.required<string>("model_id");
        auto ctx = get_system(model_id);
        srv_shared_lock sl(ctx->mtx);
        // For now, we take all attributes:
        vector<int> attr_ids((int) run_parameters::e_attr::size);
        std::iota(attr_ids.begin(), attr_ids.end(), 0);

        // Generate result:
        json ndata = data;
        ndata["time_axis"] = generic_dt(); // Time_axis is required
        read_proxy_handler vis(ndata, srv, [this](json const& data) { return handle_run_params_request(data); });
        json result;
        result["model_key"] = model_id;
        result["values"] = get_system_run_parameters(*(ctx->mdl->run_params), attr_ids, run_parameters::e_attr_seq_t{}, vis);
        // Generate response:
        std::string response = "";
        auto sink = std::back_inserter(response);
        {
            emit_object<decltype(sink)> oo(sink);
            oo.def("request_id", req_id)
                .def("result", result);
        }
        if (vis.subscription)
            return bg_work_result{response, std::move(vis.subscription)};
        else
            return bg_work_result{response};
    };
}
