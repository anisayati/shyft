/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <shyft/web_api/web_api_grammar.h>
#include <shyft/web_api/energy_market/request_handler.h>
#include <shyft/energy_market/hydro_power/xy_point_curve.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <map>
#include <memory>

namespace shyft::web_api::grammar {
    using std::map;
    using std::shared_ptr;
    
    using shyft::web_api::energy_market::value_type;
    using shyft::web_api::energy_market::proxy_attr_range;
    using shyft::web_api::energy_market::json;
    using shyft::web_api::energy_market::request;
    
    using shyft::energy_market::hydro_power::point;
    using shyft::energy_market::hydro_power::xy_point_curve;
    using shyft::energy_market::hydro_power::xy_point_curve_with_z;
    using shyft::energy_market::hydro_power::turbine_efficiency;
    using shyft::energy_market::hydro_power::turbine_description;
    
    using shyft::energy_market::stm::srv::server;
    
    /** Integer List Grammar **/
    template<typename Iterator, typename Skipper=qi::ascii::space_type>
    struct integer_list_grammar : public qi::grammar<Iterator, std::vector<int>(), Skipper> {
        integer_list_grammar();

        qi::rule<Iterator, vector<int>(), Skipper> start;
		qi::rule<Iterator, int(), Skipper> integer_ = int_;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };
    
    /** Grammar for a point: **/
    template<typename Iterator, typename Skipper=qi::ascii::space_type>
    struct xy_point_grammar : public qi::grammar<Iterator, point(), Skipper> {
        xy_point_grammar();
        
        qi::rule<Iterator, point(), Skipper> start;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };

    /** Grammar for a list of points: **/
    template<typename Iterator, typename Skipper=qi::ascii::space_type>
    struct xy_point_list_grammar : public qi::grammar<Iterator, xy_point_curve(), Skipper> {
        xy_point_list_grammar();
        
        qi::rule<Iterator, xy_point_curve(), Skipper> start;
        xy_point_grammar<Iterator, Skipper> point_;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };
    
    /** Grammar for xy_point_curve_with_z **/
    template<typename Iterator, typename Skipper=qi::ascii::space_type>
    struct xyz_curve_grammar : public qi::grammar<Iterator, xy_point_curve_with_z(), Skipper> {
        xyz_curve_grammar();
        
        qi::rule<Iterator, xy_point_curve_with_z(), Skipper> start;
        xy_point_list_grammar<Iterator, Skipper> xy_list_;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };
    
    /** Grammar for a list of xyz curves: **/
    template<typename Iterator, typename Skipper=qi::ascii::space_type>
    struct xyz_list_grammar : public qi::grammar<Iterator, vector<xy_point_curve_with_z>(), Skipper> {
        xyz_list_grammar();
        
        qi::rule<Iterator, vector<xy_point_curve_with_z>(), Skipper> start;
        xyz_curve_grammar<Iterator, Skipper> curve_;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };
    
    /** Grammar for turbine efficiency **/
    template<typename Iterator, typename Skipper=qi::ascii::space_type>
    struct turbine_efficiency_grammar : public qi::grammar<Iterator, turbine_efficiency(), Skipper> {
        turbine_efficiency_grammar();
        
        qi::rule<Iterator, turbine_efficiency(), Skipper> start;
        xyz_curve_grammar<Iterator, Skipper> xyz_curve_;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };
    
    /** Grammar for turbine description **/
    template<typename Iterator, typename Skipper=qi::ascii::space_type>
    struct turbine_description_grammar : public qi::grammar<Iterator, turbine_description(), Skipper> {
        turbine_description_grammar();
        
        qi::rule<Iterator, turbine_description(), Skipper> start;
        turbine_efficiency_grammar<Iterator, Skipper> turbine_efficiency_;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };
    

    /** @brief Template class for parsing into shared_ptr< map<utctime, shared_ptr<T>> >
     * 
     * @tparam Iterator: Input iterator type,
     * @tparam T: Base type of range. That is wrapped in shared_ptr< map< utctime, shared_ptr< T > > >
     * @tparam TGrammar: Parser type for T
     * @tparam Skipper: What to omit in parsing.
     */
    template<typename Iterator, typename T, typename TGrammar, typename Skipper=qi::ascii::space_type>
    struct t_map_grammar : public qi::grammar<Iterator, shared_ptr< map<utctime, shared_ptr<T> > >(), Skipper> {
        using range_type = shared_ptr<T>;
        using map_type = map<utctime, range_type>;
        using value_type = shared_ptr< map_type >;
                
        t_map_grammar();
        
        qi::rule<Iterator, value_type(), Skipper> start;
        TGrammar range_;
        utctime_grammar<Iterator> time_;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };
    
    /** Grammar for proxy_attr_range **/
    template<typename Iterator, typename Skipper=qi::ascii::space_type>
    struct proxy_attr_range_grammar : public qi::grammar<Iterator, proxy_attr_range(), Skipper> {
        proxy_attr_range_grammar();
        
        qi::rule<Iterator, proxy_attr_range(), Skipper> start;
        quoted_string_grammar<Iterator, Skipper> quoted_string_;
        apoint_ts_grammar<Iterator, Skipper> apoint_ts_;
        time_axis_grammar<Iterator, Skipper> time_axis_;
        t_map_grammar<Iterator, xy_point_curve, xy_point_list_grammar<Iterator, Skipper>, Skipper> t_xy_rule;
        t_map_grammar<Iterator, vector<xy_point_curve_with_z>, xyz_list_grammar<Iterator, Skipper>, Skipper> t_xyz_curves_rule;
        t_map_grammar<Iterator, turbine_description, turbine_description_grammar<Iterator, Skipper>, Skipper> t_turbine_description_rule;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };
    
    /** Grammar for list of proxy attrbitues **/
    template<typename Iterator, typename Skipper=qi::ascii::space_type>
    struct proxy_attr_list_grammar : public qi::grammar<Iterator, vector<proxy_attr_range>(), Skipper> {
        proxy_attr_list_grammar();
        
        proxy_attr_range_grammar<Iterator, Skipper> proxy_;
        qi::rule<Iterator, vector<proxy_attr_range>(), Skipper> start;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };

    /** Grammar for JSON struct **/
    template<typename Iterator, typename Skipper=qi::ascii::space_type>
    struct json_grammar : public qi::grammar<Iterator, json(), Skipper> {
        json_grammar();

        qi::rule<Iterator, json(), Skipper> json_;
        qi::rule<Iterator, std::vector<json>(), Skipper> json_list_;
        qi::rule<Iterator, value_type(), Skipper> value_;
        qi::rule<Iterator, std::pair<std::string, value_type>(), Skipper> pair_;
        utcperiod_grammar<Iterator, Skipper> period_;
        time_axis_grammar<Iterator, Skipper> time_axis_;
        quoted_string_grammar<Iterator,Skipper> quoted_string_;
        integer_list_grammar<Iterator, Skipper> integer_list_;
        proxy_attr_range_grammar<Iterator, Skipper> proxy_;
        proxy_attr_list_grammar<Iterator, Skipper> proxy_list_;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };

    template<typename Iterator, typename Skipper=qi::ascii::space_type>
    struct request_grammar : public qi::grammar<Iterator, request(), Skipper> {
        request_grammar();
        qi::rule<Iterator, std::string(), Skipper> keyword_;
        json_grammar<Iterator, Skipper> json_;
        qi::rule<Iterator, request(), Skipper> request_;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };
    
    /** @brief grammar for reading a ts_url of type dstm://model_id/hps_id/component_id/attribute_id
     * and bind to the requested attribute
     */
    template<typename Iterator, typename Skipper=qi::ascii::space_type>
    struct dstm_ts_url_grammar : public qi::grammar<Iterator, shyft::time_series::dd::apoint_ts(), Skipper> {
        dstm_ts_url_grammar(server *const srv);
        
        qi::rule<Iterator, std::string(), Skipper> mid_;
        qi::rule<Iterator, shyft::time_series::dd::apoint_ts(), Skipper> ts_;
        phx::function<error_handler_> const error_handler = error_handler_{};
    private:
        server *const srv;
    };

    using request_iterator_em= const char*;
    using request_skipper_em= qi::ascii::space_type;
    // Basic value type parsers:
    extern template struct integer_list_grammar<request_iterator_em, request_skipper_em>;
    extern template struct xy_point_grammar<request_iterator_em, request_skipper_em>;
    extern template struct xy_point_list_grammar<request_iterator_em, request_skipper_em>;
    extern template struct xyz_curve_grammar<request_iterator_em, request_skipper_em>;
    extern template struct xyz_list_grammar<request_iterator_em, request_skipper_em>;
    extern template struct turbine_efficiency_grammar<request_iterator_em, request_skipper_em>;
    extern template struct turbine_description_grammar<request_iterator_em, request_skipper_em>;
    
    // Parsers for value types of proxy_attributes:
    extern template struct t_map_grammar<request_iterator_em, xy_point_curve, xy_point_list_grammar<request_iterator_em, request_skipper_em>, request_skipper_em>;
    extern template struct t_map_grammar<request_iterator_em, xy_point_curve_with_z, xyz_curve_grammar<request_iterator_em, request_skipper_em>, request_skipper_em>;
    extern template struct t_map_grammar<request_iterator_em, vector<xy_point_curve_with_z>, xyz_list_grammar<request_iterator_em, request_skipper_em>, request_skipper_em>;
    extern template struct t_map_grammar<request_iterator_em, turbine_description, turbine_description_grammar<request_iterator_em, request_skipper_em>, request_skipper_em>;
    extern template struct proxy_attr_range_grammar<request_iterator_em, request_skipper_em>;
    extern template struct proxy_attr_list_grammar<request_iterator_em, request_skipper_em>;
    
    
    using t_xy_grammar = t_map_grammar<request_iterator_em, xy_point_curve, xy_point_list_grammar<request_iterator_em, request_skipper_em>, request_skipper_em>;
    using t_xyz_grammar = t_map_grammar<request_iterator_em, xy_point_curve_with_z, xyz_curve_grammar<request_iterator_em, request_skipper_em>, request_skipper_em>;
    using t_xyz_list_grammar = t_map_grammar<request_iterator_em, vector<xy_point_curve_with_z>, xyz_list_grammar<request_iterator_em, request_skipper_em>, request_skipper_em>;
    using t_turbine_description_grammar = t_map_grammar<request_iterator_em, turbine_description, turbine_description_grammar<request_iterator_em, request_skipper_em>, request_skipper_em>;
    extern template struct proxy_attr_range_grammar<request_iterator_em, request_skipper_em>;
    // Parsers for web_api request and json structure
    extern template struct json_grammar<request_iterator_em, request_skipper_em>;
    extern template struct request_grammar<request_iterator_em, request_skipper_em>;
    extern template struct dstm_ts_url_grammar<request_iterator_em, request_skipper_em>;
}
