#include <shyft/web_api/targetver.h>
#include <boost/asio/buffer.hpp>
#include <shyft/web_api/dtss_web_api.h>
#include <shyft/dtss/dtss.h>
#include <shyft/core/subscription.h>
#include <shyft/dtss/dtss_subscription.h>
#include <shyft/web_api/web_api_grammar.h>
#include <shyft/web_api/web_api_generator.h>


namespace shyft::web_api {
    using shyft::time_series::dd::apoint_ts;
    using namespace shyft::time_series::dd;
    using namespace shyft;
    using generator::atsv_generator;
    using generator::ts_info_vector_generator;
    using boost::spirit::karma::generate;
    using std::vector;
    using namespace shyft::dtss;
    using shyft::core::subscription::observer_base;
    using shyft::web_api::grammar::phrase_parser;
    namespace grammar {
        extern template struct web_request_grammar<const char*>;
    }
    
    /** @brief variant visitor dispatcher for messages
     *
     * So given type of message, generate the response for that message.
     */
    struct message_dispatch {
        using return_type=bg_work_result;
        dtss_server *srv{nullptr};//<< non-null dtss_server pointer.

        
        std::string gen_ok_response(std::string const&req_id, std::string diag) {
            return "{ \"request_id\" : \""+req_id + "\",\"diagnostics\": \""+ diag +"\" }";
        }

        bg_work_result operator()(find_ts_request const&fts) {
            auto r=srv->do_find_ts(fts.find_pattern);
            std::string response=string("{\"request_id\":\"")+fts.request_id+string("\",\"result\":");
            auto  sink=std::back_inserter(response);
            if(!generate(sink,ts_info_vector_generator<decltype(sink)>(),r)) {
                response= "failed to generate response for " +fts.request_id;
            } else {
                response +="}";
            }
            return bg_work_result{response};
        }

        bg_work_result operator()(info_request const&ir) {
            return bg_work_result{gen_ok_response(ir.request_id,"not implemented")};
        }
        std::string gen_tsv_response(std::string const&req_id, ts_vector_t const &r) {
            std::string response=string("{\"request_id\":\"")+req_id+string("\",\"tsv\":");
            auto  sink=std::back_inserter(response);
            if(!generate(sink,atsv_generator<decltype(sink)>(),r)) {
                response= "failed to genereate response for " +req_id;
            } else {
                response +="}";
            }
            return response;
        }

        bg_work_result operator()(read_ts_request const&rts) {
            ts_vector_t rtsv;
            for(auto const& sym_ts:rts.ts_ids)
                rtsv.emplace_back(apoint_ts(sym_ts));
            auto read_fx=[srv=this->srv,read_period=rts.read_period,cache=rts.cache,clip_period=rts.clip_period](ats_vector tsv)->ats_vector {
                return srv->do_evaluate_ts_vector(read_period,tsv,cache,false,clip_period);// use but do not update cache! make update explicit!
            };
            if(rts.subscribe) {
                dtss::subscription::ts_expression_observer_ ts_sub{make_shared<dtss::subscription::ts_expression_observer>(srv->sm,rts.request_id,rtsv, read_fx)};
                return bg_work_result{gen_tsv_response(rts.request_id,ts_sub->recompute_result()),ts_sub};
            } else {
                return bg_work_result{gen_tsv_response(rts.request_id,read_fx(rtsv))};
            }
        }


        bg_work_result operator()(average_ts_request const& rts) {
            ts_vector_t rtsv;
            for(auto const& sym_ts:rts.ts_ids)
                rtsv.emplace_back(apoint_ts(sym_ts));

            auto read_fx=[srv=this->srv,ta=rts.ta,read_period=rts.read_period,cache=rts.cache](ats_vector tsv)->ats_vector {
                auto raw=srv->do_evaluate_ts_vector(read_period,tsv,cache,false,utcperiod{});// use but do not update cache! make update explicit!
                return raw.average(ta);
            };
            if(rts.subscribe) {
                dtss::subscription::ts_expression_observer_ ts_sub{make_shared<dtss::subscription::ts_expression_observer>(srv->sm,rts.request_id,rtsv, read_fx)};
                return bg_work_result{gen_tsv_response(rts.request_id,ts_sub->recompute_result()), ts_sub};
            } else {
                return bg_work_result{gen_tsv_response(rts.request_id,read_fx(rtsv))};
            }
        }

        bg_work_result operator()(percentile_ts_request const& rts) {
            ts_vector_t rtsv;
            for(auto const& sym_ts:rts.ts_ids)
                rtsv.emplace_back(apoint_ts(sym_ts));
            vector<int64_t> p;p.reserve(rts.percentiles.size());for(auto const i:rts.percentiles) p.emplace_back(i);
            auto read_fx=[srv=this->srv,ta=rts.ta, percentiles=p,read_period=rts.read_period,cache=rts.cache](ats_vector tsv)->ats_vector {
                return srv->do_evaluate_percentiles(read_period,tsv,ta,percentiles,cache,false);// use but do not update cache! make update explicit!
            };
            if(rts.subscribe) {
                dtss::subscription::ts_expression_observer_ ts_sub{ make_shared<dtss::subscription::ts_expression_observer>(srv->sm,rts.request_id,rtsv, read_fx) };
                return bg_work_result{gen_tsv_response(rts.request_id,ts_sub->recompute_result()),ts_sub};
            } else {
                return bg_work_result{gen_tsv_response(rts.request_id,read_fx(rtsv))};
            }
        }

        bg_work_result operator()(store_ts_request const& r) {
            if(r.merge_store) {
                srv->do_merge_store_ts(r.tsv,r.cache);
            } else {
                srv->do_store_ts(r.tsv,r.recreate_ts,r.cache);
            }
            return bg_work_result{gen_ok_response(r.request_id,"")};
        }

        bg_work_result operator()(unsubscribe_request const&r) {
            return bg_work_result{gen_ok_response(r.request_id,""),r.subscription_id};
        }

    };


    bg_work_result request_handler::do_the_work(string request) {

        bg_work_result r;// as pr usual, we stash the result here,single return point
        grammar::web_request wr;// variant of any c++ type web-requests that we can work with.
        grammar::web_request_grammar<const char*> web_req_;// the grammar parser 
        message_dispatch msg_dispatch{srv};// the message dispatcher(needs srv to implement actions)
        bool ok_parse=false;
        std::string response;
        try {
           ok_parse=phrase_parser( request.c_str(),web_req_,wr);
            if(ok_parse) {
                r = boost::apply_visitor(msg_dispatch,wr);
            } else {
                r = bg_work_result{"not understood:" + request};
            }
        } catch (std::runtime_error const &re) {
            r = bg_work_result{string("request_parse:")+ re.what()};
        }
        return r;
    }
    bg_work_result request_handler::do_subscription_work(shyft::core::subscription::observer_base_ const& sub_job) {
        if(sub_job->recalculate()) {
            return bg_work_result{message_dispatch{srv}.gen_tsv_response(sub_job->request_id,std::dynamic_pointer_cast<dtss::subscription::ts_expression_observer>(sub_job)->published_view)};
        } else {
            return bg_work_result{};// empty, no real change
        }
    }
}
