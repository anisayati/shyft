#include <shyft/web_api/web_api_grammar.h>
#include <sstream>

namespace shyft::web_api::grammar {

    template <typename Iterator>
    void error_handler_::operator()(qi::info const& what , Iterator err_pos, Iterator last) const {
            std::stringstream ss;
            ss
            << "syntax error! expecting "
            << what // what failed?
            << " here: \""
            << std::string(err_pos, last) // iterators to error-pos, end
            << "\""
            << std::endl
            ;
            throw std::runtime_error(ss.str());
        }


    template
    void error_handler_::operator()<const char*>(qi::info const &what,const char* , const char*) const;

}
