#include "test_pch.h"

#include <shyft/web_api/targetver.h>
#include <memory>

#include <boost/beast/core.hpp>
#include <boost/beast/version.hpp>
#include <boost/beast/websocket.hpp>
#include <boost/asio/connect.hpp>
#include <boost/asio/io_service.hpp>

#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/ats_vector.h>

#include <shyft/web_api/energy_market/request_handler.h>
#include <shyft/web_api/energy_market/generators.h>
#include <shyft/web_api/energy_market/grammar.h>
#include <shyft/web_api/energy_market/generators/hydro_power.h>
#include <shyft/web_api/generators/json_struct.h>
#include <shyft/core/fs_compat.h>
#include "build_stm_system.h"
#include "mocks.h"
#include <test/test_utils.h>
#include <csignal>

#include <iostream>


using std::string;
using std::string_view;
using std::make_shared;
using std::vector;
using shyft::time_series::dd::ats_vector;
using namespace shyft::energy_market::stm;

namespace shyft::energy_market::test {
    using stm::srv::server;

    struct test_server: server{
        shyft::web_api::energy_market::request_handler bg_server;
        std::future<int> web_srv;///< mutex,
        
        //-- to verify fx-callback
        string fx_mid;
        string fx_arg;
        bool fx_handler(string mid,string json_arg) {
            fx_mid=mid;
            fx_arg=json_arg;
            return true;
        }

        explicit test_server() : server() {
            bg_server.srv = this;
            this->fx_cb=[=](string m,string a)->bool {return this->fx_handler(m,a);};            
        }
        
        explicit test_server(const string& root_dir) : server() {
            bg_server.srv = this;
            dtss->add_container("test", root_dir);
        }

        void start_web_api(string host_ip,int port,string doc_root,int fg_threads,int bg_threads) {
            if(!web_srv.valid()) {
                web_srv= std::async(std::launch::async,
                    [this,host_ip,port,doc_root,fg_threads,bg_threads]()->int {
                        return shyft::web_api::run_web_server(
                        bg_server,
                        host_ip,
                        static_cast<unsigned short>(port),
                        make_shared<string>(doc_root),
                        fg_threads,
                        bg_threads);

                    }
                );
            }
        }
        bool web_api_running() const {return web_srv.valid();}
        void stop_web_api() {
            if(web_srv.valid()) {
                std::raise(SIGTERM);
                (void) web_srv.get();
            }
        }
    };

    //-- test client
    using tcp = boost::asio::ip::tcp;               // from <boost/asio/ip/tcp.hpp>
    namespace websocket = boost::beast::websocket;  // from <boost/beast/websocket.hpp>
    using boost::system::error_code;
    // Sends a WebSocket message and prints the response, from examples made by boost.beast/Vinnie Falco
    class session : public std::enable_shared_from_this<session> {
        tcp::resolver resolver_;
        websocket::stream<tcp::socket> ws_;
        boost::beast::multi_buffer buffer_;
        string host_;
        string port_;
        string text_;
        string response_;
        string fail_;
        std::function<string(string const&)> report_response;

        // Report a failure
        void
        fail(error_code ec, char const* what) {
            fail_= string(what) + ": " + ec.message() + "\n";
        }
        #define fail_on_error(ec,diag) if((ec)) return fail((ec),(diag));
    public:
        // Resolver and socket require an io_context
        explicit
        session(boost::asio::io_context& ioc)
            : resolver_(ioc)
            , ws_(ioc)
        {
        }
        string response() const {return response_;}
        string diagnostics() const {return fail_;}
        // Start the asynchronous operation
        template <class Fx>
        void
        run(string_view host, int port, string_view text, Fx&& rep_response) {
            // Save these for later
            host_ = host;
            text_ = text;
            port_ = std::to_string(port);
            report_response=rep_response;
            resolver_.async_resolve(host_,port_,// Look up the domain name
                                    [me=shared_from_this()](error_code ec,tcp::resolver::results_type results) {
                                        me->on_resolve(ec,results);
                                    }
            );
        }

        void
        on_resolve(error_code ec,tcp::resolver::results_type results) {
            fail_on_error(ec, "resolve")
            // Make the connection on the IP address we get from a lookup
            boost::asio::async_connect(ws_.next_layer(),results.begin(),results.end(),
                // for some reason, does not compile: [me=shared_from_this()](error_code ec)->void {me->on_connect(ec);}
                                       std::bind(&session::on_connect,shared_from_this(),std::placeholders::_1)
            );
        }

        void
        on_connect(error_code ec){
            fail_on_error(ec, "connect")

            ws_.async_handshake(host_, "/", // Perform the websocket handshake
                                [me=shared_from_this()](error_code ec) {me->on_handshake(ec);}
            );
        }

        void
        on_handshake(error_code ec) {
            fail_on_error(ec, "handshake")
            if(text_.size()) {
                ws_.async_write( // Send the message
                    boost::asio::buffer(text_),
                    [me=shared_from_this()](error_code ec,size_t bytes_transferred){
                        me->on_write(ec,bytes_transferred);
                    }
                );
            } else { // empty text to send means we are done and should close connection
                ws_.async_close(websocket::close_code::normal,
                                [me=shared_from_this()](error_code ec) {me->on_close(ec);}
                );
            }
        }

        void
        on_write(error_code ec,std::size_t bytes_transferred) {
            boost::ignore_unused(bytes_transferred);
            fail_on_error(ec, "write")

            ws_.async_read(buffer_, // Read a message into our buffer
                           [me=shared_from_this()](error_code ec,size_t bytes_transferred) {
                               me->on_read(ec,bytes_transferred);
                           }
            );
        }

        void
        on_read(error_code ec,std::size_t bytes_transferred) {
            boost::ignore_unused(bytes_transferred);
            fail_on_error(ec, "read")
            response_=boost::beast::buffers_to_string(buffer_.data());
            buffer_.consume(buffer_.size());
            text_= report_response(response_);
            if(text_.size()) { // more messages to send?
                ws_.async_write( // send the message..
                    boost::asio::buffer(text_),
                    [me=shared_from_this()](error_code ec,size_t bytes_transferred){
                        me->on_write(ec,bytes_transferred);
                    }
                );
            } else { // else close it.
                ws_.async_close(websocket::close_code::normal,
                                [me=shared_from_this()](error_code ec) {me->on_close(ec);}
                );
            }
        }

        void
        on_close(error_code ec) {
            fail_on_error(ec,"close")
        }
        #undef fail_on_error
    };

    unsigned short get_free_port() {
        using namespace boost::asio;
        io_service service;
        ip::tcp::acceptor acceptor(service, ip::tcp::endpoint(ip::tcp::v4(), 0));// pass in 0 to get a free port.
        return  acceptor.local_endpoint().port();
    }
    
    /** engine that perform a publish-subscribe against a specified host
     * 
     * Same pattern as used in test for the dtss web_api (in cpp/test/web_api/web_server.cpp)
     */
    class pub_sub_session : public std::enable_shared_from_this<pub_sub_session> {
        tcp::resolver resolver_;
        websocket::stream<tcp::socket> ws_;
        boost::beast::multi_buffer buffer_;
        string host_;
        string port_;
        string fail_;
        test_server* const srv; ///< Hold the server so we can use its dtss.
        int num_waits=0; ///<How many expected releases of subscribed read pattern
        // report a failure
        void fail(error_code ec, char const* what) {
            fail_ = string(what) + ": " + ec.message() + "\n";
        }
        #define fail_on_error(ec, diag) if((ec)) return fail((ec), (diag));
    public:
        // Resolver and socket require an io_context
        explicit pub_sub_session(boost::asio::io_context& ioc, test_server* const srv): resolver_(ioc), ws_(ioc), srv{srv} {}
        vector<string> responses_;
        string diagnostics() const { return fail_; }
        
        // Start the asynchronous operation
        void run(string_view host, int port) {
            // Save these for later
            host_ = host;
            port_ = std::to_string(port);
            resolver_.async_resolve(host_, port_, // Look up the domain name
                                    [me=shared_from_this()](error_code ec, tcp::resolver::results_type results) {
                                        me->on_resolve(ec, results);
                                    }
                      );
        }
        
        void on_resolve(error_code ec, tcp::resolver::results_type results) {
            fail_on_error(ec, "resolve");
            // Make the connection on the IP address we get from a lookup
            boost::asio::async_connect(ws_.next_layer(), results.begin(), results.end(),
                                       std::bind(&pub_sub_session::on_connect, shared_from_this(), std::placeholders::_1)
                         );
        }
        
        void on_connect(error_code ec){
            fail_on_error(ec, "connect");
            ws_.async_handshake(host_, "/", // Perform websocket handshake
                [me=shared_from_this()](error_code ec) {
                    me->send_read_and_subscribe(ec);
                }
            );
        }
        
        void send_read_and_subscribe(error_code ec) {
            fail_on_error(ec, "send_read_and_subscribe");
            ws_.async_write(
                boost::asio::buffer(R"_(
                    read_attributes {"request_id": "subscribe_and_read",
                                     "model_id": "simple",
                                     "hps_id": 1,
                                     "time_axis": {"t0" : 1, "dt": 1,  "n": 4},
                                     "read_period": ["1970-01-01T00:00:00Z", "1970-01-01T03:00:00Z"],
                                     "subscribe": true,
                                     "reservoirs": {
                                        "component_ids": [1],
                                        "attribute_ids": [0, 3, 15, 16]
                                     }
                                }
                )_"),
                [me=shared_from_this()](error_code ec, size_t bytes_transferred){
                    me->start_read(ec, bytes_transferred);
                }
            );
        }
        
        void start_read(error_code ec, size_t bytes_transferred) {
            boost::ignore_unused(bytes_transferred);
            fail_on_error(ec, "start_read");
            ws_.async_read(buffer_,
                        [me=shared_from_this()](error_code ec2, size_t bytes_transferred2){
                            me->on_read(ec2, bytes_transferred2);
                        }
                    );
        }
        
        void on_read(error_code ec, std::size_t bytes_transferred) {
            boost::ignore_unused(bytes_transferred);
            fail_on_error(ec, "read");
            string response = boost::beast::buffers_to_string(buffer_.data());
            responses_.push_back(response);
            buffer_.consume(buffer_.size());
            //std::cout << "Got response: " << response << "\n";
            if(response.find("finale") != string::npos) {
                ws_.async_close(websocket::close_code::normal,
                                [me=shared_from_this()](error_code ec) { me->on_close(ec); }
                    );
            } else {
                if (response.find("subscribe_and_read")!=string::npos) {
                    if (num_waits == 0) { // First time we send an update to the model
                        ++num_waits;
                        ws_.async_write(// Send an update of the attribute
                            boost::asio::buffer(R"_(
                                set_attributes {"request_id": "change_attribute",
                                    "model_id": "simple",
                                    "hps_id": 1,
                                    "merge": true,
                                    "reservoirs": {
                                        "component_ids": [1],
                                        "attribute_ids": [3, 16],
                                        "values": [{
                                            "1970-01-01T00:00:01Z": [(1.0, 3.0), (1.5, 2.5), (2.0, 2.0), (2.5, 1.5), (3.0, 1.0)]
                                        },{
                                            "id"        : "abcd" ,
                                            "pfx"       : true,
                                            "time_axis" : {"time_points" : [ 1, 2, 3, 4, 5 ]},
                                            "values"    : [ 3.0, 3.0, 3.0, 3.0 ]
                                        }]
                                    }
                                })_"),
                                [me=shared_from_this()](error_code, size_t) {
                                    // Nothing to do here
                                }
                        );
                    } else if (num_waits == 1) {// Second time we send an update to the model
                        ++num_waits;
                        ws_.async_write(// We send an update to the reference to a time series stored in dtss which a subscribed attribute depends on
                            boost::asio::buffer(R"_(
                                set_attributes {"request_id": "second_change",
                                    "model_id": "simple",
                                    "hps_id": 1,
                                    "reservoirs": {
                                        "component_ids": [1],
                                        "attribute_ids": [8],
                                        "values": [
                                        {
                                            "id": "abcd",
                                            "pfx": true,
                                            "time_axis": {"time_points": [1, 2, 3, 4, 5]},
                                            "values": [5.0, 5.0, 5.0, 5.0]
                                        }]
                                    }
                                }
                            )_"),
                            [me=shared_from_this()](error_code, size_t) {
                                // Nothing to do here
                            }
                        );
                    } else if (num_waits == 2) { // Third time we send an update to the model. This time using the dtss.
                        ++num_waits;
                        ats_vector tsv;
                        calendar utc;
                        auto t = utc.time(1970, 1, 1);
                        auto dt = shyft::core::deltahours(1);
                        int n = 10;
                        time_axis::fixed_dt ta(t, dt, n);
                        tsv.push_back(apoint_ts("shyft://test/ts4aa", apoint_ts(ta, 0.2, shyft::time_series::POINT_INSTANT_VALUE)));
                        
                        srv->dtss->do_store_ts(tsv, true, false);
                    } else if(num_waits==3) { // change ts-only, ref Ibrahim web-demo-case
                        ++num_waits;
                        ws_.async_write(// Send an update of the attribute
                            boost::asio::buffer(R"_(
                                set_attributes {"request_id": "ts_only_change",
                                    "model_id": "simple",
                                    "hps_id": 1,
                                    "merge": true,
                                    "reservoirs": {
                                        "component_ids": [1],
                                        "attribute_ids": [0],
                                        "values": [{
                                            "id"        : "abcd" ,
                                            "pfx"       : true,
                                            "time_axis" : {"time_points" : [ 1, 2, 3, 4, 5 ]},
                                            "values"    : [ 4.0, 4.0, 4.0, 4.0 ]
                                        }]
                                    }
                                })_"),
                                [me=shared_from_this()](error_code, size_t) {
                                    // Nothing to do here
                                }
                        );
                        
                    } else {
                        ws_.async_write(
                            boost::asio::buffer(R"_(unsubscribe {"request_id":"finale", "subscription_id":"subscribe_and_read"})_"),
                            [me=shared_from_this()](error_code, size_t) {
                                // Nothing to do here
                            }
                        );
                    }
                }
                
                //-- anyway, always continue to read (unless we hit the final request-id sent with the unsubscribe message
                ws_.async_read(buffer_,
                    [me=shared_from_this()](error_code ec, size_t bytes_transferred) {
                        me->on_read(ec, bytes_transferred);
                    }
                );
            }
        }
        
        void on_close(error_code ec) {
            fail_on_error(ec, "close");
        }
        
    #undef fail_on_error
    };

    /** engine that perform a publish-subscribe against a specified host
     *
     * Same pattern as used in test for the dtss web_api (in cpp/test/web_api/web_server.cpp)
     */
    class run_params_session : public std::enable_shared_from_this<run_params_session> {
        tcp::resolver resolver_;
        websocket::stream<tcp::socket> ws_;
        boost::beast::multi_buffer buffer_;
        string host_;
        string port_;
        string fail_;
        test_server* const srv; ///< Hold the server so we can use its dtss.
        int num_waits=0; ///<How many expected releases of subscribed read pattern
        // report a failure
        void fail(error_code ec, char const* what) {
            fail_ = string(what) + ": " + ec.message() + "\n";
        }
        #define fail_on_error(ec, diag) if((ec)) return fail((ec), (diag));
    public:
        // Resolver and socket require an io_context
        explicit run_params_session(boost::asio::io_context& ioc, test_server* const srv): resolver_(ioc), ws_(ioc), srv{srv} {}
        vector<string> responses_;
        string diagnostics() const { return fail_; }

        // Start the asynchronous operation
        void run(string_view host, int port) {
            // Save these for later
            host_ = host;
            port_ = std::to_string(port);
            resolver_.async_resolve(host_, port_, // Look up the domain name
                                    [me=shared_from_this()](error_code ec, tcp::resolver::results_type results) {
                                        me->on_resolve(ec, results);
                                    }
                      );
        }

        void on_resolve(error_code ec, tcp::resolver::results_type results) {
            fail_on_error(ec, "resolve");
            // Make the connection on the IP address we get from a lookup
            boost::asio::async_connect(ws_.next_layer(), results.begin(), results.end(),
                                       std::bind(&run_params_session::on_connect, shared_from_this(), std::placeholders::_1)
                         );
        }

        void on_connect(error_code ec){
            fail_on_error(ec, "connect");
            ws_.async_handshake(host_, "/", // Perform websocket handshake
                [me=shared_from_this()](error_code ec) {
                    me->send_initial(ec);
                }
            );
        }

        void send_initial(error_code ec) {
            fail_on_error(ec, "send_initial");
            ws_.async_write(
                boost::asio::buffer(R"_(run_params {"request_id": "initial", "model_id": "simple", "subscribe": true})_"),
                [me=shared_from_this()](error_code ec, size_t bytes_transferred){
                    me->start_read(ec, bytes_transferred);
                }
            );
        }

        void start_read(error_code ec, size_t bytes_transferred) {
            boost::ignore_unused(bytes_transferred);
            fail_on_error(ec, "start_read");
            ws_.async_read(buffer_,
                        [me=shared_from_this()](error_code ec2, size_t bytes_transferred2){
                            me->on_read(ec2, bytes_transferred2);
                        }
                    );
        }

        void on_read(error_code ec, std::size_t bytes_transferred) {
            boost::ignore_unused(bytes_transferred);
            fail_on_error(ec, "read");
            string response = boost::beast::buffers_to_string(buffer_.data());
            responses_.push_back(response);
            //std::cout << "Got response: " << response << "\n";
            buffer_.consume(buffer_.size());
            if(response.find("finale") != string::npos) {
                ws_.async_close(websocket::close_code::normal,
                                [me=shared_from_this()](error_code ec) { me->on_close(ec); }
                    );
            } else {
                if (response.find("initial")!=string::npos) {
                    if (num_waits == 0) { // First time we update a model. Here via a simple notify change
                        ++num_waits;
                        auto mdl = srv->do_get_model("simple");
                        auto & pa = mdl->run_params->n_inc_runs;
                        pa = 3;
                        srv->sm->notify_change(pa.url("dstm://Msimple/"));
                    } else {
                        ws_.async_write(
                            boost::asio::buffer(R"_(unsubscribe {"request_id":"finale", "subscription_id":"initial"})_"),
                            [me=shared_from_this()](error_code, size_t) {
                                // Nothing to do here
                            }
                        );
                    }
                }

                //-- anyway, always continue to read (unless we hit the final request-id sent with the unsubscribe message
                ws_.async_read(buffer_,
                    [me=shared_from_this()](error_code ec, size_t bytes_transferred) {
                        me->on_read(ec, bytes_transferred);
                    }
                );
            }
        }

        void on_close(error_code ec) {
            fail_on_error(ec, "close");
        }

    #undef fail_on_error
    };
}

using std::to_string;
using shyft::core::utctime;
using shyft::core::utctime_now;
using shyft::core::to_seconds64;



TEST_SUITE("em_web_api") {
    TEST_CASE("read_model") {
        dlib::set_all_logging_levels(dlib::LNONE);
        //-- keep test directory, unique, and with auto-cleanup.
        auto dirname = "em.web_api.test." +to_string(to_seconds64(utctime_now()) );
        test::utils::temp_dir tmpdir(dirname.c_str());
        string doc_root=(tmpdir/"doc_root").string();
        //dir_cleanup wipe{tmpdir}; // note..: ~path raises a SIGSEGV fault when boost is built with different compile flags than shyft.
        // See also: https://stackoverflow.com/questions/19469887/segmentation-fault-with-boostfilesystem
        // However, this directory is required, but never used in this test.

        // Set up test server:
        using namespace shyft::energy_market::test;
        using std::vector;
        test_server a;
        string host_ip{"127.0.0.1"};
        a.set_listening_ip(host_ip);
        a.start_server();
        // Store some models:
        auto mdl = test::create_stm_system();
        a.do_add_model("sorland", mdl);

        auto mdl2 = test::create_simple_system(2,"simple");
        mdl2->run_params->n_inc_runs = 2;
        mdl2->run_params->n_full_runs = 3;
        mdl2->run_params->head_opt = true;
        a.do_add_model("simple", mdl2);

        int port=get_free_port();
        a.start_web_api(host_ip, port, doc_root, 1, 1);

        //We'll also dress the model up with some time-series:
        //utctimespan dt{deltahours(1)};
        //fixed_dt ta{_t(0),dt,6};
        std::this_thread::sleep_for(std::chrono::milliseconds(700));
        REQUIRE_EQ(true,a.web_api_running());
        vector<string> requests{R"_(read_model {"request_id": "1", "model_id": "simple"})_",
                                R"_(read_attributes {"request_id": "2",
                                        "model_id": "simple",
                                        "hps_id": 1,
                                        "time_axis": {"t0":0.0,"dt": 3600,"n": 2},
                                        "reservoirs": {
                                            "component_ids": [1],
                                            "attribute_ids": [16]
                                        },
                                        "units": {
                                            "component_ids": [1],
                                            "attribute_ids": [12]
                                        },
                                        "catchments": {
                                            "component_ids": [12],
                                            "attribute_ids": [3,4,5]
                                        }
                                    })_",
                                R"_(get_model_infos {"request_id": "3"})_",
                                R"_(get_hydro_components {"request_id": "4",
                                        "model_id": "simple",
                                        "hps_id": 1
                                })_",
                                R"_(get_hydro_components {"request_id": "5",
                                        "model_id": "simple",
                                        "hps_id": 1,
                                        "available_data": true
                                })_",
                                R"_(run_params {"request_id" : "6", "model_id" : "simple"})_",
        };
        vector<string> responses;
        size_t r=0;
        {
            boost::asio::io_context ioc;
            auto s1=std::make_shared<session>(ioc);
            //cout<<"Starting client session\n";
            s1->run(host_ip, port, requests[r],//
            [&responses,&r,&requests](string const&web_response)->string{
                    responses.push_back(web_response);
                    ++r;
                    return r >= requests.size()?string(""):requests[r];
                }
            );
            // Run the I/O service. The call will return when
            // the socket is closed.
            ioc.run();
            s1.reset();
            //cout<<"Stopping web_api\n";
        }
        vector<string> expected{R"_({"request_id":"1","result":{"id":2,"name":"simple","json":"","hps":[{"id":1,"name":"simple"}]}})_",
                                R"_({"request_id":"2","result":{"model_id":2,"hps_id":1,"reservoirs":[{"component_data":[{"attribute_id":16,"data":{"pfx":true,"data":[[0.0,1.0],[3600.0,1.0]]}}],"component_id":1}],"units":[{"component_data":[{"attribute_id":12,"data":"not found"}],"component_id":1}],"catchments":[{"component_data":"Unable to find component","component_id":12}]}})_",
                                R"_({"request_id":"3","result":[{"model_key":"simple","id":2,"name":"simple"},{"model_key":"sorland","id":1,"name":"stm_system"}]})_",
                                R"_({"request_id":"4","result":{"model_id":2,"hps_id":1,"reservoirs":[{"id":1,"name":"simple_res"}],"units":[{"id":1,"name":"simple_unit"}],"power_plants":[{"id":2,"name":"simple_pp","units":[1]}],"waterways":[{"id":1,"name":"r->u","upstreams":[{"role":"input","target":"R1"}],"downstreams":[{"role":"main","target":"A1"}]}]}})_",
                                R"_({"request_id":"5","result":{"model_id":2,"hps_id":1,"reservoirs":[{"id":1,"name":"simple_res","set_attrs":[14,16]}],"units":[{"id":1,"name":"simple_unit","set_attrs":[6,13]}],"power_plants":[{"id":2,"name":"simple_pp","units":[1],"set_attrs":[]}],"waterways":[{"id":1,"name":"r->u","upstreams":[{"role":"input","target":"R1"}],"downstreams":[{"role":"main","target":"A1"}],"set_attrs":[3]}]}})_",
                                R"_({"request_id":"6","result":{"model_key":"simple","values":[{"attribute_id":0,"data":2},{"attribute_id":1,"data":3},{"attribute_id":2,"data":true},{"attribute_id":3,"data":"not found"}]}})_",
                                };
        REQUIRE_EQ(expected.size(), responses.size());
        
        
        for(size_t i=0; i<expected.size(); i++) {
            if (!expected[i].empty()) {
                CHECK_EQ(responses[i], expected[i]);
            } else {
                CHECK_EQ(responses[i].size(), 10);
            }
        }
        a.stop_web_api();
    }
    
       TEST_CASE("set_attributes") {
        dlib::set_all_logging_levels(dlib::LNONE);
        //-- keep test directory, unique, and with auto-cleanup.
        auto dirname = "em.web_api.test." +to_string(to_seconds64(utctime_now()) );
        test::utils::temp_dir tmpdir(dirname.c_str());
        string doc_root=(tmpdir/"doc_root").string();
        //dir_cleanup wipe{tmpdir}; // ~path raises a SIGSEGV fault when boost is built with different compile flags than shyft.
        // See also: https://stackoverflow.com/questions/19469887/segmentation-fault-with-boostfilesystem
        // However, this directory is required, but never used in this test.

        // Set up test server:
        using namespace shyft::energy_market::test;
        using std::vector;
        test_server a;
        string host_ip{"127.0.0.1"};
        a.set_listening_ip(host_ip);
        a.start_server();
        // Store some models:
        auto mdl = test::create_stm_system();
        a.do_add_model("sorland", mdl);

        auto mdl2 = test::create_simple_system(2,"simple");
        a.do_add_model("simple", mdl2);

        int port=get_free_port();
        a.start_web_api(host_ip, port, doc_root, 1, 1);

        std::this_thread::sleep_for(std::chrono::milliseconds(700));
        REQUIRE_EQ(true,a.web_api_running());
        vector<string> requests{R"_(set_attributes {"request_id": "1",
                                        "model_id": "simple",
                                        "hps_id": 1,
                                        "reservoirs": {
                                            "component_ids": [1],
                                            "attribute_ids": [16, 3],
                                            "values": [{
                                                "id"        : "abcd" ,
                                                "pfx"       : true,
                                                "time_axis" : {"time_points" : [ 1, 2, 3, 4, 5 ]},
                                                "values"    : [ 0.1, 0.2,null,0.4 ]
                                                },
                                                {
                                                    "1970-01-01T00:00:00Z": [(1.0, 3.0), (1.5, 2.5), (2.0, 2.0), (2.5, 1.5), (3.0, 1.0)],
                                                    "1970-01-01T00:00:01Z": [(1.0, 1.0), (1.5, 2.0)]
                                                }]
                                        },
                                        "units": {
                                            "component_ids": [1],
                                            "attribute_ids": [12],
                                            "values": ["too", "many", "values"]
                                        },
                                        "waterways": {
                                            "component_ids": [1],
                                            "attribute_ids": [0],
                                            "values": ["wrong type"]
                                        },
                                        "power_plants": {
                                            "component_ids": [1],
                                            "attribute_ids": [4, 6,7],
                                            "values": []
                                        }
                                    }
                                )_",
                                R"_(read_attributes {"request_id": "2",
                                        "model_id": "simple",
                                        "hps_id": 1,
                                        "time_axis": {"time_points" : [ 1, 2, 3, 4, 5 ]},
                                        "reservoirs": {
                                            "component_ids": [1],
                                            "attribute_ids": [16, 3]
                                        }
                                    
                                })_",
								R"_(set_attributes {"request_id": "A4",
										"model_id": "simple",
										"hps_id": 1,
										"reservoirs":{
											"component_ids": [1],
											"attribute_ids": [0],
											"values": [
												{
													"id" : "abcd" ,
													"pfx"		: true,
													"time_axis" : {"time_points" : [946684800]},
													"values"    : [ 210 ]
												}
											]
										}

								})_"
        };
        vector<string> responses;
        size_t r=0;
        {
            boost::asio::io_context ioc;
            auto s1=std::make_shared<session>(ioc);
            //cout<<"Starting client session\n";
            s1->run(host_ip, port, requests[r],//
            [&responses,&r,&requests](string const&web_response)->string{
                    responses.push_back(web_response);
                    ++r;
                    return r >= requests.size()?string(""):requests[r];
                }
            );
            // Run the I/O service. The call will return when
            // the socket is closed.
            ioc.run();
            s1.reset();
            //cout<<"Stopping web_api\n";
        }
        vector<string> expected{
                                R"_({"request_id":"1","result":{"model_id":2,"hps_id":1,"reservoirs":[{"component_id":1,"status":[{"attribute_id":3,"status":"OK"},{"attribute_id":16,"status":"OK"}]}],"units":[{"message":"Number of values did not match number of components and attributes. 3 =/= 1*1"}],"power_plants":[{"message":"Number of values did not match number of components and attributes. 0 =/= 3*1"}],"waterways":[{"component_id":1,"status":[{"attribute_id":0,"status":"type mismatch"}]}]}})_",
                                R"_({"request_id":"2","result":{"model_id":2,"hps_id":1,"reservoirs":[{"component_data":[{"attribute_id":3,"data":{[1.0:[(1.0,1.0),(1.5,2.0)]]}},{"attribute_id":16,"data":{"pfx":true,"data":[[1.0,0.1],[2.0,0.2],[3.0,null],[4.0,0.4]]}}],"component_id":1}]}})_",
								R"_(request_parse:time_axis::point_dt() needs at least two time-points)_"
                               };
        REQUIRE_EQ(expected.size(), responses.size());
        
        
        for(size_t i=0; i<expected.size(); i++) {
            if (!expected[i].empty()) {

                CHECK_EQ(responses[i], expected[i]);
            } else {
                CHECK_EQ(responses[i].size(), 10);
            }
        }
        a.stop_web_api();
    }
    
    TEST_CASE("publish_subscribe") {
        using namespace shyft::energy_market::test;
        string host_ip{"127.0.0.1"};
        int port = get_free_port();
        test::utils::temp_dir tmp_dir("shyft.energy_market.web_api.sub.");
        string doc_root = (tmp_dir/"doc_root").string();
        shyft::energy_market::test::test_server srv(doc_root);
        srv.set_listening_ip(host_ip);
        REQUIRE(srv.dtss != nullptr);
        srv.start_server();
        try {
            // Store a simple model that has some attributes
            auto mdl = test::create_simple_system(2,"simple");
            auto hps = mdl->hps[0];
            // We set an expression as an attribute:
            auto tsv = mocks::mk_expressions("test");
            
            auto rsv = std::dynamic_pointer_cast<reservoir>(hps->find_reservoir_by_id(1));
            // rsv->inflow and rsv->level are set as local attributes. Here we let rsv->volume be an unbound expression 
            apoint_ts inflow( rsv->inflow.url("dstm://Msimple/") );
            apoint_ts lvl( rsv->level.url("dstm://Msimple/") );
            rsv->volume = (inflow + lvl)*tsv[0]; // A more complicated expression, containing both local attributes and stored time series
            rsv->inflow = apoint_ts(tsv[0].time_axis(), 2.0, shyft::time_series::POINT_AVERAGE_VALUE);
            rsv->level = apoint_ts(tsv[0].time_axis(), 1.0, shyft::time_series::POINT_AVERAGE_VALUE);
            rsv->lrl = apoint_ts(tsv[0].time_axis(), -0.01, shyft::time_series::POINT_AVERAGE_VALUE); // Attribute not contained in an expression

            
            rsv->level_min = apoint_ts(tsv[0].id()); // Reference to a time series stored in the dtss
            auto pp = std::dynamic_pointer_cast<power_plant>(hps->find_power_plant_by_id(2));
            auto u = std::dynamic_pointer_cast<unit>(pp->units[0]);
            u->production = apoint_ts(tsv[2].id()); // A terminal
            pp->production_schedule = apoint_ts(tsv[3].id()); // An expression
            srv.dtss->do_store_ts(tsv, true, false);
            srv.do_add_model("simple", mdl);

            srv.start_web_api(host_ip, port, doc_root, 1, 1);
            REQUIRE_EQ(true, srv.web_api_running());
            std::this_thread::sleep_for(std::chrono::milliseconds(700));
            boost::asio::io_context ioc;
            auto s1 = std::make_shared<pub_sub_session>(ioc, &srv);
            s1->run(host_ip, port);
            ioc.run();
            // Set up expected responses and comparisons.
            vector<string> expected_responses{
                string(R"_({"request_id":"subscribe_and_read","result":{"model_id":2,"hps_id":1,"reservoirs":[{"component_data":[{"attribute_id":0,"data":{"pfx":true,"data":[[1.0,-0.01],[2.0,-0.01],[3.0,-0.01],[4.0,-0.01]]}},{"attribute_id":3,"data":"not found"},{"attribute_id":15,"data":{"pfx":true,"data":[[1.0,-6000.0],[2.0,-6000.0],[3.0,-6000.0],[4.0,-6000.0]]}},{"attribute_id":16,"data":{"pfx":true,"data":[[1.0,1.0],[2.0,1.0],[3.0,1.0],[4.0,1.0]]}}],"component_id":1}]}})_"),
                string(R"_({"request_id":"change_attribute","result":{"model_id":2,"hps_id":1,"reservoirs":[{"component_id":1,"status":[{"attribute_id":3,"status":"OK"},{"attribute_id":16,"status":"OK"}]}]}})_"),
                string(R"_({"request_id":"subscribe_and_read","result":{"model_id":2,"hps_id":1,"reservoirs":[{"component_data":[{"attribute_id":0,"data":{"pfx":true,"data":[[1.0,-0.01],[2.0,-0.01],[3.0,-0.01],[4.0,-0.01]]}},{"attribute_id":3,"data":{[1.0:[(1.0,3.0),(1.5,2.5),(2.0,2.0),(2.5,1.5),(3.0,1.0)]]}},{"attribute_id":15,"data":{"pfx":true,"data":[[1.0,-10000.0],[2.0,-10000.0],[3.0,-10000.0],[4.0,-10000.0]]}},{"attribute_id":16,"data":{"pfx":true,"data":[[1.0,3.0],[2.0,3.0],[3.0,3.0],[4.0,3.0]]}}],"component_id":1}]}})_"),
                string(R"_({"request_id":"second_change","result":{"model_id":2,"hps_id":1,"reservoirs":[{"component_id":1,"status":[{"attribute_id":8,"status":"stored to dtss"}]}]}})_"),
                string(R"_({"request_id":"subscribe_and_read","result":{"model_id":2,"hps_id":1,"reservoirs":[{"component_data":[{"attribute_id":0,"data":{"pfx":true,"data":[[1.0,-0.01],[2.0,-0.01],[3.0,-0.01],[4.0,-0.01]]}},{"attribute_id":3,"data":{[1.0:[(1.0,3.0),(1.5,2.5),(2.0,2.0),(2.5,1.5),(3.0,1.0)]]}},{"attribute_id":15,"data":{"pfx":true,"data":[[1.0,25.0],[2.0,25.0],[3.0,25.0],[4.0,25.0]]}},{"attribute_id":16,"data":{"pfx":true,"data":[[1.0,3.0],[2.0,3.0],[3.0,3.0],[4.0,3.0]]}}],"component_id":1}]}})_"),
                string(R"_({"request_id":"subscribe_and_read","result":{"model_id":2,"hps_id":1,"reservoirs":[{"component_data":[{"attribute_id":0,"data":{"pfx":true,"data":[[1.0,-0.01],[2.0,-0.01],[3.0,-0.01],[4.0,-0.01]]}},{"attribute_id":3,"data":{[1.0:[(1.0,3.0),(1.5,2.5),(2.0,2.0),(2.5,1.5),(3.0,1.0)]]}},{"attribute_id":15,"data":{"pfx":true,"data":[[1.0,1.0],[2.0,1.0],[3.0,1.0],[4.0,1.0]]}},{"attribute_id":16,"data":{"pfx":true,"data":[[1.0,3.0],[2.0,3.0],[3.0,3.0],[4.0,3.0]]}}],"component_id":1}]}})_"),
                string(R"_({"request_id":"ts_only_change","result":{"model_id":2,"hps_id":1,"reservoirs":[{"component_id":1,"status":[{"attribute_id":0,"status":"OK"}]}]}})_"),
                string(R"_({"request_id":"subscribe_and_read","result":{"model_id":2,"hps_id":1,"reservoirs":[{"component_data":[{"attribute_id":0,"data":{"pfx":true,"data":[[1.0,4.0],[2.0,4.0],[3.0,4.0],[4.0,4.0]]}},{"attribute_id":3,"data":{[1.0:[(1.0,3.0),(1.5,2.5),(2.0,2.0),(2.5,1.5),(3.0,1.0)]]}},{"attribute_id":15,"data":{"pfx":true,"data":[[1.0,1.0],[2.0,1.0],[3.0,1.0],[4.0,1.0]]}},{"attribute_id":16,"data":{"pfx":true,"data":[[1.0,3.0],[2.0,3.0],[3.0,3.0],[4.0,3.0]]}}],"component_id":1}]}})_"),
                string(R"_({"request_id":"finale","subscription_id":"subscribe_and_read","diagnostics":""})_")
            };
            auto responses = s1->responses_;
            s1.reset();
            
        
            REQUIRE_EQ(responses.size(), expected_responses.size());
            for (int i = 0; i < responses.size(); ++i) {
                bool found_match=false; // order of responses might differ for the two last
                for(int j=0;j<responses.size() && !found_match;++j) {
                    found_match= responses[j]==expected_responses[i];
                }
                if(!found_match) {
                    MESSAGE("failed for the "<< i << "th response: "<<expected_responses[i]<<"!="<<responses[i]);
                    CHECK(found_match);
                }
            }
        } catch(...) {}
        srv.stop_web_api();
    }
    
    TEST_CASE("dstm_fx") {
        dlib::set_all_logging_levels(dlib::LNONE);
        //-- keep test directory, unique, and with auto-cleanup.
        auto dirname = "em.web_api.test.fx." +to_string(to_seconds64(utctime_now()) );
        test::utils::temp_dir tmpdir(dirname.c_str());
        string doc_root=(tmpdir/"doc_root").string();
        // Set up test server:
        using namespace shyft::energy_market::test;
        using std::vector;
        test_server a;
        string host_ip{"127.0.0.1"};
        a.set_listening_ip(host_ip);
        a.start_server();

        int port=get_free_port();
        a.start_web_api(host_ip, port, doc_root, 1, 1);

        std::this_thread::sleep_for(std::chrono::milliseconds(700));
        REQUIRE_EQ(true,a.web_api_running());
        vector<string> requests{
            R"_(fx {"request_id": "1", "model_id": "simple","fx_arg":"{'optimize':'this'}"})_",
        };
        vector<string> responses;
        size_t r=0;
        {
            boost::asio::io_context ioc;
            auto s1=std::make_shared<session>(ioc);
            s1->run(host_ip, port, requests[r],//
            [&responses,&r,&requests](string const&web_response)->string{
                    responses.push_back(web_response);
                    ++r;
                    return r >= requests.size()?string(""):requests[r];
                }
            );
            // Run the I/O service. The call will return when
            // the socket is closed.
            ioc.run();
            s1.reset();
        }
        vector<string> expected{R"_({"request_id":"1","diagnostics":""})_",};
        REQUIRE_EQ(expected.size(), responses.size());
        FAST_CHECK_EQ(a.fx_mid,"simple");
        FAST_CHECK_EQ(a.fx_arg,"{'optimize':'this'}");
        for(size_t i=0; i<expected.size(); i++) {
            if (!expected[i].empty()) {
                CHECK_EQ(responses[i], expected[i]);
            } else {
                CHECK_EQ(responses[i].size(), 10);
            }
        }
        a.stop_web_api();
    }
    
     TEST_CASE("merge_set") {
        dlib::set_all_logging_levels(dlib::LNONE);
        //-- keep test directory, unique, and with auto-cleanup.
        auto dirname = "em.web_api.test.merge." +to_string(to_seconds64(utctime_now()) );
        test::utils::temp_dir tmpdir(dirname.c_str());
        string doc_root=(tmpdir/"doc_root").string();
        // Set up test server:
        using namespace shyft::energy_market::test;
        using std::vector;
        test_server a;
        string host_ip{"127.0.0.1"};
        a.set_listening_ip(host_ip);
        a.start_server();
        
        auto mdl = test::create_simple_system(2,"simple");
        a.do_add_model("simple", mdl);
        int port=get_free_port();
        a.start_web_api(host_ip, port, doc_root, 1, 1);

        std::this_thread::sleep_for(std::chrono::milliseconds(700));
        REQUIRE_EQ(true,a.web_api_running());
        vector<string> requests{
            R"_(set_attributes {"request_id": "1",
                                        "model_id": "simple",
                                        "hps_id": 1,
                                        "merge": true,
                                        "reservoirs": {
                                            "component_ids": [1],
                                            "attribute_ids": [16],
                                            "values": [{
                                                "id"        : "abcd" ,
                                                "pfx"       : true,
                                                "time_axis" : {"time_points" : [ 1, 2, 3, 4, 5 ]},
                                                "values"    : [ 0.1, 0.2,null,0.4 ]
                                                }]
                                        }
                                })_",
            R"_(read_attributes {"request_id": "2",
                                        "model_id": "simple",
                                        "hps_id": 1,
                                        "time_axis": {"time_points" : [0, 1, 2, 3, 4, 3600, 7200, 10800, 14400, 18000, 20000]},
                                        "reservoirs": {
                                            "component_ids": [1],
                                            "attribute_ids": [16]
                                        }
                                    
                                })_"
        };
        vector<string> responses;
        size_t r=0;
        {
            boost::asio::io_context ioc;
            auto s1=std::make_shared<session>(ioc);
            s1->run(host_ip, port, requests[r],//
            [&responses,&r,&requests](string const&web_response)->string{
                    responses.push_back(web_response);
                    ++r;
                    return r >= requests.size()?string(""):requests[r];
                }
            );
            // Run the I/O service. The call will return when
            // the socket is closed.
            ioc.run();
            s1.reset();
        }
        vector<string> expected{R"_({"request_id":"1","result":{"model_id":2,"hps_id":1,"reservoirs":[{"component_id":1,"status":[{"attribute_id":16,"status":"OK"}]}]}})_",
            R"_({"request_id":"2","result":{"model_id":2,"hps_id":1,"reservoirs":[{"component_data":[{"attribute_id":16,"data":{"pfx":true,"data":[[0.0,0.55],[1.0,0.15],[2.0,null],[3.0,null],[4.0,0.7],[3600.0,1.0],[7200.0,1.0],[10800.0,1.0],[14400.0,1.0],[18000.0,null]]}}],"component_id":1}]}})_"
        };
        REQUIRE_EQ(expected.size(), responses.size());
        for(size_t i=0; i<expected.size(); i++) {
            if (!expected[i].empty()) {
                CHECK_EQ(responses[i], expected[i]);
            } else {
                CHECK_EQ(responses[i].size(), 10);
            }
        }
        a.stop_web_api();
    }
    
    TEST_CASE("attributes_from_dtss") {
        dlib::set_all_logging_levels(dlib::LNONE);
        //-- keep test directory, unique, and with auto-cleanup.
        auto dirname = "em.web_api.test.dtss" +to_string(to_seconds64(utctime_now()) );
        test::utils::temp_dir tmpdir(dirname.c_str());
        string doc_root=(tmpdir/"doc_root").string();
        //dir_cleanup wipe{tmpdir}; // note..: ~path raises a SIGSEGV fault when boost is built with different compile flags than shyft.
        // See also: https://stackoverflow.com/questions/19469887/segmentation-fault-with-boostfilesystem

        // Set up test server:
        using namespace shyft::energy_market::test;
        using std::vector;
        test_server srv(doc_root);
        string host_ip{"127.0.0.1"};
        srv.set_listening_ip(host_ip);
        srv.start_server();
        auto mdl = test::create_simple_system(2,"simple");
        auto hps = mdl->hps[0];
        // We set an expression as an attribute:
        auto tsv = mocks::mk_expressions("test");
        auto rsv = std::dynamic_pointer_cast<reservoir>(hps->find_reservoir_by_id(1));
        rsv->level_historic = apoint_ts(tsv[0].id()); // A terminal
        rsv->level_schedule = apoint_ts(tsv[1].id()); // An expression
        // rsv->inflow and rsv->level are set as local attributes. Here we let rsv->volume be an unbound expression
        string prefix = "dstm://Msimple/";
        apoint_ts inflow( rsv->inflow.url(prefix) );
        apoint_ts lvl( rsv->level.url(prefix) );
        rsv->volume = inflow + lvl;
        
        auto pp = std::dynamic_pointer_cast<power_plant>(hps->find_power_plant_by_id(2));
        auto u = std::dynamic_pointer_cast<unit>(pp->units[0]);
        u->production = apoint_ts(tsv[2].id()); // A terminal
        pp->production_schedule = apoint_ts(tsv[3].id()); // An expression
        srv.dtss->do_store_ts(tsv, true, true);
        
        srv.do_add_model("simple", mdl);
        SUBCASE("dstm_ts_url") {
            auto p = rsv->inflow.get().total_period();
            vector<string> ts_ids = {rsv->inflow.url(prefix), rsv->level.url(prefix)};
            auto tsv = srv.dtss_read_callback(ts_ids, p);
            CHECK_EQ(rsv->inflow.get(), tsv[0]);
            CHECK_EQ(rsv->level.get(), tsv[1]);
        }
        int port=get_free_port();
        srv.start_web_api(host_ip, port, doc_root, 1, 1);


        std::this_thread::sleep_for(std::chrono::milliseconds(700));
        REQUIRE_EQ(true,srv.web_api_running());
        vector<string> requests{R"_(read_attributes {"request_id": "1",
                                        "model_id": "simple",
                                        "hps_id": 1,
                                        "time_axis": {"t0":1800,"dt": 3600, "n": 2},
                                        "reservoirs": {
                                            "component_ids": [1],
                                            "attribute_ids": [6,7, 15]
                                        },
                                        "units": {
                                            "component_ids": [1],
                                            "attribute_ids": [13]
                                        },
                                        "power_plants": {
                                            "component_ids": [2],
                                            "attribute_ids": [5]
                                        }
                                    })_",
                                R"_(set_attributes {"request_id": "2",
                                        "model_id": "simple",
                                        "hps_id": 1,
                                        "merge": false,
                                        "reservoirs": {
                                            "component_ids": [1],
                                            "attribute_ids": [6, 14, 16, 15],
                                            "values": [{
                                                "id": "abcd",
                                                "pfx": true,
                                                "time_axis" : {"time_points" : [ 1, 2, 3, 4, 5 ]},
                                                "values"    : [ 0.1, 0.1, 0.1, 0.1 ]
                                            },
                                            {
                                                "id": "abcd",
                                                "pfx": true,
                                                "time_axis" : {"time_points" : [ 1, 2, 3, 4, 5 ]},
                                                "values"    : [ 0.2, 0.2, 0.2, 0.2 ]
                                            },
                                            {
                                                "id": "abcd",
                                                "pfx": true,
                                                "time_axis" : {"time_points" : [ 1, 2, 3, 4, 5 ]},
                                                "values"    : [ 0.3, 0.3, 0.3, 0.3 ]
                                            },
                                            {
                                                "id": "abcd",
                                                "pfx": true,
                                                "time_axis" : {"time_points" : [ 1, 2, 3, 4, 5 ]},
                                                "values"    : [ 0.4, 0.4, 0.4, 0.4 ]
                                            }]
                                        }
                                })_",
                                R"_(read_attributes {"request_id": "3",
                                        "model_id": "simple",
                                        "hps_id": 1,
                                        "time_axis": {"t0":1,"dt": 1,"n": 2},
                                        "reservoirs": {
                                            "component_ids": [1],
                                            "attribute_ids": [6, 14, 15]
                                        }
                                })_"
        };
        vector<string> responses;
        size_t r=0;
        {
            boost::asio::io_context ioc;
            auto s1=std::make_shared<session>(ioc);
            s1->run(host_ip, port, requests[r],//
            [&responses,&r,&requests](string const&web_response)->string{
                    responses.push_back(web_response);
                    ++r;
                    return r >= requests.size()?string(""):requests[r];
                }
            );
            // Run the I/O service. The call will return when
            // the socket is closed.
            ioc.run();
            s1.reset();
            //cout<<"Stopping web_api\n";
        }
        vector<string> expected{R"_({"request_id":"1","result":{"model_id":2,"hps_id":1,"reservoirs":[{"component_data":[{"attribute_id":6,"data":{"pfx":true,"data":[[1800.0,-2000.0],[5400.0,-2000.0]]}},{"attribute_id":7,"data":{"pfx":true,"data":[[1800.0,-8000.0],[5400.0,-8000.0]]}},{"attribute_id":15,"data":{"pfx":true,"data":[[1800.0,2.75],[5400.0,null]]}}],"component_id":1}],"units":[{"component_data":[{"attribute_id":13,"data":{"pfx":true,"data":[[1800.0,-3000.0],[5400.0,-3000.0]]}}],"component_id":1}],"power_plants":[{"component_data":[{"attribute_id":5,"data":{"pfx":true,"data":[[1800.0,-12000.0],[5400.0,-12000.0]]}}],"component_id":2}]}})_",
        R"_({"request_id":"2","result":{"model_id":2,"hps_id":1,"reservoirs":[{"component_id":1,"status":[{"attribute_id":6,"status":"stored to dtss"},{"attribute_id":14,"status":"OK"},{"attribute_id":15,"status":"Time series is an expression. Cannot be set."},{"attribute_id":16,"status":"OK"}]}]}})_",
        R"_({"request_id":"3","result":{"model_id":2,"hps_id":1,"reservoirs":[{"component_data":[{"attribute_id":6,"data":{"pfx":true,"data":[[1.0,0.1],[2.0,0.1]]}},{"attribute_id":14,"data":{"pfx":true,"data":[[1.0,0.2],[2.0,0.2]]}},{"attribute_id":15,"data":{"pfx":true,"data":[[1.0,0.5],[2.0,0.5]]}}],"component_id":1}]}})_"
                                };
        REQUIRE_EQ(expected.size(), responses.size());
        
        
        for(size_t i=0; i<expected.size(); i++) {
            if (!expected[i].empty()) {
                CHECK_EQ(responses[i], expected[i]);
            } else {
                CHECK_EQ(responses[i].size(), 10);
            }
        }
        srv.stop_web_api();
    }

    TEST_CASE("stm_system_subscribe") {
        using namespace shyft::energy_market::test;
        string host_ip{"127.0.0.1"};
        int port = get_free_port();
        test::utils::temp_dir tmp_dir("shyft.energy_market.web_api.sub.");
        string doc_root = (tmp_dir/"doc_root").string();
        shyft::energy_market::test::test_server srv(doc_root);
        srv.set_listening_ip(host_ip);
        REQUIRE(srv.dtss != nullptr);
        srv.start_server();
        try {
            // Store a simple model that has some attributes
            auto mdl = test::create_simple_system(2,"simple");
            mdl->run_params->n_inc_runs = 2;
            mdl->run_params->n_full_runs = 3;
            mdl->run_params->head_opt = true;

            srv.do_add_model("simple", mdl);

            srv.start_web_api(host_ip, port, doc_root, 1, 1);
            REQUIRE_EQ(true, srv.web_api_running());
            std::this_thread::sleep_for(std::chrono::milliseconds(700));
            boost::asio::io_context ioc;
            auto s1 = std::make_shared<run_params_session>(ioc, &srv);
            s1->run(host_ip, port);
            ioc.run();
            // Set up expected responses and comparisons.
            vector<string> expected_responses{
                string(R"_({"request_id":"initial","result":{"model_key":"simple","values":[{"attribute_id":0,"data":2},{"attribute_id":1,"data":3},{"attribute_id":2,"data":true},{"attribute_id":3,"data":"not found"}]}})_"),
                string(R"_({"request_id":"initial","result":{"model_key":"simple","values":[{"attribute_id":0,"data":3},{"attribute_id":1,"data":3},{"attribute_id":2,"data":true},{"attribute_id":3,"data":"not found"}]}})_"),
                string(R"_({"request_id":"finale","subscription_id":"initial","diagnostics":""})_")
            };
            auto responses = s1->responses_;
            s1.reset();


            REQUIRE_EQ(responses.size(), expected_responses.size());
            for (int i = 0; i < responses.size(); ++i) {
                bool found_match=false; // order of responses might differ for the two last
                for(int j=0;j<responses.size() && !found_match;++j) {
                    found_match= responses[j]==expected_responses[i];
                }
                if(!found_match) {
                    MESSAGE("failed for the "<< i << "th response: "<<expected_responses[i]<<"!="<<responses[i]);
                    CHECK(found_match);
                }
            }
        } catch(...) {}
        srv.stop_web_api();
    }
}
