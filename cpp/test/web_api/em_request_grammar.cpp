#include "test_pch.h"
#include <vector>
#include <shyft/web_api/energy_market/request_handler.h>
#include <shyft/web_api/energy_market/grammar.h>
#include "test_parser.h"
//#include <iostream>

using std::vector;

using shyft::time_series::dd::apoint_ts;
using shyft::web_api::energy_market::proxy_attr_range;
using shyft::energy_market::hydro_power::xy_point_curve;
using shyft::energy_market::stm::t_xy_;

TEST_SUITE("em_grammar") {
    using shyft::web_api::energy_market::is_equal;
    using shyft::web_api::energy_market::json;
    using shyft::web_api::energy_market::request;
    TEST_CASE("json/integer_list_grammar") {
        shyft::web_api::grammar::integer_list_grammar<const char*> grammar;
        vector<int> test;
        auto ok = test::phrase_parser(R"_([1, 2, 3, 4, 5])_", grammar, test);
        CHECK_EQ(true, ok);
        CHECK_EQ(test, vector<int>{1, 2, 3, 4, 5});
    }

    TEST_CASE("json/simple") {
        shyft::web_api::grammar::json_grammar<const char *> grammar;
        json result;
        auto ok = test::phrase_parser(R"_(
        {
            "a": [1,2,3,4]
        }
        )_",grammar, result);
        CHECK_EQ(true, ok);
        vector<int> expected {1,2,3,4};
        // Check that key "a" is present:
        auto it = result.m.find("a");
        CHECK_EQ(true, (it != result.m.end()));
        CHECK_EQ(true, is_equal(expected, boost::get<vector<int>>(result["a"])));
    }

    TEST_CASE("json/boolean") {
        shyft::web_api::grammar::json_grammar<const char *> grammar;
        json result;
        auto ok = test::phrase_parser(R"_(
        {
            "booltrue": true,
            "boolfalse": false
        }
        )_", grammar, result);
        CHECK_EQ(true, ok);
        // CHECK KEYS:
        CHECK_EQ(true, result.m.find("booltrue") != result.m.end());
        CHECK_EQ(true, result.m.find("boolfalse") != result.m.end());
        // CHECK VALUES:
        CHECK_EQ(true, boost::get<bool>(result["booltrue"]));
        CHECK_EQ(false, boost::get<bool>(result["boolfalse"]));
    }

    TEST_CASE("json/multiple") {
        shyft::web_api::grammar::json_grammar<const char *> grammar;
        json result;
        auto ok = test::phrase_parser(R"_(
        {
            "a": [1,2,3,4],
            "b": 3,
            "c3": "a string"
        }
        )_",grammar, result);
        CHECK_EQ(true, ok); // Checks that we have a full match
        // CHECK KEYS:
        CHECK_EQ(true, result.m.find("a") != result.m.end());
        CHECK_EQ(true, result.m.find("b") != result.m.end());
        CHECK_EQ(true, result.m.find("c3") != result.m.end());
        // CHECK VALUES:
        CHECK_EQ(true, is_equal(vector<int>{1,2,3,4}, result.required<vector<int>>("a")));
        CHECK_EQ(3, result.required<int>("b"));
        CHECK_EQ("a string", result.required<std::string>("c3"));
    }
    
    TEST_CASE("json/utcperiod") {
        shyft::web_api::grammar::json_grammar<const char *> grammar;
        using shyft::core::utcperiod;
        using shyft::core::calendar;
        using shyft::core::from_seconds;
        
        json result;
        auto ok = test::phrase_parser(R"_(
            {"t1" : ["2018-02-18T01:02:03Z", "2018-03-01T00:00:00Z"],
             "t2" : [1.0, 2.0]            
            }
        )_", grammar, result);
        CHECK_EQ(true, ok);
        CHECK_EQ(true, result.m.find("t1") != result.m.end());
        CHECK_EQ(true, result.m.find("t2") != result.m.end());
        
        calendar cal;
        CHECK_EQ(utcperiod(cal.time(2018,2,18,1,2,3), cal.time(2018,3,1,0,0,0)), result.required<utcperiod>("t1"));
        CHECK_EQ(utcperiod(from_seconds(1), from_seconds(2)), result.required<utcperiod>("t2"));
    }
    
    TEST_CASE("json/time_axis"){
        shyft::web_api::grammar::json_grammar<const char *> grammar;
        using shyft::time_axis::generic_dt;
        using shyft::core::calendar;
        using shyft::core::utctime;
        using shyft::core::from_seconds;
        
        json result;
        CHECK_EQ(true,test::phrase_parser(
            R"_({"ta1" : {"t0":1.0,"dt": 2.0,"n": 10},
                 "ta2" : {"calendar" : "Europe/Oslo", "t0" : "2018-01-01T00:00:00Z", "dt" : 86400, "n" : 20},
                 "ta3" : { "time_points" : [ 1, 2, 3, 4 ]}
            })_"
            ,grammar
            ,result
            )
        );
        CHECK_EQ(true, result.m.find("ta1") != result.m.end());
        CHECK_EQ(true, result.m.find("ta2") != result.m.end());
        CHECK_EQ(true, result.m.find("ta3") != result.m.end());
        
        generic_dt e1{from_seconds(1.0),from_seconds(2.0),10};
        auto osl=std::make_shared<shyft::core::calendar>("Europe/Oslo");
        generic_dt e2{osl,calendar().time(2018,1,1),from_seconds(86400.0),20};
        generic_dt e3{std::vector<utctime>{from_seconds(1.0),from_seconds(2.0),from_seconds(3.0),from_seconds(4.0)}};
        CHECK_EQ(e1, result.required<generic_dt>("ta1"));
        CHECK_EQ(e2, result.required<generic_dt>("ta2"));
        CHECK_EQ(e3, result.required<generic_dt>("ta3"));
    }

    TEST_CASE("json/recursive") {
        shyft::web_api::grammar::json_grammar<const char*> grammar;
        json result;
        auto ok = test::phrase_parser(R"_(
        {
          "a": [1, 2, 3, 4],
          "subdata": {
            "b": "a string"
          }
        }
        )_", grammar, result);
        CHECK_EQ(true, ok);
        // CHECK KEYS:
        CHECK_EQ(true, result.m.find("a") != result.m.end());
        CHECK_EQ(true, result.m.find("subdata") != result.m.end());
        // CHECK VALUES:
        CHECK_EQ(true, is_equal(vector<int>{1,2,3,4}, boost::get<vector<int>>(result["a"])));
        auto subdata = boost::get<json>(result["subdata"]);
        CHECK_EQ(true, subdata.m.find("b") != subdata.m.end());
        CHECK_EQ("a string", boost::get<std::string>(subdata["b"]));
    }

    TEST_CASE("json/recursive/list") {
        shyft::web_api::grammar::json_grammar<const char*> grammar;
        json result;
        auto ok = test::phrase_parser(R"_(
        {
          "a": [1, 2, 3, 4],
          "subdata": [{
            "b": "a string"
          },
          {
            "b": "another string"
          }]
        }
        )_", grammar, result);
        CHECK_EQ(true, ok);
        // CHECK KEYS:
        CHECK_EQ(true, result.m.find("a") != result.m.end());
        CHECK_EQ(true, result.m.find("subdata") != result.m.end());
        // CHECK VALUES:
        CHECK_EQ(true, is_equal(vector<int>{1,2,3,4}, boost::get<vector<int>>(result["a"])));
        auto subdata = boost::get<vector<json>>(result["subdata"]);
        CHECK_EQ(true, subdata[0].m.find("b") != subdata[0].m.end());
        CHECK_EQ("a string", boost::get<std::string>(subdata[0]["b"]));
        CHECK_EQ(true, subdata[1].m.find("b") != subdata[1].m.end());
        CHECK_EQ("another string", boost::get<std::string>(subdata[1]["b"]));

    }
    
    TEST_CASE("json/proxy_attr_range") {
        using namespace shyft::time_series;
        using namespace shyft::time_axis;
        shyft::web_api::grammar::json_grammar<const char*> grammar;
        json result;
        auto ok = test::phrase_parser(R"_(
        {
          "a": {
                "id"        : "abcd" ,
                "pfx"       : true,
                "time_axis" : {"time_points" : [ 1, 2, 3, 4, 5 ]},
                "values"    : [ 0.1, 0.2,null,0.4 ]
                },
          "b": [
                {
                        "2018-02-18T01:02:03Z": [(1.0, 3.0), (1.5, 2.5), (2.0, 2.0), (2.5, 1.5), (3.0, 1.0)],
                        "2018-03-01T00:00:00Z": [(0.1, 0.0), (0.2, 0.5), (0.3, 1.0), (0.4, 1.5), (0.5, 2.0)]
                },
                {
                "id"        : "abcd" ,
                "pfx"       : true,
                "time_axis" : {"time_points" : [ 1, 2, 3, 4, 5 ]},
                "values"    : [ 0.1, 0.2,null,0.4 ]
                }
            ]
        }
        )_", grammar, result);
        // Set expected values:
        apoint_ts e("abcd",
            apoint_ts(
                generic_dt(vector<utctime>{from_seconds(1),from_seconds(2),from_seconds(3),from_seconds(4),from_seconds(5)}),
                vector<double>{0.1,0.2,shyft::nan,0.4},
                POINT_AVERAGE_VALUE)
            );
        
        xy_point_curve points1({1.0, 1.5, 2.0, 2.5, 3.0}, {3.0, 2.5, 2.0, 1.5, 1.0});
        xy_point_curve points2({0.1, 0.2, 0.3, 0.4, 0.5}, {0.0, 0.5, 1.0, 1.5, 2.0});
        utctime t1, t2;calendar utc;
        t1 = utc.time(2018,2,18,1,2,3);
        t2 = utc.time(2018,3, 1, 0, 0, 0);
        auto a = std::make_shared< std::map<utctime, std::shared_ptr<xy_point_curve> > >();
        (*a)[t1] = std::make_shared<xy_point_curve>(points1);
        (*a)[t2] = std::make_shared<xy_point_curve>(points2);
        CHECK_EQ(ok, true);
        // CHECK KEYS:
        CHECK_NE(result.m.find("a"), result.m.end());
        CHECK_NE(result.m.find("b"), result.m.end());
        // CHECK VALUES:
        auto temp1 = boost::get<proxy_attr_range>(result["a"]);
        CHECK_EQ(boost::get<apoint_ts>(temp1), e);
        
        auto temp2 = boost::get<vector<proxy_attr_range>>(result["b"]);
        auto b = boost::get<t_xy_>(temp2[0]);
        CHECK_EQ(*((*a)[t1]), *((*b)[t1]));
        CHECK_EQ(*((*a)[t2]), *((*b)[t2])); 
        auto c = boost::get<apoint_ts>(temp2[1]);
        CHECK_EQ(e, c);
        
    }

    TEST_CASE("request") {
        shyft::web_api::grammar::request_grammar<const char *> grammar;
        request res;
        auto ok = test::phrase_parser(R"_(
        read_ts {
            "a": [1,2,3,4],
            "b": 3,
            "c3": "a string"
        }
        )_",grammar, res);
        CHECK_EQ(true, ok); // Checks that we have a full match
        // CHECK KEYWORD:
        CHECK_EQ(res.keyword, "read_ts");
        // CHECK KEYS:
        CHECK_EQ(true, res.request_data.m.find("a") != res.request_data.m.end());
        CHECK_EQ(true, res.request_data.m.find("b") != res.request_data.m.end());
        CHECK_EQ(true, res.request_data.m.find("c3") != res.request_data.m.end());
        // CHECK VALUES:
        CHECK_EQ(true, is_equal(vector<int>{1,2,3,4}, boost::get<vector<int>>(res.request_data["a"])));
        CHECK_EQ(3, boost::get<int>(res.request_data["b"]));
        CHECK_EQ("a string", boost::get<std::string>(res.request_data["c3"]));
    }
    
}




