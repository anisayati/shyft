#include "doctest/doctest.h"
#include <shyft/energy_market/stm/shop/shop_system.h>
#include <shyft/core/utctime_utilities.h>
#include <memory>
#include <vector>
#include <string>

#ifdef _DEBUG
// Set these to true for improved debugging (in case it gets included in commit we should keep default false for release builds)
static const bool shop_console_output = false;
static const bool shop_log_files = false;
#else
static const bool shop_console_output = false;
static const bool shop_log_files = false;
#endif

TEST_SUITE("stm_shop_adapter")
{

TEST_CASE("getters")
{
	using std::string;
	using namespace std::string_literals;
	using namespace shyft::energy_market;
	using namespace shyft::energy_market::stm;
	using namespace shyft::energy_market::stm::shop;
	using shyft::core::utctime;
	using shyft::core::utcperiod;

	const shyft::core::calendar calendar("Europe/Oslo");
	const auto t_begin = shyft::core::create_from_iso8601_string("2018-01-01T01:00:00Z");
	const auto t_end = shyft::core::create_from_iso8601_string("2018-01-01T19:00:00Z");
	const auto make_constant_ts = [&t_begin, &t_end](double value) { return apoint_ts(shyft::time_series::dd::gta_t(t_begin, t_end - t_begin, 1), value, POINT_AVERAGE_VALUE); };

	SUBCASE("basic")
	{
		const double v_dbl = 1.23;
		CHECK(shop_adapter::exists(v_dbl));
		CHECK(shop_adapter::valid(v_dbl));
		CHECK(shop_adapter::get(v_dbl) == v_dbl);
		const int v_int = 123;
		CHECK(shop_adapter::exists(v_int));
		CHECK(shop_adapter::valid(v_int));
		CHECK(shop_adapter::get(v_int) == v_int);
		const utctime v_time = shyft::core::utctime_now();
		CHECK(shop_adapter::exists(v_time));
		CHECK(shop_adapter::valid(v_time));
		CHECK(shop_adapter::get(v_time) == v_time);
		const auto v_ts = make_constant_ts(100.0);
		CHECK(shop_adapter::exists(v_ts));
		CHECK(shop_adapter::valid(v_ts));
		CHECK(shop_adapter::get(v_ts) == v_ts);
	}
	SUBCASE("temporal ts")
	{
		const double ts_value = 100.0;
		const auto ts_empty = apoint_ts();
		CHECK(shop_adapter::exists(ts_empty) == true);
		CHECK(shop_adapter::valid(ts_empty) == true); //  Valid, because it is treated as a regular non-temporal ts value
		CHECK(shop_adapter::valid_temporal(ts_empty, t_begin) == false); // Temporal value not valid at specified time
		CHECK_THROWS(shop_adapter::get(ts_empty, t_begin)); // Attribute is empty
		const auto ts_const = make_constant_ts(ts_value);
		CHECK(shop_adapter::valid(ts_const) == true);
		CHECK(shop_adapter::valid_temporal(ts_const, t_begin) == true);
		CHECK(shop_adapter::get(ts_const, t_begin) == ts_value);
		auto t = calendar.add(t_end, calendar.SECOND, -1);
		CHECK(shop_adapter::valid_temporal(ts_const, t) == true);
		CHECK(shop_adapter::get(ts_const, t) == ts_value);
		t = calendar.add(t_begin, calendar.SECOND, -1);
		CHECK(shop_adapter::valid_temporal(ts_const, t) == false);
		CHECK_THROWS(shop_adapter::get(ts_const, t)); // Attribute is not valid at t
		CHECK(shop_adapter::valid_temporal(ts_const, t_end) == false);
		CHECK_THROWS(shop_adapter::get(ts_const, t_end)); // Attribute is not valid at t
	}
	SUBCASE("temporal xy")
	{
		const auto txy = make_shared<map<utctime, xy_point_curve_>>();
		CHECK(shop_adapter::exists(txy) == true);
		CHECK(shop_adapter::valid_temporal(txy, t_begin) == false);
		CHECK_THROWS(shop_adapter::get(txy, t_begin)); // Attribute is empty
		const auto xy = make_shared<hydro_power::xy_point_curve>(
			std::vector<double>{ 0.0, 2.0, 3.0, 5.0, 16.0 },
			std::vector<double>{ 80.0, 90.0, 95.0, 100.0, 105.0 });
		txy.get()->emplace(t_begin, xy);
		CHECK(shop_adapter::valid_temporal(txy, t_begin) == true);
		CHECK(shop_adapter::get(txy, t_begin) == xy);
		auto t = calendar.add(t_end, calendar.SECOND, -1);
		CHECK(shop_adapter::valid_temporal(txy, t) == true);
		CHECK(shop_adapter::get(txy, t) == xy);
		t = calendar.add(t_begin, calendar.SECOND, -1);
		CHECK(shop_adapter::valid_temporal(txy, t) == false);
		CHECK_THROWS(shop_adapter::get(txy, t)); // Attribute is not valid at t
		CHECK(shop_adapter::valid_temporal(txy, t_end) == true);
		CHECK(shop_adapter::get(txy, t_end) == xy); // Note: In contrast to temporal ts, this is valid at t_end and beyond since the validity period is open ended!
	}
	SUBCASE("stm attribute")
	{
		int id = 1;
		stm_hps_ hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
		stm_hps_builder builder(hps);
		reservoir_ rsv_stm = builder.create_reservoir(id++, "reservoir"s, ""s);

		// stm ts attribute
		CHECK(shop_adapter::exists(rsv_stm->inflow) == false);
		CHECK(shop_adapter::valid(rsv_stm->inflow) == false);
		const double ts_value = 100.0;
		const auto ts_empty = apoint_ts();
		rsv_stm->inflow = ts_empty;
		CHECK(shop_adapter::exists(rsv_stm->inflow) == true);
		CHECK(shop_adapter::valid(rsv_stm->inflow) == true);
		CHECK(shop_adapter::get(rsv_stm->inflow) == ts_empty);
		const auto ts_const = make_constant_ts(ts_value);
		rsv_stm->inflow = ts_const;
		CHECK(shop_adapter::get(rsv_stm->inflow) == ts_const);

		// stm temporal ts attribute
		rsv_stm->hrl = ts_empty;
		CHECK(shop_adapter::get(rsv_stm->hrl) == ts_empty);
		CHECK(shop_adapter::valid(rsv_stm->hrl) == true);
		CHECK(shop_adapter::valid_temporal(rsv_stm->hrl, t_begin) == false);
		CHECK_THROWS(shop_adapter::get(rsv_stm->hrl, t_begin)); // Attribute is empty
		rsv_stm->hrl = ts_const;
		CHECK(shop_adapter::valid_temporal(rsv_stm->hrl, t_begin) == true);
		CHECK(shop_adapter::get(rsv_stm->hrl, t_begin) == ts_value);
		auto t = calendar.add(t_end, calendar.SECOND, -1);
		CHECK(shop_adapter::valid_temporal(rsv_stm->hrl, t) == true);
		CHECK(shop_adapter::get(rsv_stm->hrl, t) == ts_value);
		t = calendar.add(t_begin, calendar.SECOND, -1);
		CHECK(shop_adapter::valid_temporal(rsv_stm->hrl, t) == false);
		CHECK_THROWS(shop_adapter::get(rsv_stm->hrl, t)); // Attribute is not valid at t
		CHECK(shop_adapter::valid_temporal(rsv_stm->hrl, t_end) == false);
		CHECK_THROWS(shop_adapter::get(rsv_stm->hrl, t_end)); // Attribute is not valid at t

		// stm temporal xy attribute
		rsv_stm->volume_descr = make_shared<map<utctime, xy_point_curve_>>();
		CHECK_THROWS(shop_adapter::get(rsv_stm->volume_descr, t_begin)); // Attribute is empty
		const auto xy = make_shared<hydro_power::xy_point_curve>(
			std::vector<double>{ 0.0, 2.0, 3.0, 5.0, 16.0 },
			std::vector<double>{ 80.0, 90.0, 95.0, 100.0, 105.0 });
		rsv_stm->volume_descr.get()->emplace(t_begin, xy);
		CHECK(shop_adapter::valid_temporal(rsv_stm->volume_descr, t_begin) == true);
		CHECK(shop_adapter::get(rsv_stm->volume_descr, t_begin) == xy);
		t = calendar.add(t_end, calendar.SECOND, -1);
		CHECK(shop_adapter::valid_temporal(rsv_stm->volume_descr, t) == true);
		CHECK(shop_adapter::get(rsv_stm->volume_descr, t) == xy);
		t = calendar.add(t_begin, calendar.SECOND, -1);
		CHECK(shop_adapter::valid_temporal(rsv_stm->volume_descr, t) == false);
		CHECK_THROWS(shop_adapter::get(rsv_stm->volume_descr, t)); // Attribute is not valid at t
		CHECK(shop_adapter::valid_temporal(rsv_stm->volume_descr, t_end) == true);
		CHECK(shop_adapter::get(rsv_stm->volume_descr, t_end) == xy);  // Note: In contrast to temporal ts, this is valid at t_end and beyond since the validity period is open ended!
	}
}

TEST_CASE("setters")
{
	using std::string;
	using namespace std::string_literals;
	using namespace shyft::energy_market;
	using namespace shyft::energy_market::stm;
	using namespace shyft::energy_market::stm::shop;
	using shyft::core::utctime;
	using shyft::core::utcperiod;
	using shyft::core::utctimespan;
	using shyft::core::to_seconds64;
	using shyft::time_axis::point_dt;
	using shyft::core::deltahours;
	using shyft::core::deltaminutes;


	const shyft::core::calendar calendar("Europe/Oslo");
	const auto t_begin = shyft::core::create_from_iso8601_string("2018-01-01T01:00:00Z");
	const auto t_end = shyft::core::create_from_iso8601_string("2018-01-01T19:00:00Z");
	const utcperiod t_period{ t_begin, t_end };
	const auto make_constant_ts = [](utcperiod t_period, double value) { return apoint_ts(shyft::time_series::dd::gta_t(t_period.start, t_period.timespan(), 1), value, POINT_AVERAGE_VALUE); };
	const auto make_ts = [&t_period, &make_constant_ts](double value) { return make_constant_ts(t_period, value); };
	const auto t_step = shyft::core::deltahours(1);

	int id = 1;

	stm_hps_ hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
	stm_hps_builder builder(hps);
	reservoir_ rsv_stm = builder.create_reservoir(id++, "reservoir"s, ""s);
	shop_system api{ t_period, t_step };
	api.set_logging_to_stdstreams(shop_console_output);
	api.set_logging_to_files(shop_log_files);
	//auto rsv_shop = api.api.create<shop_reservoir>(rsv_stm->name);

	SUBCASE("basic")
	{
		const double v_dbl_src = 1.23;
		double v_dbl_dst = -999.0;
		CHECK(api.adapter.valid_to_set(v_dbl_dst, v_dbl_src));
		CHECK(api.adapter.set(v_dbl_dst, v_dbl_src) == v_dbl_dst);
		CHECK(v_dbl_dst == v_dbl_src);
		const int v_int_src = 123;
		int v_int_dst = -999;
		CHECK(api.adapter.valid_to_set(v_int_dst, v_int_src));
		CHECK(api.adapter.set(v_int_dst, v_int_src) == v_int_dst);
		CHECK(v_int_dst == v_int_src);
		const utctime v_time_src = shyft::core::utctime_now();
		utctime v_time_dst = shyft::core::no_utctime;
		CHECK(api.adapter.valid_to_set(v_time_dst, v_time_src));
		CHECK(api.adapter.set(v_time_dst, v_time_src) == v_time_dst);
		CHECK(v_time_dst == v_time_src);
		const double ts_value = 100.0;
		const auto v_ts_src = make_ts(ts_value);
		auto v_ts_dst = apoint_ts();
		CHECK(api.adapter.valid_to_set(v_ts_dst, v_ts_src));
		CHECK(api.adapter.set(v_ts_dst, v_ts_src) == v_ts_dst);
		CHECK(v_ts_dst == v_ts_src);
		v_dbl_dst = -999.0;
		CHECK(api.adapter.valid_to_set(v_dbl_dst, v_ts_src));
		CHECK(api.adapter.set(v_dbl_dst, v_ts_src) == v_dbl_dst);
		CHECK(v_dbl_dst == ts_value);
	}

	SUBCASE("basic stm attribute not exists")
	{
		const double v_init = -999.0;
		double v = v_init;
		CHECK(api.adapter.valid_to_set(v, rsv_stm->lrl) == false);
		CHECK_THROWS(api.adapter.set(v, rsv_stm->lrl)); // Throws: Attempt to read not-yet-set attribute for object
		api.adapter.set_if(v, rsv_stm->lrl); // Does nothing, and not throwing, because attribute does not exist
		CHECK(v == v_init); // Still unchanged
		CHECK(api.adapter.set_or(v, rsv_stm->lrl, 1.23) == v); // Sets default because attribute does not exist
		CHECK(v == 1.23);
		v = v_init;
		CHECK(api.adapter.set_optional(v, rsv_stm->lrl) == v); // Does not throw, does nothing, because attribute does not exist
		CHECK(v == v_init); // Still unchanged
		v = v_init;
		CHECK(api.adapter.set_required(v, rsv_stm->lrl, 1.23) == v); // Sets default value because attribute does not exist
		CHECK(v == 1.23); // New default value set
	}

	SUBCASE("stm attribute exists and valid")
	{
		const double ts_value = 100.0;
		rsv_stm->lrl = make_ts(ts_value);
		const double v_init = -999.0;
		double v = v_init;
		CHECK(api.adapter.valid_to_set(v, rsv_stm->lrl) == true);
		CHECK(api.adapter.set(v, rsv_stm->lrl) == v);
		CHECK(v == ts_value);
		v = v_init;
		CHECK(api.adapter.set_if(v, rsv_stm->lrl) == v);
		CHECK(v == ts_value);
		v = v_init;
		CHECK(api.adapter.set_or(v, rsv_stm->lrl, 1.23) == v);
		CHECK(v == ts_value);
		v = v_init;
		CHECK(api.adapter.set_optional(v, rsv_stm->lrl) == v);
		CHECK(v == ts_value);
		v = v_init;
		CHECK(api.adapter.set_required(v, rsv_stm->lrl) == v);
		CHECK(v == ts_value);
		v = v_init;
	}

	SUBCASE("stm attribute exists but not valid")
	{
		const double ts_value = 100.0;
		rsv_stm->lrl = make_constant_ts({t_end, calendar.add(t_end, calendar.DAY, 1)}, ts_value); // Temporal value that is valid only after the api period
		const double v_init = -999.0;
		double v = v_init;
		CHECK(api.adapter.valid_to_set(v, rsv_stm->lrl) == false);
		CHECK_THROWS(api.adapter.set(v, rsv_stm->lrl)); // Throws: Attribute is not valid at t
		CHECK(v == v_init);
		CHECK_THROWS(api.adapter.set_if(v, rsv_stm->lrl)); // Throws: Attribute is not valid at t
		CHECK(v == v_init);
		v = v_init;
		CHECK(api.adapter.set_if(v, rsv_stm->lrl, 1.23) == v); // Set 1.23 since there is no valid value (not throwing since attribute exists)
		CHECK(v == 1.23);
		v = v_init;
		CHECK_THROWS(api.adapter.set_or(v, rsv_stm->lrl, 1.23)); // Throws: Attribute is not valid at t
		CHECK(v == v_init);
		v = v_init;
		CHECK(api.adapter.set_or(v, rsv_stm->lrl, 1.23, 4.56) == v); // Set 4.56 since there is no valid value (not setting 1.23 since attribute exists)
		CHECK(v == 4.56);
		v = v_init;
		CHECK(api.adapter.set_optional(v, rsv_stm->lrl) == v); // Does nothing since attribute is not valid at t
		CHECK(v == v_init);
		v = v_init;
		CHECK(api.adapter.set_optional(v, rsv_stm->lrl, 1.23) == v); // Does nothing since attribute is not valid at t (not setting 1.23 since attribute exists)
		CHECK(v == v_init);
		v = v_init;
		CHECK(api.adapter.set_optional(v, rsv_stm->lrl, 1.23, 4.56) == v); // Set 4.56 since there is no valid value (not setting 1.23 since attribute exists)
		CHECK(v == 4.56);
		v = v_init;
		CHECK_THROWS(api.adapter.set_required(v, rsv_stm->lrl));  // Throws: Attribute is not valid at t
		CHECK(v == v_init);
		v = v_init;
		CHECK(api.adapter.set_required(v, rsv_stm->lrl, 1.23) == v); // Sets 1.23 since attribute exists but is not valid at t, and no additional default is given
		CHECK(v == 1.23);
		v = v_init;
		CHECK(api.adapter.set_required(v, rsv_stm->lrl, 1.23, 4.56) == v); // Sets 4.56 since attribute exists but is not valid at t, not setting 1.23 since this is now only for the not exists case
		CHECK(v == 4.56);
		v = v_init;
	}

	SUBCASE("stm attribute exists valid nan")
	{
		const double ts_value = shyft::nan;
		rsv_stm->lrl = make_ts(ts_value);
		const double v_init = -999.0;
		double v = v_init;
		CHECK(api.adapter.valid_to_set(v, rsv_stm->lrl) == true);
		CHECK(api.adapter.set_or(v, rsv_stm->lrl, 1.23, 4.56, 7.89) == v); // Set 7.89 since there is a valid value but it is nan
		CHECK(v == 7.89);
		v = v_init;
		CHECK(api.adapter.set_optional(v, rsv_stm->lrl, 1.23, 4.56, 7.89) == v); // Set 7.89 since there is a valid value but it is nan
		CHECK(v == 7.89);
		v = v_init;
		CHECK(api.adapter.set_required(v, rsv_stm->lrl, 1.23, 4.56, 7.89) == v); // Set 7.89 since there is a valid value but it is nan
		CHECK(v == 7.89);
		v = v_init;
	}

} // TEST_CASE

} // TEST_SUITE

