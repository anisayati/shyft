#pragma once
#include <doctest/doctest.h>
#include <shyft/energy_market/stm/shop/shop_system.h>
#include <shyft/dtss/dtss.h>
#include <shyft/core/utctime_utilities.h>
#include <memory>
#include <vector>
#include <string>

#ifndef PATH_MAX //#if _WIN32
#define PATH_MAX _MAX_PATH
#endif
using shyft::time_series::dd::apoint_ts;
using shyft::time_series::POINT_AVERAGE_VALUE;
using shyft::time_series::dd::gta_t;
using shyft::core::utctime;

static inline apoint_ts make_constant_ts(utctime t, utctime t_end , double value, size_t n=1) {
    return apoint_ts(gta_t(t,t_end-t,n),value,POINT_AVERAGE_VALUE);
}

static shyft::energy_market::stm::stm_system_ build_simple_model(shyft::core::utctime t_begin, shyft::core::utctime t_end, shyft::core::utctimespan t_step, bool always_inlet_tunnels, bool use_defaults, size_t n = 1, bool with_results=false)
{
	using std::string;
	using namespace std::string_literals;
	using namespace shyft::energy_market;
	using namespace shyft::energy_market::stm;
	using namespace shyft::energy_market::stm::shop;
	using shyft::core::utctime;
	using shyft::core::utctimespan;
	using shyft::core::to_seconds64;
	using shyft::time_axis::point_dt;

	int id = 1;
	stm_system_ stm = make_shared<stm_system>(id++, "shop_system", "for_testing");
	auto market = make_shared<energy_market_area>(id++, "NO1", "xx", stm);
	market->price = apoint_ts{
		point_dt{{ t_begin, t_begin + 6 * t_step, t_begin + 12 * t_step, t_end, t_end + t_step}},
		{ 48.0, 40.0, 35.0, 35.0 },
		shyft::time_series::POINT_AVERAGE_VALUE };
	market->max_buy = apoint_ts{
		point_dt{{ t_begin, t_begin + 6 * t_step, t_end }},
		{ 10.0, 9999.0 },
		shyft::time_series::POINT_AVERAGE_VALUE };
	market->max_sale = apoint_ts{
		point_dt{{ t_begin, t_begin + 6 * t_step, t_end }},
		{ 10.0, 9999.0 },
		shyft::time_series::POINT_AVERAGE_VALUE };
	market->load = apoint_ts{
		point_dt{{ t_begin, t_begin + 1 * t_step, t_begin + 4 * t_step, t_begin + 5 * t_step, t_begin + 6 * t_step, t_end }},
		{ 90.0, 80.0, 60.0, 10.0, 0.0 },
		shyft::time_series::POINT_AVERAGE_VALUE };
	stm->market.push_back(market);

	stm_hps_ hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
	stm_hps_builder builder(hps);

	reservoir_ rsv = builder.create_reservoir(id++, "reservoir"s, ""s);

	// Optional lrl/hrl/maxvol (defaults to volume_descr/spill_descr limits)
	if (!use_defaults) {
		rsv->lrl =make_constant_ts(t_begin,t_end,80.0, n);
		//rsv->lrl.get()->emplace(t_begin, 80.0);

		rsv->hrl = make_constant_ts(t_begin,t_end, 100.0, n);

		rsv->volume_max_static = make_constant_ts(t_begin,t_end, 16.0, n);
	}

	rsv->volume_descr = make_shared<map<utctime, xy_point_curve_>>();
	rsv->volume_descr.get()->emplace(t_begin, make_shared<hydro_power::xy_point_curve>(
		std::vector<double>{ 0.0, 2.0, 3.0, 5.0, 16.0 },
		std::vector<double>{ 80.0, 90.0, 95.0, 100.0, 105.0 }));

	rsv->spill_descr = make_shared<map<utctime, xy_point_curve_>>();
	rsv->spill_descr.get()->emplace(t_begin, make_shared<hydro_power::xy_point_curve>(
		std::vector<double>{ 100.0, 101.5, 103.0, 104.0 },
		std::vector<double>{ 0.0, 25.0, 80.0, 150.0 }));

	//rsv->endpoint_desc_currency_mwh = make_shared<map<utctime, xy_point_curve_>>();
	//rsv->endpoint_desc_currency_mwh.get()->emplace(t_begin, make_shared<hydro_power::xy_point_curve>(
	//	std::vector<double>{ 0.0 },
	//	std::vector<double>{ 36.5 }));
	rsv->endpoint_desc = apoint_ts{
		point_dt{{ t_begin, t_end - 1 * t_step, t_end }},
		{ 0.0, 36.5 },
		shyft::time_series::POINT_AVERAGE_VALUE };

	rsv->level_historic = apoint_ts{
		point_dt{{ t_begin - 1 * t_step, t_end }},
		{ 90.0 },
		shyft::time_series::POINT_AVERAGE_VALUE };

	rsv->inflow = apoint_ts{
		point_dt{{ t_begin, t_begin + 2 * t_step, t_begin + 4 * t_step, t_begin + 6 * t_step, t_begin + 8 * t_step, t_begin + 12 * t_step, t_end }},
		{ 60.0, 64.0, 68.0, 62.0, 60.0, 50.0 },
		shyft::time_series::POINT_AVERAGE_VALUE };

	if (with_results) {
		rsv->volume = make_constant_ts(t_begin, t_end, 2.0, n);
		rsv->level = make_constant_ts(t_begin, t_end, 90.0, n);
	}
	waterway_ wtr_flood = builder.create_river(id++, "waterroute flood river"s, ""s);
	wtr_flood->discharge_max_static = make_constant_ts(t_begin,t_end, 150.0, n);

	waterway_ wtr_tunnel = builder.create_tunnel(id++, "waterroute input tunnel"s, ""s);
	wtr_tunnel->head_loss_coeff = make_constant_ts(t_begin,t_end, 0.00030, n);

	waterway_ wtr_penstock = builder.create_tunnel(id++, "waterroute penstock"s, ""s);
	wtr_penstock->head_loss_coeff = make_constant_ts(t_begin,t_end,0.00005, n);

	waterway_ wtr_outlet = builder.create_tunnel(id++, "waterroute outlet"s, ""s);
	waterway_ wtr_shaft = builder.create_tunnel(id++, "waterroute shaft"s, ""s);
	waterway_ wtr_tailrace = builder.create_tunnel(id++, "waterroute tailrace"s, ""s);
	waterway_ wtr_river = builder.create_river(id++, "waterroute river"s, ""s);

	unit_ gu = builder.create_unit(id++, "aggregate"s, ""s);
	power_plant_ ps = builder.create_power_plant(id++, "plant"s, ""s);
	power_plant::add_aggregate(ps, gu);

	ps->outlet_level = make_constant_ts(t_begin,t_end, 10.0, n);

	// Optional p_min/p_max/p_nom (defaults to generator_efficiency limits)
	if (!use_defaults) {
		gu->production_min_static = make_constant_ts(t_begin,t_end, 20.0, n);
		gu->production_max_static = make_constant_ts(t_begin,t_end, 80.0, n);
		//gu->production_min = apoint_ts{
		//	point_dt{{ t_begin, t_end }},
		//	{ 20.0 } };
		//gu->production_max = apoint_ts{
		//	point_dt{{ t_begin, t_end }},
		//	{ 80.0 } };
		//gu->production_nom = apoint_ts{
		//	point_dt{{ t_begin, t_end }},
		//	{ 80.0 } };
	}

	gu->generator_efficiency = make_shared<map<utctime, xy_point_curve_>>();
	gu->generator_efficiency.get()->emplace(t_begin, make_shared<hydro_power::xy_point_curve>(
		std::vector<double>{ 20.0, 40.0, 60.0, 80.0 },
		std::vector<double>{ 96.0, 98.0, 99.0, 98.0 }));

	gu->turbine_description = make_shared<map<utctime, turbine_description_>>();
	turbine_description_ td = make_shared<hydro_power::turbine_description>();

	td->efficiencies.push_back( hydro_power::turbine_efficiency{
        {  
            {{{ 20.0, 40.0, 60.0, 80.0, 100.0, 110.0 },
              { 70.0, 85.0, 92.0, 94.0, 92.0, 90.0 }},70.0}
        }
    } );
    
	gu->turbine_description.get()->emplace(t_begin, td);
	if (with_results) {
		gu->production = apoint_ts{
			point_dt{{t_begin, t_begin + 1*t_step, t_begin + 5*t_step, t_begin + 12*t_step, t_end}},
			{72.39, 70.0, 20.0, 0.0},
			shyft::time_series::POINT_AVERAGE_VALUE};
		gu->discharge = apoint_ts {
			point_dt{{t_begin, t_begin + 4*t_step, t_begin + 6*t_step, t_begin + 11*t_step, t_begin + 12*t_step, t_begin + 16*t_step, t_begin + 17*t_step, t_end}},
			{110.0, 85.100016353481479, 80.0, 80.0, 0.0, 0.0, 64.516761366803621},
			shyft::time_series::POINT_AVERAGE_VALUE};
	}
	connect(wtr_flood).input_from(rsv, hydro_power::flood).output_to(wtr_river);
	connect(wtr_tunnel).input_from(rsv).output_to(wtr_penstock);
	if (always_inlet_tunnels) {
		waterway_ wtr_inlet = builder.create_tunnel(id++, "waterroute inlet"s, ""s);
		connect(wtr_penstock).output_to(wtr_inlet);
		connect(wtr_inlet).output_to(gu);
	} else { // Since we only have one aggregate we have the option to connect the penstock directly to it
		connect(wtr_penstock).output_to(gu);
	}
	connect(gu).output_to(wtr_outlet);
	connect(wtr_outlet).output_to(wtr_shaft);
	connect(wtr_shaft).output_to(wtr_tailrace);
	connect(wtr_tailrace).output_to(wtr_river);

#if 0
	// Additional topology, for manual testing
	reservoir_ rsv2 = builder.create_reservoir(id++, "reservoir2"s, ""s);
	rsv2->volume_descr = make_shared<map<utctime, xy_point_curve_>>();
	rsv2->volume_descr.get()->emplace(t_begin, make_shared<hydro_power::xy_point_curve>(
		std::vector<double>{ 0.0, 2.0, 3.0, 5.0, 16.0 },
		std::vector<double>{ 80.0, 90.0, 95.0, 100.0, 105.0 }));

	waterway_ wtr_rsv_rsv2 = builder.create_river(id++, "wtr_rsv_rsv2"s, ""s);
	waterway::add_gate(wtr_rsv_rsv2, id++, "wtr_rsv_rsv2_gt1", "");
	waterway::add_gate(wtr_rsv_rsv2, id++, "wtr_rsv_rsv2_gt2", "");
	waterway::add_gate(wtr_rsv_rsv2, id++, "wtr_rsv_rsv2_gt3", "");
	connect(wtr_rsv_rsv2).input_from(rsv2, hydro_power::main).output_to(rsv);

	waterway_ wtr_bypass = builder.create_river(id++, "waterroute bypass river"s, ""s);
	wtr_bypass->discharge_max_static = make_shared<map<utctime, double>>();
	wtr_bypass->discharge_max_static.get()->emplace(t_begin, 150.0);
	connect(wtr_bypass).input_from(rsv, hydro_power::bypass).output_to(wtr_river);

	waterway_ wtr_flood2 = builder.create_river(id++, "waterroute flood river 2"s, ""s);
	wtr_flood2->discharge_max_static = make_shared<map<utctime, double>>();
	wtr_flood2->discharge_max_static.get()->emplace(t_begin, 150.0);
	connect(wtr_flood2).input_from(rsv2, hydro_power::flood).output_to(rsv);

	waterway_ wtr_bypass2 = builder.create_river(id++, "waterroute bypass river 2"s, ""s);
	wtr_bypass2->discharge_max_static = make_shared<map<utctime, double>>();
	wtr_bypass2->discharge_max_static.get()->emplace(t_begin, 150.0);
	waterway::add_gate(wtr_bypass2, id++, "wtr_bypass2_gt1", "");
	waterway::add_gate(wtr_bypass2, id++, "wtr_bypass2_gt2", "");
	waterway::add_gate(wtr_bypass2, id++, "wtr_bypass2_gt3", "");
	connect(wtr_bypass2).input_from(rsv2, hydro_power::bypass).output_to(rsv);
#endif

	stm->hps.push_back(hps);
	return stm;
}

static void check_time_series_at_points(const apoint_ts& ts1, const apoint_ts& ts2, const shyft::core::utctime t0, const shyft::core::utctimespan dt, const std::vector<int> n_vec) {
    for (auto n : n_vec) {
    	CHECK_EQ(ts1(t0 + n*dt), doctest::Approx(ts2(t0 + n*dt)));
    }
}

static void check_results(const shyft::energy_market::stm::stm_system_& stm1, const shyft::energy_market::stm::stm_system_& stm2, const shyft::core::utctime t0, const shyft::core::utctime t_end, const shyft::core::utctimespan t_step) {
	auto hps1 = stm1->hps[0];
	auto hps2 = stm2->hps[0];
	//1. CHECK reservoir:
	auto rsv1 = std::dynamic_pointer_cast<shyft::energy_market::stm::reservoir>(hps1->find_reservoir_by_name("reservoir"));
	CHECK(rsv1);
	auto rsv2 = std::dynamic_pointer_cast<shyft::energy_market::stm::reservoir>(hps2->find_reservoir_by_name("reservoir"));
	// 1.1 volume
	CHECK_EQ(true, rsv1->volume.exists());
	CHECK_EQ(true, rsv2->volume.exists());
	check_time_series_at_points(rsv1->volume, rsv2->volume, t0, t_step, {0}); // Currently mimicking the tests in test_model_simple.cpp

	// 1.2 level
	CHECK_EQ(true, rsv1->level.exists());
	CHECK_EQ(true, rsv2->level.exists());
	check_time_series_at_points(rsv1->level, rsv2->level, t0, t_step, {0});

	// 2 unit:
	auto u1 = std::dynamic_pointer_cast<shyft::energy_market::stm::unit>(hps1->find_unit_by_name("aggregate"));
	CHECK(u1);
	auto u2 = std::dynamic_pointer_cast<shyft::energy_market::stm::unit>(hps2->find_unit_by_name("aggregate"));
	CHECK(u2);
	// 2.1 production:
	CHECK_EQ(true, u1->production.exists());
	CHECK_EQ(true, u2->production.exists());
	check_time_series_at_points(u1->production, u2->production, t0, t_step, {0, 1, 5, 12});
	// 2.2 discharge
	CHECK_EQ(true, u1->discharge.exists());
	CHECK_EQ(true, u2->discharge.exists());
	check_time_series_at_points(u1->discharge, u2->discharge, t0, t_step, {0, 4, 6, 11, 12, 16, 17});
}

/** @brief Builds the same model as build_simple_model, but now
 * all time series attached to the model are unbound references to 
 * times series stored in a dtss.
 */
static shyft::energy_market::stm::stm_system_ build_simple_model_with_dtss(shyft::dtss::server& dtss, shyft::core::utctime t_begin, shyft::core::utctime t_end, shyft::core::utctimespan t_step, bool always_inlet_tunnels, bool use_defaults, size_t n=1)
{
	using std::string;
	using namespace std::string_literals;
	using namespace shyft::energy_market;
	using namespace shyft::energy_market::stm;
	using namespace shyft::energy_market::stm::shop;
	using shyft::core::utctime;
	using shyft::core::utctimespan;
	using shyft::core::to_seconds64;
	using shyft::time_axis::point_dt;
    using shyft::dtss::ts_vector_t;
    using shyft::dtss::id_vector_t;
    
    ts_vector_t tsv;
    tsv.reserve(16);
    
	int id = 1;
	stm_system_ stm = make_shared<stm_system>(id++, "shop_system", "for_testing");
	
    //------------------------
    // stm.market
    //------------------------
    auto market = make_shared<energy_market_area>(id++, "NO1", "xx", stm);
    apoint_ts no1_price = apoint_ts{
		point_dt{{ t_begin, t_begin + 6 * t_step, t_begin + 12 * t_step, t_end, t_end + t_step}},
		{ 48.0, 40.0, 35.0, 35.0 },
		shyft::time_series::POINT_AVERAGE_VALUE };
    tsv.emplace_back(apoint_ts("shyft://test/no1_price", no1_price));
	market->price = apoint_ts("shyft://test/no1_price");
    
    apoint_ts no1_max_buy(point_dt{{ t_begin, t_begin + 6 * t_step, t_end }},
		{ 10.0, 9999.0 },
		shyft::time_series::POINT_AVERAGE_VALUE);
    tsv.emplace_back(apoint_ts{"shyft://test/no1_max_buy", no1_max_buy});
	market->max_buy = apoint_ts("shyft://test/no1_max_buy");
    
    apoint_ts no1_max_sale(point_dt{{ t_begin, t_begin + 6 * t_step, t_end }},
		{ 10.0, 9999.0 },
		shyft::time_series::POINT_AVERAGE_VALUE);
    tsv.emplace_back(apoint_ts{"shyft://test/no1_max_sale", no1_max_sale});
	market->max_sale = apoint_ts{"shyft://test/no1_max_sale"};
    
    apoint_ts no1_load(point_dt{{ t_begin, t_begin + 1 * t_step, t_begin + 4 * t_step, t_begin + 5 * t_step, t_begin + 6 * t_step, t_end }},
		{ 90.0, 80.0, 60.0, 10.0, 0.0 },
		shyft::time_series::POINT_AVERAGE_VALUE);
    tsv.emplace_back(apoint_ts{"shyft://test/no1_load", no1_load});
	market->load = apoint_ts{"shyft://test/no1_load"};
	
    stm->market.push_back(market);

    //----------------------
    // stm.hps
    //----------------------
	stm_hps_ hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
	stm_hps_builder builder(hps);

	reservoir_ rsv = builder.create_reservoir(id++, "reservoir"s, ""s);

	// Optional lrl/hrl/maxvol (defaults to volume_descr/spill_descr limits)
	if (!use_defaults) {
        auto rsv_lrl = make_constant_ts(t_begin, t_end, 80.0, n);
        tsv.emplace_back(apoint_ts{"shyft://test/rsv_lrl", rsv_lrl});
		rsv->lrl = apoint_ts{"shyft://test/rsv_lrl"};

        auto rsv_hrl = make_constant_ts(t_begin, t_end, 100.0, n);
        tsv.emplace_back(apoint_ts{"shyft://test/rsv_hrl", rsv_hrl});
		rsv->hrl = apoint_ts{"shyft://test/rsv_hrl"};

        auto rsv_vol_max = make_constant_ts(t_begin, t_end, 16.0, n);
        tsv.emplace_back(apoint_ts{"shyft://test/rsv_vol_max", rsv_vol_max});
		rsv->volume_max_static = apoint_ts{"shyft://test/rsv_vol_max"};
	}

	rsv->volume_descr = make_shared<map<utctime, xy_point_curve_>>();
	rsv->volume_descr.get()->emplace(t_begin, make_shared<hydro_power::xy_point_curve>(
		std::vector<double>{ 0.0, 2.0, 3.0, 5.0, 16.0 },
		std::vector<double>{ 80.0, 90.0, 95.0, 100.0, 105.0 }));

	rsv->spill_descr = make_shared<map<utctime, xy_point_curve_>>();
	rsv->spill_descr.get()->emplace(t_begin, make_shared<hydro_power::xy_point_curve>(
		std::vector<double>{ 100.0, 101.5, 103.0, 104.0 },
		std::vector<double>{ 0.0, 25.0, 80.0, 150.0 }));

	//rsv->endpoint_desc_currency_mwh = make_shared<map<utctime, xy_point_curve_>>();
	//rsv->endpoint_desc_currency_mwh.get()->emplace(t_begin, make_shared<hydro_power::xy_point_curve>(
	//	std::vector<double>{ 0.0 },
	//	std::vector<double>{ 36.5 }));
    apoint_ts rsv_endpoint_desc(point_dt{{ t_begin, t_end - 1 * t_step, t_end }},
		{ 0.0, 36.5 },
		shyft::time_series::POINT_AVERAGE_VALUE);
    tsv.emplace_back(apoint_ts{"shyft://test/rsv_endpoint_desc", rsv_endpoint_desc});
	rsv->endpoint_desc = apoint_ts{"shyft://test/rsv_endpoint_desc"};

    apoint_ts rsv_level_historic(point_dt{{ t_begin - 1 * t_step, t_end }},
		{ 90.0 },
		shyft::time_series::POINT_AVERAGE_VALUE);
    tsv.emplace_back(apoint_ts{"shyft://test/rsv_level_historic", rsv_level_historic});
	rsv->level_historic = apoint_ts{"shyft://test/rsv_level_historic"};

    apoint_ts rsv_inflow(point_dt{{ t_begin, t_begin + 2 * t_step, t_begin + 4 * t_step, t_begin + 6 * t_step, t_begin + 8 * t_step, t_begin + 12 * t_step, t_end }},
		{ 60.0, 64.0, 68.0, 62.0, 60.0, 50.0 },
		shyft::time_series::POINT_AVERAGE_VALUE);
    tsv.emplace_back(apoint_ts{"shyft://test/rsv_inflow", rsv_inflow});
	rsv->inflow = apoint_ts{"shyft://test/rsv_inflow"};

	waterway_ wtr_flood = builder.create_river(id++, "waterroute flood river"s, ""s);
	apoint_ts wtr_flood_discharge_max_static = make_constant_ts(t_begin, t_end, 150.0, n);
    tsv.emplace_back(apoint_ts{"shyft://test/wtr_flood_discharge_max_static", wtr_flood_discharge_max_static});
    wtr_flood->discharge_max_static = apoint_ts{"shyft://test/wtr_flood_discharge_max_static"};

	waterway_ wtr_tunnel = builder.create_tunnel(id++, "waterroute input tunnel"s, ""s);
    apoint_ts wtr_tunnel_head_loss_coeff = make_constant_ts(t_begin, t_end, 0.00030, n);
    tsv.emplace_back(apoint_ts{"shyft://test/wtr_tunnel_head_loss_coeff", wtr_tunnel_head_loss_coeff});
	wtr_tunnel->head_loss_coeff = apoint_ts{"shyft://test/wtr_tunnel_head_loss_coeff"};

	waterway_ wtr_penstock = builder.create_tunnel(id++, "waterroute penstock"s, ""s);
    apoint_ts wtr_penstock_head_loss_coeff = make_constant_ts(t_begin, t_end, 0.00005, n);
    tsv.emplace_back(apoint_ts{"shyft://test/wtr_penstock_head_loss_coeff", wtr_penstock_head_loss_coeff});
	wtr_penstock->head_loss_coeff = apoint_ts{"shyft://test/wtr_penstock_head_loss_coeff"};

	waterway_ wtr_outlet = builder.create_tunnel(id++, "waterroute outlet"s, ""s);
	waterway_ wtr_shaft = builder.create_tunnel(id++, "waterroute shaft"s, ""s);
	waterway_ wtr_tailrace = builder.create_tunnel(id++, "waterroute tailrace"s, ""s);
	waterway_ wtr_river = builder.create_river(id++, "waterroute river"s, ""s);

	unit_ gu = builder.create_unit(id++, "aggregate"s, ""s);
	power_plant_ ps = builder.create_power_plant(id++, "plant"s, ""s);
	power_plant::add_aggregate(ps, gu);

    apoint_ts ps_outlet_level = make_constant_ts(t_begin, t_end, 10.0, n);
    tsv.emplace_back(apoint_ts{"shyft://test/ps_outlet_level", ps_outlet_level});
	ps->outlet_level = apoint_ts{"shyft://test/ps_outlet_level"};

	// Optional p_min/p_max/p_nom (defaults to generator_efficiency limits)
	if (!use_defaults) {
        apoint_ts gu_production_min_static = make_constant_ts(t_begin, t_end, 20.0, n);
        tsv.emplace_back(apoint_ts{"shyft://test/gu_production_min_static", gu_production_min_static});
		gu->production_min_static = apoint_ts{"shyft://test/gu_production_min_static"};
        
        apoint_ts gu_production_max_static = make_constant_ts(t_begin, t_end, 80.0, n);
        tsv.emplace_back(apoint_ts{"shyft://test/gu_production_max_static", gu_production_max_static});
		gu->production_max_static = apoint_ts{"shyft://test/gu_production_max_static"};
		//gu->production_min = apoint_ts{
		//	point_dt{{ t_begin, t_end }},
		//	{ 20.0 } };
		//gu->production_max = apoint_ts{
		//	point_dt{{ t_begin, t_end }},
		//	{ 80.0 } };
		//gu->production_nom = apoint_ts{
		//	point_dt{{ t_begin, t_end }},
		//	{ 80.0 } };
	}

	gu->generator_efficiency = make_shared<map<utctime, xy_point_curve_>>();
	gu->generator_efficiency.get()->emplace(t_begin, make_shared<hydro_power::xy_point_curve>(
		std::vector<double>{ 20.0, 40.0, 60.0, 80.0 },
		std::vector<double>{ 96.0, 98.0, 99.0, 98.0 }));

	gu->turbine_description = make_shared<map<utctime, turbine_description_>>();
	turbine_description_ td = make_shared<hydro_power::turbine_description>();

	td->efficiencies.push_back( hydro_power::turbine_efficiency{
        {  
            {{{ 20.0, 40.0, 60.0, 80.0, 100.0, 110.0 },
              { 70.0, 85.0, 92.0, 94.0, 92.0, 90.0 }},70.0}
        }
    } );
    
	gu->turbine_description.get()->emplace(t_begin, td);

	connect(wtr_flood).input_from(rsv, hydro_power::flood).output_to(wtr_river);
	connect(wtr_tunnel).input_from(rsv).output_to(wtr_penstock);
	if (always_inlet_tunnels) {
		waterway_ wtr_inlet = builder.create_tunnel(id++, "waterroute inlet"s, ""s);
		connect(wtr_penstock).output_to(wtr_inlet);
		connect(wtr_inlet).output_to(gu);
	} else { // Since we only have one aggregate we have the option to connect the penstock directly to it
		connect(wtr_penstock).output_to(gu);
	}
	connect(gu).output_to(wtr_outlet);
	connect(wtr_outlet).output_to(wtr_shaft);
	connect(wtr_shaft).output_to(wtr_tailrace);
	connect(wtr_tailrace).output_to(wtr_river);

#if 0
	// Additional topology, for manual testing
	reservoir_ rsv2 = builder.create_reservoir(id++, "reservoir2"s, ""s);
	rsv2->volume_descr = make_shared<map<utctime, xy_point_curve_>>();
	rsv2->volume_descr.get()->emplace(t_begin, make_shared<hydro_power::xy_point_curve>(
		std::vector<double>{ 0.0, 2.0, 3.0, 5.0, 16.0 },
		std::vector<double>{ 80.0, 90.0, 95.0, 100.0, 105.0 }));

	waterway_ wtr_rsv_rsv2 = builder.create_river(id++, "wtr_rsv_rsv2"s, ""s);
	waterway::add_gate(wtr_rsv_rsv2, id++, "wtr_rsv_rsv2_gt1", "");
	waterway::add_gate(wtr_rsv_rsv2, id++, "wtr_rsv_rsv2_gt2", "");
	waterway::add_gate(wtr_rsv_rsv2, id++, "wtr_rsv_rsv2_gt3", "");
	connect(wtr_rsv_rsv2).input_from(rsv2, hydro_power::main).output_to(rsv);

	waterway_ wtr_bypass = builder.create_river(id++, "waterroute bypass river"s, ""s);
	wtr_bypass->discharge_max_static = make_shared<map<utctime, double>>();
	wtr_bypass->discharge_max_static.get()->emplace(t_begin, 150.0);
	connect(wtr_bypass).input_from(rsv, hydro_power::bypass).output_to(wtr_river);

	waterway_ wtr_flood2 = builder.create_river(id++, "waterroute flood river 2"s, ""s);
	wtr_flood2->discharge_max_static = make_shared<map<utctime, double>>();
	wtr_flood2->discharge_max_static.get()->emplace(t_begin, 150.0);
	connect(wtr_flood2).input_from(rsv2, hydro_power::flood).output_to(rsv);

	waterway_ wtr_bypass2 = builder.create_river(id++, "waterroute bypass river 2"s, ""s);
	wtr_bypass2->discharge_max_static = make_shared<map<utctime, double>>();
	wtr_bypass2->discharge_max_static.get()->emplace(t_begin, 150.0);
	waterway::add_gate(wtr_bypass2, id++, "wtr_bypass2_gt1", "");
	waterway::add_gate(wtr_bypass2, id++, "wtr_bypass2_gt2", "");
	waterway::add_gate(wtr_bypass2, id++, "wtr_bypass2_gt3", "");
	connect(wtr_bypass2).input_from(rsv2, hydro_power::bypass).output_to(rsv);
#endif
    dtss.do_store_ts(tsv, true, true);
	stm->hps.push_back(hps);
	return stm;
}

static std::vector<shyft::energy_market::stm::shop::shop_command> optimization_commands(std::size_t run_id, bool write_files)
{
	using namespace shyft::energy_market::stm::shop;
	std::vector<shop_command> commands{
		shop_command::set_method_primal(),
		shop_command::set_code_full(),
		shop_command::start_sim(3),
		shop_command::set_code_incremental(),
		shop_command::start_sim(3),
	};
	if (write_files) {
		char filename[PATH_MAX];
		sprintf(filename, "shop_log_%zu.txt", run_id);
		commands.insert(commands.begin(), shop_command::log_file(filename));
		sprintf(filename, "shop_results_%zu.txt", run_id);
		commands.push_back(shop_command::return_simres(filename));
		sprintf(filename, "shop_series_%zu.txt", run_id);
		commands.push_back(shop_command::save_series(filename));
		sprintf(filename, "shop_series_%zu.xml", run_id);
		commands.push_back(shop_command::save_xmlseries(filename));
		sprintf(filename, "shop_genres_%zu.txt", run_id);
		commands.push_back(shop_command::return_simres(filename));
	}
	return commands;
}
