#ifdef SHYFT_WITH_SHOP

#ifndef _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#endif
#ifndef _CRT_NONSTDC_NO_DEPRECATE
#define _CRT_NONSTDC_NO_DEPRECATE
#endif
#include "fixture.h"
#include <string>
#include <vector>
#include <algorithm>
#include <fstream>
#include <iostream>
#include <map>
#include <memory>

using namespace std;
using namespace string_literals;


namespace generate_shop_cxx_api {
//
// Helper function for printing an enum from specified set of entries
//
template<typename T> // T = iterable string-string (name-identifier) array-like type, typically vector<string>.
void print_enum1(ostream& os, const string& enumName, const T& enumEntries) {
	os << "enum class " << enumName << endl << "{" << endl;
	for (auto it = cbegin(enumEntries); it != cend(enumEntries); ++it)
		os << "\t" << *it << "," << endl;
	os << "};" << endl
				 << "using " << enumName << "_info_t = std::tuple<" << enumName << ", const char* const>;" << endl
				 << "static constexpr " << enumName << "_info_t " << enumName << "_info[] {" << endl;
	for (auto it = cbegin(enumEntries); it != cend(enumEntries); ++it)
		os << "\t{ " << enumName << "::" << *it << ", \"" << *it << "\"}," << endl;
	os << "};" << endl;
};
template<typename T> // T = iterable string-string (name-identifier) map-like type, typically map<string,string> if order does not matter, or vector<pair<string,string>> if insert order needs to be kept.
void print_enum2(ostream& os, const string& enumName, const T& enumEntries) {
	os << "enum class " << enumName << endl << "{" << endl;
	for (auto it = cbegin(enumEntries); it != cend(enumEntries); ++it)
		os << "\t" << it->second << "," << endl;
	os << "};" << endl
				 << "using " << enumName << "_info_t = std::tuple<" << enumName << ", const char* const>;" << endl
				 << "static constexpr " << enumName << "_info_t " << enumName << "_info[] {" << endl;
	for (auto it = cbegin(enumEntries); it != cend(enumEntries); ++it)
		os << "\t{ " << enumName << "::" << it->second << ", \"" << it->first << "\"}," << endl;
	os << "};" << endl;
};
template<typename T> // T = iterable string-{string,int} (name-{identifier,value}) map-like type, typically map<string,pair<string,int>> if order does not matter, or vector<pair<string,pair<string,int>>> if insert order needs to be kept.
void print_enum3(ostream& os, const string& enumName, const T& enumEntries) {
	os << "enum class " << enumName << endl << "{" << endl;
	for (auto it = cbegin(enumEntries); it != cend(enumEntries); ++it)
		os << "\t" << it->second.first << " = " << it->second.second << "," << endl;
	os << "};" << endl
				 << "using " << enumName << "_info_t = std::tuple<" << enumName << ", const char* const>;" << endl
				 << "static constexpr " << enumName << "_info_t " << enumName << "_info[] {" << endl;
	for (auto it = cbegin(enumEntries); it != cend(enumEntries); ++it)
		os << "\t{ " << enumName << "::" << it->second.first << ", \"" << it->first << "\"}," << endl;
	os << "};" << endl;
};
//
// Generate enum representing object types
//
void print_object_types(ShopSystem* shop, ostream& os)
{
	const int n = ShopGetObjectTypeCount(shop);
	vector<pair<string,string>> enumEntries;
	enumEntries.reserve(n);
	for (int i = 0; i < n; ++i) {
		const char* v = ShopGetObjectTypeName(shop, i);
		enumEntries.emplace_back(v, v);
	}
	print_enum2(os, "shop_object_type", enumEntries);
}

//
// Generate various enums and structs for representing attribute information 
//
auto print_attribute_info_types(ShopSystem* shop, ostream& os,
	map<string, string>& dataTypes, map<string, string>& xUnits, map<string, string>& yUnits)
{
	// Collect attribute info
	auto makeUnitIdentifier = [](string unit) { // Helper func for converting unit string into valid enum identifier
		if (unit == "%") {
			unit = "percent";
		} else {
			size_t p = unit.find('/', 0);
			while (p != string::npos) {
				unit[p] = '_';
				unit.insert(p + 1, "per_");
				p = unit.find('/', p + 5);
			}
			transform(unit.begin(), unit.end(), unit.begin(), ::tolower);
		}
		return unit;
	};
	for (int i = 0, n = ShopGetObjectTypeCount(shop); i < n; ++i) {
		int nAttributes = 128; // NB: Must be large enough to include all attributes for every type!
		int attributeIndexList[128]; // Array of size according to nAttributes
		ShopGetObjectTypeAttributeIndices(shop, i, nAttributes, attributeIndexList);
		for (int i = 0; i < nAttributes; ++i) {
			int attributeIndex = attributeIndexList[i];
			char objectTypeName[64]; char attributeName[64];
			char attributeDataFuncName[64]; char attributeDatatype[64];
			char xUnit[64]; char yUnit[64];
			bool isObjectParam, isInput, isOutput;
			ShopGetAttributeInfo(shop, attributeIndex, objectTypeName, isObjectParam,
				attributeName, attributeDataFuncName, attributeDatatype, xUnit, yUnit, isInput, isOutput);
			string dataTypeIdentifier, xUnitIdentifier, yUnitIdentifier;
			if (dataTypes.find(attributeDatatype) == dataTypes.end())
				dataTypes[attributeDatatype] = "shop_"s + attributeDatatype;
			if (xUnits.find(xUnit) == xUnits.end())
				xUnits[xUnit] = makeUnitIdentifier(xUnit);
			if (yUnits.find(yUnit) == yUnits.end())
				yUnits[yUnit] = makeUnitIdentifier(yUnit);
		}
	}

	// Print attribute data type enum
	print_enum2(os, "shop_data_type", dataTypes);
	os << endl;

	// Print attribute X unit enum
	print_enum2(os, "shop_x_unit", xUnits);
	os << endl;

	// Print attribute Y unit enum
	print_enum2(os, "shop_y_unit", yUnits);
	os << endl;

	// Print attribute info struct
	os
		<< "struct shop_attribute_info" << endl
		<< "{" << endl
		<< "\tshop_data_type data_type;" << endl
		<< "\tshop_x_unit x_unit;" << endl
		<< "\tshop_y_unit y_unit;" << endl
		<< "\tbool is_object_parameter;" << endl
		<< "\tbool is_input;" << endl
		<< "\tbool is_output;" << endl
		<< "};" << endl;
}

//
// Generate enum representing attribute types for a single object type
//
void print_attribute_types_for_object_type(ShopSystem* shop, ostream& os, const int objectTypeIndex,
	const map<string, string>& dataTypes, const map<string, string>& xUnits, const map<string, string>& yUnits)
{
	const char* const objectTypeName = ShopGetObjectTypeName(shop, objectTypeIndex);
	os << "enum class shop_" << objectTypeName << "_attribute" << endl << "{" << endl;
	int nAttributes = 128; // NB: Must be large enough to include all attributes for every type!
	int attributeIndexList[128]; // Array of size according to nAttributes
	ShopGetObjectTypeAttributeIndices(shop, objectTypeIndex, nAttributes, attributeIndexList);
	for (int i = 0; i < nAttributes; ++i) {
		int attributeIndex = attributeIndexList[i];
		const char* attributeName = ShopGetAttributeName(shop, attributeIndex);
		os << "\t" << attributeName << " = " << attributeIndex << "," << endl;
	}
	os << "};" << endl;
	if (nAttributes > 0) {
		os  << "using " << objectTypeName << "_attribute_info_t = std::tuple<shop_" << objectTypeName << "_attribute, const char* const, shop_attribute_info>;" << endl
			<< "static constexpr " << objectTypeName << "_attribute_info_t shop_" << objectTypeName << "_attribute_info[] {" << endl;
		for (int i = 0; i < nAttributes; ++i) {
			int attributeIndex = attributeIndexList[i];
			char objectTypeName[64]; char attributeName[64];
			char attributeDataFuncName[64]; char attributeDatatype[64];
			char xUnit[64]; char yUnit[64];
			bool isObjectParam, isInput, isOutput;
			ShopGetAttributeInfo(shop, attributeIndex, objectTypeName, isObjectParam,
				attributeName, attributeDataFuncName, attributeDatatype, xUnit, yUnit, isInput, isOutput);
			string dataTypeIdentifier, xUnitIdentifier, yUnitIdentifier;
			if (dataTypes.find(attributeDatatype) != dataTypes.end())
				dataTypeIdentifier = dataTypes.at(attributeDatatype);
			if (xUnits.find(xUnit) != xUnits.end())
				xUnitIdentifier = xUnits.at(xUnit);
			if (yUnits.find(yUnit) != yUnits.end())
				yUnitIdentifier = yUnits.at(yUnit);
			os  << "\t{ shop_" << objectTypeName << "_attribute::" << attributeName << ", "
				<< "\"" << attributeName << "\", "
				<< "{"
				<< "shop_data_type::" << dataTypeIdentifier << ", "
				<< "shop_x_unit::" << xUnitIdentifier << ", "
				<< "shop_y_unit::" << yUnitIdentifier << ", "
				<< "/*is_object_parameter*/ " << (isObjectParam ? "true" : "false") << ", "
				<< "/*is_input*/ " << (isInput ? "true" : "false") << ", "
				<< "/*is_output*/ " << (isOutput ? "true" : "false") << ", "
				<< "} "
				<< "}," << endl;
		}
		os << "};" << endl;
	}
}

//
// Generate enum representing attribute types for a single object type
//
string api_class_name(string_view sintef_name) {
    if (sintef_name == "generator") return "aggregate";
    if (sintef_name == "plant") return "power_station";
    return string(sintef_name);
}

void print_shop_proxy_class_begin(ostream&os, string_view class_name, int o_type) {
    auto c_name = api_class_name(class_name);
    os << "template<class A>\n";
    os << "struct " << c_name << ":obj<A," << o_type << "> {\n";
    os << "    using super=obj<A," << o_type << ">;\n";
    os << "    " << c_name << "()=default;\n";
    os << "    " << c_name << "(A* s,int oid):super(s, oid) {}\n";
    os << "    " << c_name << "(const " << c_name << "& o):super(o) {}\n";
    os << "    " << c_name << "(" << c_name << "&& o):super(std::move(o)) {}\n";
    os << "    " << c_name << "& operator=(const " << c_name << "& o) {\n";
    os << "        super::operator=(o);\n";
    os << "        return *this;\n";
    os << "    }\n";
    os << "    " << c_name << "& operator=(" << c_name << "&& o) {\n";
    os << "        super::operator=(std::move(o));\n";
    os << "        return *this;\n";
    os << "    }\n";
    os << "    // attributes\n";
}
void print_shop_proxy_attribute(ostream&os, string_view class_name, string_view attr_name, int aid, string_view a_type, bool read_only,string_view x_unit,string_view y_unit) {
    string attr_type;
    if (a_type == "double") attr_type = "double";
    else if (a_type == "int") attr_type = "int";
    else if (a_type == "txy") attr_type = "typename A::_txy";
    else if (a_type == "xy") attr_type = "typename A::_xy";
    else if (a_type == "xy_array") attr_type = "vector<typename A::_xy>";
    else if (a_type == "double_array") attr_type = "vector<double>";
    if (attr_type.size()) {
        os << "    " << (read_only ? "ro" : "rw") << "<" << api_class_name(class_name) << "," << aid << "," << attr_type << "> " << string(attr_name) << "{this}; // x["<<string(x_unit)<<"],y["<<string(y_unit)<<"]\n";
    }  else {
        os << "    //--TODO: " << (read_only ? "ro" : "rw") << "<" << api_class_name(class_name) << "," << aid << "," << string(a_type) << "> " << string(attr_name) << "{this}; // x[" << string(x_unit) << "],y[" << string(y_unit) << "]\n";
        cout << "skipping " << string(class_name) << "." << string(attr_name) << " of unsupported type " << string(a_type) << "\n";
    }
}
void print_shop_proxy_class_end(ostream&os) {
    os << "};\n";
}

void print_shop_proxy_for_object_type(ShopSystem* shop, ostream& os, const int objectTypeIndex,
    const map<string, string>& dataTypes, const map<string, string>& xUnits, const map<string, string>& yUnits)
{
    const char* const objectTypeName = ShopGetObjectTypeName(shop, objectTypeIndex);
    print_shop_proxy_class_begin(os, objectTypeName, objectTypeIndex);
    
    int nAttributes = 128; // NB: Must be large enough to include all attributes for every type!
    int attributeIndexList[128]; // Array of size according to nAttributes
    ShopGetObjectTypeAttributeIndices(shop, objectTypeIndex, nAttributes, attributeIndexList);
    for (int i = 0; i < nAttributes; ++i) {
        int attributeIndex = attributeIndexList[i];
        char objectTypeName[64]; char attributeName[64];
        char attributeDataFuncName[64]; char attributeDatatype[64];
        char xUnit[64]; char yUnit[64];
        bool isObjectParam, isInput, isOutput;
        ShopGetAttributeInfo(shop, attributeIndex, objectTypeName, isObjectParam,
            attributeName, attributeDataFuncName, attributeDatatype, xUnit, yUnit, isInput, isOutput);
        string dataTypeIdentifier, xUnitIdentifier, yUnitIdentifier;
        if (dataTypes.find(attributeDatatype) != dataTypes.end())
            dataTypeIdentifier = dataTypes.at(attributeDatatype);
        if (xUnits.find(xUnit) != xUnits.end())
            xUnitIdentifier = xUnits.at(xUnit);
        if (yUnits.find(yUnit) != yUnits.end())
            yUnitIdentifier = yUnits.at(yUnit);
        print_shop_proxy_attribute(os, objectTypeName, attributeName, attributeIndex, attributeDatatype, isOutput,xUnitIdentifier,yUnitIdentifier);
    }
    print_shop_proxy_class_end(os);
}

void print_shop_proxy_header_begin(ostream& os) {
    os << "#pragma once\n";
    os << "// auto genereated files based on active version of Sintef SHOP api dll\n";
    os << "#include \"shop_proxy.h\"\n";
    os << "namespace shop {\n\n";
    os << "using proxy::obj;\n";
    os << "using shop::proxy::rw;\n";
    os << "using shop::proxy::ro;\n\n";
}

void print_shop_proxy_header_end(ostream& os) {
    os << "\n}\n";
}
//
// Generate enum representing attribute types for all object types
//
void print_attribute_types(ShopSystem* shop, ostream& os, ostream&proxy_os)
{
	// Print attributes for each object, and collect unique attribute information such as data type, unit etc.
	map<string, string> dataTypes, xUnits, yUnits;
	print_attribute_info_types(shop, os, dataTypes, xUnits, yUnits);
    print_shop_proxy_header_begin(proxy_os);
	for (int i = 0, n = ShopGetObjectTypeCount(shop); i < n; ++i) {
		os << endl;
		print_attribute_types_for_object_type(shop, os, i, dataTypes, xUnits, yUnits);
        print_shop_proxy_for_object_type(shop,proxy_os, i, dataTypes, xUnits, yUnits);
	}
    print_shop_proxy_header_end(proxy_os);
}

//
// Generate enum representing command types
//
void print_command_types(ShopSystem* shop, ostream& os)
{
	const int nCommandTypeBuffer = 1000;
	vector<char*> commandTypes(nCommandTypeBuffer);
	for (int i = 0; i < nCommandTypeBuffer; ++i)
		commandTypes[i] = new char[128];
	int nCommandTypes = nCommandTypeBuffer;
	if (ShopGetCommandTypesInSystem(shop, nCommandTypes, &commandTypes[0])) {
		vector<pair<string, string>> enumEntries;
		for (int i = 0; i < nCommandTypes; ++i) {
			string enumEntry = commandTypes[i];
			replace(enumEntry.begin(), enumEntry.end(), ' ', '_');
			enumEntries.emplace_back(commandTypes[i], enumEntry);
		}
		print_enum2(os, "shop_command", enumEntries);
	}
	for (int i = 0; i < nCommandTypeBuffer; ++i)
		delete[] commandTypes[i];
}

//
// Generate various other enums and structs
//
void print_other(ShopSystem* shop, ostream& os)
{

	print_enum1(os, "shop_relation", vector<string>({
		{"connection_standard"},
		{"connection_bypass"},
		{"connection_spill"},
		{"generator_of_plant"},
		{"pump_of_plant"},
		{"needle_combination_of_generator"},
	}));
	os << endl;
	print_enum2(os, "shop_severity", vector<pair<string, string>>({
		{"ok","OK"}
	}));
	os << endl;
	print_enum1(os, "shop_time_resolution_unit", vector<string>({
		{"hour"},
		{"minute"}
	}));
}
}
using namespace generate_shop_cxx_api;
TEST_SUITE("generate_shop_cxx_api") {
	/** The intenion here is to generate the new api, then compare it with the existing one, and fail if different.
	* currently we just generate the new shop_enums.new.h
	*/
	TEST_CASE("generate_shop_enums") {
		string generated_file_name = "shop_enums.new.h";
        string proxy_file_name = "shop_api.new.h";
		ofstream ofs(generated_file_name, ofstream::out);
        ofstream pofs(proxy_file_name, ofstream::out);
		REQUIRE(ofs);
        REQUIRE(pofs);
		unique_ptr<ShopSystem, bool(*)(ShopSystem*)> shop_safe(ShopInit(),ShopFree);
		ShopSystem * shop = shop_safe.get();// for convinience calling the next functions
		ShopSetSilentConsole(shop, true);
		ofs << "#pragma once" << endl
			<< "#include <tuple>" << endl
			<< "namespace shop::enums {"
			<< endl;
		print_object_types(shop, ofs);
		ofs << endl;
		print_attribute_types(shop, ofs,pofs);
		ofs << endl;
		print_command_types(shop, ofs);
		ofs << endl;
		print_other(shop, ofs);
		ofs << "\n}" << endl;
	}
}
#endif
