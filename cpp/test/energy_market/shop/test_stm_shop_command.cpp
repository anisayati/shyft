#include "doctest/doctest.h"
#include <shyft/energy_market/stm/shop/shop_command.h>
#include <string>

TEST_SUITE("stm_shop_command") {

TEST_CASE("shop command")
{
	// NB: Using 6 decimal places for any floating point values in strings, because currently the
	// shop_command is using std::to_string to convert the double value into string and it seems
	// to always use 6 decimals without option to specify anything else.

	using shyft::energy_market::stm::shop::shop_command;

	SUBCASE("keyword only")
	{
		const char* const command_string = "quit";
		shop_command cmd{ command_string };
		CHECK(cmd.keyword == "quit");
		CHECK(cmd.specifier.empty());
		CHECK(cmd.options.size() == 0);
		CHECK(cmd.objects.size() == 0);
		CHECK((std::string)cmd == command_string);
	}
	SUBCASE("keyword and specifier")
	{
		const char* const command_string = "save tunnelloss";
		shop_command cmd{ command_string };
		CHECK(cmd.keyword == "save");
		CHECK(cmd.specifier == "tunnelloss");
		CHECK(cmd.options.size() == 0);
		CHECK(cmd.objects.size() == 0);
		CHECK((std::string)cmd == command_string);
		shop_command cmd2 = shop_command::save_tunnelloss();
		CHECK(cmd == cmd2);
		CHECK((std::string)cmd == (std::string)cmd2);
	}
	SUBCASE("keyword specifier and one option")
	{
		const char* const command_string = "set method /dual";
		shop_command cmd{ command_string };
		CHECK(cmd.keyword == "set");
		CHECK(cmd.specifier == "method");
		CHECK(cmd.options.size() == 1);
		if (cmd.options.size() == 1) {
			CHECK(cmd.options.front() == "dual");
		}
		CHECK(cmd.objects.size() == 0);
		CHECK((std::string)cmd == command_string);
		shop_command cmd2 = shop_command::set_method_dual();
		CHECK(cmd == cmd2);
		CHECK((std::string)cmd == (std::string)cmd2);
	}
	SUBCASE("keyword specifier and string value")
	{
		const char* const command_string = "log file filename.ext";
		shop_command cmd{ command_string };
		CHECK(cmd.keyword == "log");
		CHECK(cmd.specifier == "file");
		CHECK(cmd.options.size() == 0);
		CHECK(cmd.objects.size() == 1);
		if (cmd.objects.size() == 1) {
			CHECK(cmd.objects.front() == "filename.ext");
		}
		CHECK((std::string)cmd == command_string);
		shop_command cmd2 = shop_command::log_file("filename.ext");
		CHECK(cmd == cmd2);
		CHECK((std::string)cmd == (std::string)cmd2);
	}
	SUBCASE("keyword specifier and int value")
	{
		const char* const command_string = "set max_num_threads 8";
		shop_command cmd{ command_string };
		CHECK(cmd.keyword == "set");
		CHECK(cmd.specifier == "max_num_threads");
		CHECK(cmd.options.size() == 0);
		CHECK(cmd.objects.size() == 1);
		if (cmd.objects.size() == 1) {
			CHECK(cmd.objects.front() == "8");
		}
		CHECK((std::string)cmd == command_string);
		shop_command cmd2 = shop_command::set_max_num_threads(8);
		CHECK(cmd == cmd2);
		CHECK((std::string)cmd == (std::string)cmd2);
	}
	SUBCASE("keyword specifier and double value")
	{
		const char* const command_string = "set fcr_n_band 0.400000";
		shop_command cmd{ command_string };
		CHECK(cmd.keyword == "set");
		CHECK(cmd.specifier == "fcr_n_band");
		CHECK(cmd.options.size() == 0);
		CHECK(cmd.objects.size() == 1);
		if (cmd.objects.size() == 1) {
			CHECK(cmd.objects.front() == "0.400000");
		}
		CHECK((std::string)cmd == command_string);
		shop_command cmd2 = shop_command::set_fcr_n_band(0.4);
		CHECK(cmd == cmd2);
		CHECK((std::string)cmd == (std::string)cmd2);
	}
	SUBCASE("keyword specifier option and string value")
	{
		const char* const command_string = "return simres /gen filename.ext";
		shop_command cmd{ command_string };
		CHECK(cmd.keyword == "return");
		CHECK(cmd.specifier == "simres");
		CHECK(cmd.options.size() == 1);
		if (cmd.options.size() == 1) {
			CHECK(cmd.options.front() == "gen");
		}
		CHECK(cmd.objects.size() == 1);
		if (cmd.objects.size() == 1) {
			CHECK(cmd.objects.front() == "filename.ext");
		}
		CHECK((std::string)cmd == command_string);
		shop_command cmd2 = shop_command::return_simres_gen("filename.ext");
		CHECK(cmd == cmd2);
		CHECK((std::string)cmd == (std::string)cmd2);
	}
	SUBCASE("keyword specifier option and double value")
	{
		const char* const command_string = "set mipgap /relative 0.300000";
		shop_command cmd{ command_string };
		CHECK(cmd.keyword == "set");
		CHECK(cmd.specifier == "mipgap");
		CHECK(cmd.options.size() == 1);
		if (cmd.options.size() == 1) {
			CHECK(cmd.options.front() == "relative");
		}
		CHECK(cmd.objects.size() == 1);
		if (cmd.objects.size() == 1) {
			CHECK(cmd.objects.front() == "0.300000");
		}
		CHECK((std::string)cmd == command_string);
		shop_command cmd2 = shop_command::set_mipgap(false, 0.3);
		CHECK(cmd == cmd2);
		CHECK((std::string)cmd == (std::string)cmd2);
	}
	SUBCASE("keyword specifier and three options")
	{
		const char* const command_string = "penalty flag /on /reservoir /ramping";
		shop_command cmd{ command_string };
		CHECK(cmd.keyword == "penalty");
		CHECK(cmd.specifier == "flag");
		CHECK(cmd.options.size() == 3);
		if (cmd.options.size() == 3) {
			CHECK(cmd.options[0] == "on");
			CHECK(cmd.options[1] == "reservoir");
			CHECK(cmd.options[2] == "ramping");
		}
		CHECK(cmd.objects.size() == 0);
		CHECK((std::string)cmd == command_string);
		shop_command cmd2 = shop_command::penalty_flag_reservoir_ramping(true);
		CHECK(cmd == cmd2);
		CHECK((std::string)cmd == (std::string)cmd2);
	}
	SUBCASE("keyword specifier two options and double value")
	{
		const char* const input_command_string = "\t penalty   cost   /reservoir \t /ramping    123.500000   ";
		const char* const output_command_string = "penalty cost /reservoir /ramping 123.500000";
		shop_command cmd{ input_command_string };
		CHECK(cmd.keyword == "penalty");
		CHECK(cmd.specifier == "cost");
		CHECK(cmd.options.size() == 2);
		if (cmd.options.size() == 2) {
			CHECK(cmd.options[0] == "reservoir");
			CHECK(cmd.options[1] == "ramping");
		}
		CHECK(cmd.objects.size() == 1);
		if (cmd.objects.size() == 1) {
			CHECK(cmd.objects[0] == "123.500000");
		}
		CHECK((std::string)cmd == output_command_string);
		shop_command cmd2 = shop_command::penalty_cost_reservoir_ramping(123.50);
		CHECK(cmd == cmd2);
		CHECK((std::string)cmd == (std::string)cmd2);
	}
#ifdef SHOP_COMMAND_MULTIPLE_OBJECTS
	SUBCASE("multiple objects")
	{
		const char* const command_string = "the quick brown fox jumps over the lazy dog";
		shop_command cmd{ command_string };
		CHECK(cmd.keyword == "the");
		CHECK(cmd.specifier == "quick");
		CHECK(cmd.options.size() == 0);
		CHECK(cmd.objects.size() == 7);
		if (cmd.objects.size() == 7) {
			CHECK(cmd.objects[0] == "brown");
			CHECK(cmd.objects[1] == "fox");
			CHECK(cmd.objects[2] == "jumps");
			CHECK(cmd.objects[3] == "over");
			CHECK(cmd.objects[4] == "the");
			CHECK(cmd.objects[5] == "lazy");
			CHECK(cmd.objects[6] == "dog");
		}
		CHECK((std::string)cmd == command_string);
	}
#endif
	SUBCASE("invalid syntax")
	{
#ifndef SHOP_COMMAND_MULTIPLE_OBJECTS
		CHECK_THROWS(shop_command cmd{ "The quick brown fox" });
		CHECK_THROWS(shop_command cmd{ "The quick /brown fox jumps" });
		CHECK_THROWS(shop_command cmd{ "The quick brown fox jumps over the lazy dog" });
#endif
		CHECK_THROWS(shop_command cmd{ "The /quick" });
		CHECK_THROWS(shop_command cmd{ "The /quick /brown" });
		CHECK_THROWS(shop_command cmd{ "The /quick /brown /fox" });
		CHECK_THROWS(shop_command cmd{ "The quick /brown fox /jumps" });
		CHECK_THROWS(shop_command cmd{ "The quick brown /fox" });
		CHECK_THROWS(shop_command cmd{ "The quick brown /fox /jumps" });
	}
}

}
