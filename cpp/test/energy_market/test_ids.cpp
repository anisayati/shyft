

#include <type_traits>
#include <vector>
#include <map>
#include <tuple>
#include <utility>
#include <stdexcept>
#include <string>
#include <memory>
#include <iosfwd> // just for the experiment/debug purposes

#include <shyft/energy_market/dataset.h>
#include <shyft/energy_market/proxy_attr.h>

#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/stm/water_route.h>
#include <shyft/energy_market/stm/aggregate.h>
#include <shyft/energy_market/stm/hps_ds.h>
#include <shyft/energy_market/stm/market_ds.h>

# include <doctest/doctest.h>

TEST_SUITE("ids") {
    TEST_CASE("get_set") {
        using namespace shyft::energy_market;
        using namespace shyft::energy_market::stm;
        using std::runtime_error;
		using shyft::core::utctime;
		using shyft::time_series::dd::apoint_ts;
		using shyft::time_axis::point_dt;

		utctime t0{ 0 };
		utctime t1{ 1 };
		auto ta = point_dt(vector<utctime>{t0}, t1);
        //--arrange the test
        auto sys=make_shared<stm_system>(1,"first","");
        auto ull=make_shared<stm_hps>(1,"Ulla-førre");
        ull->ids = make_shared<hps_ds>();// attach empty ids
        sys->hps.push_back(ull);
        sys->market.push_back(make_shared<energy_market_area>(1,"einar aas område","json-here",sys));
        auto b = make_shared<reservoir>(16606,"blåsjø","",ull);
        ull->reservoirs.push_back(b);
        //-- act
        CHECK_EQ(b->hrl.exists(),false);// none set yet.
        auto t_d = apoint_ts(ta, 1233.0);
		b->hrl = t_d;
        apoint_ts y=b->hrl;
        //-- assert
        CHECK_EQ(y(t0),doctest::Approx(1233.0));
		y.set(0,1000.0);// = 1000.0;
		b->hrl = t_d; // yes, it can act as an ordinary property.
        y=b->hrl;
        CHECK_EQ(y(t0),doctest::Approx(1000.0));
        auto a = make_shared<reservoir>(16600,"lauv","",ull);
        ull->reservoirs.push_back(a);
        CHECK_EQ(a->lrl,b->lrl);
		auto l = apoint_ts(ta, 100.0);
        a->lrl.set(l);
        CHECK_NE(a->lrl,b->lrl);
        auto h = apoint_ts(ta, 1010.0);
        a->hrl=h;
        CHECK_NE(a->hrl,b->hrl);
        b->hrl.get().set(0,1010.0);
        CHECK_EQ(b->hrl.get()(t0),doctest::Approx(b->hrl.get()(t0)));
        b->hrl.remove();
        CHECK_NE(a->hrl,b->hrl);
        //-- test what happen if we try to read a not yet-set apptribute
        CHECK_THROWS_AS(b->hrl.get(),runtime_error);
    }
}
