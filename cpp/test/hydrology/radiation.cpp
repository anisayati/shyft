#include "test_pch.h"
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/time_series.h>
#include <armadillo>
#include <vector>
#include <chrono>
#include <boost/math/constants/constants.hpp>
#include <shyft/hydrology/methods/radiation.h>
#include <cmath>
#include <random>
#include <tuple>
#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point.hpp>
#include <boost/geometry/srs/epsg.hpp>
#include <boost/geometry/srs/projection.hpp>

namespace shyft::test {

    class trapezoidal_average {
    private:
        double area = 0.0;
        double f_a = 0.0;; // Left hand side of next integration subinterval
        double t_start = 0.0; // Start of integration period
        double t_a = 0.0; // Left hand side time of next integration subinterval
    public:
        explicit trapezoidal_average() {}

        /** \brief initialize must be called to reset states before being used during ode integration.
         */
        void initialize(double f0, double t_start) {
            this->f_a = f0;
            this->t_start = t_start;
            t_a = t_start;
            area = 0.0;
        }

        /** \brief Add contribution to average using a simple trapezoidal rule
         *
         * See: http://en.wikipedia.org/wiki/Numerical_integration
         */
        void add(double f, double t) {
            area += 0.5*(f_a + f)*(t - t_a);
            f_a = f;
            t_a = t;
        }

        double result() const { /*std::cout<<" times "<<(t_a-t_start)<<std::endl; */return area/(t_a - t_start); }
    };

}
namespace boost::geometry::traits {
    // dress up shyft::core::geo_point as a boost geometry  2D point in this context
    // notice that tag-based dispatch require us to do this in boost::geometry::traits (the dressing room!)
    template<> struct tag<shyft::core::geo_point> {using type= point_tag;};
    template<> struct coordinate_type<shyft::core::geo_point> {using type=double;};
    template<> struct coordinate_system<shyft::core::geo_point> {using type=cs::cartesian;};
    template<> struct dimension<shyft::core::geo_point>:boost::mpl::int_<2> {};
    template<> struct access<shyft::core::geo_point,0> {
        static double get(shyft::core::geo_point const&p) {return p.x;}
        static void set(shyft::core::geo_point & p, double value){p.x=value;}
    };
    template<> struct access<shyft::core::geo_point,1> {
        static double get(shyft::core::geo_point const&p) {return p.y;}
        static void set(shyft::core::geo_point & p, double value){p.y=value;}
    };
    
}
namespace test::boost_proj {
    namespace bg= boost::geometry;
    using bg::degree;
    using bg::cs::cartesian;
    using bg::cs::geographic;
    using bg::distance;
    using bg::srs::projection;
    using bg::srs::proj4;
    using bg::srs::epsg;
    using bg::model::point;
    
    using point_ll=point<double, 2,geographic<degree> >;
    using point_xy=shyft::core::geo_point;//point<double, 2,cartesian>;

    void do_test_proj4(){
        point_ll pt_ll(1, 1);
        point_ll pt_ll2(0, 0);
        point_xy pt_xy(0, 0);
        projection<> prj = proj4("+proj=tmerc +ellps=WGS84 +units=m");
        prj.forward(pt_ll, pt_xy);
        prj.inverse(pt_xy, pt_ll2);
        FAST_CHECK_LE(distance(pt_ll,pt_ll2),1e-8);
    }
    
    void do_test_epsg() {
        projection<> prj(epsg(32632));// UTM 32N, approx norway, cartesian x,y
        point_ll pt_ll(10.6367397,59.9151928);// lilleaker oslo, long,lat epsg 4326, wgs84
        point_xy pt_expected(591520.915557,6643097.688005);
        point_xy pt_xy(0, 0);
        prj.forward(pt_ll,pt_xy);
        auto dxy= distance(pt_xy,pt_expected);
        FAST_CHECK_LE(dxy,0.1);
        std::cout<<" epsg 32632 : pt_xy:"<< bg::get<0>(pt_xy)<<","<<bg::get<1>(pt_xy)<<" dist:"<<dxy<<std::endl;
        point_ll pt_r;
        prj.inverse(pt_xy,pt_r);
        auto dll=distance(pt_r,pt_ll);
        FAST_CHECK_LE(dll,1e-7);
        std::cout<<" epsg 4346 : long,lat "<< bg::get<0>(pt_r)<<","<<bg::get<1>(pt_r)<<"dist(ll):"<<dll<<std::endl;
        
    }
    
}
TEST_SUITE("radiation") {
    using shyft::core::radiation::parameter;
    using shyft::core::radiation::response;
    using shyft::core::radiation::calculator;
//    using shyft::core::radiation::surface_normal;
    using shyft::core::calendar;
    using shyft::core::utctime;
    using shyft::test::trapezoidal_average;
    bool verbose= getenv("SHYFT_VERBOSE")!=nullptr;
//    bool verbose = true;
    // test basics: creation, etc


    TEST_CASE("boost_proj") {
        test::boost_proj::do_test_proj4();
        test::boost_proj::do_test_epsg();
    }
    TEST_CASE("test_radiation_equal_operator") {
        double albedo = 0.25;
        double turbidity = 1.0;
        double al=0.34;
        double bl=0.14;
        double ac=1.35;
        double bc=0.35;
        double as = 1.25;
        double bs = 0.25;
        
        parameter p(albedo, turbidity, al, bl, ac, bc, as, bs);
        parameter p1(albedo+1.0, turbidity, al, bl, ac, bc, as, bs);
        parameter p2(albedo, turbidity+1.0, al, bl, ac, bc, as, bs);
        parameter p3(albedo, turbidity, al+1.0, bl, ac, bc, as, bs);
        parameter p4(albedo, turbidity, al, bl+1.0, ac, bc, as, bs);
        parameter p5(albedo, turbidity, al, bl, ac+1.0, bc, as, bs);
        parameter p6(albedo, turbidity, al, bl, ac, bc+1.0, as, bs);
        parameter p7(albedo, turbidity, al, bl, ac, bc, as+1.0, bs);
        parameter p8(albedo, turbidity, al, bl, ac, bc, as, bs+1.0);
        
        TS_ASSERT(p != p1);
        TS_ASSERT(p != p2);
        TS_ASSERT(p != p3);
        TS_ASSERT(p != p4);
        TS_ASSERT(p != p5);
        TS_ASSERT(p != p6);
        TS_ASSERT(p != p7);
        TS_ASSERT(p != p8);
        
        p1.albedo = albedo;
        p2.turbidity = turbidity;
        p3.al = al;
        p4.bl = bl;
        p5.ac = ac;
        p6.bc = bc;
        p7.as = as;
        p8.bs = bs;
        
        TS_ASSERT(p == p1);
        TS_ASSERT(p == p2);
        TS_ASSERT(p == p3);
        TS_ASSERT(p == p4);
        TS_ASSERT(p == p5);
        TS_ASSERT(p == p6);
        TS_ASSERT(p == p7);
        TS_ASSERT(p == p8);
    }



    TEST_CASE("check_solar_radiation_horizontal_inst"){
        parameter p;
        response r;
        p.albedo = 0.2;
        p.turbidity = 1.0;
        calculator rad(p);
        calendar utc_cal;
        double lat = 44.0;
        utctime t;
        // checking for horizontal surface Eugene, OR, p.64, fig.1b
        double slope = 0.0;
        double aspect = 0.0;
        utctime ta;
        trapezoidal_average av_rahor;
        trapezoidal_average av_ra;
        trapezoidal_average av_rs;
        std::uniform_real_distribution<double> ur(150.0, 390.0);
        std::default_random_engine gen;
//
        SUBCASE("June_translated") {
//            std::cout << "========= June translated ========" << std::endl;
            ta = utc_cal.time(2002, 06, 21, 00, 00, 0, 0);
            //rad.pnet_sw(r, lat, ta, surface_normal, 20.0, 50.0, 150.0);
            rad.net_radiation(r, lat, ta, slope, aspect, 20.0, 50.0, 150.0, ur(gen));
            av_rahor.initialize(rad.ra_radiation_hor(), 0.0);
            av_ra.initialize(rad.ra_radiation(), 0.0);
            av_rs.initialize(r.net_sw, 0.0);
            for (int h = 0; h < 24; ++h) {
                t = utc_cal.time(2002, 06, 21, h, 00, 0, 0); // June
                rad.net_radiation(r, lat, t, slope,aspect, 20.0, 50.0, 150.0, ur(gen));
                av_rahor.add(rad.ra_radiation_hor(), h);
                av_ra.add(rad.ra_radiation(), h);
                av_rs.add(r.net_sw, h);
            }

//            std::cout << "ra: " << av_ra.result() << std::endl;
//            std::cout << "rs: " << av_rs.result() << std::endl;
            CHECK_EQ(av_ra.result(), doctest::Approx(500.0).epsilon(0.05));
            CHECK_EQ(av_rahor.result(), doctest::Approx(av_ra.result()).epsilon(0.05));
            //CHECK_EQ(av_rs.result(), doctest::Approx(370.0).epsilon(0.05));

        }
        SUBCASE("June") {
//
            ta = utc_cal.time(2002, 06, 21, 00, 00, 0, 0);
            //rad.pnet_sw(r, lat, ta, surface_normal, 20.0, 50.0, 150.0);
            rad.net_radiation(r, lat, ta, slope,aspect, 20.0, 50.0, 150.0);
            av_rahor.initialize(rad.ra_radiation_hor(), 0.0);
            av_ra.initialize(rad.ra_radiation(), 0.0);
            av_rs.initialize(r.net_sw, 0.0);
            for (int h = 1; h < 24; ++h) {
                t = utc_cal.time(2002, 06, 21, h, 00, 0, 0); // June
                rad.net_radiation(r, lat, t, slope, aspect, 20.0, 50.0, 150.0);
                av_rahor.add(rad.ra_radiation_hor(), h);
                av_ra.add(rad.ra_radiation(), h);
                av_rs.add(r.net_sw, h);
            }

            if (verbose){
std::cout << "========= Horizontal =======" << std::endl;
std::cout << "========= June ========" << std::endl;
                std::cout << "ra: " << av_ra.result()<<std::endl;
                std::cout << "rs: " << av_rs.result()<<std::endl;
            }
                    CHECK_EQ(av_ra.result(), doctest::Approx(500.0).epsilon(0.05));
                    CHECK_EQ(av_rahor.result(), doctest::Approx(av_ra.result()).epsilon(0.05));
                    CHECK_EQ(av_rs.result(), doctest::Approx(303.0).epsilon(0.05));

        }
        SUBCASE("January") {
//
            ta = utc_cal.time(2002, 01, 1, 00, 00, 0, 0);
            rad.net_radiation(r, lat, ta, slope,aspect, 20.0, 50.0, 150.0);
            av_rahor.initialize(rad.ra_radiation_hor(), 0.0);
            av_ra.initialize(rad.ra_radiation(), 0.0);
            av_rs.initialize(r.net_sw, 0.0);
            for (int h = 1; h < 24; ++h) {
                t = utc_cal.time(2002, 01, 1, h, 00, 0, 0); // January
                rad.net_radiation(r, lat, t, slope,aspect, 20.0, 50.0, 150.0);
                av_rahor.add(rad.ra_radiation_hor(), h);
                av_ra.add(rad.ra_radiation(), h);
                av_rs.add(r.net_sw, h);
            }

            if (verbose){
std::cout << "========= January =======" << std::endl;
            std::cout << "ra: " << av_ra.result()<<std::endl;
            std::cout << "rs: " << av_rs.result()<<std::endl;
            }
                    CHECK_EQ(av_ra.result(), doctest::Approx(130.0).epsilon(0.05));
                    CHECK_EQ(av_rahor.result(), doctest::Approx(av_ra.result()).epsilon(0.05));
                    CHECK_EQ(av_rs.result(), doctest::Approx(63.0).epsilon(0.05));
        }
        SUBCASE("December") {
//
            ta = utc_cal.time(2002, 12, 21, 00, 00, 0, 0);
            rad.net_radiation(r, lat, ta, slope,aspect, 20.0, 50.0, 150.0);
            av_rahor.initialize(rad.ra_radiation_hor(), 0.0);
            av_ra.initialize(rad.ra_radiation(), 0.0);
            av_rs.initialize(r.net_sw, 0.0);
            for (int h = 1; h < 24; ++h) {
                t = utc_cal.time(2002, 12, 21, h, 00, 0, 0); // January
                rad.net_radiation(r, lat, t, slope,aspect, 20.0, 50.0, 150.0);
                av_rahor.add(rad.ra_radiation_hor(), h);
                av_ra.add(rad.ra_radiation(), h);
                av_rs.add(r.net_sw, h);
            }

            if (verbose){
std::cout << "========= December =======" << std::endl;
            std::cout << "ra: " << av_ra.result()<<std::endl;
            std::cout << "rs: " << av_rs.result()<<std::endl;
            }
                    CHECK_EQ(av_ra.result(), doctest::Approx(130.0).epsilon(0.05));
                    CHECK_EQ(av_rahor.result(), doctest::Approx(av_ra.result()).epsilon(0.05));
                    CHECK_EQ(av_rs.result(), doctest::Approx(61.0).epsilon(0.05));
        }

    }
    TEST_CASE("check_solar_radiation_horizontal_step"){
    parameter p;
    response r;
    p.albedo = 0.2;
    p.turbidity = 1.0;
    calculator rad(p);
    calendar utc_cal;
    double lat = 44.0;
    utctime t1,t2;
    // checking for horizontal surface Eugene, OR, p.64, fig.1b
    double slope = 0.0;
    double aspect = 0.0;
    std::uniform_real_distribution<double> ur(150.0, 390.0);
    std::default_random_engine gen;
    //
    SUBCASE("June_translated") {
        //
        double rastep = 0.0;
        double rsostep = 0.0;
        for (int h = 1; h < 24; ++h) {
            t1 = utc_cal.time(2002, 06, 21, h-1, 00, 0, 0); // June
            auto dt = shyft::core::deltahours(1);
            rad.net_radiation_step(r, lat, t1,dt, slope, aspect, 20.0, 50.0, 150.0,ur(gen));
            rastep+=r.ra;
            rsostep+=r.net_sw;
        }

        if (verbose){
std::cout << "========= Horizontal =======" << std::endl;
std::cout << "========= June translated ========" << std::endl;
        std::cout << "ra: " << rastep<<std::endl;
        std::cout << "rs: " << rsostep<<std::endl;
        }


    }
    SUBCASE("June") {
//
        double rastep = 0.0;
        double rsostep = 0.0;
        for (int h = 1; h < 24; ++h) {
            t1 = utc_cal.time(2002, 06, 21, h-1, 00, 0, 0); // June
            auto dt = shyft::core::deltahours(1);
            rad.net_radiation_step(r, lat, t1,dt, slope, aspect, 20.0, 50.0, 150.0);
            rastep+=r.ra;
            rsostep+=r.net_sw;
        }

        if (verbose){
std::cout << "========= June step ========" << std::endl;
        std::cout << "ra: " << rastep<<std::endl;
        std::cout << "rs: " << rsostep<<std::endl;
        }
        CHECK_EQ(rastep, doctest::Approx(500.0).epsilon(0.05));
        CHECK_EQ(rsostep, doctest::Approx(290.0).epsilon(0.05));


    }
    SUBCASE("January") {
//
        double rastep = 0.0;
        double rsostep = 0.0;
        for (int h = 1; h < 24; ++h) {
            t1 = utc_cal.time(2002, 01, 1, h-1, 00, 0, 0); // June
            t2 = utc_cal.time(2002, 01, 1, h, 00, 0, 0); // June
            auto dt = shyft::core::deltahours(1);
            rad.net_radiation_step(r, lat, t1,dt, slope, aspect, 20.0, 50.0, 150.0);
            rastep+=r.ra;
            rsostep+=r.net_sw;
        }
        if (verbose){
std::cout << "========= January =======" << std::endl;
        std::cout << "ra: " << rastep<<std::endl;
        std::cout << "rs: " << rsostep<<std::endl;
        }
        CHECK_EQ(rastep, doctest::Approx(130.0).epsilon(0.05));
        CHECK_EQ(rsostep, doctest::Approx(61.0).epsilon(0.05));
    }
    SUBCASE("December") {
//
        double rastep = 0.0;
        double rsostep = 0.0;
        for (int h = 1; h < 24; ++h) {
            t1 = utc_cal.time(2002, 12, 21, h-1, 00, 0, 0); // June
            t2 = utc_cal.time(2002, 12, 21, h, 00, 0, 0); // June
auto dt = shyft::core::deltahours(1);
            rad.net_radiation_step(r, lat, t1,dt, slope, aspect, 20.0, 50.0, 150.0);
            rastep+=r.ra;
            rsostep+=r.net_sw;
        }
        if (verbose){
std::cout << "========= December =======" << std::endl;
        std::cout << "ra: " << rastep<<std::endl;
        std::cout << "rs: " << rsostep<<std::endl;
        }
        CHECK_EQ(rastep, doctest::Approx(130.0).epsilon(0.05));
        CHECK_EQ(rsostep, doctest::Approx(60.0).epsilon(0.05));
    }

}


   TEST_CASE("check_solar_radiation_slope_45s"){
        parameter p;
        response r;
        p.albedo = 0.2;
        p.turbidity = 1.0;
        calculator rad(p);
        calendar utc_cal;
        double lat = 44.0;
        utctime t;
        // checking for horizontal surface Eugene, OR, p.64, fig.1d
        // 24h  average radiation
        double slope = 45;//*shyft::core::radiation::pi/180; // 45 S
       // double proj = sin(slope);
        double aspect = 0.0;//*shyft::core::radiation::pi/180;// facing south
        //arma::vec surface_normal({proj*cos(aspect),proj*sin(aspect),cos(slope)});
        utctime ta;
        trapezoidal_average av_rahor;
        trapezoidal_average av_ra;
        trapezoidal_average av_rs;
        std::uniform_real_distribution<double> ur(100.0, 390.0);
        std::default_random_engine gen;
//
        SUBCASE("June_translated") {
//
            ta = utc_cal.time(2002, 06, 21, 00, 00, 0, 0);
            rad.net_radiation(r, lat, ta, slope,aspect, 20.0, 50.0, 150.0);
            av_rahor.initialize(rad.ra_radiation_hor(), 0.0);
            av_ra.initialize(rad.ra_radiation(), 0.0);
            av_rs.initialize(r.net_sw, 0.0);
            for (int h = 1; h < 24; ++h) {
                t = utc_cal.time(2002, 06, 21, h, 00, 0, 0); // June
                rad.net_radiation(r, lat, t, slope,aspect, 20.0, 50.0, 150.0);
                av_rahor.add(rad.ra_radiation_hor(), h);
                av_ra.add(rad.ra_radiation(), h);
                av_rs.add(r.net_sw, h);
            }

            if (verbose){
std::cout << "========= Slope 45S =======" << std::endl;
std::cout << "========= June translated ========" << std::endl;
            std::cout << "rahor: " << av_rahor.result() << std::endl;
            std::cout << "ra: " << av_ra.result() << std::endl;
            std::cout << "rs: " << av_rs.result() << std::endl;
            std::cout << "sun_rise: " << rad.sun_rise() << std::endl;
            std::cout << "sun_set: " << rad.sun_set() << std::endl;
            }
            CHECK_EQ(av_ra.result(), doctest::Approx(390.0).epsilon(0.05));
            //CHECK_EQ(av_rs.result(), doctest::Approx(310.0).epsilon(0.05));
        }
        SUBCASE("June") {
//
            ta = utc_cal.time(2002, 06, 21, 00, 00, 0, 0);
//            rad.net_radiation(r, lat, ta, surface_normal, 20.0, 50.0, 150.0);
            rad.net_radiation(r, lat, ta, slope,aspect, 20.0, 50.0, 150.0);
            av_rahor.initialize(rad.ra_radiation_hor(), 0.0);
            av_ra.initialize(rad.ra_radiation(), 0.0);
            av_rs.initialize(r.net_sw, 0.0);
            for (int h = 1; h < 24; ++h) {
                t = utc_cal.time(2002, 06, 21, h, 00, 0, 0); // June
                rad.net_radiation(r, lat, t, slope,aspect, 20.0, 50.0, 150.0);
                av_rahor.add(rad.ra_radiation_hor(), h);
                av_ra.add(rad.ra_radiation(), h);
                av_rs.add(r.net_sw, h);
            }
            if (verbose){
std::cout << "========= June ========" << std::endl;
            std::cout << "rahor: " << av_rahor.result() << std::endl;
            std::cout << "ra: " << av_ra.result() << std::endl;
            std::cout << "rs: " << av_rs.result() << std::endl;
            std::cout << "sun_rise: " << rad.sun_rise() << std::endl;
            std::cout << "sun_set: " << rad.sun_set() << std::endl;
            }
                    CHECK_EQ(av_ra.result(), doctest::Approx(390.0).epsilon(0.05));
                    CHECK_EQ(av_rs.result(), doctest::Approx(261.0).epsilon(0.05));
        }
        SUBCASE("January") {
//
            ta = utc_cal.time(2002, 01, 1, 00, 00, 0, 0);
//            rad.net_radiation(r, lat, ta, surface_normal, 20.0, 50.0, 150.0);
            rad.net_radiation(r, lat, ta, slope,aspect, 20.0, 50.0, 150.0);
            av_rahor.initialize(rad.ra_radiation_hor(), 0.0);
            av_ra.initialize(rad.ra_radiation(), 0.0);
            av_rs.initialize(r.net_sw, 0.0);
            for (int h = 1; h < 24; ++h) {
                t = utc_cal.time(2002, 01, 1, h, 00, 0, 0); // June
                rad.net_radiation(r, lat, t, slope,aspect, 20.0, 50.0, 150.0);
                av_rahor.add(rad.ra_radiation_hor(), h);
                av_ra.add(rad.ra_radiation(), h);
                av_rs.add(r.net_sw, h);
            }
            if (verbose){
std::cout << "========= January ========" << std::endl;
            std::cout << "rahor: " << av_rahor.result() << std::endl;
            std::cout << "ra: " << av_ra.result() << std::endl;
            std::cout << "rs: " << av_rs.result() << std::endl;
            std::cout << "sun_rise: " << rad.sun_rise() << std::endl;
            std::cout << "sun_set: " << rad.sun_set() << std::endl;
            }
                    CHECK_EQ(av_ra.result(), doctest::Approx(390.0).epsilon(0.05));
                    CHECK_EQ(av_rs.result(), doctest::Approx(147.0).epsilon(0.05));
        }
        SUBCASE("December") {
//
            ta = utc_cal.time(2002, 12, 12, 00, 00, 0, 0);
//            rad.net_radiation(r, lat, ta, surface_normal, 20.0, 50.0, 150.0);
            rad.net_radiation(r, lat, ta, slope,aspect, 20.0, 50.0, 150.0);
            av_rahor.initialize(rad.ra_radiation_hor(), 0.0);
            av_ra.initialize(rad.ra_radiation(), 0.0);
            av_rs.initialize(r.net_sw, 0.0);
            for (int h = 1; h < 24; ++h) {
                t = utc_cal.time(2002, 12, 12, h, 00, 0, 0); // June
                rad.net_radiation(r, lat, t, slope,aspect, 20.0, 50.0, 150.0);
                av_rahor.add(rad.ra_radiation_hor(), h);
                av_ra.add(rad.ra_radiation(), h);
                av_rs.add(r.net_sw, h);
            }
            if (verbose){
std::cout << "========= December ========" << std::endl;
            std::cout << "rahor: " << av_rahor.result() << std::endl;
            std::cout << "ra: " << av_ra.result() << std::endl;
            std::cout << "rs: " << av_rs.result() << std::endl;
            }
                    CHECK_EQ(av_ra.result(), doctest::Approx(390.0).epsilon(0.05));
                    CHECK_EQ(av_rs.result(), doctest::Approx(147.0).epsilon(0.05));
        }

    }
    TEST_CASE("check_solar_radiation_45s_step"){
        parameter p;
        response r;
        p.albedo = 0.2;
        p.turbidity = 1.0;
        calculator rad(p);
        calendar utc_cal;
        double lat = 44.0;
        utctime t1,t2;
auto dt = shyft::core::deltahours(1);
        // checking for horizontal surface Eugene, OR, p.64, fig.1b
        double slope = 45.0;
        double aspect = 0.0;
        std::uniform_real_distribution<double> ur(150.0, 390.0);
        std::default_random_engine gen;

        SUBCASE("June") {
//
            double rastep = 0.0;
            double rsostep = 0.0;
            for (int h = 1; h < 24; ++h) {
            t1 = utc_cal.time(2002, 06, 21, h-1, 00, 0, 0); // June
            t2 = utc_cal.time(2002, 06, 21, h, 00, 0, 0); // June
            rad.net_radiation_step(r, lat, t1,dt, slope, aspect, 20.0, 50.0, 150.0);
            rastep+=r.ra;
            rsostep+=r.net_sw;
            }

            if (verbose){
            std::cout << "========= June step ========" << std::endl;
            std::cout << "rastep: " << rastep << std::endl;
            std::cout << "rsostep: " << rsostep << std::endl;
            }
            CHECK_EQ(rastep, doctest::Approx(390.0).epsilon(0.05));
            CHECK_EQ(rsostep, doctest::Approx(251.0).epsilon(0.05));


        }
        SUBCASE("January") {
//
            double rastep = 0.0;
            double rsostep = 0.0;
            for (int h = 1; h < 24; ++h) {
            t1 = utc_cal.time(2002, 01, 1, h-1, 00, 0, 0); // June
            t2 = utc_cal.time(2002, 01, 1, h, 00, 0, 0); // June
            rad.net_radiation_step(r, lat, t1,dt, slope, aspect, 20.0, 50.0, 150.0);
            rastep+=r.ra;
            rsostep+=r.net_sw;
            }
            if (verbose){
            std::cout << "========= January =======" << std::endl;
            std::cout << "rastep: " << rastep << std::endl;
            std::cout << "rsostep: " << rsostep << std::endl;
            }
            CHECK_EQ(rastep, doctest::Approx(370.0).epsilon(0.05));
            CHECK_EQ(rsostep, doctest::Approx(144.0).epsilon(0.05));
        }
        SUBCASE("December") {

            double rastep = 0.0;
            double rsostep = 0.0;
            for (int h = 1; h < 24; ++h) {
            t1 = utc_cal.time(2002, 12, 21, h-1, 00, 0, 0); // June
            t2 = utc_cal.time(2002, 12, 21, h, 00, 0, 0); // June
            rad.net_radiation_step(r, lat, t1,dt, slope, aspect, 20.0, 50.0, 150.0);
            rastep+=r.ra;
            rsostep+=r.net_sw;
            }
            if (verbose){
            std::cout << "========= December =======" << std::endl;
            std::cout << "rastep: " << rastep << std::endl;
            std::cout << "rsostep: " << rsostep << std::endl;
            }
            CHECK_EQ(rastep, doctest::Approx(370.0).epsilon(0.05));
            CHECK_EQ(rsostep, doctest::Approx(142.0).epsilon(0.05));
        }

    }

    TEST_CASE("check_solar_radiation_slope_90s"){
        parameter p;
        response r;
        p.albedo = 0.05;
        p.turbidity = 1.0;
        calculator rad(p);
        calendar utc_cal;
        double lat = 44.0;
        utctime t;
        // checking for horizontal surface Eugene, OR, p.64, fig.1d
        // 24h  average radiation
        double slope = 90;//*shyft::core::radiation::pi/180; // 45 S
        // double proj = sin(slope);
        double aspect = 0.0;//*shyft::core::radiation::pi/180;// facing south
        //arma::vec surface_normal({proj*cos(aspect),proj*sin(aspect),cos(slope)});
        utctime ta;
        trapezoidal_average av_rahor;
        trapezoidal_average av_ra;
        trapezoidal_average av_rs;
        std::uniform_real_distribution<double> ur(100.0, 390.0);
        std::default_random_engine gen;

        SUBCASE("June_translated") {

            ta = utc_cal.time(2002, 06, 21, 00, 00, 0, 0);
            rad.net_radiation(r, lat, ta, slope,aspect, 20.0, 50.0, 150.0);
            av_rahor.initialize(rad.ra_radiation_hor(), 0.0);
            av_ra.initialize(rad.ra_radiation(), 0.0);
            av_rs.initialize(r.net_sw, 0.0);
            for (int h = 1; h < 24; ++h) {
                t = utc_cal.time(2002, 06, 21, h, 00, 0, 0); // June
                rad.net_radiation(r, lat, t, slope,aspect, 20.0, 50.0, 150.0);
                av_rahor.add(rad.ra_radiation_hor(), h);
                av_ra.add(rad.ra_radiation(), h);
                av_rs.add(r.net_sw, h);
            }
            if (verbose){
            std::cout << "========= Slope 90S =======" << std::endl;
            std::cout << "========= June translated ========" << std::endl;
            std::cout << "rahor: " << av_rahor.result() << std::endl;
            std::cout << "ra: " << av_ra.result() << std::endl;
            std::cout << "rs: " << av_rs.result() << std::endl;
            std::cout << "sun_rise: " << rad.sun_rise() << std::endl;
            std::cout << "sun_set: " << rad.sun_set() << std::endl;
            }
            CHECK_EQ(av_ra.result(), doctest::Approx(110.0).epsilon(0.05));
        //CHECK_EQ(av_rs.result(), doctest::Approx(310.0).epsilon(0.05));
        }
        SUBCASE("June") {

            ta = utc_cal.time(2002, 06, 21, 00, 00, 0, 0);
            //            rad.net_radiation(r, lat, ta, surface_normal, 20.0, 50.0, 150.0);
            rad.net_radiation(r, lat, ta, slope,aspect, 20.0, 50.0, 150.0);
            av_rahor.initialize(rad.ra_radiation_hor(), 0.0);
            av_ra.initialize(rad.ra_radiation(), 0.0);
            av_rs.initialize(r.net_sw, 0.0);
            for (int h = 1; h < 24; ++h) {
                t = utc_cal.time(2002, 06, 21, h, 00, 0, 0); // June
                rad.net_radiation(r, lat, t, slope,aspect, 20.0, 50.0, 150.0);
                av_rahor.add(rad.ra_radiation_hor(), h);
                av_ra.add(rad.ra_radiation(), h);
                av_rs.add(r.net_sw, h);
            }
            if (verbose){
            std::cout << "========= June ========" << std::endl;

            std::cout << "rahor: " << av_rahor.result() << std::endl;
            std::cout << "ra: " << av_ra.result() << std::endl;
            std::cout << "rs: " << av_rs.result() << std::endl;
            std::cout << "sun_rise: " << rad.sun_rise() << std::endl;
            std::cout << "sun_set: " << rad.sun_set() << std::endl;
            }
            CHECK_EQ(av_ra.result(), doctest::Approx(110.0).epsilon(0.05));
            CHECK_EQ(av_rs.result(), doctest::Approx(110.0).epsilon(0.05));
        }
        SUBCASE("January") {

            ta = utc_cal.time(2002, 01, 1, 00, 00, 0, 0);
            //            rad.net_radiation(r, lat, ta, surface_normal, 20.0, 50.0, 150.0);
            rad.net_radiation(r, lat, ta, slope,aspect, 20.0, 50.0, 150.0);
            av_rahor.initialize(rad.ra_radiation_hor(), 0.0);
            av_ra.initialize(rad.ra_radiation(), 0.0);
            av_rs.initialize(r.net_sw, 0.0);
            for (int h = 1; h < 24; ++h) {
                t = utc_cal.time(2002, 01, 1, h, 00, 0, 0); // June
                rad.net_radiation(r, lat, t, slope,aspect, 20.0, 50.0, 150.0);
                av_rahor.add(rad.ra_radiation_hor(), h);
                av_ra.add(rad.ra_radiation(), h);
                av_rs.add(r.net_sw, h);
            }
            if (verbose){
            std::cout << "========= January ========" << std::endl;
            std::cout << "rahor: " << av_rahor.result() << std::endl;
            std::cout << "ra: " << av_ra.result() << std::endl;
            std::cout << "rs: " << av_rs.result() << std::endl;
            std::cout << "sun_rise: " << rad.sun_rise() << std::endl;
            std::cout << "sun_set: " << rad.sun_set() << std::endl;
            }
            CHECK_EQ(av_ra.result(), doctest::Approx(410.0).epsilon(0.05));
            CHECK_EQ(av_rs.result(), doctest::Approx(186.0).epsilon(0.05));
        }
        SUBCASE("December") {
            ;
            ta = utc_cal.time(2002, 12, 12, 00, 00, 0, 0);
            //            rad.net_radiation(r, lat, ta, surface_normal, 20.0, 50.0, 150.0);
            rad.net_radiation(r, lat, ta, slope,aspect, 20.0, 50.0, 150.0);
            av_rahor.initialize(rad.ra_radiation_hor(), 0.0);
            av_ra.initialize(rad.ra_radiation(), 0.0);
            av_rs.initialize(r.net_sw, 0.0);
            for (int h = 1; h < 24; ++h) {
                t = utc_cal.time(2002, 12, 12, h, 00, 0, 0); // June
                rad.net_radiation(r, lat, t, slope,aspect, 20.0, 50.0, 150.0);
                av_rahor.add(rad.ra_radiation_hor(), h);
                av_ra.add(rad.ra_radiation(), h);
                av_rs.add(r.net_sw, h);
            }
            if (verbose){
            std::cout << "========= December ========" << std::endl;
            std::cout << "rahor: " << av_rahor.result() << std::endl;
            std::cout << "ra: " << av_ra.result() << std::endl;
            std::cout << "rs: " << av_rs.result() << std::endl;
            }
            CHECK_EQ(av_ra.result(), doctest::Approx(410.0).epsilon(0.05));
            CHECK_EQ(av_rs.result(), doctest::Approx(186.0).epsilon(0.05));
        }

    }

    TEST_CASE("check_solar_radiation_90s_step"){
        parameter p;
        response r;
        p.albedo = 0.05;
        p.turbidity = 1.0;
        calculator rad(p);
        calendar utc_cal;
        double lat = 44.0;
        utctime t1,t2;
auto dt = shyft::core::deltahours(1);
        // checking for horizontal surface Eugene, OR, p.64, fig.1b
        double slope = 90.0;
        double aspect = 0.0;
        std::uniform_real_distribution<double> ur(150.0, 390.0);
        std::default_random_engine gen;

        SUBCASE("June") {

            double rastep = 0.0;
            double rsostep = 0.0;
            for (int h = 1; h < 24; ++h) {
            t1 = utc_cal.time(2002, 06, 21, h-1, 00, 0, 0); // June
            t2 = utc_cal.time(2002, 06, 21, h, 00, 0, 0); // June
            rad.net_radiation_step(r, lat, t1,dt, slope, aspect, 20.0, 50.0, 150.0);
            rastep+=r.ra;
            rsostep+=r.net_sw;
            }
            if (verbose){
            std::cout << "========= slope 90s =======" << std::endl;
            std::cout << "========= June step ========" << std::endl;
            std::cout << "rastep: " << rastep << std::endl;
            std::cout << "rsostep: " << rsostep << std::endl;
            }
            CHECK_EQ(rastep, doctest::Approx(110.0).epsilon(0.05));
            CHECK_EQ(rsostep, doctest::Approx(100.0).epsilon(0.05));


        }
        SUBCASE("January") {

            double rastep = 0.0;
            double rsostep = 0.0;
            for (int h = 1; h < 24; ++h) {
            t1 = utc_cal.time(2002, 01, 1, h-1, 00, 0, 0); // June
            t2 = utc_cal.time(2002, 01, 1, h, 00, 0, 0); // June
            rad.net_radiation_step(r, lat, t1,dt, slope, aspect, 20.0, 50.0, 150.0);
            rastep+=r.ra;
            rsostep+=r.net_sw;
            }
            if (verbose){
            std::cout << "========= January =======" << std::endl;
            std::cout << "rastep: " << rastep << std::endl;
            std::cout << "rsostep: " << rsostep << std::endl;
            }
            CHECK_EQ(rastep, doctest::Approx(390.0).epsilon(0.05));
            CHECK_EQ(rsostep, doctest::Approx(180.0).epsilon(0.05));
        }
        SUBCASE("December") {

            double rastep = 0.0;
            double rsostep = 0.0;
            for (int h = 1; h < 24; ++h) {
            t1 = utc_cal.time(2002, 12, 21, h-1, 00, 0, 0); // June
            t2 = utc_cal.time(2002, 12, 21, h, 00, 0, 0); // June
            rad.net_radiation_step(r, lat, t1,dt, slope, aspect, 20.0, 50.0, 150.0);
            rastep+=r.ra;
            rsostep+=r.net_sw;
            }
            if (verbose){
            std::cout << "========= December =======" << std::endl;
            std::cout << "rastep: " << rastep << std::endl;
            std::cout << "rsostep: " << rsostep << std::endl;
            }
            CHECK_EQ(rastep, doctest::Approx(390.0).epsilon(0.05));
            CHECK_EQ(rsostep, doctest::Approx(180.0).epsilon(0.05));
        }

    }
    TEST_CASE("check_solar_radiation_90n_step"){
        parameter p;
        response r;
        p.albedo = 0.05;
        p.turbidity = 1.0;
        calculator rad(p);
        calendar utc_cal;
        double lat = 44.0;
        utctime t1,t2;
auto dt = shyft::core::deltahours(1);
        // checking for horizontal surface Eugene, OR, p.64, fig.1b
        double slope = 90.0;
        double aspect = 180.0;
        std::uniform_real_distribution<double> ur(150.0, 390.0);
        std::default_random_engine gen;

        SUBCASE("June") {

            double rastep = 0.0;
            double rsostep = 0.0;
            for (int h = 1; h < 24; ++h) {
                t1 = utc_cal.time(2002, 06, 21, h-1, 00, 0, 0); // June
                t2 = utc_cal.time(2002, 06, 21, h, 00, 0, 0); // June
                rad.net_radiation_step(r, lat, t1,dt, slope, aspect, 20.0, 50.0, 150.0);
                rastep+=r.ra;
                rsostep+=r.net_sw;
            }
            if (verbose){
            std::cout << "========= slope 90n =======" << std::endl;
            std::cout << "========= June step ========" << std::endl;
            std::cout << "rastep: " << rastep << std::endl;
            std::cout << "rsostep: " << rsostep << std::endl;
            }
            CHECK_EQ(rastep, doctest::Approx(110.0).epsilon(0.05));
            CHECK_EQ(rsostep, doctest::Approx(59.0).epsilon(0.05));


        }
        SUBCASE("January") {

            double rastep = 0.0;
            double rsostep = 0.0;
            for (int h = 1; h < 24; ++h) {
                t1 = utc_cal.time(2002, 01, 1, h-1, 00, 0, 0); // June
                t2 = utc_cal.time(2002, 01, 1, h, 00, 0, 0); // June
                rad.net_radiation_step(r, lat, t1,dt, slope, aspect, 20.0, 50.0, 150.0);
                rastep+=r.ra;
                rsostep+=r.net_sw;
            }
            if (verbose){
            std::cout << "========= January =======" << std::endl;
            std::cout << "rastep: " << rastep << std::endl;
            std::cout << "rsostep: " << rsostep << std::endl;
            }
            CHECK_EQ(rastep, doctest::Approx(0.0).epsilon(0.05));
            CHECK_EQ(rsostep, doctest::Approx(11.0).epsilon(0.05));
        }
        SUBCASE("December") {

            double rastep = 0.0;
            double rsostep = 0.0;
            for (int h = 1; h < 24; ++h) {
                t1 = utc_cal.time(2002, 12, 21, h-1, 00, 0, 0); // June
                t2 = utc_cal.time(2002, 12, 21, h, 00, 0, 0); // June
                rad.net_radiation_step(r, lat, t1,dt, slope, aspect, 20.0, 50.0, 150.0);
                rastep+=r.ra;
                rsostep+=r.net_sw;
            }
            if (verbose){
            std::cout << "========= December =======" << std::endl;
            std::cout << "rastep: " << rastep << std::endl;
            std::cout << "rsostep: " << rsostep << std::endl;
            }
            CHECK_EQ(rastep, doctest::Approx(0.0).epsilon(0.05));
            CHECK_EQ(rsostep, doctest::Approx(11.0).epsilon(0.05));
        }

    }
//
}
