from ....utilities import environ_util

# Set Shop API specific environment variable ICC_COMMAND_PATH,
# value pointing to the shared library path where the solver libraries
# and license file should be located.
environ_util.set_environment('ICC_COMMAND_PATH', environ_util.lib_path)

# Import extension library
from ._shop import *
