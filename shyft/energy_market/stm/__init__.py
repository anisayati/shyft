from ..core import _core  # need to pull in dependent base-types
from ._stm import *

# backward compatible names after renaming
Aggregate=Unit
AggregateList=UnitList
WaterRoute=Waterway
PowerStation=PowerPlant
HydroPowerSystem.create_aggregate=HydroPowerSystem.create_unit
HydroPowerSystem.create_power_station=HydroPowerSystem.create_power_plant
HydroPowerSystem.create_water_route=HydroPowerSystem.create_waterway
# end backward compat section
