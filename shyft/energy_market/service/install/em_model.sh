#!/usr/bin/env bash
#
# Notice that this file could need customization to adapt to the server that it runs.
#
#
ltm_root_dir=/home/energycorp.com/srv_ltmapprod
m_root=/mnt/tsdb1/model_root
log_file=${ltm_root_dir}/LTM-AP/log/em_model.log
#
# with the above parameters set, start the action
#
source ${ltm_root_dir}/miniconda/etc/profile.d/conda.sh
conda activate _ltm
# parameter to ensure we limit vmem usage so that we avoid oom killer:
# not needed for model-store(yet). : export MALLOC_ARENA_MAX=1
python -u -c "from statkraft.energy_market.service import boot; boot.start_service(model_root=r'${m_root}', log_file=r'${log_file}')"
