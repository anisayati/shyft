
shyft.dashboard.widgets
=======================


.. automodule:: shyft.dashboard.widgets
   :members:
   :undoc-members:
   :show-inheritance:

**Submodules**


.. toctree::

   shyft.dashboard.widgets.date_selector
   shyft.dashboard.widgets.logger_box
   shyft.dashboard.widgets.message_viewer
   shyft.dashboard.widgets.selector_models
   shyft.dashboard.widgets.sliders
   shyft.dashboard.widgets.zoomables
