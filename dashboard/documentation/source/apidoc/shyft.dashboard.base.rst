
shyft.dashboard.base
====================


.. automodule:: shyft.dashboard.base
   :members:
   :undoc-members:
   :show-inheritance:

**Submodules**


.. toctree::

   shyft.dashboard.base.app
   shyft.dashboard.base.constants
   shyft.dashboard.base.gate_model
   shyft.dashboard.base.gate_presenter
   shyft.dashboard.base.hashable
   shyft.dashboard.base.ports
   shyft.dashboard.base.selector_model
   shyft.dashboard.base.selector_presenter
   shyft.dashboard.base.selector_views
