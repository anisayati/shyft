
shyft.dashboard.graph
=====================


.. automodule:: shyft.dashboard.graph
   :members:
   :undoc-members:
   :show-inheritance:

**Submodules**


.. toctree::

   shyft.dashboard.graph.category_graph
